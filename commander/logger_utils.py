"""
Utilities for dealing with logging
"""

import contextlib
import logging

from time import time as wall_time

def get_child_logger(logger, name):
    """Makes a Logger that's the child of another Logger
    """
    if logger is logging.getLogger(): # root case
        return logging.getLogger(name)
    else:
        r = logging.getLogger('%s.%s' % (logger.name, name))
        return r

@contextlib.contextmanager
def log_task(logger, msg=None, done_msg='    ...done in {dt}'):
    if msg is not None:
        logger.info(msg)
    t0 = wall_time()
    yield
    dt = wall_time() - t0
    if done_msg is not None:
        lower_msg = msg and msg[0:1].lower() + msg[1:]
        dt = format_duration(dt)
        logger.info(done_msg.format(msg=msg, lower_msg=lower_msg, dt=dt))

class TimingResults(object):
    pass
        
@contextlib.contextmanager
def log_done_task(logger, msg):
    t0 = wall_time()
    result = TimingResults()
    yield result
    dt = wall_time() - t0
    logger.info(msg + ' (%s)' % format_duration(dt))
    result.dt = dt

@contextlib.contextmanager
def timed(msg, stream=None): # for interactive use
    if stream is None:
        from sys import stderr as stream
    stream.write(msg + '... ')
    stream.flush()
    t0 = wall_time()
    yield
    dt = wall_time() - t0
    stream.write('done in %s\n' % format_duration(dt))
    stream.flush()

def log_after_task(logger, done_msg):
    return log_task(logger, None, done_msg)


def format_duration(dt):
    if dt >= 1:
        unit = 's'
    elif dt >= 1e-3:
        unit = 'ms'
        dt *= 1e3
    elif dt >= 1e-6:
        unit = 'us'
        dt *= 1e6
    else:
        unit = 'ns'
        dt *= 1e9
    return '%.1f %s' % (dt, unit)
