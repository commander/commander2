from __future__ import division

import numpy as np

from ..sphere.mmajor import compute_power_spectrum

def mnorm(x):
    l = int(np.sqrt(x.shape[0])) - 1
    assert x.shape[0] == (l + 1)**2
    p = compute_power_spectrum(0, l, x)
    return np.sqrt(p)

def quadgaussian_beam(nside, lmax, w):
    from commander.beams import _fwhm_to_sigma
    fwhm = w * 2 * np.pi / (4 * nside)
    sigma = _fwhm_to_sigma(fwhm)
    ls = np.arange(lmax + 1)
    return np.exp(-(.5 * ls * (ls + 1) * sigma*sigma)**2)
