"""
Driver routines and command-line applications
---------------------------------------------

These are mostly just stringing-together code for various usecases;
no real logic should be in this module.

"""


import sys
import argparse
import logging
import numpy as np
from .context import CommanderContext

__all__ = ['constrained_realization_command_line', 'cmdr_command_line']

class InvalidUseError(ValueError):
    pass

_cmdr_commands = {}
_cmdr_arg_setup_funcs = {}

def cmdr_command(name):
    def decorator(func):
        _cmdr_commands[name] = func
        return func
    return decorator

def cmdr_args(name):
    def decorator(func):
        _cmdr_arg_setup_funcs[name] = func
        return func
    return decorator

def cmdr_command_line():
    parser = argparse.ArgumentParser(description='Tool for inspecting Commander runs/results')
    subparsers = parser.add_subparsers(title='subcommands')
    for name, func in _cmdr_commands.iteritems():
        subcmd_parser = subparsers.add_parser(name)
        _cmdr_arg_setup_funcs[name](subcmd_parser)
        subcmd_parser.set_defaults(command=func)

    args = parser.parse_args()

    logging.basicConfig()
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    nc_rng = np.random.RandomState()

    ctx = CommanderContext(nc_rng, logger, cache_path=None)
    try:
        args.command(args, ctx)
    except InvalidUseError, e:
        raise e
        # may change this to parser.error later

@cmdr_args('dumpmap')
def cmdr_dumpmap_args(parser):
    parser.add_argument('--nside', type=int, default=None)
    parser.add_argument('source_hdf_file')
    parser.add_argument('dset_name')
    parser.add_argument('row', type=int)
    parser.add_argument('target_fits_file')

@cmdr_command('dumpmap')
def cmdr_dumpmap(args, ctx):
    from .fits import ring_map_to_fits
    
    if args.nside is None:
        raise NotImplementedError()
    import h5py
    with h5py.File(args.source_hdf_file, 'r') as f:
        dset = f.get(args.dset_name)
        if not isinstance(dset, h5py.Dataset):
            raise InvalidUseError('%s is not a dataset' % args.dset_name)
        format = dset.attrs.get('format', None)
        if format != 'mmajor.real':
            raise InvalidUseError()
        sh_coefs = dset[args.row, ...]
        lmax = dset.attrs['lmax']

    map = ctx.sh_synthesis(args.nside, 0, lmax, sh_coefs)
    ring_map_to_fits(args.target_fits_file, map)

@cmdr_args('showmap')
def cmdr_showmap(parser):
    parser.add_argument('source_hdf_file')
    parser.add_argument('dset_name')
    parser.add_argument('row', type=int)


def constrained_realization(ctx, output_filename, observations, signal_components,
                            group_name='/', lprecond=30, eps=1e-7, filemode=None,
                            wiener_only=False, eliminate_masked_pixels=True):
    import h5py, os
    from . import h5store, constrained_realization as cr
    import signal

    io_logger = ctx.get_logger('io')

    if filemode is None and os.path.exists(output_filename):
        raise NotImplementedError('%s exists (to decide: what should we do in this case?)' % output_filename)
    io_logger.info('Writing information about model and data to %s' % output_filename)
    with h5py.File(output_filename, filemode or 'w') as f:
        group = f.require_group(group_name)
        # Dump some information including the model and data description
        group.attrs['producing_code'] = 'cm.app.constrained_realization'
        group.attrs['description'] = 'Constrained realization'
        h5store.dump_object(group, 'model', signal_components)
        h5store.dump_object(group, 'data', observations)
        
        # Dump approximate signal-to-noise
        max_lmax = max([comp.lmax for comp in signal_components if isinstance(comp, signal.SphericalHarmonicSignal)])
        noise_power = np.zeros((len(signal_components), len(observations) + 1, max_lmax + 1))
        for i, comp in enumerate(signal_components):
            if not isinstance(comp, signal.SphericalHarmonicSignal):
                continue # for now, leave as 0
            for j, obs_set in enumerate([observations] + [[obs] for obs in observations]):
                noise_power[i, j, :comp.lmax + 1] = cr.compute_approximate_noise_power(ctx, comp, obs_set)
        dset = f.create_dataset('noise_power', data=noise_power)
        dset.attrs['lmax'] = max_lmax
        dset.attrs['observations'] = ','.join(['(all)'] + [obs.name for obs in observations])
        dset.attrs['signals'] = ','.join(comp.name for comp in signal_components)
        del noise_power

        # Dump priors
        priors = np.zeros((len(signal_components), max_lmax + 1))
        for i, comp in enumerate(signal_components):
            if not isinstance(comp, signal.SphericalHarmonicSignal):
                continue # for now, leave as 0
            priors[i, :comp.lmax + 1] = comp.load_power_spectrum()
        dset = f.create_dataset('prior_variance', data=priors)
        dset.attrs['lmax'] = max_lmax
        dset.attrs['signals'] = ','.join(comp.name for comp in signal_components)
        
    sampler = cr.CrSampler(ctx, observations, signal_components,
                           lprecond=lprecond, eps=eps,
                           eliminate_masked_pixels=eliminate_masked_pixels)
    if wiener_only:
        realizations = sampler.wiener_filter()
    else:
        realizations = sampler.constrained_realization()

    io_logger.info('Writing result to %s' % output_filename)
    with h5py.File(output_filename, 'a') as f:
        group = f.require_group(group_name)
        h5store.append_power_spectra(group, realizations, signal_components)
        h5store.append_signals(group, realizations, signal_components)

    return realizations


def constrained_realization_command_line(observations, signal_components,
                                         lprecond=30, eps=1e-7,
                                         seed=None,
                                         eliminate_masked_pixels=True,
                                         wiener_only=False,
                                         cache_path=None):
    from .constrained_realization import CrSampler
    logging.basicConfig()
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    nc_rng = np.random.RandomState(seed)

    ctx = CommanderContext(nc_rng, logger, cache_path=cache_path)

    sampler = CrSampler(ctx, observations, signal_components,
                        lprecond=lprecond, eps=eps,
                        eliminate_masked_pixels=eliminate_masked_pixels)



    if wiener_only:
        signals = sampler.wiener_filter()
    else:
        signals = sampler.constrained_realization()
    return ctx, signals

    #spectrum = sampler.spectrum()
    #return spectrum

