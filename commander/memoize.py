"""
:mod:`commander.memoize` - Memoization
======================================

Should polarization data be loaded or not? The spherical harmonic expansion
of the inverse-noise-variance-map need to be computed to construct preconditioners,
but how many block-precondition

To avoid having to orchestrate such details all over our program, we
use memoization.  Individual routines simply load or compute what they
need, and often the result will be cached.

Of course, this does not solve the fundamental problem -- we do not
want to memoize everything forever -- but it allows thinking about the
problem on a higher level (TODO: examples as we get experience using this).

Notes on algorithm:

 - The hashing is temporal; only valid within a given process. To enforce
   the failure of using hashes for anything involving persistance,
   the PID and hostname are included.

Extra author and license information
------------------------------------

`filter_args` function is from joblib (BSD Style, 3 clauses) by Gael
Varoquaux, Copyright (c) 2009 Gael Varoquaux.

"""

import functools
import hashlib
import struct
import inspect
import os
import cPickle as pickle

import numpy as np

def get_session_hash():
    import os
    import socket
    h = create_hasher()
    h.update(struct.pack('Q', os.getpid()))
    h.update(socket.gethostname())
    h.update('\0')
    return h.digest()

# Global variables

create_hasher = hashlib.sha1 # single point of configuration for hash function



session_hash = get_session_hash()
NOT_PRESENT = object() # since None can be an actual result

# End global variables


#
# API
#

class Hasher(object):
    def __init__(self, x=None, stable=True):
        self._hasher = hashlib.sha1()
        self.stable = stable
        if x is not None:
            self.update(x)
    
    def update(self, x):
        if isinstance(x, int):
            self._hasher.update(struct.pack('Q', x))
        elif isinstance(x, float):
            self._hasher.update(struct.pack('d', x))
        elif isinstance(x, (list, tuple)):
            self._hasher.update('list-or-tuple:')
            self._hasher.update(struct.pack('Q', len(x)))
            for child in x:
                self.update(child)
        elif isinstance(x, np.ndarray):
            self.stable = False
            self._hasher.update('ndarray:%d:' % id(x))
        elif isinstance(x, dict):
            # Do some hoop-jumping in order to hash a dict; first hash the keys,
            # and sort the items by the key hashes
            items = []
            for key, value in x.iteritems():
                h = Hasher(key, True)
                self.stable = self.stable or h.stable
                items.append((h.digest(), value))
            items.sort() # keys should be unique so should never compare any values
            self._hasher.update('dict:')
            self._hasher.update(struct.pack('Q', len(x)))
            for key_hash, value in items:
                self._hasher.update(key_hash)
                self.update(value)
        elif x is None or x is True or x is False: # important to use 'is' for boolean here
            self._hasher.update(str(x))
            self._hasher.update('\0')
        else:
            get_hash = getattr(x, 'secure_hash', None)
            if get_hash is not None:
                self._hasher.update('object:')
                x_stable, x_hash = get_hash()
                self.stable = self.stable and x_stable
                self._hasher.update(x_hash)
            else:
                self._hasher.update('buffer:')
                self._hasher.update(x)
                self._hasher.update('\0')

    def digest(self):
        return self._hasher.digest()

    def hexdigest(self):
        return self._hasher.hexdigest()

    def make_temporal(self):
        self.stable = False

class MemoContext(object):
    """
    Subclass this class (or implement its protocol) in order to provide a
    context where memoized objects can be stored.

    Results/objects are memoized by a (name, key) tuple. The key is supposed
    to always be unique (a sha1 hash), so the name is just present for
    """
    def __init__(self):
        self.memo = {}
        self._shash = create_hasher(struct.pack('Q', id(self))).digest()

    def _get_sub_memo(self, name):
        sub_memo = self.memo.get(name, None)
        if sub_memo is None:
            sub_memo = self.memo[name] = {}
        return sub_memo
    
    def memo_get(self, name, key, tags):
        """Looks for a memoized result with the given name and key"""
        return self._get_sub_memo(name).get(key, NOT_PRESENT)

    def memo_put(self, name, key, tags, value):
        """Stores a memoized result"""
        self._get_sub_memo(name)[key] = value

    def memo_summary(self):
        lines = ['Warning: Overlapping memory not (yet) indicated']
        for name, sub_memo in sorted(self.memo.items()):
            lines.append('Objects filed under "%s"' % name)
            for hash, obj in sub_memo.iteritems():
                nbytes = self.get_memoized_object_size(obj)
                nbytes = format_byte_count(nbytes) if nbytes is not None else 'NA'
                lines.append('    %s : %s (%s)' % (hash, type(obj), nbytes))
        return '\n'.join(lines)

    def get_memoized_object_size(self, obj):
        """Human-readable information about memoized object.

        When inspecting the memoization store, one is typically not interested
        in the repr() as such, but more about statistics (in particular memory used).

        For now, don't worry about overlap...
        """
        if isinstance(obj, np.ndarray):
            return obj.size * obj.itemsize
        else:
            return None

    def _memo_log_info(self, *args, **kw):
        # Override this to do logging for the memoization
        pass

    def _memo_log_debug(self, *args, **kw):
        # Override this to do logging for the memoization
        pass

class DiskMemoContext(MemoContext):
    """Extends MemoContext by also caching to disk if given tags are present.

    Setting cache_path to None disables the cache.
    """
    
    def __init__(self, cache_path=None, disk_tags=('disk',)):
        MemoContext.__init__(self)
        self._cache_path = None if cache_path is None else os.path.realpath(cache_path)
        self._dist_tags = frozenset(disk_tags)
        if self._cache_path is not None and not os.path.isdir(self._cache_path):
            raise RuntimeError('%s does not exist or is not a directory' % self._cache_path)
        if self._cache_path is None:
            self._memo_log_info('Disk cache turned off')
        else:
            self._memo_log_info('Disk cache located at %s' % self._cache_path)


    def _memo_can_cache_to_disk(self, tags):
        if self._cache_path is None:
            return False
        if 'stable-hash' not in tags:
            return False
        return bool(self._dist_tags.intersection(tags))
    
    def memo_put(self, name, key, tags, value):
        if self._memo_can_cache_to_disk(tags):
            self.memo_store_to_disk(name, key, tags, value)
        MemoContext.memo_put(self, name, key, tags, value)

    def memo_get(self, name, key, tags):
        result = MemoContext.memo_get(self, name, key, tags)
        if result is NOT_PRESENT and self._memo_can_cache_to_disk(tags):
            result = self.memo_load_from_disk(name, key, tags)
        return result

    def _memo_make_disk_path(self, name, key):
        filename = '%s-%s' % (name, key)
        return os.path.join(self._cache_path, filename), '$cache/' + filename

    def memo_store_to_disk(self, name, key, tags, value):
        path, path_desc = self._memo_make_disk_path(name, key)
        with file(path, 'w') as f:
            pickle.dump(value, f, 2)
        size = os.path.getsize(path)
        self._memo_log_info('Stored %s (%s)' % (path_desc, format_byte_count(size)))

    def memo_load_from_disk(self, name, key, tags):
        path, path_desc = self._memo_make_disk_path(name, key)
        try:
            f = file(path)
        except (IOError, OSError):
            self._memo_log_debug('%s-%s not found in cache' % (name, key))
            return NOT_PRESENT        
        try:
            self._memo_log_info('Loaded %s' % path_desc)
            return pickle.load(f)
        finally:
            f.close()        

def format_byte_count(nbytes):
    if nbytes > 1024**3:
        units = 'GiB'
        nbytes /= 1024**3
    elif nbytes > 1024**2:
        units = 'MiB'
        nbytes /= 1024**2
    elif nbytes > 1024:
        units = 'KiB'
        nbytes /= 1024
    else:
        units = 'bytes'
    return '%.1f %s' % (nbytes, units)
        

# Decorators        

def memoize_method(name=None, tags=(), protect_result=True):
    """
    Like @memoize, but assumes that the context is the second parameter
    (after self) rather than the first.
    """
    def memoize_decorator(method):
        @functools.wraps(method)
        def replacement_method(*args, **kw):
            return memoized_call(name or method.__name__, tags, method, args, 1, kw,
                                 protect_result)
        return replacement_method
    return memoize_decorator

def memoize(name=None, tags=(), protect_result=True):
    def memoize_decorator(func):
        @functools.wraps(func)
        def replacement_func(*args, **kw):
            return memoized_call(name or func.__name__, tags, func, args, 0, kw,
                                 protect_result)
        return replacement_func
    return memoize_decorator

memoize_in_self = memoize


#
# Implementation utilities
#

class CannotMakeImmutableError(TypeError):
    pass

def as_readonly(obj):
    if isinstance(obj, (tuple, list)):
        return tuple([as_readonly(x) for x in obj])
    elif isinstance(obj, (float, int, bytes, unicode, frozenset)):
        return obj
    elif isinstance(obj, set):
        return frozenset([as_readonly(x) for x in obj])
    elif isinstance(obj, dict):
        raise CannotMakeImmutableError('dict results not supported from memoization')
    elif isinstance(obj, np.ndarray):
        obj.setflags(write=False)
        return obj
    else:
        try:
            as_immutable = obj.as_immutable
        except AttributeError:
            raise CannotMakeImmutableError('memoization result: %s not supported and does not '
                                           'have as_immutable method' % type(obj))
        return as_immutable()
    
class UnhashableError(TypeError):
    pass

_func_ids = {}

def memoized_call(name, tags, func, args_tuple, ctx_pos_in_args, kw_dict, protect_result):
    ctx = args_tuple[ctx_pos_in_args]

    should_store = kw_dict.get('memo_store', True)
    if 'memo_store' in kw_dict:
        del kw_dict['memo_store']
    
    ignore_lst = []
    filtered_args = filter_args(func, ignore_lst, args_tuple, kw_dict)
    filtered_args = tuple(sorted(filtered_args.items()))
    
    h = Hasher(stable=True)
    # We want hashes to be stable between executions, so hash function by fully
    # qualified name and the name given
    fid = '%s.%s:%s' % (func.__module__, func.__name__, name)
    if _func_ids.setdefault(fid, func) is not func:
        raise RuntimeError('Function id %s not unique!' % fid)
    h.update(fid)
    # Proceed to hash arguments
    for arg_name, arg_value in filtered_args:
        # TODO: Rewrite filter_args to make the following line less hacke
        if arg_value is ctx:
            continue
        h.update(arg_name)
        try:
            h.update(arg_value)
        except UnhashableError:
            raise TypeError("Argument '%s' of type %s (or one of its children) does not support secure hashing; "
                            "see commander.memoize" %
                            (arg_name, type(arg_value)))
    key = h.hexdigest()
    if h.stable:
        tags = tuple(tags) + ('stable-hash',)

    try:
        memo_getter = ctx.memo_get
        memo_putter = ctx.memo_put
    except AttributeError:
        raise TypeError("'context' object (first argument) does not support "
                        "the MemoContext protocol (remember to use @memoize_method on methods)")

    result = memo_getter(name, key, tags)
    if result is NOT_PRESENT:
        result = func(*args_tuple, **kw_dict)
        if should_store:
            if protect_result:
                result = as_readonly(result)
            memo_putter(name, key, tags, result)
    return result
    

#
# The following function is from joblib; but altered to pass through the
# first `ctx` argument.
#
def filter_args(func, ignore_lst, args=(), kwargs=dict()):
    """ Filters the given args and kwargs using a list of arguments to
        ignore, and a function specification.

        Parameters
        ----------
        func: callable
            Function giving the argument specification
        ignore_lst: list of strings
            List of arguments to ignore (either a name of an argument
            in the function spec, or '*', or '**')
        *args: list
            Positional arguments passed to the function.
        **kwargs: dict
            Keyword arguments passed to the function

        Returns
        -------
        filtered_args: list
            List of filtered positional arguments.
        filtered_kwdargs: dict
            List of filtered Keyword arguments.
    """
    args = list(args)
    if isinstance(ignore_lst, basestring):
        # Catch a common mistake
        raise ValueError('ignore_lst must be a list of parameters to ignore '
            '%s (type %s) was given' % (ignore_lst, type(ignore_lst)))
    # Special case for functools.partial objects
    if (not inspect.ismethod(func) and not inspect.isfunction(func)):
        if ignore_lst:
            warnings.warn('Cannot inspect object %s, ignore list will '
                'not work.' % func, stacklevel=2)
        return {'*': args, '**': kwargs}
    arg_spec = inspect.getargspec(func)

    # We need to if/them to account for different versions of Python
    if hasattr(arg_spec, 'args'):
        arg_names = arg_spec.args
        arg_defaults = arg_spec.defaults
        arg_keywords = arg_spec.keywords
        arg_varargs = arg_spec.varargs
    else:
        arg_names, arg_varargs, arg_keywords, arg_defaults = arg_spec
    arg_defaults = arg_defaults or {}
    
    if inspect.ismethod(func):
        raise NotImplementedError('memoizing bound methods not supported yet')

    arg_dict = dict()
    arg_position = -1
    for arg_position, arg_name in enumerate(arg_names):
        if arg_position < len(args):
            # Positional argument or keyword argument given as positional
            arg_dict[arg_name] = args[arg_position]
        else:
            position = arg_position - len(arg_names)
            if arg_name in kwargs:
                arg_dict[arg_name] = kwargs.pop(arg_name)
            else:
                try:
                    arg_dict[arg_name] = arg_defaults[position]
                except (IndexError, KeyError):
                    # Missing argument
                    raise TypeError('Wrong number of arguments for %s%s:\n'
                                     '     %s(%s, %s) was called.'
                        % (func,
                           inspect.formatargspec(*inspect.getargspec(func)),
                           func,
                           repr(args)[1:-1],
                           ', '.join('%s=%s' % (k, v)
                                    for k, v in kwargs.iteritems())
                           )
                        )

    varkwargs = dict()
    for arg_name, arg_value in sorted(kwargs.items()):
        if arg_name in arg_dict:
            arg_dict[arg_name] = arg_value
        elif arg_keywords is not None:
            varkwargs[arg_name] = arg_value
        else:
            raise ValueError("Ignore list for %s() contains an unexpected "
                            "keyword argument '%s'" % (func, arg_name))

    if arg_keywords is not None:
        arg_dict['**'] = varkwargs
    if arg_varargs is not None:
        varargs = args[arg_position + 1:]
        arg_dict['*'] = varargs

    # Now remove the arguments to be ignored
    for item in ignore_lst:
        if item in arg_dict:
            arg_dict.pop(item)
        else:
            raise ValueError("Ignore list: argument '%s' is not defined for "
            "function %s%s" %
                            (item, name,
                             inspect.formatargspec(arg_names,
                                                   arg_varargs,
                                                   arg_keywords,
                                                   arg_defaults,
                                                   )))
    # XXX: Return a sorted list of pairs?
    return arg_dict
