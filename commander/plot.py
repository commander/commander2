from __future__ import division

import os
import tempfile

import numpy as np
from matplotlib import pyplot as plt

from .sphere import mmajor

def subplots(i, j, **kw):
    """
    This really doesn't belong in a library such as Commander, but
    the subplots shipping with matplotlib is wacky, so having a sane wrapper
    easily available is valuable...
    """
    fig, axs = plt.subplots(i, j, **kw)
    axs = np.asarray(axs)
    if axs.ndim == 1:
        axs = axs[None, :] if i == 1 else axs[:, None]
    elif axs.ndim == 0:
        axs = axs[None, None]
    return fig, axs
        
def plotmatrix(matrix, ax=None, logabs=False):
    if ax is None:
        ax = plt.gca()
    if logabs:
        matrix = np.log10(np.abs(matrix))
    im = ax.imshow(matrix, interpolation='nearest')
    plt.colorbar(im, ax=ax)

def plot_ring_map(map, width=1000, mask=None, ax=None, title=None, bar=True, **kw):
    from .sphere import healpix

    if ax is None:
        ax = plt.gca()

    if mask is not None:
        if map.shape != mask.shape:
            raise ValueError('map.shape != mask.shape')
        map = map.copy()
        map[mask == 0] = np.nan

    projected_map = healpix.project_to_mollweide(map, width)
    img = ax.imshow(projected_map, interpolation='nearest', **kw)
    if title:
        ax.set_title(title)
    if bar:
        plt.colorbar(img, ax=ax, orientation='horiztonal', fraction=0.05, pad=0.05,
                    aspect=40)
    return img

def plot_gauss_map(map, **kw):
    # Find ntheta, nphi;
    npix = map.shape[0]
    lmax = int(np.round(np.sqrt(npix // 2))) - 1
    ntheta = lmax + 1
    nphi = 2 * (lmax + 1)
    map = map.reshape(ntheta, nphi)
    plt.imshow(map, interpolation='none', **kw)

def plot_sh_map(ctx, alm, nside, lmin, lmax, width=1000, ax=None, title=None, mask=None,
                **kw):
    map = sh_synthesis(nside, alm)
    return plot_ring_map(map, width=width, ax=ax, title=title, mask=mask, **kw)

def scale_power_spectrum_for_plot(Cl):
    l = np.arange(Cl.shape[0])
    l[0] = 1
    result = Cl * (l * (l + 1)) / (2 * np.pi)
    result[0] = np.nan
    return result

def power_spectrum_scaling(lmax):
    l = np.arange(lmax + 1)
    l[0] = 1
    result = (l * (l + 1)) / (2 * np.pi)
    return result
    
def plot_power_spectrum(Cl, ax=None, **kw):
    l = np.arange(2, Cl.shape[0])
    scaled_Cl = scale_power_spectrum_for_plot(Cl)
    if ax is None:
        ax = plt
    return ax.plot(l, scaled_Cl[2:], **kw)
    
def spherical_signal_spectrum_plot(ctx, observations, component_model, realized_signal, lmax=None,
                                   ax=None, legend='upper right'):
    if lmax is None:
        lmax = component_model.lmax
    if ax is None:
        fig, axs = subplots(1, 1)
        ax = axs[0, 0]
        
    Cl = component_model.load_power_spectrum(lmax)
    ls = np.arange(lmax + 1)
    scaling = ls * (ls + 1) / (2 * np.pi)
    scaling[0] = 1 / (2 * np.pi)

    sigma_l = mmajor.compute_power_spectrum(0, component_model.lmax, realized_signal)[:lmax + 1]
        
    ax.plot(scaling * sigma_l, 'b-', label='Sample $\mathbf{s}$')
    ax.plot(scaling * Cl, 'b--', label='$C_\ell$')

    colors = ['g', 'r', 'k']
    colors *= (len(observations) // len(colors) + 1)

    for obs, c in zip(observations, colors):
        ninv_map = ctx.get_mask_map(obs) / ctx.get_TT_rms_map(obs)**2
        noise_power = 4 * np.pi / obs.npix * (1 / ninv_map.mean())
        beam = ctx.get_symmetric_beam_and_pixel_window_transfer(obs, 0, lmax)
        ax.plot(scaling * noise_power, c + '--', label='Approx. noise power for %s' % obs.name)
        ax.plot(scaling * Cl * beam**2, c + '-', label='$C_\ell b_\ell^2$ for %s' % obs.name)
        
    ax.legend(loc=legend)

def signal_and_noise_plot(obs, Cl, add_rms=0, ax=None):
    if ax is None:
        ax = plt.gca()
    rms = obs.load_rms_map()
    sigma0 = np.mean(rms[~np.isnan(rms)]) + add_rms
    bl = obs.load_sh_beam()
    nl = 4 * np.pi / obs.npix * sigma0**2

    lmax = Cl.shape[0] - 1
    l = np.arange(lmax + 1)
    scale = l * (l + 1) / 2 * np.pi

    ax.plot(Cl * scale, label='Cl')
    ax.plot(nl / bl**2 * scale, label='nl/bl^2')
    ax.set_ylim((0, 1.05 * np.max(Cl * scale)))
    ax.legend()
    return ax

def signal_to_noise_ratio_plot(obs, Cl, add_rms=0, ax=None):
    if ax is None:
        ax = plt.gca()
    rms = obs.load_rms_map()
    sigma0 = np.mean(rms[~np.isnan(rms)]) + add_rms
    bl = obs.load_sh_beam()
    nl = 4 * np.pi / obs.npix * sigma0**2

    lmax = Cl.shape[0] - 1
    l = np.arange(lmax + 1)
    scale = l * (l + 1) / 2 * np.pi

    ax.semilogy(Cl / (nl / bl**2))
    return ax
