import os

import numpy as np
import logging

from .memoize import memoize, memoize_method, memoize_in_self, DiskMemoContext
from .logger_utils import get_child_logger, log_task, log_after_task

class CommanderContext(DiskMemoContext):
    def __init__(self, nc_rng=None, logger=None, cache_path=None, seed=None):
        if logger is None:
            logging.basicConfig()
            logger = logging.getLogger()
            logger.setLevel(logging.DEBUG)
        self.logger = logger
        self.memo_logger = self.get_logger('memo')
        DiskMemoContext.__init__(self, cache_path, disk_tags=('disk',))

        if nc_rng is None:
            nc_rng = np.random.RandomState(seed)

        self.nc_rng = nc_rng
        self.data_logger = self.get_logger('data')
        self.compute_logger = self.get_logger('compute')

    def get_logger(self, name):
        return get_child_logger(self.logger, name)

    def _memo_log_info(self, *args, **kw):
        self.memo_logger.info(*args, **kw)

    def _memo_log_debug(self, *args, **kw):
        self.memo_logger.debug(*args, **kw)
