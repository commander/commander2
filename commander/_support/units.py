"""
Units
-----


"""

from __future__ import division

import numpy as np
import re

_angle_re = re.compile(r"([0-9.]+)\s*(deg|min|sec|\*|''|')")
def as_radians(x):
    if isinstance(x, str):
        matches = _angle_re.findall(x)
        degrees = 0
        for val, unit in matches:
            val = float(val)
            if unit in ('deg', '*'):
                pass
            elif unit in ('min', "'"):
                val /= 60
            elif unit in ('sec', "''"):
                val /= 3600
            degrees += val
        return degrees / 180 * np.pi
    else:
        return float(x)

def as_temperature(t):
    msg = 'Temperature must be float (microkelvin) or a string on the form "2.3 mK"; got: %r' % t
    if isinstance(t, float):
        return t
    elif isinstance(t, str):
        import re
        m = re.match('([0-9.]+)\s*(mK|uK|muK|K)?', t)
        if m is None:
            raise ValueError(msg)
        value = float(m.group(1))
        scale = get_temperature_conversion_factor(m.group(2))
        return value * scale
    else:
        raise TypeError(msg)

def get_temperature_conversion_factor(from_units):
    """
    Returns the conversion factor necessary to go from `from_units` to the 
    internal units (microkelvin).
    """
    if from_units == 'K':
        return 1e6
    elif from_units == 'mK':
        return 1e3
    elif from_units == 'uK':
        return 1
    else:
        raise ValueError('Unrecognized temperature unit: %s' % from_units)

def bytes2human(size):
    """
    Convert some number of bytes into a human-readable string.
    """
    units = ['B', 'KB', 'MB', 'GB']
    cur = 1024
    string = "%s %s" % (round(size/1024**(len(units)-1), 1), units[-1])
    for i in range(len(units)):
        if size // cur == 0:
            string = "%s %s" % (round(float(size)*1024/cur, 2), units[i])
            return string
        else:
            cur *= 1024
    return string
