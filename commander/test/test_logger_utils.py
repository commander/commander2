from nose.tools import eq_
from .. import logger_utils

def test_format_duration():
    dt = 1e-10
    for s in ['0.1 ns', '1.0 ns', '10.0 ns', '100.0 ns', '1.0 us', '10.0 us',
              '100.0 us', '1.0 ms', '10.0 ms', '100.0 ms', '1.0 s', '10.0 s']:
        yield eq_, s, logger_utils.format_duration(dt)
        dt *= 10
