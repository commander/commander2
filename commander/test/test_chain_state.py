
from .._testsupport import *
from ..analysis import Chain
from ..analysis.chain import ParamSyncError
import tables as tb
import numpy as np

def get_tfile_and_root(d):
    tfile = d + '/testfile.hdf'
    root = '/'
    return tfile, root

@temp_working_dir_fixture
def test_init_ver1(d):
    def func():
        tchain = Chain()
    yield assert_raises, TypeError, func

@temp_working_dir_fixture
def test_init_ver2(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, 
                              save_sampler_info=True)
@temp_working_dir_fixture
def test_init_ver3(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec,
                              save_sampler_info=False)
@temp_working_dir_fixture
def test_init_ver4(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec,
                              save_sampler_info=False)
@temp_working_dir_fixture
def test_init_with_derived(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    d_paramnames = ['alpha_d', 'beta_d']
    tchain = Chain.create_new(tfile, root, param_spec, 
                              save_sampler_info=True,
                              derived_paramnames=d_paramnames)
   
@temp_working_dir_fixture
def test_init_with_duplicate_param_names(d):
    tfile, root = get_tfile_and_root(d)
    param_spec = [('alpha', 1.0), ('alpha', 12), 
                  ('gamma', np.array([5.0, 3.0, 2.0]))]
    with assert_raises_msg(ValueError, "Duplicate parameter names were specified."):
        tchain = Chain.create_new(tfile, root, param_spec, 
                                  save_sampler_info=True)
    tfile, root = get_tfile_and_root(d)
    param_spec = [('alpha', 1.0), ('beta', 12), 
                  ('gamma', np.array([5.0, 3.0, 2.0]))]
    with assert_raises_msg(ValueError, 
                           "Duplicate derived parameter names were specified."):
        tchain = Chain.create_new(tfile, root, param_spec, 
                                  save_sampler_info=True, 
                                  derived_paramnames=['alpha_d', 'alpha_d'])

@temp_working_dir_fixture
def test_init_with_different_root(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    root += 'test'
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True)

@temp_working_dir_fixture
def test_init_with_several_chains(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, 
                              save_sampler_info=True)
    tchain.close_file()
    root = '/testnode'
    tchain = Chain.create_new(tfile, root, param_spec, 
                              save_sampler_info=True)
    tchain.close_file()
    def func():
        root = '/testnode'
        tchain = Chain.create_new(tfile, root, param_spec, 
                                  save_sampler_info=True)
    yield assert_raises, tb.NodeError, func

@temp_working_dir_fixture
def test_load(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, 
                              save_sampler_info=True)
    tchain.close_file()
    tchain = Chain.load(tfile, root)

@temp_working_dir_fixture
def test_load_sanity(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    d_paramnames = ['alpha_d', 'beta_d']
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, 
                              derived_paramnames=d_paramnames, 
                              save_sampler_info=True)
    tchain.close_file()
    tchain = Chain.load(tfile, root)
    yield ok_, 'alpha' in tchain.grpParams
    #The derived parameter tables aren't added to the groups before they're 
    #being added to the chain for the first time.
    yield ok_, 'alpha_d' not in tchain.grpDParams
    yield eq_, tuple(tchain.curr_chain_vals[0]),(-1, 0, 0, 0, 0, 0)
    yield eq_, tchain._save_sampler_info, True
    yield eq_, tchain['alpha'], 1.0
    yield eq_, tchain['beta'], 12
    yield ok_, np.all(tchain['gamma'] == np.array([5.0, 3.0, 2.0]))
    tchain.close_file()
    #Create a new chain within current file and try to load it
    param_spec = [('alpha', 5.0), ('beta', 9), 
                ('gamma', np.array([2.0, 8.0, 19.5]))]
    root += 'newchain'
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=False)
    tchain.close_file()
    tchain = Chain.load(tfile, root)
    yield ok_, 'alpha' in tchain.grpParams
    yield eq_, tuple(tchain.curr_chain_vals[0]), (-1, 0, 0, 0)
    yield eq_, tchain._save_sampler_info, False
    yield eq_, tchain['alpha'], 5.0
    yield eq_, tchain['beta'], 9
    yield ok_, np.all(tchain['gamma'] == np.array([2.0, 8.0, 19.5]))

#Tests for checking whether the resulting files actually make any sense
@temp_working_dir_fixture
def test_sanity(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    d_paramnames = ['alpha_d', 'beta_d']
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True,
                              derived_paramnames=d_paramnames)
    tchain.close_file()
    tfile = tb.openFile(tfile, mode='r')
    yield ok_, 'params' in tfile.root
    yield ok_, 'derived_params' in tfile.root
    yield ok_, 'sampler_info' in tfile.root
    yield ok_, 'chain' in tfile.root
    yield ok_, 'sampler_IDs' in tfile.root
    yield ok_, isinstance(tfile.root.params, tb.Group)
    yield ok_, isinstance(tfile.root.derived_params, tb.Group)
    yield ok_, isinstance(tfile.root.sampler_info, tb.Group)
    yield ok_, isinstance(tfile.root.chain, tb.Table)
    yield ok_, isinstance(tfile.root.sampler_IDs, tb.Table)
    #Check if our input parameters are present
    yield ok_, 'alpha' in tfile.root.params
    #And that they are Earrays
    yield ok_, isinstance(tfile.root.params.alpha, tb.EArray)
    #And that they are of the right dtype
    yield eq_, tfile.root.params.alpha.dtype, 'float64'
    yield eq_, tfile.root.params.beta.dtype, np.dtype(int).name
    yield eq_, tfile.root.params.gamma.dtype, 'float64'
    #And that they have the right shape
    yield eq_, tfile.root.params.alpha.shape, (1, 1)
    yield eq_, tfile.root.params.gamma.shape, (1, 3)
    #And that they have the right initial values
    yield eq_, tfile.root.params.alpha[0], 1.0
    yield eq_, tfile.root.params.beta[0], 12
    yield ok_, np.all(tfile.root.params.gamma[0] == np.array([5.0, 3.0, 2.0]))

    #Test the 'chain' table
    #There should be two entries in the table as of now - the 'init' entry
    #and the first parameter entry
    yield eq_, len(tfile.root.chain), 1
    yield eq_, tfile.root.chain.colnames[0], 'sampler'
    yield eq_, tfile.root.chain.colnames[1], 'alpha'
    yield eq_, tfile.root.chain.colnames[2], 'beta'
    yield eq_, tfile.root.chain.colnames[3], 'gamma'
    yield eq_, tfile.root.chain.colnames[4], 'alpha_d'
    yield eq_, tfile.root.chain.colnames[5], 'beta_d'
    #3 parameters, 2 derived parameters and the sampler
    yield eq_, len(tfile.root.chain.cols), 6
    #Check that all parameter entries are initialized to zero and that the
    #sampler is initialized to -1
    yield eq_, tuple(tfile.root.chain[0]), (-1, 0, 0, 0, 0, 0)

#Do similar with save_sampler_info=False
@temp_working_dir_fixture
def test_sanity_ver2(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=False)
    tchain.close_file()
    tfile = tb.openFile(tfile, mode='r')
    yield ok_, 'params' in tfile.root
    #Now sampler_info is NOT supposed to be in the file
    yield ok_, 'sampler_info' not in tfile.root
    yield ok_, 'chain' in tfile.root
    yield ok_, isinstance(tfile.root.params, tb.Group)
    yield ok_, isinstance(tfile.root.chain, tb.Table)

@temp_working_dir_fixture
def test_getitem(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True)
    yield eq_, tchain['alpha'], 1.0
    yield eq_, tchain['beta'], 12
    yield ok_, tchain['gamma'] is param_spec[2][1][...]
    yield eq_, tchain[('alpha', 'beta')], (1.0, 12)
    yield ok_, tchain[('beta', 'gamma')][1] is param_spec[2][1][...]

@temp_working_dir_fixture
def test_get_chain(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True)
    yield eq_, tchain.get_chain('alpha'), np.array([1.0])
    yield eq_, tchain.get_chain('beta'), np.array([12])
    yield ok_, np.all(tchain.get_chain('gamma') == np.array([5.0, 3.0, 2.0]))
    #Do some cheating
    tchain._update_parameter('alpha', 4.0)
    tchain._update_parameter('beta', 47)
    tchain._update_parameter('gamma', np.array([84.9, 32.0, 39.9]))
    yield ok_, np.all(tchain.get_chain('alpha') == np.array([[1.0],
                                                             [4.0]]))
    yield ok_, np.all(tchain.get_chain('beta') == np.array([[12],
                                                            [47]]))
    yield ok_, np.all(tchain.get_chain('gamma') == 
                      np.array([[5.0, 3.0, 2.0],
                                [84.9, 32.0, 39.9]]))
    yield ok_, isinstance(tchain.get_chain('alpha'), np.ndarray)

@temp_working_dir_fixture
def test_gibbs(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    filename, root = get_tfile_and_root(d)
    tchain = Chain.create_new(filename, root, param_spec, save_sampler_info=True)
    tchain.gibbs(('alpha',), (4.0,), info={'time':20.3, 'iterations':50})
    tfile = tchain.f
    yield eq_, tchain['alpha'], 4.0
    yield eq_, tuple(tfile.root.chain[1]), (0, 1, 0, 0)
    yield eq_, tfile.root.sampler_info.gibbs.colnames[0], 'iterations'
    yield eq_, tfile.root.sampler_info.gibbs.colnames[1], 'time'
    yield eq_, tuple(tfile.root.sampler_info.gibbs[0]), (50, 20.3)

    tchain.gibbs(('alpha', 'beta'), (8.0, 4), info={'moretime':14.0}, 
                 sampler_key='gibbs_v2')
    yield eq_, tchain[('alpha', 'beta')], (8.0, 4)
    yield eq_, tuple(tfile.root.chain[2]), (1, 2, 1, 0)
    yield eq_, tfile.root.sampler_info.gibbs_v2.colnames[0], 'moretime'
    yield eq_, tuple(tfile.root.sampler_info.gibbs_v2[0]), (14.0,)

    tchain.gibbs(('gamma',), ([2.0, 14.2, 91.0],), info={'time':90.1, 
                                                         'iterations':109})
    yield ok_, np.all(tchain['gamma'] == np.array([2.0, 14.2, 91.0]))
    yield ok_, param_spec[2][1][...] is tchain['gamma']
    yield eq_, tuple(tfile.root.sampler_info.gibbs[1]), (109, 90.1)
    yield eq_, tuple(tfile.root.chain[3]), (0, 2, 1, 1)

    tchain.gibbs(('alpha', 'gamma'), (83.0, [74.0, 1230.02, 1.0]), 
                 info={'moretime':95.1}, sampler_key='gibbs_v2')
    yield eq_, tchain[('alpha', 'gamma')][0],  83.0
    yield ok_, np.all(tchain[('alpha', 'gamma')][1] == 
                      np.array([74.0, 1230.02, 1.0]))
    yield ok_, param_spec[2][1][...] is tchain['gamma']
    yield eq_, tuple(tfile.root.sampler_info.gibbs_v2[1]), (95.1,)
    yield eq_, tuple(tfile.root.chain[4]), (1, 3, 1, 2)

    #There should be an error when trying to provide an info dict that doesn't
    #match what has been sent in before for that sampler
    def func():
        tchain.gibbs(('alpha',), (239.9,), info={'moremoretime':239.9}, 
                     sampler_key='gibbs_v2')
    yield assert_raises, ValueError, func

    #and after such an attempt, the parameter and chain values shouldn't have
    #changed
    yield eq_, tchain['alpha'], 83.0
    yield eq_, len(tfile.root.chain), 5
    yield eq_, tuple(tfile.root.chain[4]), (1, 3, 1, 2)
    #By now, we should have two samplers in the sampler_IDs table.
    yield eq_, tfile.root.sampler_IDs[0][0], 0
    yield eq_, tfile.root.sampler_IDs[0][1], 'gibbs'
    yield eq_, tfile.root.sampler_IDs[1][0], 1
    yield eq_, tfile.root.sampler_IDs[1][1], 'gibbs_v2'

    #We now try to close the current chain and load it again
    tchain.close_file()
    tchain = Chain.load(filename, root)
    tfile = tchain.f
    yield eq_, tchain['alpha'], 83.0
    yield eq_, len(tfile.root.chain), 5
    yield eq_, tuple(tchain.curr_chain_vals[0]), (1, 3, 1, 2)
    yield eq_, tchain.sampler_IDs['gibbs'], 0
    yield eq_, tchain.sampler_IDs['gibbs_v2'], 1

@temp_working_dir_fixture
def test_metropolis(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    filename, root = get_tfile_and_root(d)
    tchain = Chain.create_new(filename, root, param_spec, save_sampler_info=True)
    logp_after = np.log(1.0)
    logp_before = np.log(1e-5)
    tchain.metropolis(('alpha',), (23.94,), logp_before, logp_after, 
                      info={'time':3.0, 'iterations':239})
    tfile = tchain.f
    yield eq_, tchain['alpha'], 23.94
    yield eq_, tuple(tfile.root.chain[1]), (0, 1, 0, 0)
    yield eq_, tfile.root.sampler_info.metropolis.colnames[0], 'accepted'
    yield eq_, tfile.root.sampler_info.metropolis.colnames[1], 'logp'
    yield eq_, tfile.root.sampler_info.metropolis.colnames[2], 'iterations'
    yield eq_, tfile.root.sampler_info.metropolis.colnames[3], 'time'
    yield eq_, tuple(tfile.root.sampler_info.metropolis[0]), (True, logp_after, 
                                                         239, 3.0)
    #Try to reject a sample
    logp_after = np.log(1e-5)
    logp_before = np.log(1.0)
    tchain.metropolis(('beta',), (74,), logp_before, logp_after, 
                      info={'moretime':53.0}, sampler_key='metropolis_v2')
    yield eq_, tchain['beta'], 12
    yield eq_, tuple(tfile.root.chain[2]), (1, 1, 0, 0)
    yield eq_, tfile.root.sampler_info.metropolis_v2.colnames[0], 'accepted'
    yield eq_, tfile.root.sampler_info.metropolis_v2.colnames[1], 'logp'
    yield eq_, tfile.root.sampler_info.metropolis_v2.colnames[2], 'moretime'
    yield eq_, tuple(tfile.root.sampler_info.metropolis_v2[0]), (False, logp_before, 
                                                              53.0)
    #By now, we should have two samplers in the sampler_IDs table.
    yield eq_, tfile.root.sampler_IDs[0][0], 0
    yield eq_, tfile.root.sampler_IDs[0][1], 'metropolis'
    yield eq_, tfile.root.sampler_IDs[1][0], 1
    yield eq_, tfile.root.sampler_IDs[1][1], 'metropolis_v2'

    #We now try to close the current chain and load it again
    tchain.close_file()
    tchain = Chain.load(filename, root)
    tfile = tchain.f
    yield eq_, tchain['alpha'], 23.94
    yield eq_, len(tfile.root.chain), 3
    yield eq_, tuple(tchain.curr_chain_vals[0]), (1, 1, 0, 0)
    yield eq_, tchain.sampler_IDs['metropolis'], 0
    yield eq_, tchain.sampler_IDs['metropolis_v2'], 1

@temp_working_dir_fixture
def test_derivedparams_v1(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    d_paramnames = ['alpha_d', 'beta_d']
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True,
                              derived_paramnames=d_paramnames)
    d_parvals = [np.array([4.5, 3.49]), False]
    #Add an iteration of derived parameters.
    tchain.add_iteration(d_paramnames, newvalues=d_parvals,
                         is_derived=True, param_dependencies={'alpha_d':
                                                              ['alpha',],
                                                              'beta_d':
                                                              ['beta',]})
    yield ok_, np.all(tchain['alpha_d'] == np.array([4.5, 3.49]))
    yield ok_, d_parvals[0] is tchain['alpha_d']
    yield ok_, d_parvals[1] is not tchain['beta_d']
    tchain.close_file()
    tfile = tb.openFile(tfile, mode='r')
    yield eq_, tuple(tfile.root.chain[-1]), (-2, 0, 0, 0, 1, 1)

@temp_working_dir_fixture
def test_derivedparams_v2(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    d_paramnames = ['alpha_d', 'beta_d']
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True,
                              derived_paramnames=d_paramnames)
    d_parvals = [np.array([4.5, 3.49]), False]
    #Add an iteration of derived parameters.
    tchain.add_iteration(d_paramnames, newvalues=d_parvals,
                         is_derived=True, param_dependencies={'alpha_d':
                                                              ['alpha',],
                                                              'beta_d':
                                                              ['beta',]})
    #Now add an iteration of regular parameters
    tchain.add_iteration(('alpha', 'beta'), sampler_key='testsampler', 
                         newvalues=(49.1, 67), is_derived=False)
    #Since both alpha and beta have changed, adding an iteration with the *same*
    #values for the derived parameters should raise an error.
    with assert_raises(ParamSyncError):
        tchain.add_iteration(d_paramnames, newvalues=None, is_derived=True)
    #We should also get an error when trying to access the most recent derived
    #parameter.
    with assert_raises(ParamSyncError):
        x = tchain['alpha_d']
    #But if we add an iteration with new values, there should be no error
    d_parvals = [np.array([87.2, 5.49]), True]
    tchain.add_iteration(d_paramnames, newvalues=d_parvals, is_derived=True)
    #And after that, we should be able to add another iteration of the derived
    #parameters without changing them.
    tchain.add_iteration(d_paramnames, newvalues=None, is_derived=True)
    #If we add an iteration of a parameter that the derived parameters don't
    #depend on, there should be no problem.
    tchain.add_iteration(('alpha',), sampler_key='morn', newvalues=(39.2,),
                         is_derived=False)
    tchain.add_iteration(('beta_d',), newvalues=None, is_derived=True)
    tchain.close_file()
    tfile = tb.openFile(tfile, mode='r')
    yield eq_, tuple(tfile.root.chain[-1]), (-2, 2, 1, 0, 2, 2)

@temp_working_dir_fixture
def test_derivedparams_v3(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    d_paramnames = ['alpha_d', 'beta_d']
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True,
                              derived_paramnames=d_paramnames)
    d_parvals = [np.array([4.5, 3.49]), False]
    #Add an iteration of derived parameters. Should be possible to have no
    #dependencies.
    tchain.add_iteration(d_paramnames, newvalues=d_parvals,
                         is_derived=True, param_dependencies={'alpha_d':
                                                              [],
                                                              'beta_d':
                                                              ['beta',]})
    #Now add an iteration of regular parameters
    tchain.add_iteration(('alpha', 'beta'), sampler_key='testsampler', 
                         newvalues=(49.1, 67), is_derived=False)
    #Now getting the derived parameter that doesn't depend on anything should
    #work
    x = tchain['alpha_d']
    #while getting the one that depends on beta should raise an error.
    with assert_raises(ParamSyncError):
        x = tchain['beta_d']
@temp_working_dir_fixture
def test_load_derivedparams(d):
    param_spec = [('alpha', 1.0), ('beta', 12), 
                ('gamma', np.array([5.0, 3.0, 2.0]))]
    d_paramnames = ['alpha_d', 'beta_d']
    tfile, root = get_tfile_and_root(d)
    tchain = Chain.create_new(tfile, root, param_spec, save_sampler_info=True,
                              derived_paramnames=d_paramnames)
    #Add a derived parameter iteration
    d_parvals = [np.array([87.2, 5.49]), True]
    tchain.add_iteration(d_paramnames, newvalues=d_parvals,
                         is_derived=True, param_dependencies={'alpha_d':
                                                              ['alpha',],
                                                              'beta_d':
                                                              ['beta',]})
    tchain.close_file()
    tchain = Chain.load(tfile, root)
    yield ok_, 'alpha_d' in tchain.grpDParams
    yield eq_, tuple(tchain.curr_chain_vals[0]), (-2, 0, 0, 0, 1, 1)
    #If we try to add another iteration without specifying the dependencies
    #, it should raise an error because this information must be
    #respecified upon loading.
    with assert_raises(KeyError):
        tchain.add_iteration(d_paramnames, newvalues=d_parvals,
                             is_derived=True)
    yield eq_, tuple(tchain.curr_chain_vals[0]), (-2, 0, 0, 0, 1, 1)
    #but with the dependencies specified, everything should work
    tchain.add_iteration(d_paramnames, newvalues=d_parvals,
                         is_derived=True, param_dependencies={'alpha_d':
                                                              ['alpha',],
                                                              'beta_d':
                                                              ['beta',]})
