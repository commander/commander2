from .._testsupport import *

from ..memoize import memoize, MemoContext, memoize_method, memoize_in_self

from nose import SkipTest

class MockContext(MemoContext):
    def __init__(self):
        MemoContext.__init__(self)
        self.log = []

    def info(self, s):
        self.log.append(s)

class MyClass(object):
    def temporal_hash(self):
        return str(id(self))
    
    @memoize_method('mymethod')
    def mymethod(self, ctx, arg):
        ctx.info('called mymethod')
        return 2 * arg

class MyContext(MockContext):
    @memoize_in_self('mymethod')
    def mymethod(self, arg):
        self.info('called mymethod')
        return 2 * arg

@memoize('foo')
def compute_foo(ctx, arg):
    ctx.info('called compute_foo')
    return 3 * arg

def test_errors():
    raise SkipTest()
    with assert_raises(TypeError):
        compute_foo(2, 3) # first arg not a context

    with assert_raises(TypeError):
        compute_foo(2, 3, 4) # too many args

    with assert_raises(TypeError):
        compute_foo(MockContext()) # too few args

    with assert_raises(TypeError):
        compute_foo(MockContext(), not_existing_arg=3)

    with assert_raises(TypeError):
        compute_foo(MockContext(), 2, not_existing_arg=3)


def test_function():
    raise SkipTest()
    ctx = MockContext()
    assert 9 == compute_foo(ctx, 3)
    assert ctx.log == ['called compute_foo']
    assert 9 == compute_foo(ctx, 3)
    assert ctx.log == ['called compute_foo'] # i.e., only called once
    assert 12 == compute_foo(ctx, 4)
    assert ctx.log == ['called compute_foo', 'called compute_foo'] # called twice
    assert 12 == compute_foo(ctx, arg=4)
    assert ctx.log == ['called compute_foo', 'called compute_foo'] # called twice
    assert 15 == compute_foo(ctx, arg=5)
    assert ctx.log == ['called compute_foo', 'called compute_foo', 'called compute_foo']

def test_method():
    raise SkipTest()
    ctx = MockContext()
    obj = MyClass()
    assert 6 == obj.mymethod(ctx, 3)
    assert ctx.log == ['called mymethod']
    assert 6 == obj.mymethod(ctx, 3)
    assert ctx.log == ['called mymethod']
    assert 8 == obj.mymethod(ctx, 4)
    assert ctx.log == ['called mymethod', 'called mymethod']

def test_self():
    raise SkipTest()
    ctx = MyContext()
    assert 6 == ctx.mymethod(3)
    assert ctx.log == ['called mymethod']
    assert 6 == ctx.mymethod(3)
    assert ctx.log == ['called mymethod']
    assert 8 == ctx.mymethod(4)
    assert ctx.log == ['called mymethod', 'called mymethod']
    
