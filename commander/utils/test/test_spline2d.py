import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

from .. import spline2d

def test_basic():
    x = np.linspace(0, 1, 10)
    y = np.linspace(0, 1, 10)
    f = np.zeros((10, 10), order='F')
    f[...] = ((x[:, None] - .4)**2 + (y[None, :] - .5)**2)

    spline = spline2d.Spline2d(x, y, f)
    x_hi = y_hi = np.linspace(0.1, 0.9, 1000)
    
    f_hi = spline.evaluate(x_hi, y_hi)

    if 0:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        xp, yp = np.mgrid[0:1:10j, 0:1:10j]
        ax.plot_wireframe(xp, yp, f)

        xp, yp = np.mgrid[0.1:.9:50j, .1:.9:50j]
        ax.plot_wireframe(xp, yp, f_hi, color='r')
        plt.show()

