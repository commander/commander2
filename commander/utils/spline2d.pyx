import numpy as np
cimport numpy as cnp
cimport cython

cdef extern from "stdint.h":
    ctypedef int int32_t

cdef extern:
    void splie2_full_precomp_cwrapper(int32_t m, int32_t n, double *x, double *y,
                                      double *f, double *coeff)
    double splin2_full_precomp_cwrapper(int32_t m, int32_t n, double *x, double *y,
                                        double *coeff, double x0, double y0)

cdef class Spline2d:
    cdef public int32_t m, n

    cdef double[::1] x, y
    cdef double[::1,:,:,:] coeff

    def __init__(self, double[::1] x, double[::1] y, double[::1, :] f):
        cdef:
            double[::1, :, :, :] coeff
        self.m = m = x.shape[0]
        self.n = n = y.shape[0]
        self.x = x
        self.y = y
        self.coeff = coeff = np.empty((4, 4, m, n), order='F')
        splie2_full_precomp_cwrapper(m, n, &x[0], &y[0], &f[0,0], &coeff[0,0,0,0])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def evaluate(self, double[::1] x0, double[::1] y0):
        cdef:
            double[::1, :] out
            int32_t i, j
        out = np.empty((x0.shape[0], y0.shape[0]), order='F')
        for j in range(y0.shape[0]):
            for i in range(x0.shape[0]):
                out[i, j] = splin2_full_precomp_cwrapper(self.m, self.n, &self.x[0], &self.y[0],
                                                         &self.coeff[0,0,0,0], x0[i], y0[j])

        return out
        
