from __future__ import division

import numpy as np
import oomatrix as om
from oomatrix import Matrix
from oomatrix.implcore import MatrixImpl, computation, FLOP
from oomatrix.impl.dense import Strided
from oomatrix.selection import Selection
from oomatrix.implcore import computation
from oomatrix.impl.dense import Strided
from oomatrix.impl.diagonal import Diagonal

from .mmajor import lm_count, lm_to_idx, scatter_l_to_lm
from . import sharp, mmajor, healpix_data

#
# Spherical harmonics
#
def diagonal_matrix_by_l(array, lmin=0, lmax=None, name=None):
    """
    Arguments
    ---------
    
    array : array
        Should be a 1D array with one element per l. These elements will
        be copied

    Result
    ------
    
    A diagonal Matrix.
    
    """
    if lmax is None:
        lmax = array.shape[0] - 1
    if array.shape[0] != lmax + 1 - lmin:
        raise ValueError('array length does not match lmin/lmax')
    diagonal = mmajor.scatter_l_to_lm(array, lmin=lmin)
    return om.diagonal_matrix(diagonal, name=name)

class LSelectionMMajor(Selection):
    def __init__(self, rows_lrange, cols_lrange,
                 nrows=None, ncols=None, rows_offset=0, cols_offset=0):
        # if nrows/ncols is None, it is assumed the whole input/output vector
        # is one block; otherwise, the sh-coefs are embedded in nrows/ncols
        expected_nrows = lm_count(rows_lrange[0], rows_lrange[1] - 1)
        expected_ncols = lm_count(cols_lrange[0], cols_lrange[1] - 1)
        if nrows is None:
            nrows = expected_nrows
        if ncols is None:
            ncols = expected_ncols
        self.nrows = nrows
        self.ncols = ncols
        self.rows_offset = rows_offset
        self.cols_offset = cols_offset
        self.rows_lrange = rows_lrange
        self.cols_lrange = cols_lrange
        self.dtype = None

    def get_element(self, i, j):
        return 0

    def select_rows(self, array, out):
        lstart_in, lstop_in = self.cols_lrange
        lstart_out, lstop_out = self.rows_lrange
        for abs_m in range(0, min(lstop_in, lstop_out) + 1):
            lstart = max(abs_m, lstart_in, lstart_out)
            lstop = min(lstop_in, lstop_out)
            input_idx = lm_to_idx(lstart_in, lstop_in - 1, 0, lstart, abs_m)
            input_idx += self.cols_offset
            output_idx = lm_to_idx(lstart_out, lstop_out - 1, 0, lstart, abs_m)
            output_idx += self.rows_offset
            n = lstop - lstart
            if abs_m > 0:
                n *= 2 # include negative m too
            out[output_idx:output_idx + n, ...] = (
                array[input_idx:input_idx + n, ...])

    def transpose(self):
        return LSelectionMMajor(self.cols_lrange, self.rows_lrange,
                                self.ncols, self.nrows,
                                self.cols_offset, self.rows_offset)

    def __eq__(self, other):
        if not isinstance(other, LSelectionMMajor):
            return False
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self == other

def lselection_matrix(rows_lrange, cols_lrange, name=None):
    return om.Matrix(LSelectionMMajor(rows_lrange, cols_lrange), name=name)        

@computation(LSelectionMMajor.h, LSelectionMMajor, cost=0)
def transpose_l_selection(P):
    return P.transpose()


for kind, order in [(Strided, 'C')]:#, (ColumnMajor, 'F')]:
    @computation(LSelectionMMajor * kind, kind, cost=0)
    def select_rows(self, dense_matrix):
        array = dense_matrix.array
        out = np.zeros((self.nrows,) + array.shape[1:], dtype=array.dtype,
                       order=order)
        self.select_rows(array, out)
        return kind(out)


    @computation(kind * LSelectionMMajor, kind, cost=0)
    def select_columns(dense_matrix, self):
        # TODO: If we supported .t, this could be automatic...
        array = dense_matrix.array
        out = np.zeros(array.shape[:-1] + (self.ncols,), dtype=array.dtype,
                       order=order)
        self.transpose().select_rows(array.T, out.T)
        return kind(out)

def _select_diagonal(P1, D, P2):
    if P1 != P2:
        return NotImplemented
    out = np.zeros(P1.nrows, dtype=D.dtype)
    P1.select_rows(D.array, out)
    return Diagonal(out)

@computation(LSelectionMMajor * Diagonal * LSelectionMMajor.h, Diagonal, cost=0)
def select_diagonal(P1, D, P2):
    return _select_diagonal(P1, D, P2)

@computation(LSelectionMMajor.h * Diagonal * LSelectionMMajor, Diagonal, cost=0)
def transpose_select_diagonal(P1, D, P2):
    # TODO support .t of D to make this automatic
    # TODO not able to call select_diagonal after it is decorated
    return _select_diagonal(P2.transpose(), D, P1.transpose())



#
# SHTs
#

class SphericalHarmonicSynthesis(MatrixImpl):
    def __init__(self, plan):
        if not isinstance(plan, sharp.BaseRealMmajorPlan):
            raise TypeError()

        self.plan = plan
        self.npix = plan.npix_global
        self.nsh = mmajor.lm_count(0, plan.lmax)
        self.dtype = np.float64
        self.nrows = self.npix
        self.ncols = self.nsh

class SphericalHarmonicAnalysis(MatrixImpl):
    def __init__(self, plan):
        if not isinstance(plan, sharp.BaseRealMmajorPlan):
            raise TypeError()
        
        self.plan = plan
        self.npix = plan.npix_global
        self.nsh = mmajor.lm_count(0, plan.lmax)
        self.dtype = np.float64
        self.nrows = self.nsh
        self.ncols = self.npix


def sht_cost(sht_node, array):
    npix = (sht_node.ncols
            if sht_node.kind is SphericalHarmonicAnalysis else
            sht_node.nrows)
    nlm = (sht_node.ncols
           if sht_node.kind is SphericalHarmonicAnalysis else
           sht_node.nrows)
    lmax = np.sqrt(nlm)
    nside = np.sqrt(npix) / 12
    nmaps = array.ncols
    fft_cost = nmaps * (4 * nside - 1) * (4 * nside * np.log(4 * nside))
    legendre_cost = (2 * nside * lmax / 2) * (5 + 2 * nmaps)
    return (fft_cost + legendre_cost) * FLOP

@computation(SphericalHarmonicSynthesis * Strided, Strided, cost=sht_cost)
def synthesis(self, other):
    nmaps = other.array.shape[1]
    map = np.empty((self.npix, nmaps), order='F')
    for j in range(nmaps):
        self.plan.synthesis(other.array[:, j].copy(), map[:, j])
    return Strided(map)

@computation(SphericalHarmonicAnalysis.h * Strided, Strided, cost=sht_cost)
def adjoint_analysis(self, other):
    nmaps = other.array.shape[1]
    map = np.empty((self.npix, nmaps), order='F')
    for j in range(nmaps):
        self.plan.adjoint_analysis(other.array[:, j].copy(), map[:, j])
    return Strided(map)

@computation(SphericalHarmonicAnalysis * Strided, Strided, cost=sht_cost)
def analysis(self, other):
    nmaps = other.array.shape[1]
    alm = np.empty((self.nsh, nmaps), order='F')
    for j in range(nmaps):
        self.plan.analysis(other.array[:, j].copy(), alm[:, j])
    return Strided(alm)

@computation(SphericalHarmonicSynthesis.h * Strided, Strided, cost=sht_cost)
def adjoint_synthesis(self, other):
    nmaps = other.array.shape[1]
    alm = np.empty((self.nsh, nmaps), order='F')
    for j in range(nmaps):
        self.plan.adjoint_synthesis(other.array[:, j].copy(), alm[:, j])
    return Strided(alm)


#
# Convenience factories
#
def sh_analysis_matrix(nside, lmax, name='YhW'):
    weights = healpix_data.get_ring_weights_T(nside)
    plan = sharp.RealMmajorHealpixPlan(nside, lmax, weights=weights)
    return om.Matrix(SphericalHarmonicAnalysis(plan), name)

def sh_synthesis_matrix(nside, lmax, name='Y'):
    plan = sharp.RealMmajorHealpixPlan(nside, lmax)
    return om.Matrix(SphericalHarmonicSynthesis(plan), name)
