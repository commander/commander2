from __future__ import division
import numpy as np
import commander as cm
from matplotlib import pyplot as plt

from numpy.testing import assert_almost_equal

from nose import SkipTest

try:
    import oomatrix as om
    from oomatrix import compute_array, Matrix
    has_oomatrix = True
except ImportError:
    has_oomatrix = False
#
# These tests mostly checks that ring weight conventions are as we suppose
# them to be.
#

def test_monopole_synthesis():
    if not has_oomatrix:
        raise SkipTest()
    from .. import matrices, mmajor

    lmax = 32
    nside = 16
    nsh = (lmax + 1)**2
    
    z = np.zeros(nsh)
    z[0] = np.sqrt(4 * np.pi)
    Y = matrices.sh_synthesis_matrix(nside, lmax)
    x = om.compute_array(Y * z[:, None])[:, 0]
    assert_almost_equal(x, 1)

def test_sht_matrices():
    if not has_oomatrix:
        raise SkipTest()
    from .. import matrices, mmajor
    nside = 16
    npix = 12 * nside**2
    Y = matrices.sh_synthesis_matrix(nside, 2 * nside)
    YhW = matrices.sh_analysis_matrix(nside, 2 * nside)

    I = om.compute_array(YhW * Y * np.eye(Y.ncols))
    i = np.arange(I.shape[0])
    assert np.max(np.abs(I[i, i] - 1)) < 0.02
    I[i, i] = 0
    assert np.max(np.abs(I)) < 0.02

    A = om.compute_array(Y.h * Y * np.eye(Y.ncols))
    i = np.arange(A.shape[0])
    a = npix / 4 / np.pi
    assert np.max(np.abs(A[i, i] - a)) / a < 0.02
    A[i, i] = 0
    assert np.max(np.abs(A)) / a < 0.02

    B = om.compute_array(YhW * YhW.h * np.eye(YhW.nrows))
    b = 4 * np.pi / npix
    assert np.max(np.abs(B[i, i] - b)) / b < 0.03
    B[i, i] = 0
    assert np.max(np.abs(B)) / b < 0.03

#
# l-selection
#
def get_vec(lrange, n=1, order='C'):
    if not has_oomatrix:
        raise SkipTest()
    from .. import matrices, mmajor
    u = mmajor.scatter_l_to_lm(np.arange(lrange[0], lrange[1]).astype(np.double), lmin=lrange[0])
    u = np.repeat(u[:, None], n, axis=1)
    u = u.copy('C')
    return u

def get_scattered_vec(vector_lrange, data_lrange, n=1, order='C'):
    if not has_oomatrix:
        raise SkipTest()
    v = get_vec(vector_lrange, n, order)
    lmin, lmax = data_lrange
    v[~((lmin <= v) & (v < lmax))] = 0
    return v

def embed(v, start, n):
    if not has_oomatrix:
        raise SkipTest()
    from .. import matrices, mmajor
    out = np.zeros((n,) + v.shape[1:], v.dtype)
    assert start + v.shape[0] <= n
    out[start:start + v.shape[0], ...] = v
    return out

def test_select_rows():
    if not has_oomatrix:
        raise SkipTest()
    from .. import matrices, mmajor
    # Probe select_rows in detail
    def doit(rows_lrange, cols_lrange,
             nrows=None, ncols=None,
             rows_offset=0, cols_offset=0):
        n = 4

        u_arr = get_vec(cols_lrange, n)
        if ncols is not None:
            u_arr = embed(u_arr, cols_offset, ncols)
        v0 = get_scattered_vec(rows_lrange, cols_lrange, n)
        if nrows is not None:
            v0 = embed(v0, rows_offset, nrows)
        P = om.Matrix(matrices.LSelectionMMajor(rows_lrange, cols_lrange,
                                                        nrows, ncols,
                                                        rows_offset, cols_offset))
        v_arr = compute_array(P * u_arr)
        assert np.all(v_arr == v0)

    yield doit, (3, 7), (2, 8) # rows subset of cols
    yield doit, (3, 6), (0, 5) # overlap
    yield doit, (3, 7), (5, 10) # other overlap
    yield doit, (2, 8), (3, 7) # cols subset of rows
    yield doit, (3, 5), (3, 5) # I
    # use nrows/rows_offset etc., i.e. the sh-coefs are part of a block vector
    yield doit, (3, 7), (0, 5), 150, 200, 50, 60
    yield doit, (3, 5), (3, 5), 150, 200, 50, 60

def test_select_others():
    if not has_oomatrix:
        raise SkipTest()
    from .. import matrices, mmajor
    # probe column selection, transpose, etc.
    cols_lrange = (0, 11)
    rows_lrange = (3, 6)
    rows_start, nrows = 40, 400
    cols_start, ncols = 23, 300
    n = 3
    P = om.Matrix(matrices.LSelectionMMajor(rows_lrange, cols_lrange,
                                            nrows, ncols,
                                            rows_start, cols_start))
    v = get_vec(rows_lrange, n)
    u = get_scattered_vec(cols_lrange, rows_lrange, n)
    v = embed(v, rows_start, nrows)
    u = embed(u, cols_start, ncols)
    assert np.all(u.T == compute_array(Matrix(v.T.copy('C')) * P))
    assert np.all(u.T == compute_array(Matrix(v.T.copy('F')) * P))
    assert np.all(u == compute_array(P.h * v))
    assert np.all(u == compute_array(P.h * v.copy('F')))


def test_select_diagonal():
    if not has_oomatrix:
        raise SkipTest()
    from .. import matrices, mmajor
    raise SkipTest("This feature doesn't work until oomatrix is fixed a bit")
 
    def doit(rows_lrange, cols_lrange):
        from oomatrix.impl.diagonal import Diagonal
        P = Matrix(matrices.LSelectionMMajor(rows_lrange, cols_lrange))
        # P * D * P.h
        u = get_vec(cols_lrange)
        D = Matrix(u[:, 0], diagonal=True)
        v0 = get_scattered_vec(rows_lrange, cols_lrange)
        result = compute(P * D * P.h)
#        1/0 # problem is that oomatrix three-term @computation is disabled
        print result.get_kind()
        assert result.get_kind() is Diagonal
        assert np.all(result.diagonal() == v0[:, 0])
        # P.h * D * P
        u = get_vec(rows_lrange)
        D = Matrix(u[:, 0], diagonal=True)
        v0 = get_scattered_vec(cols_lrange, rows_lrange)
        result = compute(P.h * D * P)
        assert result.get_kind() is Diagonal
        assert np.all(result.diagonal() == v0[:, 0])

    yield doit, (3, 7), (2, 8) # rows subset of cols
    yield doit, (3, 6), (0, 5) # overlap
    yield doit, (3, 7), (5, 10) # other overlap
    yield doit, (2, 8), (3, 7) # cols subset of rows
    yield doit, (3, 5), (3, 5) # I
