from __future__ import division

import os
from functools import partial

from nose import SkipTest

from ..._private.mpinoseutils import *

import numpy as np
from numpy.testing import assert_almost_equal
from numpy.linalg import norm
from scipy.special.orthogonal import p_roots

from matplotlib import pyplot as plt

from ..sharp import *
from ..sharp import _mirror_weights
from .. import sharp
from ..mmajor import lm_to_idx, lm_count
from .. import healpix
from ..healpix_data import get_ring_weights_T

lmax = 32
lmax_lo = 13
nside = 16

nsh = (lmax + 1)**2
nsh_lo = (lmax_lo + 1)**2
npix = 12 * nside**2

#
# HEALPix grid
#

def test_mirror_weights():
    nside = 4
    def doit(ring_start, ring_stop):
        weights = np.asarray([0, 1, 2, 3, 4, 5, 6, 7], dtype=np.double)
        nrings = healpix.get_irings(nside, ring_start, ring_stop).shape[0]
        return _mirror_weights(weights, nside, nrings, ring_start, ring_stop)

    assert np.all(doit(0, 8) == [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0])
    assert np.all(doit(3, 4) == [3, 3])
    assert np.all(doit(2, 4) == [2, 3, 3, 2])
    assert np.all(doit(6, 8) == [6, 7, 6])

def test_monopole_synthesis():
    z = np.zeros(nsh)
    z[0] = np.sqrt(4 * np.pi)
    x = sh_synthesis(nside, z)
    assert_almost_equal(x, 1)

def test_monopole_analysis():
    lmax = 64
    nside = 32
    
    x = np.ones(12 * nside**2)
    z = sh_analysis(lmax, x)

    assert_almost_equal(z[0], np.sqrt(4 * np.pi))
    assert np.all(np.abs(z[1:]) < 1e-3) # HEALPix "analysis"

def test_dipole_synthesis():
    # This test is important to make sure we scale with the
    # right factors for m!=0
    def hammer_unit(l, m):
        z = np.zeros(nsh)
        i = lm_to_idx(0, lmax, 0, l, m)
        z[i] = 1
        return sh_synthesis(nside, z)
    
    theta, phi = healpix.pix2ang_ring(nside, np.arange(npix, dtype=np.intc)).T

    # test proper scaling for m != 0 by looking at Y_{1,1}
    Real_Y1p1_hat = hammer_unit(1, 1)
    Real_Y1p1 = (np.sqrt(3. / 2. / np.pi) / 2 *
                 (-np.sin(theta)) *
                 np.sqrt(2) * np.cos(phi))
    assert_almost_equal(Real_Y1p1_hat, Real_Y1p1)
 
    # look at Y_{1,-1}
    Real_Y1m1_hat = hammer_unit(1, -1)
    Real_Y1m1 = (np.sqrt(3. / 2. / np.pi) / 2 *
                 np.sin(theta) *
                 np.sqrt(2) * np.sin(phi))

    assert_almost_equal(Real_Y1m1_hat, Real_Y1m1)

def test_roundtrip():
    # By picking a lower lmax we can make the round-trip sort of work
    # even with the HEALPix grid...
    rng = np.random.RandomState(33)
    x0 = rng.normal(size=nsh_lo)
    z1 = sh_synthesis(nside, x0)
    x2 = sh_analysis(lmax_lo, z1)
    assert_almost_equal(x2, x0, decimal=3)

def test_weight_treatment():
    nside = 4
    lmax = 8
    nsh = (lmax + 1)**2
    npix = 12 * nside**2
    
    def hammer(func, n, m):
        x = np.zeros(m)
        out = np.zeros((n, m))
        for i in range(m):
            x[i] = 1
            out[:, i] = func(x)
            x[i] = 0
        return out

    weights = get_ring_weights_T(nside)
    #weights[:] = 1
    plan = sharp.RealMmajorHealpixPlan(nside, lmax, weights)

    # With weights
    WY = hammer(plan.adjoint_analysis, npix, nsh)
    YtW = hammer(plan.analysis, nsh, npix)
    assert np.max(np.abs(WY.T - YtW)) < 1e-15

    # Without weights
    Y = hammer(plan.synthesis, npix, nsh)
    Yt = hammer(plan.adjoint_synthesis, nsh, npix)
    assert np.max(np.abs(Yt.T - Y)) < 1e-15

#@mpitest(2)
def test_mpi_sht_synthesis():
    raise SkipTest("A tiny bit out of date")
    nside = 32
    lmax = 32
    npix = 12 * nside * nside
    rng = np.random.RandomState(comm.Get_rank())

    if comm.Get_rank() == 0:
        mmin, mmax = 0, lmax // 2
        ring_start, ring_stop = 0, nside
    else:
        mmin, mmax = lmax // 2 + 1, lmax
        ring_start, ring_stop = nside, 2 * nside

    x0 = rng.normal(size=lm_count(0, lmax, mmin, mmax))
    z1 = real_mmajor_alm2map_mpi(comm, nside, lmax, mmin, mmax, ring_start, ring_stop, x0)

    x0_full = np.hstack(comm.allgather(x0))
    
    z1_nompi = real_mmajor_alm2map(nside, lmax, x0_full)
    z1_full = healpix.allgather_map(comm, nside, ring_start, ring_stop, z1)
    assert_almost_equal(z1_nompi, z1_full)

#@mpitest(2)
def test_mpi_sht_analysis():
    raise SkipTest("A tiny bit out of date")
    nside = 32
    lmax = 32
    npix = 12 * nside * nside
    rng = np.random.RandomState(comm.Get_rank())
    weights = get_ring_weights_T(nside)

    if comm.Get_rank() == 0:
        mmin, mmax = 0, lmax // 2
        ring_start, ring_stop = 0, nside
    else:
        mmin, mmax = lmax // 2 + 1, lmax
        ring_start, ring_stop = nside, 2 * nside

    z0 = rng.normal(size=healpix.get_npix(nside, ring_start, ring_stop))

    x1_local = real_mmajor_map2alm_mpi(comm, nside, lmax, mmin, mmax, ring_start, ring_stop, z0,
                                       weights)
    x1_full = np.hstack(comm.allgather(x1_local))
    
    z0_full = healpix.allgather_map(comm, nside, ring_start, ring_stop, z0)
    x1_nompi = real_mmajor_map2alm(nside, lmax, z0_full, weights)

    assert_almost_equal(x1_nompi, x1_full)

#
# Gauss-Legendre grid
#

def test_gaussleg_grid():
    for lmax_grid, lmax in [(10, 10), (11, 10), (15, 10)]:
        plan = sharp.RealMmajorGaussPlan(lmax_grid, lmax)
        rng = np.random.RandomState(34)
        alm0 = rng.normal(size=(lmax + 1)**2).astype(np.double)
        alm1 = plan.analysis(plan.synthesis(alm0))
        # wohoo, we can make a round-trip
        assert np.max(np.abs(alm0 - alm1) / np.abs(alm0)) < 1.1e-11
