from .. import sympix
import numpy as np

def test_basic():
    k = 5
    nrings_min = 200
    g = sympix.make_sympix_grid(nrings_min, k)
    if 1:
        # debug plot showing that string of numbers below make sense
        sympix.plot_sympix_grid_efficiency(nrings_min, k, g)
        from matplotlib.pyplot import show
        show()
    assert np.all(g.ring_lengths == [10, 20, 24, 30, 36, 36, 48, 48, 60, 60, 75, 75,
                                     75, 75, 75, 90, 90, 90, 90, 90, 90])

def test_neighbours():
    ring_lengths = [8, 8, 8 * 5 // 4, 8 * 5 // 4]
    indices, indptr = sympix.sympix_csc_neighbours(ring_lengths)
    x = np.ones(indices.shape[0])#np.arange(indices.shape[0], dtype=np.float) % 3 + 1
    if 1:
        npix = 2 * np.sum(ring_lengths)
        from matplotlib.pyplot import matshow, show
        from scipy.sparse import csc_matrix
        print indices, indptr
        print npix
        M = csc_matrix((x, indices, indptr), shape=(npix, npix)).toarray()
        matshow(M)
        show()
    
def test_plot():
    nrings_min = 10
    grid = sympix.SymPixGrid([8, 10, 15, 20])
    print grid.nrings_half, grid.ring_lengths
    map = np.zeros(grid.npix)
    for i in range(grid.nrings_half):
        J = grid.ring_lengths[i]
        map[grid.offsets[i]:grid.offsets[i] + J] = np.arange(J)
        map[grid.offsets[i] + J:grid.offsets[i] + 2 * J] = np.arange(J)
    image = np.zeros((100, 100))
    sympix.sympix_plot(grid, map, image)
    
    if 1:
        from matplotlib.pyplot import matshow, show
        matshow(image)
        show()
