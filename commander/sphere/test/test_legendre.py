import numpy as np

from numpy.testing import assert_almost_equal
from nose.tools import eq_, ok_

from .. import legendre, beams
from scipy.special import sph_harm, p_roots

def test_norm_Plm():
    def test(theta, m, lmax):
        Plm = legendre.compute_normalized_associated_legendre(m, theta, lmax)
        Plm_p = sph_harm(m, np.arange(m, lmax + 1), 0, theta)[None, :]
        if not np.allclose(Plm_p, Plm):
            print Plm_p
            print Plm
        return ok_, np.allclose(Plm_p, Plm)

    yield test(np.pi/2, 0, 10)
    yield test(np.pi/4, 0, 10)
    yield test(3 * np.pi/4, 0, 10)
    yield test(np.pi/4, 1, 4)
    yield test(np.pi/4, 2, 4)
    yield test(np.pi/4, 50, 50)
    yield test(np.pi/2, 49, 50)

    yield test(0.001, 250, 300) # is close to 0

    
def test_legendre_roots():
    """
    Test the Legendre root-finding algorithm from libsharp by comparing it with 
    the SciPy version.
    """
    def test(order):
        xs, ws = p_roots(order) # from SciPy
        xc, wc = legendre.legendre_roots(order) # from Commander (libsharp)
        if not np.allclose(xs, xc):
            print xs
            print xc
        if not np.allclose(ws, wc):
            print ws
            print wc
        return ok_, (np.allclose(xs, xc) and np.allclose(ws, wc))
    
    yield test(32)
    yield test(128)

def test_legendre_transform():
    lmax = 20
    bl = beams.gaussian_beam_by_l(lmax, '10 deg')
    l = np.arange(lmax + 1)
    bl *= (2 * l + 1)


    theta = np.linspace(0, np.pi, 19, endpoint=True)
    x = np.cos(theta)

    P = legendre.compute_normalized_associated_legendre(0, theta, lmax)
    P *= np.sqrt(4 * np.pi) / np.sqrt(2 * l + 1)

    y0 = np.dot(P, bl)
    y = legendre.legendre_transform(x, bl)
    assert_almost_equal(y0, y)

    y32 = legendre.legendre_transform(x.astype(np.float32), bl)
    assert np.max(np.abs(y32 - y) / np.abs(y32)) < 1e-4
    
    
    

"""
def test_legendre_coeffs():
    
    #Test that the Legendre transform of a real-space Gaussian beam matches 
    #the (approximate) analytic solution.
    # FIXME: The analytic expression is not exact, so there is bound to be a slight difference?
    
    # Define symmetric Gaussian beams in pixel space and harmonic space
    def b_theta(th, sigma):
        return np.exp(-0.5 * (th/sigma)**2.) / (2. * np.pi * sigma**2.)
    def b_l(l, sigma):
        return np.exp(-0.5*l*(l+1.) * sigma**2.) / (4. * np.pi)
    
    def test(lmax, sigma):
        l = np.arange(lmax + 1)
        
        # Get zeros and weights of the Legendre polynomials
        # (Zeros are symmetric about x=0, so only keep the nonzero ones)
        xi, wi = legendre.legendre_roots(2 * lmax)
        xi = xi[lmax:]; wi = 2.*wi[lmax:]
        
        # Get Legendre expansion coeffs. by Gauss-Legendre quadrature integration
        leg = legendre.compute_normalized_associated_legendre(0, np.arccos(xi), lmax)
        leg *= np.sqrt(4 * np.pi) / np.sqrt(2 * l + 1)
        f = np.atleast_2d( wi * b_theta(np.arccos(xi), sigma) ).T * leg
        bl = 0.25 * np.sum(f, axis=0) # Factor of 1/4 to make normalisation the same
        
        # Get analytic expression for Legendre expansion coeffs.
        bl_ana = b_l(l, sigma)
        
        # Test to make sure results are close to each other
        if not np.allclose(bl, bl_ana):
            print bl
            print bl_ana
        return ok_, np.allclose(bl, bl_ana)
    
    print "TESTING"
    yield test(32, 0.1)
    yield test(64, 0.01)
    yield test(512, 0.1)
    yield test(512, 0.001)
    
"""
