import numpy as np
from pprint import pprint

from .. import mmajor

def test_lm_count():
    for lmin, lmax in [(0, 3), (1, 3), (0, 0), (100, 101), (1, 100)]:
        assert mmajor.lm_count(lmin, lmax, 0, lmax) == (lmax + 1)**2 - lmin**2

def test_mmajor_ordering_scheme():
    # Place coefficients according to lm_to_idx and look at it.
    # Then the tests below make sure round-trips work
    def doit(lmin, lmax, mmin, mmax):
        arr = [None] * mmajor.lm_count(lmin, lmax, mmin, mmax)
        for l in range(lmin, lmax + 1):
            for abs_m in range(mmin, mmax + 1):
                if abs_m > l:
                    continue
                for m in ([-abs_m, abs_m] if abs_m > 0 else [abs_m]):
                    assert abs(m) <= l
                    # Map to idx and check round-trip
                    i = mmajor.lm_to_idx(lmin, lmax, mmin, l, m)
                    assert 0 <= i < len(arr)
                    i0=mmajor.idx_to_lm(lmin, lmax, mmin, i)
                    assert mmajor.idx_to_lm(lmin, lmax, mmin, i) == (l, m)
                    assert arr[i] is None
                    arr[i] = (l, m)
        # All indices should be used
        assert not any(x is None for x in arr)
        
        return arr

    # mmin=0, mmax=lmax
    assert doit(0, 3, 0, 3) == [(0, 0),          (1, 0),          (2, 0),          (3, 0),
                                (1, 1), (1, -1), (2, 1), (2, -1), (3, 1), (3, -1),
                                (2, 2), (2, -2), (3, 2), (3, -2),
                                (3, 3), (3, -3)]

    assert doit(1, 3, 0, 3) == [                 (1, 0),          (2, 0),          (3, 0),
                                (1, 1), (1, -1), (2, 1), (2, -1), (3, 1), (3, -1),
                                (2, 2), (2, -2), (3, 2), (3, -2),
                                (3, 3), (3, -3)]

    assert doit(0, 0, 0, 0) == [(0, 0)]

    # The following just runs through some more assertions
    doit(100, 101, 0, 101)
    doit(1, 100, 0, 100)
    doit(0, 100, 0, 100)

    # different mmin, mmax
    assert doit(0, 3, 1, 2) == [(1, 1), (1, -1), (2, 1), (2, -1), (3, 1), (3, -1),
                                (2, 2), (2, -2), (3, 2), (3, -2)]
    lm_all = doit(0, 100, 0, 100)
    splits = [0, 10, 12, 14, 25, 47, 80, 99, 100 + 1]
    lm_concat = []
    for mmin, mmin_next in zip(splits[:-1], splits[1:]):
        lm_concat.extend(doit(0, 100, mmin, mmin_next - 1))
    assert lm_all == lm_concat

    # lmin > mmin
    assert doit(3, 4, 0, 3) == [(3, 0),          (4, 0),
                                (3, 1), (3, -1), (4, 1), (4, -1),
                                (3, 2), (3, -2), (4, 2), (4, -2),
                                (3, 3), (3, -3), (4, 3), (4, -3)]

def test_mmajor_find_lmax():
    def doit(lmin, lmax):
        n = mmajor.lm_count(lmin, lmax)
        got = mmajor.find_lmax(lmin, n)
        assert lmax == got

    yield doit, 0, 0
    yield doit, 0, 4
    yield doit, 1, 5
    yield doit, 2, 5
    yield doit, 100, 101
    yield doit, 101, 101

def test_mmajor_scatter_l_to_lm():
    def get(lmin, lmax):
        ls = np.arange(lmin, lmax + 1)
        data = ls.astype(np.double)
        out = mmajor.scatter_l_to_lm(data, lmin=lmin)
        for l in range(lmin, lmax + 1):
            for m in range(-l, l + 1):
                i = mmajor.lm_to_idx(lmin, lmax, 0, l, m)
                assert out[i] == l
        return out

    assert np.all([0,1,2,3,  1,1,2,2,3,3,  2,2,3,3,  3,3] == get(0, 3))    
    assert np.all([2,3,  2,2,3,3,  2,2,3,3,  3,3] == get(2, 3))


def test_mmajor_power_spectrum():
    lmin, lmax = 0, 5
    Cl = np.arange(lmax + 1, dtype=np.double)
    x = mmajor.scatter_l_to_lm(Cl)
    sx = np.sqrt(x)
    out = mmajor.compute_power_spectrum(lmin, lmax, sx)
    assert np.max(np.abs(Cl - out)) < 1e-15
