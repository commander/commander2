import os
from nose.tools import eq_, ok_
import numpy as np

from ..healpix_data import get_healpix_data
    

def test_weight_ring():
    res = get_healpix_data()
    x = res.get_raw_ring_weights(8)
    eq_((3, 16), x.shape)
    ok_(np.abs(1.16567683090317 - x[0,0]) < 1e-10)
    y = res.get_raw_ring_weights(8)
    ok_(y is x)

    # FITS files don't always have the same format, so try for an nside
    # with different format
    eq_((3, 1024), res.get_raw_ring_weights(512).shape)

def test_pixel_window():
    res = get_healpix_data()
    t, p = res.get_pixel_window(8)
    eq_((33,), t.shape)
    eq_((33,), p.shape)
    eq_(np.float64, t.dtype)
    eq_(1.0, np.round(t[0], 4))
    t2, p2 = res.get_pixel_window(nside=8)
    ok_(t is t2 and p is p2)
    eq_((2049,), res.get_pixel_window(nside=512)[0].shape)
