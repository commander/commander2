import numpy as np
from numpy.testing import assert_almost_equal
from nose.tools import eq_, ok_

from .. import beams

"""
def test_arbitrary_beam_by_l():
    
    # Check that arbitrary_beam_by_l() calculates Legendre coefficients correctly.
    # FIXME: How accurate should be expect the analytic expression to be?
    
    def gaussian_beam_theta(th, sigma):
        return np.exp(-0.5 * (th/sigma)**2.) / (2. * np.pi * sigma**2.)
    def gaussian_beam_l(l, sigma):
        return (2.*l +1.) * np.exp(-0.5*l*(l+1.) * sigma**2.) / (4. * np.pi)
    def sigma_lmax_for_nside(nside):
        lmax = 6 * nside - 1
        fwhm = (2.*np.pi / (4. * nside))
        sigma = fwhm / np.sqrt(8 * np.log(2))
        return sigma, lmax
    
    def test(sigma, lmax):
        # Calculate Legendre coefficients numerically for Gaussian beam
        params = (sigma,)
        bl = beams.arbitrary_beam_by_l(gaussian_beam_theta, params, lmax)
        
        # Get analytic form for Legendre coefficients
        l = np.arange(lmax + 1)
        bl_ana = gaussian_beam_l(l, sigma)
        
        if not np.allclose(bl, bl_ana):
            print bl
            print bl_ana
        return ok_, np.allclose(bl, bl_ana)
    
    sigma, lmax = sigma_lmax_for_nside(64)
    yield test(sigma, lmax)
    
    sigma, lmax = sigma_lmax_for_nside(128)
    yield test(sigma, lmax)
    
    sigma, lmax = sigma_lmax_for_nside(512)
    yield test(sigma, lmax)
"""
    
