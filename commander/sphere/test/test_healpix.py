import numpy as np
from matplotlib import pyplot as plt
from numpy import pi

from nose.tools import eq_, ok_
from numpy.testing import assert_almost_equal

from .. import healpix

nside = 4


def test_ring_z():
    z = healpix.get_ring_z(nside)
    eq_(z.shape, (4 * nside - 1,))

    hvecs = healpix.pix2vec_ring(nside, np.arange(12 * nside**2, dtype=np.int32))
    hz = np.unique(hvecs[:, 2])[::-1] # unique sorts, so reverse order
    assert_almost_equal(z, hz)

    # Get slice
    z = healpix.get_ring_z(nside, np.arange(3, 6, dtype=np.int32))
    assert_almost_equal(z, hz[3:6])

    # Thetas
    thetas = healpix.get_ring_theta(nside, np.arange(3, 6, dtype=np.int32))
    assert_almost_equal(thetas, np.arccos(hz[3:6]))

def test_get_npix():
    nside = 4
    npix = 12 * nside**2
    assert healpix.get_npix(4) == npix
    assert healpix.get_npix(4, 0, 8) == npix
    lst = [healpix.get_npix(nside, 0, i) for i in range(2 * nside + 1)]
    assert lst == [0, 8, 24, 48, 80, 112, 144, 176, 192]
    for split in range(2 * nside + 1):
        assert npix == healpix.get_npix(nside, 0, split) + healpix.get_npix(nside, split, 2 * nside)

def test_get_ring_pixel_counts():
    ringlens = healpix.get_ring_pixel_counts(4)
    assert np.all(ringlens ==
                  [4, 8, 12, 16, 16, 16, 16, 16, 16, 16, 16, 16, 12, 8, 4])
    assert np.all(ringlens[0:2] ==
                  healpix.get_ring_pixel_counts(4, np.arange(2, dtype=np.int32)))
    assert np.all(ringlens[4:7] ==
                  healpix.get_ring_pixel_counts(4, np.arange(4, 7, dtype=np.int32)))

def test_get_irings():
    assert np.all(healpix.get_irings(4, 0, 2) == [0, 1, 13, 14])
    assert np.all(healpix.get_irings(4, 1, 3) == [1, 2, 12, 13])
    assert np.all(healpix.get_irings(4, 0, 8) == range(4 * 4 - 1))


def test_get_phi0():
    # Grap indices for first pixel on each ring
    counts = healpix.get_ring_pixel_counts(nside)
    p0_indices = np.r_[0, np.cumsum(counts)[:-1]].astype(np.int32)

    phi0_fid = healpix.pix2ang_ring(nside, p0_indices)[:, 1]
    phi0 = healpix.get_phi0(nside)
    assert_almost_equal(phi0_fid, phi0)
