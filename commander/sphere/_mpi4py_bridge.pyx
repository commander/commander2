"""
When using OpenMPI, we really want to avoid importing mpi4py.MPI
unless we're within an mpiexec; we don't want accidental
single-process MPI. However, to get the C MPI_Comm from mpi4py
we must cimport mpi4py.MPI.Comm in Cython, which will also import
mpi4py.MPI at run-time. Hence this module which is only imported
at such a time that one wants to use MPI.
"""

from cpython.pycapsule cimport PyCapsule_GetPointer

from mpi4py.MPI cimport Comm
from mpi4py.mpi_c cimport MPI_Comm

def mpi4py_Comm_to_handle(Comm mpi4py_comm, capsule_to_c_comm):
    cdef MPI_Comm *out = <MPI_Comm*>PyCapsule_GetPointer(capsule_to_c_comm,
                                                         "_mpi4py_bridge_MPI_Comm")
    out[0] = mpi4py_comm.ob_mpi
