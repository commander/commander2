from __future__ import division
import numpy as np

from .._support import units
from . import legendre
from .healpix import healpix_pixel_size
from .healpix_data import get_healpix_pixel_window

def fwhm_to_sigma(fwhm):
    fwhm = units.as_radians(fwhm)
    return fwhm / np.sqrt(8 * np.log(2))

def gaussian_beam_by_l(lmax, fwhm):
    """
    fwhm : float
        Full-width half-max of Gaussian beam, in radians
    """
    sigma = fwhm_to_sigma(fwhm)
    ls = np.arange(lmax + 1)
    return np.exp(-0.5 * ls * (ls+1) * sigma*sigma)

def gaussian_beam_normalization(fwhm, eps=1e-15):
    """
    Normalization prefactor for Gaussian beam in pixel domain
    """
    sigma = fwhm_to_sigma(fwhm)
    # Use lmax such that bl has fallen to less than eps.
    # We let l(l+1) ~= l^2.
    lmax = int(np.sqrt(-2 * np.log(eps) / sigma**2)) + 1
    l = np.arange(lmax + 1)
    bl = np.exp(-0.5 * l * (l+1) * sigma*sigma)
    assert bl[lmax] < eps
    return beam_normalization(bl)

def beam_normalization(bl):
    l = np.arange(bl.shape[0])
    return np.sum(bl * (2 * l + 1)) / (4 * np.pi)

def gaussian_beam_angular_cutoff(fwhm, eps):
    """
    How many radians do we have to move on the sphere before the beam
    falls below the given threshold `eps`, relative to the maximum value?
    """
    sigma = fwhm_to_sigma(fwhm)
    return np.sqrt(-2 * np.log(eps) * sigma**2)


def gaussian_pixel_beam_by_l(lmax, nside, w):
    """
    Gaussian beam in l-space, with width defined as a multiple 'w' of the pixel 
    size of a Healpix grid with resolution 'nside'.
    """
    fwhm = w * healpix_pixel_size(nside)
    return gaussian_beam_by_l(lmax, fwhm)


def mhwavelet_beam_by_angle(theta, fwhm, a=3., b=.75):
    """
    Modified Mexican Hat Wavelet beam in real space, defined as 
    :math:`B(\theta) = \exp(-\frac{1}{2}(\theta/\sigma)^2) 
    [3 + \frac{3}{4}(\theta/\sigma)^2]`.
    
    This beam is designed to be more localised than a Gaussian, and resembles 
    (but is not exactly the same as) a harmonic space beam 
    :math:`b_l \propto \exp(-\frac{1}{2}\ell^2 (\ell +1)^2 / \sigma^2)`
    """
    sigma = 2.*fwhm_to_sigma(fwhm)
    norm = (9/8)**2. * 2. * np.pi * sigma**2.
    return np.exp(-0.5*(theta/sigma)**2.) * (1 - b*(theta/sigma)**2.) / norm

def mhwavelet_beam_by_l(lmax, fwhm, a=3, b=.75):
    """
    Find the Legendre coefficients :math:`b_\ell` for a modified Mexican Hat 
    Wavelet beam, where :math:`b_\ell` is defined such that 
    :math:`B(\theta) = \sum_\ell (2\ell + 1) b_\ell P_\ell(\cos \theta)`.
    
    Arguments:
      lmax -- Max. l to calculate :math:`b_\ell` up to
      fwhm -- FWHM of MH Wavelet (in radians)
    
    Returns:
      b_l -- 1D array of coefficients, for the l-range [0, lmax].
    """
    params = (fwhm, a, b)
    bl = arbitrary_beam_by_l(mhwavelet_beam_by_angle, params, lmax)
    bl /= bl[0]
    return bl

def arbitrary_beam_by_l(beam_function, params, lmax):
    """
    Find the Legendre coefficients :math:`b_\ell` for an arbitrary (symmetric) 
    real-space beam function, such that 
    :math:`B(\theta) = \sum_\ell (2\ell + 1) b_\ell P_\ell(\cos \theta)`.
    
    Arguments:
      beam_function -- Python function which takes arguments fn(theta, params)
      params -- Tuple containing parameters for the beam function (e.g. FWHM)
      lmax -- Max. l to calculate :math:`b_\ell` up to
    
    Returns:
      b_l -- 1D array of coefficients, for the l-range [0, lmax].
    """
    order = lmax
    l = np.arange(lmax + 1)
    
    # Get zeros and weights of the Legendre polynomials
    # (Zeros are symmetric about x=0, so only keep the nonzero ones)
    xi, wi = legendre.legendre_roots(2*order)
    xi = xi[order:]; wi = 2.*wi[order:]
    
    # Get Legendre polynomials and normalise them
    leg = legendre.compute_normalized_associated_legendre(0, np.arccos(xi), lmax)
    leg *= np.sqrt(4 * np.pi) / np.sqrt(2 * l + 1)
    
    # Perform Gauss-Legendre sum
    f = np.atleast_2d( wi * beam_function(np.arccos(xi), *params) ).T * leg
    return 0.25 * np.sum(f, axis=0)
    
def beam_by_theta(bl, thetas):
    from commander.sphere.legendre import legendre_transform

    lmax = bl.shape[0] - 1
    l = np.arange(lmax + 1)
    scaled_bl = bl * (2 * l + 1)
    
    return legendre_transform(np.cos(thetas), scaled_bl) / (4. * np.pi)



def standard_needlet_by_l(B, lmax):
    """
    Returns the spherical harmonic profile of a standard needlet on the sphere,
    following the recipe of arXiv:1004.5576. Instead of providing j, you provide
    lmax, and a j is computed so that the function reaches 0 at lmax.
    """
    from scipy.integrate import quad
    
    def f(t):
        if -1 < t < 1:
            return np.exp(-1 / (1 - t*t))
        else:
            return 0

    phi_norm = 1 / quad(f, -1, 1)[0]
    
    def phi(u):
        return quad(f, -1, u)[0] * phi_norm

    def phi2(t, B):
        if t <= 1 / B:
            return 1
        elif t < 1:
            return phi(1 - (2 * B) / (B - 1) * (t - 1 / B))
        else:
            return 0

    def b(eta, B):
        if eta < 1 / B:
            return 0
        elif eta < B:
            b_sq = phi2(eta / B, B) - phi2(eta, B)
            if b_sq <= 0:
                return 0
            else:
                return np.sqrt(b_sq)
        else:
            return 0

    j = (np.log(lmax) - np.log(B)) / np.log(B)
    C = float(B)**j
    return np.asarray([b(l / C, B) for l in range(lmax + 1)])

def gaussianize_tail(bl, treshold):
    """
    Given a spherical-harmonic transfer function for a Gaussian-like instrumental
    beam, replace the tail (below treshold relative to peak) with an exact Gaussian.

    The right value for treshold depends on the truncation degree lmax, please
    do not use unsupervised.
    """

    def fn(thetas):
        # thetas are given in decreasing order...
        b = beam_by_theta(bl, thetas)
        b /= b.max()
        i = np.nonzero(b > treshold)[0][0]
        val = b[i]
        theta = thetas[i]
        sigma_sq = theta**2 / (-2 * np.log(val))
        b_gauss = np.exp(-0.5 * thetas**2 / sigma_sq)
        b[:i] = b_gauss[:i]
        return b

    lmax = bl.shape[0] - 1
    ql = arbitrary_beam_by_l(fn, (), lmax)
    ql /= ql[0]
    return ql


def fourth_order_beam(lmax, ltreshold, epstreshold=0.04):
    """
    Squared Gaussian beam in SH space.
    """
    rhs = 2 * np.log(epstreshold)
    sigma = -rhs / (ltreshold**2 * (ltreshold + 1)**2)
    l = np.arange(lmax + 1)
    cl = np.exp(-l**2 * (l + 1)**2 * sigma)
    return np.sqrt(cl)
