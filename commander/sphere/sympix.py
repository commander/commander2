from __future__ import division
import sympy
from fractions import Fraction
import numpy as np
from .legendre import legendre_roots

class SymPixGrid(object):
    def __init__(self, ring_lengths, thetas=None, weights=None):
        if thetas is None or weights is None:
            th, we = _theta_and_weights(2 * len(ring_lengths))
            if thetas is None:
                thetas = th
            if weights is None:
                weights = we
        self.thetas = thetas
        self.weights = weights
        self.nrings_half = len(ring_lengths)
        self.nrings = 2 * self.nrings_half
        self.ring_lengths = np.asarray(ring_lengths, dtype=np.int32)
        self.npix = 2 * np.sum(ring_lengths)
        self.offsets = 2 * np.concatenate([[0], np.cumsum(ring_lengths)])

#    def ang2pix(self, theta, phi):
#        if theta <= np.pi / 2:
#            south = False
#        else:
#            south = True
#            theta = np.pi - theta
#            phi = (phi + 2 * np.pi) % (2 * np.pi)
        


POSSIBLE_INCREMENTS = (Fraction(6, 5),
                       Fraction(5, 4),
                       Fraction(4, 3),
                       Fraction(2, 1),
                       Fraction(3, 1),
                       Fraction(1, 1))

def _theta_and_weights(nrings):
    x, weights = legendre_roots(nrings)
    thetas = np.arccos(x[::-1][:x.shape[0]//2])
    weights = weights[::weights.shape[0]//2]
    print thetas, weights
    return thetas, weights

def roundup(x, k):
    return x if x % k else x + k - x % k

def get_min_ring_length(theta, lmax, T=None):
    """
    Returns the minimal number of pixels on a ring on latitude theta
    to represent a signal of size lmax. theta can be an array.
    """
    if T is None:
        T = max(100, 0.01 * lmax)
    ct = np.abs(np.cos(theta))
    st = np.sin(theta)
    mmax = ct + np.sqrt(ct**2 + lmax**2 * st**2 + T)
    mmax = np.floor(mmax).astype(int) + 1
    return 2 * mmax

def find_optimal_ring_lengths(n_start, min_lengths, possible_increments):
    """
    Use dynamic programming to figure out the optimal ring lengths where the
    possible increments are given by `possible_increments` as . The cost
    is computed as the distance between min_lengths and the chosen lengths.
    """
    def cost_function(length, min_length):
        return (length - min_length)**2
    
    assert all(isinstance(f, Fraction) for f in possible_increments)
    nrings = len(min_lengths)
    G = [] # list of { ringlen: (cost, prev_ring_len) }, used for caching results
               # from previously computed rings
    G.append({n_start: (0, None)})
    for i in range(1, nrings):
        possibilities = {}
        for prev_len, (prev_cost, _) in G[i - 1].items():
            for inc in possible_increments:
                ringlen = prev_len * inc
                if ringlen > 3 * min_lengths[-1]:
                    # no point to continue along this path
                    continue
                if ringlen < min_lengths[i]:
                    # we don't accept lengths less than minimum length
                    continue
                if ringlen.denominator == 1:
                    ringlen = ringlen.numerator
                    cost = prev_cost + cost_function(ringlen, min_lengths[i])
                    if cost < possibilities.get(ringlen, (np.inf, None))[0]:
                        possibilities[ringlen] = (cost, prev_len)
        G.append(possibilities)

    # Find the end solution with the lowest cost
    result = np.zeros(nrings, dtype=np.int)
    solutions = [(cost, ring_len, prev_ring_len)
                 for ring_len, (cost, prev_ring_len) in G[-1].items()]
    cost, result[nrings - 1], prev_ring_len = sorted(solutions)[0]
    # Backtrack the solution to the starting ring.
    for i in range(nrings - 2, -1, -1):
        result[i] = prev_ring_len
        prev_ring_len = G[i][prev_ring_len][1]
    return cost, result

def make_sympix_grid(nrings_min, k):
    """
    Makes our special multi-grid grid that is optimized for precomputing rotationally
    invariant operators in pixel domain.

    nrings_min: minimal number of rings; another nrings >= nrings_min will be returned
    k: tilesize

    Returns:

    (nrings, thetas, weights, ntiles_per_ring)

    `thetas`, `weights`, `n_tiles_per_ring` are for upper half of the sphere only.
    `thetas` and `weights` correspond to a Gauss-Legendre grid with `nrings` rings.
    ``np.repeat(ntiles_per_ring, k)`` gives the number of elements per ring.
    """
    def is_acceptable_ring_length(n):
        """
        Determine whether `n` is on the form ``2^a * 3^b * 5^c * k``;
        any other prime factors would non-productively be a part of every ring
        length.
        """
        if n % k != 0:
            return False
        else:
            n //= k
            for factor in sympy.factorint(n).keys():
                if factor not in [2, 3, 5, k]:
                    return False
            else:
                return True

    # Compute for upper half only
    nrings = roundup(nrings_min, 2 * k)
    thetas, weights = _theta_and_weights(nrings)
    # ring_lengths_min is monotonically increasing, so we're bounded by every k-th theta.
    # ntiles_min_arr will contain a lower bound on number of tiles on a ring
    ntiles_min_arr = [roundup(n, k) // k for n in get_min_ring_length(thetas[k - 1::k], nrings - 1)]

    n_start = ntiles_min_arr[0]
    while not is_acceptable_ring_length(n_start):
        n_start += 1

    cost, tile_ring_lengths = find_optimal_ring_lengths(n_start, ntiles_min_arr, POSSIBLE_INCREMENTS)
    return SymPixGrid(tile_ring_lengths, thetas, weights)
    
def plot_sympix_grid_efficiency(nrings_min, k, grid):
    """
    Debug/diagnositc plot to verify a given grid found.
    """
    from matplotlib.pyplot import clf, gcf, draw
    # Get pure reduced legendre grid
    x0, w0 = legendre_roots(nrings_min)
    thetas0 = np.arccos(x0[::-1])
    lens0 = get_min_ring_length(thetas0, k * grid.nrings - 1)

    lens = np.repeat(grid.ring_lengths, k)
    lens = np.concatenate([lens, lens[::-1]])
    thetas = np.concatenate([grid.thetas, np.pi - grid.thetas[::-1]])
    lens *= k

    clf()
    fig = gcf()
    ax0 = fig.add_subplot(1,2,1)
    ax1 = fig.add_subplot(1,2,2)

    pix_dists0 = 2 * np.pi * np.sin(thetas0) / lens0
    pix_dists = 2 * np.pi * np.sin(thetas) / lens

    ax0.plot(thetas0, pix_dists0)
    ax0.plot(thetas, pix_dists)

    ax1.plot(thetas0, lens0)
    ax1.plot(thetas, lens)
    draw()


class CscMaker(object):
    def __init__(self, n, nnz):
        self.indices = np.zeros(nnz, dtype=np.int32)
        self.indptr = np.zeros(n + 1, dtype=np.int32)
        self.n = n
        self.colidx = -1 # should call start_column initially
        self.ptr = 0

    def start_column(self):
        self.colidx += 1
        self.indptr[self.colidx] = self.ptr

    def put(self, rowidx):
        assert rowidx < self.n
        self.indices[self.ptr] = rowidx
        self.ptr += 1

    def finish(self):
        self.start_column()
        self.indptr[self.colidx:] = self.ptr
        self.indices = self.indices[:self.ptr]

def sympix_csc_neighbours(ring_lengths):
    """
    Given a sympix grid, compute the lower half of a CSC neighbour.
    ring_lengths is the number of pixels per ring on the northern
    hemisphere.
    """
    # Keep in mind that we operate on ring-pairs.
    nrings_half = len(ring_lengths)
    npix = 2 * np.sum(ring_lengths)
    offsets = 2 * np.concatenate([[0], np.cumsum(ring_lengths)])
    m = CscMaker(npix, 5 * npix + ring_lengths[0]**2)

    # We index pixels by (i, j), with i being ring-pair and j
    # pixel-within-ring.
    
    # On the poles, couple every pixel to each other
    J = ring_lengths[0]
    for j in range(J):
        m.start_column()
        for kk in range(j, J):
            m.put(kk)
    for j in range(J):
        m.start_column()
        for kk in range(J + j, 2 * J):
            m.put(kk)

    #
    # Caps
    #

    # Interior, non-equatorial rings
    for i in range(1, nrings_half - 1):
        J, down_J = ring_lengths[i], ring_lengths[i + 1]
        # Select
        if J == down_J:
            # Simple regular grid
            for start in [offsets[i], offsets[i] + J]:
                # start is the first pixel on the ring.
                # The first pixel start has its left neighbour *after* itself in ordering.
                p = start
                m.start_column()
                m.put(p)
                m.put(p + 1)              # right
                m.put(p + J - 1)          # left (wrapped around)
                m.put(p + 2 * J)          # down
                m.put(p + 2 * J + 1)      # down to the right
                m.put(p + 3 * J - 1)      # down to the left (wrapped around)
                
                # Pixels on ring except last
                for j in range(1, J - 1):
                    p = start + j
                    m.start_column()
                    m.put(p)
                    m.put(p + 1)          # pixel to right
                    m.put(p + 2 * J - 1)  # pixel down to left
                    m.put(p + 2 * J)      # pixel down
                    m.put(p + 2 * J + 1)  # pixel down to right

                # Last pixel on ring
                p = start + J - 1
                m.start_column()
                m.put(p)
                m.put(start + 2 * J)     # down to the right
                m.put(p + 2 * J - 1)     # down to the left
                m.put(p + 2 * J)         # down
        else:
            increment = Fraction(down_J, J)
            if increment not in POSSIBLE_INCREMENTS:
                raise ValueError("Invalid ring sizes, increments with %s" % increment)
            this_period = increment.numerator
            down_period = increment.denominator

            for start in [offsets[i], offsets[i] + J]:
                down_start = start + 2 * J
                # First pixel on ring
                p = start
                m.start_column()
                m.put(p)
                m.put(p + 1)         # right
                m.put(p + J - 1)     # left (wrapped around)
                m.put(p + 2 * J)     # down
                m.put(p + 2 * J + 1) # down and right
                m.put(p + 2 * J + down_J + 1) # down and left (wrapped around)

                # pixels in interior
                for j in range(1, J - 1):
                    m.start_column()
                    iperiod = j // this_period
                    t = j % this_period # index within period
                    p = start + j
                    down_period_start = down_start + iperiod * down_period
                    if t == 0:
                        # first pixel in period
                        assert iperiod > 0
                        m.put(p)
                        m.put(p + 1) # right
                        m.put(down_period_start - 1) # down and left
                        m.put(down_period_start + 0) # down and left
                        m.put(down_period_start + 1) # down and right
                    elif t < this_period - 1:
                        # middle of period
                        m.put(p)
                        m.put(p + 1)                     # right
                        m.put(down_period_start + t + 1) # diagonally down-right
                        m.put(down_period_start + t)     # diagonally down-left
                    else:
                        # end of period
                        m.put(p)
                        m.put(p + 1)                               # right
                        m.put(down_period_start + down_period - 2) # down and left
                        m.put(down_period_start + down_period - 1) # down
                        m.put(down_period_start + down_period)     # down and right
                # last pixel on ring
                p = start + 2 * J - 1
                m.start_column()
                m.put(p)
                m.put(down_start)                  # down to the right
                m.put(down_start + down_J - 1 - 1) # down to the left
                m.put(down_start + down_J - 1)     # down


    #
    # Equator
    #
    # North equatorial ring; the south neighbour is the south equatorial ring of same length.
    # The south ring is MIRRORED (if north is seen as left-to-right, the order of the southern
    # rings are right-to-left, with stride=-1 to SHTs).
    J = ring_lengths[-1]
    # first 6-neighbour pixel with wrapped-around lefts
    start = offsets[-2]
    p = start
    m.start_column()
    m.put(p)
    m.put(p + 1)          # right
    m.put(p + J - 1)      # left (wrapped around)
    m.put(p + J)          # down and left (wrapped around to mirrored ring)
    m.put(p + 2 * J - 1)  # down (end of mirrored ring)
    m.put(p + 2 * J - 2)  # down and right (mirrored ring)
    
    for j in range(1, J - 1):
        m.start_column()
        p = start + j
        m.put(p)
        m.put(p + 1)                 # right
        m.put(start + 2 * J - 1 - j - 1) # down and right (mirrored ring)
        m.put(start + 2 * J - 1 - j - 0) # down (mirrored ring)
        m.put(start + 2 * J - 1 - j + 1) # down and left (mirrored ring)

    # last pixel north equatorial ring
    m.start_column()
    p = start + J - 1
    m.put(p)
    m.put(p + 1)             # down
    m.put(p + 2)             # down and left
    m.put(start + 2 * J - 1) # down and right

    # Last ring -- south equatorial ring. Comments are still as if
    # seen from north, i.e., left means "west"
    start = offsets[-2] + J
    # First pixel south equatorial ring
    m.start_column()
    p = start
    m.put(p)
    m.put(p + 1)             # left
    m.put(p + J - 1)         # right

    for j in range(1, J - 1):
        m.start_column()
        p = start + j
        m.put(p)
        m.put(p + 1)  # right

    m.start_column()
    m.put(start + J - 1)
    
    m.finish()
    return m.indices, m.indptr


def sympix_plot(grid, map, image):
    """
    Plots a map to a rectangle. The `image` 2D output array is assumed to
    represent [0, 2pi] x [0, pi/2], and then the angles are mapped to pixels,
    and the map plotted to those pixels. TODO: [0, pi]
    """
    ny, nx = image.shape
    assert ny % 2 == 0

    phis = np.linspace(1e-5, 2 * np.pi - 1e-5, image.shape[1])

    # Upper half
    thetas = np.linspace(1e-5, 0.5 * np.pi - 1e-5, image.shape[0] // 2)
    rings = np.digitize(thetas, grid.thetas) - 1

    rings = np.concatenate([2 * rings, 2 * rings[::-1] + 1])
    
    for i, iring in enumerate(rings):
        if iring < 0 or iring >= grid.nrings:
            image[i, :] = np.nan
        else:
            J = grid.ring_lengths[iring // 2]
            pixel_borders = np.linspace(0, 2 * np.pi, J, endpoint=False)
            js = np.digitize(phis, pixel_borders) - 1
            js[js >= J] = J - 1
            if iring % 2 == 0:
                ring_values = map[grid.offsets[iring // 2]:grid.offsets[iring // 2] + J]
            else:
                ring_values = map[grid.offsets[iring // 2] + J:grid.offsets[iring // 2] + 2 * J]
            image[i, :] = ring_values[js]

    return image
