import os
from textwrap import dedent
import contextlib
import functools
import inspect
import tempfile
import shutil

import numpy as np
from nose.tools import ok_, eq_, assert_raises
from nose import SkipTest
from matplotlib import pyplot as plt
from numpy.testing import assert_almost_equal

I = bool(int(os.environ.get('I', '0')))

_mypath = os.path.realpath(os.path.dirname(__file__))

def filename_of(basename):
    return os.path.join(_mypath, basename)

def assert_not_raises(func, *args):
    "Turn any exception into AssertionError"
    try:
        func(*args)
    except:
        raise AssertionError()

def ndrange(shape, dtype=np.double):
    return np.arange(np.prod(shape)).reshape(shape).astype(dtype)

def alleq_(a, b):
    assert np.all(a == b)

def plot_matrix(matrix):
    ax = plt.gca()
    ax.imshow(matrix, interpolation='nearest')

@contextlib.contextmanager
def temp_working_dir():
    tempdir = tempfile.mkdtemp()
    try:
        with working_directory(tempdir):
            yield tempdir
    finally:
        shutil.rmtree(tempdir)

@contextlib.contextmanager
def working_directory(path):
    old = os.getcwd()
    try:
        os.chdir(path)
        yield
    finally:
        os.chdir(old)

@contextlib.contextmanager
def assert_raises_msg(type, msg):
    try:
        yield
    except Exception, e:
        assert isinstance(e, type)
        assert str(e) == msg

def temp_working_dir_fixture(func):
    if inspect.isgeneratorfunction(func):
        @functools.wraps(func)
        def rep():
            with temp_working_dir() as d:
                for x in func(d):
                    yield x
    else:
        @functools.wraps(func)
        def rep():
            with temp_working_dir() as d:
                return func(d)
    return rep
