ctypedef Py_ssize_t index_t
ctypedef double real_t
ctypedef double complex complex_t

cdef inline index_t imax(index_t a, index_t b):
    return a if a > b else b
