from __future__ import division

import numpy as np
from struct import pack
import hashlib
import h5py
import os

from ..memoize import memoize_method, Hasher
from ..logger_utils import log_after_task, timed
from .._support import units
from ..sphere import nside_of, gaussian_beam_by_l, get_healpix_pixel_window
from .._support.units import bytes2human

from .resourceloaders import load_sh_beam, load_beam_transfer_from_fits, expand_path


class SkyObservation(object):
    """
    Describes a sky observation (e.g. data from a single frequency band, or a 
    single detector).
    
    This class is immutable. To change the specification of the sky 
    observation, use the clone_with() method to return a new instance with 
    updated properties.
    
    Construction is very cheap, and no disk access happens in the constructor. 
    Various data are only loaded when the corresponding load() method is called 
    for the first time.

    Parameters
    ----------
    
    nside : int
        Resolution of map in HEALPix pixels

    lmax : int
        The truncation lmax of the beam
    
    map : map-like
        Data map

    rms, variance : map-like
        Source of RMS and inverse-noise-maps

    masks : list of map-like
        A list of masks, which are combined ("and"-ed/multiplied)
        
    beam : beam-like
        Beam transfer function

    name, description : str, optional
        Optional metadata; name is used by repr

    unique_basenames : bool
        If set to True, the basenames (e.g,
        ``HFI_SkyMap_143_2048_R1.10_nominal.fits``) are considered
        global unique identifiers and can are used to make a digest as
        well as comparing observations for equality. If this is not set,
        making a digest involves loading and hashing the data.
    """
    _instrumental_digest = None # cache

    def __init__(self, nside, lmax,
                 map, beam,
                 rms=None,
                 variance=None,
                 ninv=None,
                 masks=(),
                 name=None,
                 description=None,
                 unique_basenames=False,
                 _cloning=False):
        
        # Don't initialise if returning a clone of the SkyObservation object
        if _cloning: return
        
        # Define basic properties
        self.nside = nside
        self.npix = 12 * nside**2
        self.lmax = lmax
        self.unique_basenames = unique_basenames
        self.name = name
        self.description = description
        
        # Caching preferences and cache variables
        self._cached = {'data': False, 'ninv': False, 'beam': False}
        self._cache_data = self._cache_ninv = self._cache_beam = None
        
        # Datamap definition
        self._map = parse_map('temp', nside, map)
        
        # Noise definition
        self._rms = self._variance = None
        if (rms is not None) == (variance is not None): # xor
            raise TypeError("Please provide either rms or variance")
        if rms is not None:
            self._rms = parse_map('rms', nside, rms)
        if variance is not None:
            if isinstance(variance, (int, str, float)):
                raise TypeError("Please provide constant noise as rms, not variance")
            self._variance = parse_map('var', nside, variance)
        if ninv is not None:
            if not isinstance(ninv, MapLoader):
                raise NotImplementedError()
            self._ninv = ninv
        else:
            self._ninv = None

        # Mask definition
        self._masks = [parse_map('mask', nside, mask) for mask in masks]

        # Beam definition
        self._beam = parse_beam(lmax, beam)
        

    def clone_with(self, **kw):
        """
        Create a copy of this SkyObservation with some of the data replaced. 
        Returns a new instance of SkyObservation.
        
        Use this method to 'modify' the data/information stored by the 
        SkyObservation.
        
        Parameters
        ----------
        
        **kwargs : {rms, variance, ninv, beam, masks}
            Keyword arguments, which match the arguments for the SkyObservation 
            class constructor.
        """
        other = SkyObservation(None, None, None, None, _cloning=True)
        other.__dict__ = dict(self.__dict__)
        nside = other.nside
        if 'map' in kw:
            other._map = parse_map('temp', nside, kw['map'])
            
        # Make derived maps/maps showing the same thing disappear
        if 'rms' in kw or 'variance' in kw:
            other._variance = None
            other._rms = None
            other._ninv = None

        if 'masks' in kw:
            other._ninv = None
        
        # Process keyword args
        if 'rms' in kw:
            other._rms = parse_map('rms', nside, kw['rms'])
            del kw['rms']
        if 'variance' in kw:
            other._variance = parse_map('var', nside, kw['variance'])
            del kw['variance']
        if 'masks' in kw:
            other._masks = [parse_map('mask', nside, mask) for mask in kw['masks']]
            del kw['masks']
        if 'ninv' in kw:
            if not isinstance(kw['ninv'], MapLoader):
                raise NotImplementedError()
            other._ninv = kw['ninv']
            del kw['ninv']
        if 'beam' in kw:
            other._beam = parse_beam(other.lmax, kw['beam'])
            del kw['beam']

        # Raise error if there are any unidentified keyword args
        if len(kw) > 0:
            raise TypeError('Unrecognized keyword argument(s): %s' \
                           % ','.join(kw.keys()))
        return other

    ############################################################################
    # Methods for loading main data/noise/beam information
    ############################################################################
    
    def load_data_map(self, readonly=True):
        """
        Load the data map for this observation.
        
        The data map will be loaded on the fly or retrieved from the cache, 
        depending on the caching settings for this SkyObservation.
        
        Parameters
        ----------
        
        readonly : bool, optional (default: True)
            Depends on caching behaviour. If True, and caching is enabled, the 
            cached beam (which is read only) is returned. If False, a new 
            (write-enabled) copy of the cached beam is returned instead. (The 
            latter uses more memory.)
            
            If caching is not enabled, this parameter has no effect; a new copy 
            is always loaded from file and returned directly.
        """
        if self._cached['data']:
            if self._cache_data is not None:
                return self._cache_data
            self._cache_data = self._map.load()
            self._cache_data.setflags(write=False)
            
            if readonly: return self._cache_data
            return self._cache_data.copy()
        else:
            return self._map.load()
    
    def load_ninv_map(self, readonly=True):
        """
        Load the inverse noise map for this observation.
        
        The noise map will be loaded on the fly or retrieved from the cache, 
        depending on the caching settings for this SkyObservation.
        
        N.B. The way that the noise properties were originally specified in the 
        constructor of the SkyObservation (e.g. rms, variance or ninv) does not 
        matter; this method will automatically perform any necessary 
        conversions and apply masks etc., before outputting the inverse noise 
        map.
        
        Parameters
        ----------
        
        readonly : bool, optional (default: True)
            Depends on caching behaviour. If True, and caching is enabled, the 
            cached beam (which is read only) is returned. If False, a new 
            (write-enabled) copy of the cached beam is returned instead. (The 
            latter uses more memory.)
            
            If caching is not enabled, this parameter has no effect; a new copy 
            is always loaded from file and returned directly.
        """
        # Return cached value if it exists
        if self._cached['ninv'] and self._cache_ninv is not None:
            if readonly: return self._cache_ninv
            return self._cache_ninv.copy()
        
        # Otherwise, load the Ninv data from the appropriate source
        if self._ninv is not None:
            ninv = self._ninv.load()
        else:
            mask = self._load_mask_map_without_nan_mask()
            var = self.load_variance_map()
            sel = np.isnan(var)
            mask[sel] = 0
            var[sel] = 1
            ninv = mask / var
        
        # Store if caching is turned on, or just return
        if self._cached['ninv']:
            self._cache_ninv = ninv
            self._cache_ninv.setflags(write=False)
            if readonly: return self._cache_ninv
            return self._cache_ninv.copy()
        else: return ninv
    
    def load_sh_beam(self, readonly=True):
        """
        Load Legendre representation of instrumental beam, B_l.
        
        The beam will be loaded on the fly or retrieved from the cache, 
        depending on the caching settings for this SkyObservation.
        
        Parameters
        ----------
        
        readonly : bool, optional (default: True)
            Depends on caching behaviour. If True, and caching is enabled, the 
            cached beam (which is read only) is returned. If False, a new 
            (write-enabled) copy of the cached beam is returned instead. (The 
            latter uses more memory.)
            
            If caching is not enabled, this parameter has no effect; a new copy 
            is always loaded from file and returned directly.
        """
        if self._cached['beam']:
            if self._cache_beam is not None:
                return self._cache_beam
            self._cache_beam = self._beam.load()
            self._cache_beam.setflags(write=False)
            
            if readonly: return self._cache_beam
            return self._cache_beam.copy()
        else:
            return self._beam.load()
    
    def load_sh_beam_and_pixwin(self):
        """
        Load Legendre representation of instrumental beam, B_l, multiplied by 
        the pixel grid's pixel window function.
        """
        pxwin = get_healpix_pixel_window(self.nside)[0][:self.lmax+1]
        return pxwin * self.load_sh_beam()
    
    
    ############################################################################
    # Methods for loading auxiliary data (masks, input noise maps etc.)
    ############################################################################
    
    def load_variance_map(self):
        if self._rms is not None:
            map = self._rms.load()
            map **= 2
        else:
            map = self._variance.load()
        return map

    def load_rms_map(self):
        if self._rms is not None:
            map = self._rms.load()
        else:
            map = self._variance.load()
            np.sqrt(map, map)
        return map
    
    def load_mask_map(self, readonly=True):
        mask = self._load_mask_map_without_nan_mask()
        mask *= self._nan_map()
        return mask

    def _nan_map(self):
        if self._rms is not None:
            map = self._rms.load()
        else:
            map = self._variance.load()
        return 1 - np.isnan(map).astype(np.int8)

    def _load_mask_map_without_nan_mask(self):
        """
        The mask map without also masking out NaNs pixels from the data stream
        """
        if len(self._masks) == 0:
            return np.ones(self.npix, dtype=np.int8)
        else:
            mask = self._masks[0].load()
            for maskldr in self._masks[1:]:
                mask *= maskldr.load()
            return mask


    ############################################################################
    # Methods for hashing/unique identification
    ############################################################################

    def instrumental_digest(self, h=None):
        """
        A digest/hash of the instrumental properties (noise, mask and beam).
        Returns a hashlib object.
        """
        x = self._instrumental_digest
        if x is None:
            if h is None:
                h = hashlib.sha256()
            digest_array(h, self.load_ninv_map())
            digest_array(h, self.load_sh_beam())
            self._instrumental_digest = x = h.hexdigest()
        return x

    def update_digest(self, h):
        def update(loader):
            if loader is None:
                h.update('\0')
            elif loader.cheap_digest:
                h.update(loader.get_digest())
            elif self.unique_basenames and loader.has_basename:
                h.update(loader.get_basename())
            else:
                # TODO: This is a terrible waste, should cache loaded data
                digest_array(h, loader.load())

        h.update('SkyObservation(%d,%d' % (self.nside, self.lmax))
        for loader in [self._map, self._rms, self._variance, self._beam]:
            update(loader)
        # todo: sort masks by digests or something to make it order-neutral
        for mask in self._masks:
            update(mask)
        h.update(')')

    def hexdigest(self):
        h = hashlib.sha256()
        self.update_digest(h)
        return h.hexdigest()
    
    
    ############################################################################
    # Methods for storage/persistence of data held by the SkyObservation object
    ############################################################################
    
    def repack_to_hdf5(self, filename):
        """
        Loads all the data and serializes it to the given HDF5 file under a 
        group named by the digest.
        
        This is meant for caching purposes to save the time loading from FITS 
        and changing from ring-ordering to nest and so on, which can be 
        significant. Also, often only the ninv map is needed, so it is nice to 
        only load that, rather than mask and rms.

        The variance map is not stored, only rms and ninv.
        
        Parameters
        ----------
        
        filename : string
            Filename of HDF5 file to store the data in.
        """
        #TODO: Use digest per map, not overall digest
        filename = expand_path(filename)
        digest = self.hexdigest()[:20]
        gname = '/packed_sky_observations/%s' % digest
        try:
            with h5py.File(filename, 'r') as f:
                is_present = gname in f
        except IOError:
            is_present = False
        if not is_present:
            with h5py.File(filename, 'a') as f:
                g = f.require_group(gname)
                self._persist_to_hdf5(g)
        
        return SkyObservation(nside=self.nside,
                              lmax=self.lmax,
                              name=self.name,
                              description=self.description,
                              map=HdfMapLoader(filename, '%s/map' % gname),
                              rms=HdfMapLoader(filename, '%s/rms' % gname),
                              masks=[HdfMapLoader(filename, '%s/mask' % gname)],
                              ninv=HdfMapLoader(filename, '%s/ninv' % gname),
                              beam=HdfBeamLoader(self.lmax, filename, '%s/beam' % gname))

    def _persist_to_hdf5(self, h5group):
        """
        Store all data from this SkyObservation to a HDF5 file.
        """
        h5group.create_dataset('map', data=self.load_map())
        h5group.create_dataset('rms', data=self.load_rms_map())
        h5group.create_dataset('ninv', data=self.load_ninv_map())
        h5group.create_dataset('beam', data=self.load_sh_beam())
        h5group.create_dataset('mask', data=self.load_mask_map())

    
    ############################################################################
    # Methods to manage caching of data in the SkyObservation object
    ############################################################################
    
    def set_caching(self, all=None, data=None, ninv=None, beam=None):
        """
        Specify which data are to be cached in memory, if any.
        
        If caching is enabled, the data will be kept in memory from the first 
        time that its particular load() method is called.
        
        N.B. If caching is switched on and then subsequently switched off, the 
        cached object will remain in memory if it was loaded in the interim. 
        Use flush_cache() to remove cached data.
        
        Parameters
        ----------
        
        all : bool, optional
            Change the caching setting for all data (data maps, inverse noise 
            maps, and beams). If this parameter is set, all other parameters 
            are ignored.
        
        data : bool, optional
            Whether to cache the data map.
        
        ninv : bool, optional
            Whether to cache the inverse noise map.
        
        beam : bool, optional
            Whether to cache the beam.
        """
        if all is not None:
            for key in self._cached.keys(): self._cached[key] = all
        else:
            if data is not None: self._cached['data'] = data
            if ninv is not None: self._cached['ninv'] = ninv
            if beam is not None: self._cached['beam'] = beam
    
    
    def flush_cache(self, all=False, data=False, ninv=False, beam=False):
        """
        Remove cached data from memory.
        
        N.B. Flushing cached data does not update the caching properties. Data 
        will be re-cached the next time the relevant load() method is called 
        unless the caching properties are changed using set_caching().
        
        Parameters
        ----------
        
        all : bool, optional
            Remove all cached data (data maps, inverse noise maps, and beams) 
            from memory. If this parameter is set, all other parameters 
            are ignored.
        
        data : bool, optional
            Whether to cache the data map.
        
        ninv : bool, optional
            Whether to cache the inverse noise map.
        
        beam : bool, optional
            Whether to cache the beam.
        """
        if all: data = ninv = beam = True
        if data: self._cache_data = None
        if ninv: self._cache_ninv = None
        if beam: self._cache_beam = None
        
    
    def print_cache_info(self):
        """
        Output basic information about the cached data for this SkyObservation, 
        including memory usage.
        """
        print "Cache information"
        print "-----------------"
        
        # Data map
        print "Data map:",
        if self._cached['data']:
            if self._cache_data is not None:
                print "cached, loaded", bytes2human(self._cache_data.nbytes)
            else: print "cached, not loaded"
        else: print "not cached"
        
        # Ninv map
        print "Ninv map:",
        if self._cached['ninv']:
            if self._cache_ninv is not None:
                print "cached, loaded", bytes2human(self._cache_ninv.nbytes)
            else: print "cached, not loaded"
        else: print "not cached"
            
        # Beam
        print "Beam:",
        if self._cached['beam']:
            if self._cache_beam is not None:
                print "cached, loaded", bytes2human(self._cache_beam.nbytes)
            else: print "cached, not loaded"
        else: print "not cached"



#
# Utils
#
def digest_array(h, array):
    h.update('np.ndarray:' + array.dtype.char + ':'.join(str(x) for x in array.shape))
    if array.flags.c_contiguous:
        h.update(np.getbuffer(array))
    else:
        h.update(np.getbuffer(array.copy('C')))


############################################################################
# Functions for loading data maps from various sources
############################################################################

def parse_map(map_type, nside, desc):
    """
    Factory function for various map-descriptors, allowing the use of
    more convenient tuples rather than having to explicitly use
    MapDescriptor. This is based on the name of the filename at the
    beginning of the tuple.
    """
    if isinstance(desc, MapLoader):
        loader = desc
    elif isinstance(desc, np.ndarray):
        if nside_of(desc) != nside:
            raise ValueError('Wanted nside=%d but got %d' % (nside, nside_of(desc)))
        loader = ArrayMapLoader(desc.copy())
    elif isinstance(desc, (int, float)):
        loader = ConstantMapLoader(nside, desc)
    elif isinstance(desc, tuple):
        # reduce to dict case
        filename = expand_path(desc[0])
        if filename.endswith('.fits'):
            if not (2 <= len(desc) <= 3):
                raise ValueError('Too few or too many fields in FITS-tuple: %s' % repr(desc))
            kw = {'filename': filename,
                  'map_type': map_type,
                  'field': desc[1]}
            if len(desc) == 3:
                kw['units_in_file'] = desc[2]
                
            if map_type == 'mask':
                loader = ChangeMaskResolution(nside, FitsMapLoader(nside=None, **kw))
            else:
                loader = FitsMapLoader(nside=nside, **kw)
        else:
            raise ValueError('Unrecognized file type: "%s"' % filename)
    else:
        raise TypeError('Not a map; specify FITS files as tuples, e.g., ("filename.fits", 0). '
                        'See SkyObservation docs for further info. Got: %r' % desc)
    return loader

class MapLoader(object):
    pass

class ChangeMaskResolution(MapLoader):
    def __init__(self, nside, loader):
        self.nside = nside
        self.loader = loader
        self.cheap_digest = loader.cheap_digest
        self.has_basename = loader.has_basename

    def load(self):
        mask = self.loader.load()
        if nside_of(mask) != self.nside:
            # ugh; move this routine:
            from ..compute.cr.mg_smoother import udgrade_ring
            mask = udgrade_ring(mask, self.nside)
            mask[mask != 1] = 0
        return mask

    def get_basename(self):
        return '%s!ChangeMaskResolution(%d)' % (self.loader.get_basename(), self.nside)
    
    def get_digest(self):
        return repr(self)

    def __repr__(self):
        return 'ChangeMaskResolution(%d, %r)' % (self.nside, self.loader)

class ArrayMapLoader(MapLoader):
    cheap_digest = False
    has_basename = False
        
    def __init__(self, array):
        self.array = array.copy()

    def load(self):
        return self.array.copy()

class ConstantMapLoader(MapLoader):
    cheap_digest = True
    has_basename = False

    def __init__(self, nside, value):
        self.nside = nside
        self.value = float(value)

    def load(self):
        map = np.empty(12 * self.nside**2)
        map[:] = self.value
        return map

    def get_digest(self):
        return repr(self)

    def __repr__(self):
        return 'ConstantMapLoader(%d, %.20e)' % (self.nside, self.value)

class FitsMapLoader(MapLoader):
    """
    Represents a lazy load of a map from FITS file. The keyword arguments are
    simply passed to `load_map`. The optional `nside` argument is used to raise
    an exception on load if the resolution did not match.
    """
    cheap_digest = False
    has_basename = True
    
    def __init__(self, nside=None, **kw):
        self.nside = nside
        self.kw = kw

    def get_basename(self):
        return os.path.basename(self.kw['filename'])

    def load(self):
        from .fits import load_map
        map = load_map(**self.kw)
        if self.nside is not None and nside_of(map) != self.nside:
            raise ValueError('Map loaded from %r has nside %d but wanted %d' %
                             (self.kw['filename'], nside_of(map), self.nside))
        return map

class HdfMapLoader(MapLoader):
    cheap_digest = False
    has_basename = False
    
    def __init__(self, filename, dset_path):
        self.filename = filename
        self.dset_path = dset_path

    def load(self):
        import h5py
        with h5py.File(self.filename, 'r') as f:
            return f[self.dset_path][:]
    

############################################################################
# Functions for loading beams from various sources
############################################################################

def parse_beam(lmax, desc):
    if isinstance(desc, BeamLoader):
        return desc
    elif isinstance(desc, np.ndarray):
        return ArrayBeamLoader(lmax, desc[:lmax + 1].copy())
    elif isinstance(desc, str):
        if desc.startswith('gaussian:'):
            return GaussianBeamLoader(lmax, units.as_radians(desc[len('gaussian:'):]))
        elif desc.endswith('.fits'):
            desc = expand_path(desc)
            return FitsBeamLoader(lmax, desc)
        else:
            raise ValueError('Not a beam. Either provide FITS-file or string on form '
                             '"gaussian: 1 deg". Got: %r' % desc)
    else:
        raise TypeError('Not a beam: %r' % desc)


class BeamLoader(object):
    pass
    
class ArrayBeamLoader(BeamLoader):
    cheap_digest = False
    has_basename = False

    def __init__(self, lmax, beam):
        self.lmax = lmax
        self.beam = beam

    def load(self):
        return self.beam.copy()

class GaussianBeamLoader(BeamLoader):
    cheap_digest = True
    has_basename = False

    def __init__(self, lmax, fwhm):
        self.lmax = lmax
        self.fwhm = float(fwhm)

    def load(self):
        return gaussian_beam_by_l(self.lmax, self.fwhm)

    def get_digest(self):
        return repr(self)
    
    def __repr__(self):
        return 'GaussianBeamLoader(%d, %.20e)' % (self.lmax, self.fwhm)

class FitsBeamLoader(BeamLoader):
    cheap_digest = False
    has_basename = True

    def __init__(self, lmax, filename):
        self.lmax = lmax
        self.filename = filename

    def get_basename(self):
        return os.path.basename(self.filename)    
    
    def load(self):
        return load_beam_transfer_from_fits(self.filename)[:self.lmax + 1]

class HdfBeamLoader(BeamLoader):
    cheap_digest = False
    has_basename = False
    
    def __init__(self, lmax, filename, dset_path):
        self.lmax = lmax
        self.filename = filename
        self.dset_path = dset_path

    def load(self):
        import h5py
        with h5py.File(self.filename, 'r') as f:
            return f[self.dset_path][:self.lmax + 1]
