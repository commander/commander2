from ..._testsupport import *

from ..fits import load_map, ring_map_to_fits
from ..sky_observation import SkyObservation
from matplotlib import pyplot as plt
from ...plot import plot_ring_map

def test_basic():
    obs = SkyObservation(16, 3*16,
                         map=(filename_of('data_n16.fits'), 0),
                         rms=(filename_of('rms_n16.fits'), 0),
                         masks=[(filename_of('mask_n16.fits'), 0),],
                         beam='gaussian: 20 deg')
    # Simply use all loaders
    mask = obs.load_mask_map()
    rms = obs.load_rms_map()
    var = obs.load_variance_map()
    map = obs.load_map()
    ninv = obs.load_ninv_map()
    beam = obs.load_sh_beam()
    
    if I:
        for m in [mask, rms, var, map, ninv]:
            plot_ring_map(m)
            plt.show()
        plt.plot(beam)
        plt.show()

    # Check that copies are made
    obs = SkyObservation(16, 3*16,
                         map=map, masks=[mask,], rms=rms, beam=beam)
    map[:] = mask[:] = rms[:] = beam[:] = 0
    assert not np.all(obs.load_map() == 0)
    assert not np.all(obs.load_mask_map() == 0)
    assert not np.all(obs.load_rms_map() == 0)
    assert not np.all(obs.load_sh_beam() == 0)

    # Check that we get copy
    map = obs.load_map()
    map[:] = 0
    map = obs.load_map()
    assert not np.all(map == 0)

@temp_working_dir_fixture
def test_badpix(d):
    modvar = load_map('temp', filename_of('rms_n16.fits'))
    modvar **= 2
    # Make pixel 0 a HEALPix BADPIX
    modvar[0] = -1.6375e30
    ring_map_to_fits('badpixvar.fits', modvar, 'uK^2')

    obs = SkyObservation(16, 3 * 16,
                         map=(filename_of('data_n16.fits'), 0),
                         variance=('badpixvar.fits', 0),
                         masks=(),
                         beam='gaussian: 20 deg')
    
    var = obs.load_variance_map()
    assert np.isnan(var[0])
    mask = obs.load_mask_map()
    assert mask[0] == 0
    assert np.all(mask[1:] == 1)

def test_digest():
    obs = SkyObservation(16, 3*16,
                         map=(filename_of('data_n16.fits'), 0),
                         rms=(filename_of('rms_n16.fits'), 0),
                         masks=[(filename_of('mask_n16.fits'), 0),],
                         beam='gaussian: 20 deg')
    digest = obs.instrumental_digest().hexdigest()
    if I:
        print digest
