from ..._testsupport import *

from .. import fits
from ...plot import plot_ring_map
from matplotlib import pyplot as plt

def test_load_map():
    f = filename_of('rms_n16.fits')
    map = fits.load_map('temp', f)
    assert 0.05 < map.mean() < 0.1
    map = fits.load_map('temp', f, units_in_file='K')
    assert 0.05 * 1e6 < map.mean() < 0.1 * 1e6
    map = fits.load_map('raw', f)
    assert 0.05 < map.mean() < 0.1

