import os
import numpy as np

__all__ = ['beam_from_txt', 'map_from_fits', 'power_spectrum_from_txt',
           'l_array_from_fits']

class FormatError(RuntimeError):
    pass

def expand_path(path, check_exists=True):
    path = os.path.expandvars(os.path.expanduser(path))
    if "$" in path:
        raise NameError("Undefined environment variable found in path: %s" % path)
    if check_exists and not os.path.exists(path):
        raise IOError('%s does not exist' % path)
    return path

def fits_resolve_field(extension_obj, field, desc=None):
    """
    Converts the given field into a numerical value (0-based)
    if it is a string. Raises ValueError if it is out of range.

    extension_obj should be the pyfits extension object.
    """
    if isinstance(field, int):
        if field >= 0 and field < len(extension_obj.columns):
            return field
        # fall through to exception
    else:
        for idx, col in enumerate(extension_obj.columns):
            if field == col.name:
                return idx
    if desc is None:
        desc = field
    raise ValueError('Cannot find FITS field: %s' % repr(desc))


def beam_from_txt(path):
    path = expand_path(path)
    data = np.loadtxt(path, dtype=[('l', np.int),
                                   ('beam', np.double)])
    l = data['l']
    if np.any(l != np.arange(data.shape[0])):
        raise RuntimeError("Unexpected data in data file")
    return data['beam'].copy()

def l_array_from_fits(f, dtype=np.double):
     import pyfits
     if isinstance(f, str):
         f = pyfits.open(f)
     if len(f) != 2:
         raise ValueError('Unexpected FITS data for beam/spectrum')
     data = f[1].data.field(0).astype(dtype)
     return data

def load_power_spectrum_from_file(path, lmax):
    path = expand_path(path)
    if path.endswith('.fits'):
        power_spectrum = l_array_from_fits(path)
    else:
        raise ValueError("Unrecognized file type: %s" % path)
    return power_spectrum[:lmax + 1]        

def power_spectrum_from_txt(path):
    """ Loads a *scaled* power spectrum from text file (such as the
    ones output from CAMB) and rescales it to units of Kelvin.

    The data is expected to start at 2, while the result is padded with
    two extra 0's in front so that the index equals l.
    """
    path = expand_path(path)
    data = np.loadtxt(path, dtype=[('l', np.int),
                                   ('Cl', np.double)])
    l = data['l']
    if np.any(l != np.arange(2, data.shape[0] + 2)):
        raise RuntimeError("Unexpected data in data file") 
    Cl = data['Cl'] * 2 * np.pi / (l * (l + 1)) * 1e-12 # microkelvin**2
    return np.r_[0, 0, Cl]
    

def map_from_fits(desc, type, ordering, dtype=np.double):
    """
    Loads a map from FITS file in the format used for Lambda WMAP data

    Arguments
    ---------

    type: one of 'temperature', 'count', 'mask', 'raw'
        Specifies how data conversion should happen...

    ordering: one of 'healpix.ring', 'healpix.nested'

    Returns
    -------

    The map in the requested ordering
    """
    filename, extno, field = desc
    filename = expand_path(filename)
    if extno is None or field is None:
        raise ValueError("extno and field must be provided")
    if isinstance(field, int) and field < 0:
        raise ValueError("field must be >= 0")
    import pyfits
    hdulist = pyfits.open(filename)
    try:
        ext = hdulist[extno]
        field = fits_resolve_field(ext, field, desc)
        mapdata = ext.data.field(field).ravel().astype(dtype)
        units = ext.columns[field].unit
        if units is not None:
            units = units.split(',')[0]

        if type == 'temperature':
            if units == 'mK':
                mapdata *= 1e-3 # convert to Kelvin
            elif units in ('uK', 'muK'):
                mapdata *= 1e-6
            else:
                raise FormatError('Do not recognize temperature units: %s' % (units))
        elif type == 'count':
            if units != 'counts':
                raise FormatError('Do not recognize count units: %s' % (units))
        elif type in ('mask', 'raw'):
            pass
        else:
            raise ValueError('Wrong map type: %s' % type)

        npix = mapdata.shape[0]
        nside = int(np.round(np.sqrt(npix // 12)))
                                 
        got_ordering = 'healpix.' + ext.header['ORDERING'].lower()
        if got_ordering == ordering:
            pass # preserve mapdata
        elif got_ordering == 'healpix.nested' and ordering == 'healpix.ring':
            mapdata = mapdata[:, None].copy('F') # todo: fix healpix4py
            healpix.convert_nest2ring(nside, mapdata)
            mapdata = mapdata[:, 0]
        else:
            raise NotImplementedError()
    finally:
        hdulist.close()
    return mapdata


def load_sh_beam(beam):
    if isinstance(beam, np.ndarray):
        return beam.copy()
    elif isinstance(beam, str):
        if beam.endswith('.fits'):
            return load_beam_transfer_from_fits(beam)
        else:
            return load_beam_transfer_from_txt(beam)
    else:
        raise TypeError('invalid beam_descriptor')

def load_beam_transfer_from_txt(path):
    data = np.loadtxt(path, dtype=np.dtype([('l', np.int), ('beam', np.double)]))
    ls = data['l']
    lmin, lmax = ls[0], ls[-1]
    if np.any(ls != np.arange(0, ls.shape[0])):
        raise NotImplementedError('l\'s not stored consecutively in beam file')
    return data['beam'].copy()

def load_beam_transfer_from_fits(path, dtype=np.double):
    import pyfits
    f = pyfits.open(path)
    try:
        if len(f) != 2:
            raise ValueError('Unexpected FITS data for beam')
        beam = f[1].data.field(0).astype(dtype)
    finally:
        f.close()
    return beam
