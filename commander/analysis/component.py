from __future__ import division

import numpy as np
from scipy.integrate import quad
from ..sphere import sh_synthesis

"""
from . import resourceloaders, matrices
from .sphere import mmajor
from .memoize import memoize, memoize_method, Hasher, create_hasher
from .map_descriptor import parse_map_descriptor, load_ring_map, npix2nside
"""


def as_params(params, param_list):
    """
    Take a ChainStore, ordered tuple, or dictionary of parameters, and 
    output a standardised dictionary of parameters.
    """
    if isinstance(params, Chain):
        specparams = {}
        for p in self.parameters: specparams[p] = Chain[p]
    elif isinstance(params, tuple) or isinstance(params, list):
       if len(params) == len(param_list):
           specparams = {}
           for i in range(len(params)): specparams[param_list[i]] = params[i]
       else:
           raise IndexError(
            "Requires %d spectral index parameters (%d given)." \
            % len(param_list), len(params))
    elif isinstance(params, dict):
        specparams = params
    else:
        raise NotImplementedError(
        "Can't handle parameter list of type %s." % type(params))
    return specparams


class Component(object):
    """
    Superclass for components. A conponent defines:
    
      * A physical model for a component of the sky signal (e.g. CMB, 
        synchrotron), including methods for returning maps of the signal as a 
        function of frequency etc.
        
        Some of these methods are generic, and are used by Commander to 
        construct residual maps for other sampling steps.
        
      * Methods for sampling the various parameters of the physical model 
        (except for its amplitude, which is usually handled separately by a 
        joint amplitude sampler).
    
    Subclasses of Component should be immutable, i.e. they should not hold 
    state. All state should be stored in a ChainState object.
    
    Signal methods
    --------------
    get_mixing_map(obs): returns map(array_like)
        Return a map of the signal for this component appropriate to a given 
        band, specified by obs. This should not be multiplied by the amplitudes.
        
    get_amplitude_map(): returns map(array_like)
        Return a map of the amplitudes for this component.
        
    get_power_spectrum(lmax): returns array_like
        Return the power spectrum C_l for this component, from l=0 to lmax.
    
    draw_from_prior(): returns list
        Returns a list containing one sample of each of the parameters defined 
        by this component from its prior distribution. These are used as the 
        starting values (and types) for each parameter when the chain is 
        initialised.
    
    Sampler methods
    ---------------
    sample(ChainState, args): 
        Sample the parameters specified in param_list, using whatever sampling 
        algorithm has been implemented for this signal. param_list should be a 
        list of strings, or else just a string.
        
        Once a sample has been returned, sample() should call the appropriate 
        step() function of the ChainState object, in order to accept/reject the 
        proposed sample and update the MCMC chain.
        
        The signal sampler must define its own parameters, which should be 
        identified by user-specifiable strings. These should be defined in the 
        constructor.
        
        A signal sampler can optionally define multiple sampling methods, which 
        the user can then choose between. Each of the different methods should 
        be named according to the format: sample_<parameter>_<sampler_name>().
    
    sample_power_spectrum():
    sample_spectral_indices():
    
    Properties
    ----------
    parameters: list of strings
        The names of parameters which this component defines, and is thus 
        responsible for sampling. These parameter names will be used in the 
        ChainStore.
    
    priors: list of tuples
        Prior bounds (or prior-defining functions) for each parameter. These 
        should be used by the sampling algorithm as appropriate.
    """
    # Signal methods
    def get_mixing_map(self):     return None
    def get_amplitude_map(self):  return None
    def get_power_spectrum(self): return None
    def draw_from_prior(self):    return None
    
    # Sampler methods
    def sample(self, param_list): return None



################################################################################
class CmbSignalComponent(Component):
    
    def __init__(self, lmax, cl_initial, amp_param="cmb_amp", cl_param="cl",
                       store_sigmal=False):
        """
        Arguments
        ---------
        lmax: float
        cl_initial: numpy.ndarray
        amp_param: string, optional
            Default is 'amp_param'
        cl_param: string, optional
        """
        self.lmax = lmax
        self.cl_initial = cl_initial # Used for prior
        self.amplitude_count = (lmax + 1)**2 # No. of a_lm's
        
        # Set names of parameters
        self.param_name = { 'amp': amp_param,
                             'cl': cl_param }
        self.parameters = [amp_param, cl_param]
        
        # Priors are not used for amplitude/power spectrum params.
        self.priors = {amp_param:(None, None), cl_param:(None, None)}
    
    def get_mixing_map(self, obs, chain):
        return np.ones(obs.npix)
    
    def get_amplitude_map(self, obs, chain):
        alms = chain[self.parameters[0]]
        return sh_synthesis(obs.nside, alms)
        
    def get_power_spectrum(self, chain):
        return chain[self.parameters[1]]
    
    def draw_from_prior(self):
        return [np.zeros(self.amplitude_count), self.cl_initial, ]
    
    def get_amplitude_count(self, obs=None):
        return self.amplitude_count

    def get_gauss_mixing_map(self, obs):
        return np.ones(self.amplitude_count)



################################################################################
class SpectralIndexComponent(Component):

    def __init__(self, spectral_params,
                       name="spectral_index_component",
                       num_spec_idxs=1):
        """
        Base class for components that have a spectral function that depends on 
        a number of "spectral index" parameters (e.g. synchrotron, free-free).
        
        Arguments
        ---------
        
        spectral_params: list of tuples or tuple
            Assign names and prior ranges to each parameter. These names are 
            used in the ChainStore.
            
            Can be a list of tuples if there is >=1 parameter, or just a tuple 
            if there is exactly 1 parameter. The order of the list matters; the 
            first list item specifies parameter 1, the second parameter 2 etc. 
            
            The tuples should be in the format:
            (str(param_name), float(prior_low), float(prior_high))
        
        name: str
            The name of the component. This name will be used in the ChainStore. 
            Amplitude parameters for this component will be called '<name>_amp'.
        
        num_spec_idxs: int
            The number of spectral indices that are required for this 
            component. Subclasses may set this to a fixed value, or the user 
            may be allowed to choose (e.g. if there are optional indices).
        """
        
        # Max. no. of spectral index parameters
        self._num_spec_idxs = num_spec_idxs
        
        self.name = name
        self.parameters = []
        self.priors = {}
        
        if isinstance(spectral_params, tuple):
            spectral_params = [spectral_params]
        
        # Process spectral index parameter definitions.
        if isinstance(spectral_params, list):
            if len(spectral_params) > self._num_spec_idxs:
                print "Ignoring additional parameters in spectral_params."
            if len(spectral_params) < self._num_spec_idxs:
                raise IndexError(
                "spectral_params must define %i spectral indices." % self._num_spec_idxs)
            for pname, pr_low, pr_high in spectral_params:
                self.parameters.append(str(pname))
                self.priors[str(pname)] = (pr_low, pr_high)
        elif isinstance(spectral_params, tuple):
            if self._num_spec_idxs == 1:
                pname, pr_low, pr_high = spectral_params
                self.parameters.append(str(pname))
                self.priors[str(pname)] = (pr_low, pr_high)
            else:
                raise IndexError(
                "spectral_params must define %d spectral indices (%d given)." \
                % self._num_spec_idxs)
        else:
            raise NotImplementedError(
            "Can't handle spectral_params of type %s." % type(spectral_params))
        
    
    def spectrum(self, nu, spectral_params): return None
    def spectrum_over_bandpass(self, obs, spectral_params):
        """
        Integrate the spectral function over the bandpass for a given 
        SkyObservation.
        """
        lo, hi = obs.bandpass
        bandpass, err = quad(self.spectrum, lo, hi, args=(spectral_params,))
        return bandpass
    
    # Signal methods
    
    def get_mixing_map(self, obs, params): return None
        # Calculate bandpass
        # specparams = as_params(params)
        # f_nu = self.spectrum_over_bandpass(obs, specparams)
        # Get spatial template or whatever (at correct resolution)
        # obs.nside ...
        
    def get_amplitude_map(self, store):
        return store[name+"_amp"]
    def get_power_spectrum(self): return None
    
    # Sampler methods
    def sample(self, param_list): return None


#params = [("alpha", -2.0, -1.5), ("beta", -2.0, -1.5)]
#s = SpectralIndexComponent(params, num_spec_idxs=2)
