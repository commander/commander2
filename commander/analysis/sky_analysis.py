
import numpy as np
from chain import Chain

class SkyAnalysis(object):
    
    def __init__(self, components, observations, chain_name, rng, auxiliary_data=None):
        """
        SkyAnalysis manages an MCMC run.
        
        Arguments
        ---------
        
        components: list of Component
            A list of physical components, which define physical models for a 
            component on the sky, plus a method for sampling parameters of the 
            component model.
        
        #other_samplers: list
        #    A list of samplers that are not linked to individual physical 
        #    components, such as an amplitude sampler and gain sampler.
            
        observations: list of SkyObservation
            Each SkyObservation contains a datamap, noise and beam 
            specifications, and bandpass for an individual frequency band.
        
        chain_name: string, filename
            Filename used to store output of the MCMC chain.
        
        rng: RNG state (np.random.RandomState)
            Random number generator state
        
        auxiliary_data: list of tuples
            Specification for auxiliary data to be stored. The tuples should be 
            of the format ("Name", <prototype>), where <prototype> is an 
            example of the object that will be stored in Chain. This can be 
            used for storing chi^2 maps etc.
        """
        
        self.components = components
        self.observations = observations
        self.rng = rng
        self.chain_name = chain_name
        
        # Draw priors from each component; use these to set-up the Chain object
        param_def = []
        for comp in components:
            paramnames = comp.parameters
            priors = comp.draw_from_prior()
            for p in zip(paramnames, priors): param_def.append(p)
        
        # Add auxiliary parameters to the parameter definition
        if isinstance(auxiliary_data, list):
            for p in auxiliary_data: param_def.append(p)
        elif auxiliary_data == None:
            pass
        else:
            raise ValueError("auxiliary_data: expected a list of tuples, but got %s." 
                               % type(auxiliary_data))
        
        print param_def
        exit()
        
        # Create a Chain object to store MCMC parameters when they are sampled
        self.chain = Chain(chain_name, param_def)
    
    def get_chi2_map(self, obs):
        """
        Calculate chi-squared map for a given observation.
        """
        # Get noise, data
        Ninv = 1. / obs.load_rms_map()
        d = obs.load_map()
        
        # Construct sky model by looping through components
        m = np.zeros(obs.npix)
        for comp in self.components:
            m +=  comp.get_amplitude_map(obs, self.chain) \
                * comp.get_mixing_map(obs, self.chain)
        
        # Update chi2
        chi2 = (Ninv * (d - m))**2.
        return chi2
        
    
    def chisquared(self):
        """
        Calculate chi-squared for entire dataset, for current set of parameters.
        """
        chi2 = 0.
        ndf_data = 0
        ndf_model = 0
        
        # Loop through all observations
        for obs in self.observations:
            Ninv = 1. / obs.load_rms_map()
            d = obs.load_map()
            
            # Get no. deg. freedom in data
            ndf_data += d.size
            
            # Construct sky model by looping through components
            m = np.zeros(obs.npix)
            for comp in self.components:
                m +=  comp.get_amplitude_map(obs, self.chain) \
                    * comp.get_mixing_map(obs, self.chain)
            
            # FIXME: Would have to deal with gain etc. here
            # ...
            
            # Update chi2
            chi2 += np.sum( (Ninv * (d - m))**2. )
        
        # Calculate no. degrees of freedom
        # FIXME: Is this correct?
        for comp in self.components:
            ndf_model += comp.get_amplitude_count()
            # FIXME: Get other parameters, apart from amplitude
            #ndf_model += 
        
        return chi2, ndf_data #, ndf_model
