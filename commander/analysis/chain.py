
import numpy as np
import tables as tb
import time
import os.path

null_logger = None
max_sampler_name_length = 128

# Define datastructures for HDF5 file
class LogRow(tb.IsDescription):
    ParamID     = tb.UInt16Col() # Unsigned short integer
    ParamRecord = tb.UInt64Col() # Unsigned 64-bit integer

class SamplerIDs(tb.IsDescription):
    samplerID = tb.UInt16Col(pos=1)
    sampler_name = tb.StringCol(max_sampler_name_length, pos=2)

class ParamSyncError(Exception):
    def __init__(self, val):
        self.value = val
    def __str__(self):
        return repr(self.value)

# ("foo.fits", 1, "TEMPERATURE", "force uK")
# ("test.h5", "chains/0003")

class Chain(object):

    def __init__(self):
        """
        A Chain object manages the storage and retrieval of parameter 
        values as they are sampled. It also supports logging information.

        Since a Chain object can be initialized in two ways, there is no
        standard __init__ method. To initialize a new chain, call 
        Chain.create_new.
        """
        raise TypeError("Please call Chain.create_new or Chain.load")

    @staticmethod
    def create_new(h5file, chain_path, param_spec, logger=null_logger, 
                   save_sampler_info=False, title='', derived_paramnames=()):
        """
        Creates a Chain object that points to a new chain group within a
        preexisting or new HDF file.

        Parameters
        ----------
        
        h5file : string
            The path to the HDF file in which to create the 
            new chain group.
        
        chain_path : string
            The relative path within the HDF file for the local 'root' node of 
            this chain.
            
            Examples: '/' (file root)
                      '/chain1'
                      etc.
            
            Note that for paths other than root, the string should not 
            terminate with a backslash.
        
        param_spec : list of tuples
            List of tuples specifying which MC parameters need to be stored, in 
            the format ("parameter_name", initial_value).
            
            parameter_name -- Name used to access the parameter from the Chain 
                object. Values if the parameter 
                
            initial_value -- Object used as a prototype when constructing the 
                HDF5 file. Should have the same length and datatype as the 
                objects that will be returned by the sampler for this parameter.
        
        logger : Logger instance, optional
            A Logger, for specifying how to report warnings and errors. If none 
            is specified, a Null logger will be used.
        
        save_sampler_info : bool, optional
            Whether to store debug/reporting info about each sampling step.
            (default: False).
        
        title : string, optional
            If a new file is to be created, the proper root node of the created 
            file will have this title.

        derived_paramnames : list, optional
            List of strings containing the names of the derived parameters
            that will be added to the chain.
        
        The structure that will be added to the HDF5 file is as follows:
        
        chain_path/           
            params/
                param1 [array]
                param2 [array]
                ...
            derived_params/
                derived_param1 [array]
                derived_param2 [array]
                ...
            sampler_info/ (optional)
                sampler1 [table]
                sampler2 [table]
                ...
            chain [table]
            sampler_IDs [table]

        However, the derived_param1, etc. arrays will not be created until the
        put_derived_parameter method is called for the first time.

        If the chain_path node already exists and has either of these children,
        a NodeError will be raised.
        """

        # If we have duplicate parameter names, we don't want to do anything
        pnames = [pspec[0] for pspec in param_spec]
        if len(pnames) != len(set(pnames)):
            raise ValueError("Duplicate parameter names were specified.")
        if len(derived_paramnames) != len(set(derived_paramnames)):
            raise ValueError("Duplicate derived parameter names were specified.")

        self = Chain.__new__(Chain)
        self.f = tb.openFile(h5file, mode='a', title=title)
        self.root = chain_path

        # Create groups
        self.grpParams = self.f.createGroup(self.root, "params", 
                                            "Parameter samples",
                                            createparents=True)
        self.grpDParams = self.f.createGroup(self.root, "derived_params",
                                             "Derived parameter samples")
        #Since the derived parameter arrays aren't set up yet, we must save
        #their names for later
        self.d_param_names = derived_paramnames

        # Now that the local root group is created, it is easier to refer to it
        # as Group object instead of a string
        if chain_path == '/':
            self.root = self.f.root
        else:
            self.root = self.f.root.__getattr__(chain_path)

        if save_sampler_info: 
            self.grpSamplerInfo = self.f.createGroup(self.root, 
                                                     "sampler_info", 
                                                     "Sampler info")
        self._save_sampler_info = save_sampler_info

        # Set up dictionary that tracks sampler ID. Will be filled up
        # dynamically by _update_chain_and_sampler_logs(), and will add to the
        # sampler_IDs table in the hdf file
        self.sampler_IDs = {}
        self.sampler_ID_table = self.f.createTable(self.root, 'sampler_IDs',
                                                   SamplerIDs, 
                                                   'Mapping of sampler names\
                                                   to sampler ID numbers')

        # Dictionary containing the last value of the parameters. Will be filled
        # up dynamically by _update_parameter()
        self.paramvals = {}

        self._derived_dependencies = {}

        # First, we need to set up the curr_chain_vals array, since it tracks
        # the current row values of the parameters. The first entries are 0.
        # The entry in the 'sampler' column is -1, because it is the 
        # initialization step. After all parameters have been initialized, we 
        # will add this entry to the chain table
        dt = np.dtype([('sampler', np.int8),] + [(param_spec[i][0], np.int16) 
                for i in  xrange(len(param_spec))] + [(derived_paramnames[j],
                np.int16) for j in xrange(len(derived_paramnames))])

        self.curr_chain_vals = np.array([tuple([-1] + (len(param_spec) + 
                                                       len(derived_paramnames))*
                                                       [0])],
                                        dtype=dt)
        # Loop through parameter spec. and create extendable arrays (EArray) 
        # for each parameter.
        for pname, pval in param_spec:
            pval = np.atleast_1d(pval)
            atom = tb.Atom.from_dtype(pval.dtype)
            self.f.createEArray(self.grpParams, pname, atom, (0, pval.size), 
                                 pname, expectedrows=1000)
            self._update_parameter(pname, pval)

        # Normally, adding to the chain is done with _add_chain_entry(), 
        # but this step is the initialization, where we want a -1 for the sampler.
        self.chain = self.f.createTable(self.root, 'chain', self.curr_chain_vals, 
                                         "Sampling transaction log")
        return self

    @staticmethod
    def load(h5file, chain_path):
        """
        Loads an already existing chain from an HDF file.

        Note that not *all* information is present - you still need to
        specify the dependencies for the derived parameters the first time
        you use add_iteration for such parameters.

        Parameters
        ----------
        
        h5file : string
            The path to the HDF file containing the chain.
            
        chain_path : string
            Relative path within the HDF file to the chain to be loaded.

        """
        self = Chain.__new__(Chain)
        self.f = tb.openFile(h5file, mode='a')
        if chain_path == '/':
            self.root = self.f.root
        else:
            self.root = self.f.root.__getattr__(chain_path)

        self.grpParams = self.root.params
        self.grpDParams = self.root.derived_params

        self._derived_dependencies = {}

        if 'sampler_info' in self.root:
            self._save_sampler_info = True
        else:
            self._save_sampler_info = False
        
        self.sampler_ID_table = self.root.sampler_IDs
        self.sampler_IDs = {}
        for pairs in self.sampler_ID_table:
            self.sampler_IDs[pairs[1]] = pairs[0]

        self.paramvals = {}
        for partable in self.grpParams:
            self.paramvals[partable._v_name] = partable[-1]
        for d_partable in self.grpDParams:
            self.paramvals[d_partable._v_name] = d_partable[-1]

        self.chain = self.root.chain
        self.curr_chain_vals = np.array([self.chain[-1]], 
                                        dtype=self.chain[-1].dtype)

        self.d_param_names = [name for name in 
                              self.curr_chain_vals.dtype.fields.iterkeys()]

        return self
    
    ############################################################################
    # MC steps
    ############################################################################
    
    def gibbs(self, paramnames, newvalues, info=None, sampler_key='gibbs'):
        """
        Record a new sample in the chain as a Gibbs step.

        To conform with other routines, paramnames and newvalues should be
        iterables.
        
        Parameters
        ----------
        
        paramnames : iterable (list of strings)
            List of names of the parameters that were updated in this step.
        
        newvalues : iterable (list of objects)
            List of updated values for each parameter, that will be added to 
            the chain.
        
        info : dict, optional
            Contains arbitrary diagnostic info to be stored in the 
            sampling_info group (if this option was enabled when the chain was 
            created).
            
        sampler_key : string, optional
            Unique identifier for a sampler. This is used in both the chain 
            table and sampling_info group. The default is 'gibbs'.
        """
        if self._save_sampler_info:
            self.save_sampler_info(sampler_key, sampler_info={}, add_info=info)
        self.add_iteration(paramnames, sampler_key, newvalues)
        return self[paramnames]
    
    def metropolis(self, paramnames, proposed_values, logp_before, logp_after, 
                   info=None, hastings_factor=1, sampler_key='metropolis'):
        """
        Attempt to record a new sample in the chain as a Metropolis-Hastings 
        MCMC step. The step will be accepted or rejected according to a 
        standard Metropolis rejection rule, i.e. accepted if:
        
            log(u) <= logp_after - logp_before + log(hastings_factor)
            (where u <- Uniform[0, 1])
        
        If the Chain is set to save sampler info, this function will record 
        the log-probability (after the MH test), whether the proposal was 
        accepted, and the information in the info dict.
        
        Parameters
        ----------
        
        paramnames : iterable (list of strings)
            List of names of the parameters that were updated in this step.
        
        proposed_values : iterable (list of objects)
            List of updated values for each parameter, that will be added to 
            the chain if the step is accepted.
        
        logp_before : float
            (Log) probability of current set of parameters, which should be 
            calculated by the sampler being used.
        
        logp_after : float
            (Log) probability of proposed set of parameters (also calculated by 
            the sampler).
        
        info : dict, optional
            Contains arbitrary diagnostic info to be stored in the 
            sampling_info group (if this option was enabled when the chain was 
            created).
        
        hastings_factor : float, optional
            Default value is 1.0.
        
        sampler_key : string, optional
            Unique identifier for a sampler. This is used in both the chain 
            table and sampling_info group. The default is 'metropolis'.
        """

        if (np.log(np.random.uniform()) > 
                logp_after-logp_before + np.log(hastings_factor)):
            logp_now = logp_before
            newvals = None
            accepted = False
        else:
            logp_now = logp_after
            newvals = proposed_values
            accepted = True

        if self.save_sampler_info:
            self.save_sampler_info(sampler_key,
                                   sampler_info={'logp':logp_now,
                                                 'accepted':accepted},
                                   add_info=info)
        self.add_iteration(paramnames, sampler_key, newvals)

        return logp_now, self[paramnames]
    
    ############################################################################
    # Chain data access
    ############################################################################
    
    def __getitem__(self, name):
        """
        Get the most recent value of the specified parameter.

        If one or more of the parameters specified is a derived parameter,
        a check is done to see if the dependencies have changed since last
        time it was updated.

        Parameters
        ----------

        name : string or tuple of strings
            Name of the parameter(s) to get.
        """

        if isinstance(name, tuple):
            for nam in name:
                if (nam in self.grpDParams and 
                        self._dependent_param_changed(nam)):
                    raise ParamSyncError(nam)
            return tuple([self.paramvals[nam] for nam in name])
        elif isinstance(name, str):
            if name in self.grpDParams and self._dependent_param_changed(name):
                raise ParamSyncError(name)
            return self.paramvals[name]
        else:
            raise TypeError("Expecting string or tuple, got " + str(type(name)))
    
    def __getattr__(self, name):
        """
        Not implemented.
        """
        raise NotImplementedError()
    
    def get_chain(self, name):
        """Get all values of a given parameter, in chain order."""
        return np.array(self.root.params.__getattr__(name))

    def close_file(self):
        """Closes the chain file associated with the object."""
        self.f.close()

    def add_iteration(self, paramnames, sampler_key=None, newvalues=None, 
                      is_derived=False, param_dependencies={}):
        """
        Add an iteration to the chain.

        This method is what should be ultimately called by all samplers. It
        adds new entries to the /chain table, the tables corresponding to
        each parameter name in the /params group, and it updates the latest
        value of the parameter(s) as defined by __getitem__. It does not,
        however, add to the /sampler_info group, since this is an optional
        part of a sampling step.

        Parameters
        ----------

        paramnames : iterable of strings
            The names of the parameters sampled for this iteration. Must
            correspond to the names used for initializing the Chain object.

        sampler_key : string, optional
            Key identifying the sampler used for this iteration. Each unique
            key will get its own ID number in the /sampler_IDs table and the
            chain table uses the IDs to identify the samplers. If the parameter
            is derived, there is no need to provide this. In other words, if
            is_derived=False, you should provide a string here.

        newvalues : iterable of bools, ints, floats or ndarrays, optional
            If this is None, indicates that the values associated with the
            parameters in paramnames are not to be updated. Otherwise, the
            chain values and parameter values will be updated, and the
            parameter arrays in the /params group will be appended to.
            Default is None.

        is_derived : bool, optional
            Whether the parameters in question are derived parameters.
            If newvalues is None (i.e. no update to the
            derived parameters), a test will be performed to see whether the
            parameters upon which the derived parameters depend have changed
            since the last update.

        param_dependencies: dict of iterables of strings, optional
            If the parameters to be updated are derived, and it's the first
            time they're being added to the chain, this dict will be of the
            form {d_paramname:[dependency1, dependency2,...]}, where
            dependency is a name of a 'primary' parameter. Default is an
            empty dict.
        """
        if is_derived and newvalues is None:
            for pname in paramnames:
                if self._dependent_param_changed(pname):
                    raise ParamSyncError(pname)

        if newvalues is not None:
            # Update the chain information associated with these parameters
            for name, val in zip(paramnames, newvalues):
                self._update_parameter(name, val, is_derived, 
                                       param_dependencies)
                self.curr_chain_vals[name] += 1
                if is_derived:
                    for dep in self._derived_dependencies[name].iterkeys():
                        self._derived_dependencies[name][dep] = (
                            self.curr_chain_vals[dep].copy())
        if is_derived:
            sampler_key = -2
        self._add_chain_entry(sampler_key)

    def _dependent_param_changed(self, paramname):
        """
        Check whether the dependencies for a derived parameter changed since
        it was last updated.

        Parameters
        ---------
        paramname : string
            Name of the parameter to be checked.
        """

        for d, curr_val in self._derived_dependencies[paramname].iteritems():
            if self.curr_chain_vals[d] != curr_val:
                return True
        return False

    def save_sampler_info(self, sampler_key, sampler_info={}, add_info={}):
        """
        Save sampler information for a chain iteration to the /sampler_info 
        group.

        The sampler_info group contains tables corresponding to each sampler.
        The tables' rows contain information that are to be saved, for
        whatever reason. The fields are determined by the first call to this
        method, and all subsequent calls using the same sampler_key must use
        dicts that conform to the initial ones, type-wise.

        Parameters
        ----------

        sampler_key : string
            Key identifying the sampler with which the information is to be
            associated. If the key isn't currently present in the /sampler_info
            group, a new table will be made.

        sampler_info : dict, optional
            Dictionary giving the field name (key) and field value (value)
            for sampler-specific information, such as (for metropolis) whether
            the sample was accepted, the log-posterior values etc. Default is
            an empty dict.

        add_info : dict, optional
            As sampler_info, but for more 'general' info that isn't specific
            to the sampler, such as the time used for that iteration and so on.
            Default is an empty dict.

        Raises
        ------
        ValueError
            If the input dict differs from the dict input on the first call to
            this method for a given sampler_key
        """
        # Figure-out the sampler_info dict format
        info_dtype = [(key, np.result_type(val)) for key, val in 
                         sampler_info.iteritems()]
        info_dtype += [(key, np.result_type(val)) for key, val in 
                          add_info.iteritems()]
        info_dtype = np.dtype(info_dtype)
        if not self.grpSamplerInfo.__contains__(sampler_key):
            self.f.createTable(self.grpSamplerInfo, sampler_key, info_dtype, 
                               title=sampler_key)
        else:
            if (self.grpSamplerInfo.__getattr__(sampler_key).dtype != 
                    info_dtype):
                raise ValueError("dtype derived from the input dict is\
                                 different than the dtype present in the table")

        infoarr = np.array([tuple([sampler_info_value for
                                   sampler_info_value in 
                                   sampler_info.itervalues()]+
                                  [info_value for info_value in 
                                   add_info.itervalues()]),],
                           dtype=info_dtype)
        self.grpSamplerInfo.__getattr__(sampler_key).append(infoarr)
    
    def _update_parameter(self, name, val, is_derived=False, dependencies={}):
        """
        Append a new value of a parameter to the chain, and update the
        in-memory current parameter values.

        Warning: This does *not* update the chain information (i.e. the 
        sampling transaction log is not updated). As such, this method should
        preferrably be called via the add_iteration method. The method is to be
        used for both regular and derived parameters, with the parameter values
        being added to the corresponding array in the /params group, and the
        derived parameter values being added to the corresponding array in the
        /derived_params group. For array-valued parameters, the in-memory
        current parameter value will be a reference to the input array.

        Parameters
        ----------
        name : string
            Name of the parameter to be updated. If the parameter is a derived
            parameter, 'name' must correspond to one of the items passed to the
            constructor. If this is the first time this method has been called
            with a given derived parameter name, a new array will be created in
            the /derived_params group.

        val : int, bool, float, or ndarray
            Value of the parameter. This will be appended to the corresponding
            parameter array, and the 'current' value of the parameter will be
            set to this.

        is_derived : bool, optional
            Whether the parameter is a derived parameter. Default is False.

        dependencies : dict
            If the parameter is derived and this is the first time it is being
            added, this dict should contain a key equal to 'name', whose value
            is an iterable of strings, where each string is the name of a
            primary parameter upon which the derived parameter depends. If the
            derived parameter(s) doesn't depend on any parameters, the dictionary
            must still be passed the first time this funtion is called, but the
            iterable can then be empty.
            """
        if not is_derived:
            self.root.params.__getattr__(name).append(np.atleast_2d(val))
        else:
            if name not in self.d_param_names:
                raise ValueError("Unknown derived parameter name")
            if name not in self.grpDParams:
                val = np.atleast_1d(val)
                atom = tb.Atom.from_dtype(val.dtype)
                self.f.createEArray(self.grpDParams, name, atom, (0, val.size), 
                                    name, expectedrows=1000)
            if not self._derived_dependencies.has_key(name):
                #Init the dependencies dict. The reason this test is separate
                #from the above test is that when a Chain object loads from a
                #premade file, the derived parameter will be present in the
                #derived_params group, but the dependencies will have to be
                #reinitialized, since that information is not stored anywhere.
                self._derived_dependencies[name] = {}
                for dep in dependencies[name]:
                    self._derived_dependencies[name][dep] = (
                        self.curr_chain_vals[dep])
            self.root.derived_params.__getattr__(name).append(np.atleast_2d(val))

        if not self.paramvals.has_key(name):
            self.paramvals[name] = val
            return
        if isinstance(self.paramvals[name], np.ndarray):
            self.paramvals[name][...] = val
        else:
            self.paramvals[name] = val

    def _add_chain_entry(self, sampler_key):
        """
        Add an entry to the sampling transaction log.

        It uses the current lengths of the parameter tables to add to the log.
        The log will look something like this:
        
        sampler ID | param_a | param_b | param_c | derived_param_a
            2           120      139      230           672
            2           121      139      230           673
            1           121      140      231           673

        where the numbers below param_a, param_b etc. refers to the row in the
        /params/param_a table where the value for param_a that is valid after
        that step can be found. If the parameter values changed in that step,
        they will have a new number.
        
        The parameter row values will be taken from the curr_chain_vals array,
        which is updated through the _update_chain_and_sampler_logs() method.
        
        Parameters
        ----------
        
        sampler_key : string or int
            String identifying the sampler used. The string is assumed to be 
            present in the sampler_IDs dict. Providing and int means overriding
            the check for presence in sampler_IDs - it will simply use the int
            as the sampler_ID.
        """
        if isinstance(sampler_key, str):
            if not self.sampler_IDs.has_key(sampler_key):
                self.sampler_IDs[sampler_key] = len(self.sampler_IDs)
                self.sampler_ID_table.append([(self.sampler_IDs[sampler_key],
                                            sampler_key)])
            self.curr_chain_vals['sampler'] = self.sampler_IDs[sampler_key]
        else:
            self.curr_chain_vals['sampler'] = sampler_key
        self.chain.append(self.curr_chain_vals)
    
    ############################################################################
    # Utilities
    ############################################################################


# (paramname, initial_value_from_which_type_inferred)
#lmax = 100
#npix = 1000
#paramspec = [ ("cl",           np.zeros(lmax+1)),
#              ("s_cmb",        np.zeros(npix)),
#              ("s_synch",      np.zeros(npix)),
#              ("beta_synch",   np.float64(-2.))
#            ]
#cs = ChainState("test.h5", paramspec)
#
#
#def logp(beta):
#    return -.5 * beta**2
#
#beta = cs["beta"]
#logp = logp(beta, cs["alpha"])
#while True:
#    prop = beta + sd * rng.normal()
#    t0 = walltime()
#    logp_prop = logp(prop, cs["alpha"])
#    dt = walltime() - t0
#    logp, (beta,) = cs.metropolis(("beta",), (prop,), logp, logp_prop,
#                                  {"time_for_logp": dt, "foo": "bar"})
#
#
#arr = np.zeros(n) # 1GB
#prop = ...
#while True:
#    prop[:] = sd * arr + normal # 1GB
#    
#    logp, (arr_ret,) = cs.metropolis(("a",), (prop,), logp, logp_prop)
#    assert arr_ret is arr is cs["a"]
#
#    
#    cs["a"] *= 2 # raise exception!
#
#
#
#
##    def metropolis(self, paramnames, proposed_values, logp_before, logp_after, info):
#
#
#print "beta_synch:", cs['beta_synch']
