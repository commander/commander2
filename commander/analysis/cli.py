#!/usr/bin/python

import numpy as np
import argparse
import os
import pylab as plt

from .component import CmbSignalComponent
from ..compute.cr import mg_cr as mg
from ..compute.cr import mg_smoother
from ..compute import *
from ..sphere import scatter_l_to_lm
from ..io import expand_path
from .sky_analysis import SkyAnalysis

from commander.logger_utils import timed, log_done_task
import logging

def find_cmb_component(components):
    """
    Figure-out which component is the CMB component
    """
    cmb = None
    for c in components:
        if isinstance(c, CmbSignalComponent): cmb = c
    if cmb == None: raise ValueError("Must specify a CmbSignalComponent in components")
    return cmb


def precompute(args, setup):
    """
    Perform multigrid precomputations and store to a file.
    """
    components, observations, mglevels = setup
    print "----------\nPrecompute\n----------"
    
    precomp = args.precomp_file
    print "\tPrecomputations stored in:", precomp
    
    # Set-up solver, run precomputation
    print "\tSetting up MultiGrid solver..."
    cmb = find_cmb_component(components)
    solver = mg.MgSolver(mglevels, observations, cmb.cl_initial)
    
    print "\tRunning precomputations..."
    solver.cached_compute_noise_term('$LOCAL/mg-precompute-%s.h5' % precomp)
    print "Finished."


def check(args, setup):
    """
    Output diagnostic plots to check multigrid solver.
    """
    components, observations, levels = setup
    print "-----\nCheck\n-----"
    
    # Get settings from cmdline args
    precomp = args.precomp_file
    plot_file = None
    if args.plot: plot_file = args.plot
    
    # Set-up logger
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger('mg')
    
    # Get initial Cl data
    cmb = find_cmb_component(components)
    Cl = cmb.cl_initial
    rng = np.random.RandomState()
    lmax = 3000
    
    # Set-up solver, load precomputed files
    print "Setting up MultiGrid solver..."
    solver = mg.MgSolver(levels, observations, Cl)
    print "Loading precomputed data from", expand_path('$LOCAL/mg-precompute-%s.h5' % precomp)
    solver.cached_compute_noise_term('$LOCAL/mg-precompute-%s.h5' % precomp)
    print "\tDone."
    print "Precomputing prior term..."
    solver.precompute_prior_term()
    print "\tDone."
    
    # Set-up simple test system
    print "Running single V-cycle..."
    u0 = scatter_l_to_lm(np.sqrt(Cl)) * rng.normal(size=(lmax + 1)**2)
    rhs = levels[0].matvec(u0)
    u = np.zeros((lmax + 1)**2)
    us = [u.copy(),]
    rs = [rhs - levels[0].matvec(u),]
    
    # Run six MG V-cycles and plot convergence plots
    for i in range(6):
        with timed('V-cycle'):
            mg.mg_cycle(0, levels, u, rhs, logger)
            us.append(u.copy())
            rs.append(rhs - levels[0].matvec(u))
    
    print 'Mean rel. error: %.2e' % \
        np.mean(norm_by_l(us[-1] - u0) / norm_by_l(u0))
    
    # Print diagnostic information
    print 'Soln. error (after one iter.): %.2e' % \
        np.mean(norm_by_l(us[-1] - u0) / norm_by_l(u0))
    
    # (1) Composite plot of several diagnostics
    plt.subplot(221)
    mg.plot_levels(levels)
    
    # (2) Plot error spectra for each level
    plt.subplot(222)
    for u in us:
        plt.semilogy(norm_by_l(u - u0) / norm_by_l(u0))
    for lev in levels:
        if hasattr(lev, 'nside'):
            plt.axvline(lev.nside)
        else:
            plt.axvline(lev.lmax, ls=':')
    
    # (3) Plot final error map on sky
    plt.subplot(223)
    plot_ring_map(sh_synthesis(nside, u0-us[-1]))
    plt.show()
    print "Finished."


def sample(args, setup):
    """
    Run main Gibbs sampling loop.
    """
    components, observations, mglevels = setup
    print "------\nSample\n------"
    
    # Get commandline args
    precomp = args.precomp_file
    chain_name = args.chain_file
    rng = np.random.RandomState(args.seed)
    if args.samples:
        num_iter = args.samples
    else:
        num_iter = 5000
    
    # Create new SkyAnalysis object to manage the analysis
    print "Initialising SkyAnalysis..."
    s = SkyAnalysis(components, observations, chain_name, rng) #, auxiliary_data=aux)
    cmb = find_cmb_component(components)
    print "\tDone."
    
    # Set-up solver, load precomputed files
    print "Setting up MultiGrid solver..."
    solver = mg.MgSolver(mglevels, observations, cmb.cl_initial)
    solver.cached_compute_noise_term('$LOCAL/mg-precompute-%s.h5' % precomp)
    solver.precompute_prior_term()
    print "\tDone."
    
    # Main Gibbs loop
    print "Starting Gibbs sampling (%s iterations)..." % num_iter
    for i in range(1, num_iter+1):
        print "\n * Iteration %d / %d" % (i, num_iter)
        
        # (1) Sample amplitudes
        print "\tSampling amplitudes..."
        print "\t Calculating RHS"
        rhs = make_rhs( d = s.observations[0].load_map(),
                        bl = s.observations[0].load_sh_beam_and_pixwin(),
                        cl = s.chain[cmb.parameters[1]],
                        ninv_map = s.observations[0].load_ninv_map(),
                        rng = s.rng, add_fluctuations = True )
        print "[FIXME] Cl update not implemented yet! (cli.py:sample())"
        # solver = solver.with_cl(s.chain[cmb.parameters[1]]) # FIXME
        # solver.precompute_prior_term()
        print "\t Solving system..."
        alms, info = solver.sample(rhs, tol=1e-6, maxiters=1000)
        s.chain.gibbs((cmb.parameters[0],), [alms], info=info)
        
        # (2) Sample power spectra
        print "\tSampling power spectra..."
        alms = s.chain[cmb.param_name['amp']]
        cl = sample_cls(alms, observations[0].lmax)
        s.chain.gibbs((cmb.parameters[1],), [cl], info={'name':'sample_cls()'})
        
        # (3) Sample physical component parameters
        # N/A
        
        # (4) Sample auxiliary parameters
        # N/A
        
        # (5) Print chi-squared
        chi2, ndf = s.chisquared()
        #chimap = s.get_chi2_map(s.observations[0])
        print "\tChi^2 for iteration:", chi2, "/", ndf



################################################################################
# Set-up command line argument parser
################################################################################

def sky_analysis_cli(args, components, observations, mglevels):
    """
    Parse command-line arguments and then run an analysis (command-line interface).
    """
    args = args[1:] # Leading arg. is always prog. name
    setup = [components, observations, mglevels]
    
    # Create cmdline argument parser
    parser = argparse.ArgumentParser(
        description="Run Commander2 on single-band Planck data",
        epilog="""To run Commander2, first run 'precompute' to set-up the 
                  multigrid solver. Then, run 'check' to make sure that the 
                  multigrid scheme gives sensible results. Finally, run 
                  'sample' to start the main Gibbs sampling loop.""" )
    subparsers = parser.add_subparsers(help='COMMANDER MODE')

    # Precompute options
    p_precomp = subparsers.add_parser('precompute',
        help='Precompute multigrid system', 
        description="Precompute multigrid system for Commander2")
    p_precomp.add_argument('precomp_file', type=str,
        help='Name of HDF file to store precomputations in')
    p_precomp.set_defaults(func=precompute)

    # Check options
    p_check = subparsers.add_parser('check',
        help='Check behaviour of MG system',
        description="""Check that behaviour of MG system is correct by plotting 
                       some diagnostics""" )
    p_check.add_argument('precomp_file', type=str,
        help='Name of HDF file where precomputations are stored')
    p_check.add_argument('-p', '--plot',
            help="""Output filename for diagnostic plot. Plots will be 
                    displayed interactively if not specified.""" )
    p_check.set_defaults(func=check)

    # Sample options
    p_sample = subparsers.add_parser('sample',
        help='Begin sampling using precomputed info')
    p_sample.add_argument('precomp_file', type=str,
        help='Name of HDF file where precomputations are stored')
    p_sample.add_argument('chain_file', type=str,
        help='Name of HDF file to store samples in')

    p_sample.add_argument('-n', '--samples', type=int,
        help='Number of Gibbs iterations to run')
    p_sample.add_argument('-s', '--seed', type=int,
        help='Random seed to use')
    p_sample.set_defaults(func=sample)

    # Misc. options
    parser.add_argument('--hke', help=argparse.SUPPRESS, action='store_true')

    # Parse the arguments, and then run Commander in the chosen mode
    args = parser.parse_args(args)
    
    if args.hke:
       from .._support import _token
       print "\n".join([("".join([chr(int(_token[i*5:(i+1)*5])-i) for i \
       in range(len(_token)/5)]))[j*60:(j+1)*60] for j in range(47)])
       
    args.func(args, setup)

