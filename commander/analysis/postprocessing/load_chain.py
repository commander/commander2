
import numpy as np
import tables as tb
import os.path
import pylab as P

null_logger = None

class ReadOnlyChain(object):

    def __init__( self, chain_file):
        """
        Read-only chain object, for accessing chains that have already been 
        written. FIXME: Should be refactored into Chain().
        """
        
        # Open file in 'read-only' mode
        if not os.path.isfile(chain_file):
            raise IOError("File does not exist: %s" % chain_file)
        self.chain_file = chain_file
        self.f = tb.openFile(chain_file, mode='r')
        
        # Figure-out structure of file
        self.chain = self.f.root.chain
        
        # Get list of parameters
        self.param_list = [node.name for node in self.f.listNodes(where="/params")]
        print self.param_list
        
    
    ####################
    # Data access
    ####################
    
    def __getitem__(self, name):
        if not isinstance(name, str):
            if not isinstance(name, tuple):
                raise TypeError("Expecting string or tuple, got " + str(type(name)))
            return tuple([self.f.root.params.__getattr__(nam)[-1] for nam in name])
        return self.f.root.params.__getattr__(name)[-1]
    
    def __getattr__(self, name):
        raise NotImplementedError()
    
    def get_chain(self, name):
        """Get all values of a given parameter, in chain order."""
        return self.f.root.params.__getattr__(name)
    
    ####################
    # Basic processing
    ####################
    
    def mean(self, paramname):
        """Get the mean and std. dev. of a parameter."""
        # FIXME: This is quite basic at the moment.
        nd = self.f.getNode(where='/params/'+paramname)
        
        # Array is of dimension (Nsamples, Nitems), and could be very large
        # Take means/averages one item at a time
        avg, std = np.array( [  [np.mean(nd[:,i]), np.std(nd[:,i])]
                                for i in range(nd.shape[1])  ] ).T
        return avg, std
    
    ####################
    # Print information
    ####################
    
    def print_file_tree(self):
        print self.f



# TESTING
chain = ReadOnlyChain("../../../scripts/test.h5")
avg, std = chain.mean('cl')

vals = chain.get_chain('cl')

ells = np.arange(avg.size)
for row in vals:
    P.plot(ells[3:100], row[3:100])
P.errorbar(ells[3:100], avg[3:100], yerr=std[3:100], color='k')
P.show()

#print chain.chain
