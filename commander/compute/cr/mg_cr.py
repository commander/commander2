from __future__ import division

"""
Constrained realization using multi-grid


"""
from scipy.linalg import cho_factor, cho_solve

from ...sphere import (gaussian_beam_by_l, healpix_pixel_size, npix_to_nside, nside_of,
                       fwhm_to_sigma, sh_analysis, sh_synthesis, sh_adjoint_synthesis,
                       sh_adjoint_analysis, scatter_l_to_lm, lmax_of, sh_synthesis_gauss,
                       beam_by_theta)
from ... import sphere
from ..._support.multiple_dispatch import multimethod
from ...logger_utils import timed, log_done_task
from . import mblocks
from .sh_integrals import compute_approximate_Yt_D_Y_diagonal
from ...io.sky_observation import digest_array
from ...io import expand_path
import hashlib

from .mg_smoother import *

from commander.plot import plotmatrix, plot_ring_map
from matplotlib.pyplot import *

PRECOMPUTED_VERSION = 'debug' # bump to invalidate existing precomputed data

class IndentLogger(object):
    def __init__(self, wrapped, indent):
        self.wrapped = wrapped
        self._indent = indent

    def debug(self, msg):
        self.wrapped.debug(self._indent + msg)

    def info(self, msg):
        self.wrapped.info(self._indent + msg)

    def warning(self, msg):
        self.wrapped.warning(self._indent + msg)

    def error(self, msg):
        self.wrapped.error(self._indent + msg)

def indented_logger(logger):
    if isinstance(logger, IndentLogger):
        return IndentLogger(logger, logger._indent + '  ')
    else:
        return IndentLogger(logger, '  ')

#
# Define levels objects, containing restriction, prolongation, matvec,
# but not smoothers.
#

def _matvec_inplace_D_plus_B_Ni_B(d_lm, b_lm, ninv, xlm):
    nside = nside_of(ninv)
    lmax = lmax_of(xlm)

    ninv_xlm = xlm.copy()
    ninv_xlm *= b_lm
    ninv_x = sh_synthesis(nside, ninv_xlm)
    ninv_x *= ninv
    sh_adjoint_synthesis(lmax, ninv_x, out=ninv_xlm)
    ninv_xlm *= b_lm

    xlm *= d_lm
    xlm += ninv_xlm
    return xlm

def modified_gaussian_beam(fwhm, b, lmax):
    from scipy.optimize import minimize
    
    ls = np.arange(lmax + 1)
    eval_pts = np.asarray([0, fwhm])    
    
    def get_bl(sigma):
        # divide by b inside parens, as it makes sigma/b more orthogonal
        # and seems to improve convergence in some cases *shrug*
        return np.exp(-(.5 * ls * (ls + 1) * sigma*sigma/b)**b)

    def b_eval(bl, theta):
        return beam_by_theta(bl, np.asarray([theta]))[0]

    def f(sigma):
        if sigma <= 0 or sigma >= 10 * sigma0:
            return 1e30
        bl = get_bl(sigma)
        maxpoint, halfpoint = beam_by_theta(bl, eval_pts)
        return 0.5 * maxpoint - halfpoint

    
    sigma0 = fwhm_to_sigma(fwhm)
    if b == 1:
        sigma = sigma0
    else:
        sigma0 *= .8
        result = minimize(f, sigma0, tol=1e-2)
        if not result.success:
            raise ValueError('Search for fwhm did not converge for %r' % dict(fwhm=fwhm, b=b, lmax=lmax))
        sigma = result.x[0]
    return get_bl(sigma)

class MgLevel(object):
    """
    Handles computing restriction beams etc., and defines the system
    at each level.
    """
    def __init__(self, r_beam=None, fwhm=None, b=1, split='Ni', lmax=None,
                 smoothers=(), npre=1, npost=1, nrec=1):
        # Set up restriction beams
        if r_beam is None:
            if fwhm is None:
                r_beam = np.ones(lmax + 1)
            else:
                r_beam = modified_gaussian_beam(fwhm, b, lmax)
            
        # self.r_beam is modified according to self.split in prepare()
        self.configured_r_beam = r_beam
        self.lmax = lmax
        self.split = split

        # Set up smoother lists etc.
        self.exact = False
        for smoother in smoothers:
            if smoother.exact:
                self.exact = True
        if self.exact and len(smoothers) != 1:
            raise ValueError('When giving an exact solver as smoother, please only provide one')
        smoothers = list(smoothers)
        self.smoothers = smoothers
        self.nrec = nrec
        self.pre_smoothers = smoothers * npre
        self.post_smoothers = smoothers[::-1] * npost
        for smoother in smoothers:
            smoother.set_level(self)
            
    def prepare(self, ninv, ninv_digest, prev_beam, prev_dl):
        if np.any(prev_beam <= 0):
            raise AssertionError('np.any(prev_beam <= 0)')
        if np.any(self.configured_r_beam <= 0):
            from matplotlib.pyplot import clf, plot, draw
            clf()
            plot(self.configured_r_beam)
            draw()
            raise AssertionError('np.any(self.configured_r_beam <= 0)')

        lmax = self.lmax

        if self.split == 'Ni':
            r_beam = self.configured_r_beam / prev_beam # the one for Si
            beam = self.configured_r_beam
        elif self.split == 'Si':
            r_beam = self.configured_r_beam
            beam = self.configured_r_beam * prev_beam
        elif self.split == 'even':
            r_beam = self.configured_r_beam / np.sqrt(prev_beam)
            beam = self.configured_r_beam * np.sqrt(prev_beam)
        else:
            assert False
        self.dl = prev_dl * r_beam**2
        self.beam = beam
        self.ninv = ninv
        self.r_beam = r_beam
        self.r_beam_lm = scatter_l_to_lm(r_beam)
        self._compute_digest(ninv_digest)

    def precompute_noise_term(self):
        self.beam_lm = scatter_l_to_lm(self.beam)

    def precompute_prior_term(self):
        self.d_lm = scatter_l_to_lm(self.dl)

    def pre_smooth_inplace(self, u, f, logger):
        if self.exact:
            self.smoothers[0].solve_inplace(u, f, logger)
        else:
            for smoother in self.pre_smoothers:
                smoother.smooth_inplace(u, f, logger)

    def post_smooth_inplace(self, u, f, logger):
        for smoother in self.post_smoothers:
            smoother.smooth_inplace(u, f, logger)

    def matvec(self, x):
        y = x.copy()
        self.matvec_inplace(y)
        return y

    def _compute_digest(self, ninv_digest):
        h = hashlib.sha256()
        h.update(ninv_digest)
        digest_array(h, self.dl)
        digest_array(h, self.beam)
        self.digest = h.hexdigest()
            
class ShLevel(MgLevel):
    nside = None
    
    def __init__(self, lmax, **kw):
        MgLevel.__init__(self, lmax=lmax, **kw)

    def matvec_inplace(self, xlm):
        _matvec_inplace_D_plus_B_Ni_B(self.d_lm, self.beam_lm, self.ninv, xlm)

    def __repr__(self):
        return 'level lmax=%d: %r' % (self.lmax, self.pre_smoothers)

    def h5_group_name(self):
        return 'lmax=%d' % self.lmax

class PixelLevel(MgLevel):

    def __init__(self, nside, w=None, lfactor=None, lmax=None, **kw):
        """
        w - restriction beam width in pixels
        b - exponent (1 means gaussian)
        """
        if lmax is None:
            lmax = lfactor * nside
        fwhm = None
        if w is not None:
            fwhm = w * healpix_pixel_size(nside)
        self.w = w
        MgLevel.__init__(self, fwhm=fwhm, lmax=lmax, **kw)
        self.nside = nside
        
    def matvec_inplace(self, x):
        xlm = sh_adjoint_synthesis(self.lmax, x)
        xlm = _matvec_inplace_D_plus_B_Ni_B(self.d_lm, self.beam_lm, self.ninv, xlm)
        sh_synthesis(self.nside, xlm, out=x)

    def __repr__(self):
        return 'level nside=%d' % self.nside

    def h5_group_name(self):
        return 'nside=%d,lmax=%d' % (self.nside, self.lmax)

# Restrict and prolong has different methods depending on the types of
# the levels on every side.
#
# NOTE NOTE: The input x or xlm can be mutated!
#

@multimethod(PixelLevel, PixelLevel, np.ndarray)
def restrict(H, h, x):
    xlm = sh_analysis(H.lmax, x)
    xlm *= H.r_beam_lm
    return sh_synthesis(H.nside, xlm)

@multimethod(PixelLevel, PixelLevel, np.ndarray)
def prolong(H, h, x):
    xlm = sh_adjoint_synthesis(H.lmax, x)
    xlm *= H.r_beam_lm
    return sh_adjoint_analysis(h.nside, xlm)


@multimethod(ShLevel, PixelLevel, np.ndarray)
def restrict(H, h, x):
    xlm = sh_analysis(H.lmax, x)
    xlm *= H.r_beam_lm
    return xlm

@multimethod(ShLevel, PixelLevel, np.ndarray)
def prolong(H, h, xlm):
    xlm *= H.r_beam_lm
    return sh_adjoint_analysis(h.nside, xlm)


@multimethod(PixelLevel, ShLevel, np.ndarray)
def restrict(H, h, xlm):
    xlm *= H.r_beam_lm
    return sh_synthesis(H.nside, xlm)

@multimethod(PixelLevel, ShLevel, np.ndarray)
def prolong(H, h, x):
    xlm = sh_adjoint_synthesis(H.lmax, x)
    xlm *= H.r_beam_lm
    return xlm

## @multimethod(ShLevel, ShLevel, np.ndarray)
## def restrict(from_level, to_level, xlm):
##     rl = np.zeros(from_level.lmax + 1)
##     rl[:to_level.lmax + 1] = 1
##     rlm = scatter_l_to_lm(rl)
##     return xlm[rlm == 1]

## @multimethod(ShLevel, ShLevel, np.ndarray)
## def prolong(from_level, to_level, xlm):
##     rl = np.zeros(to_level.lmax + 1)
##     rl[:from_level.lmax + 1] = 1
##     out = np.zeros((to_level.lmax + 1)**2)
##     out[rl == 1] = xlm
##     return out


#
# Smoothers
#

class Smoother(object):
    exact = False
    
    def set_level(self, level):
        self.level = level

    def smooth_inplace(self, u, f, logger):
        #r = u.copy()
        #with log_done_task(logger, 'matvec'):
        #    self.level.matvec_inplace(r)
        #np.subtract(f, r, r) # r = f - r
        r = f - self.level.matvec(u)
        self.approx_solve_inplace(r, logger)
        u += r

class ShDiagonal(Smoother):
    def __init__(self, wl=None):
        self.wl = wl
    
    def precompute_noise_term(self):
        lmax = self.level.lmax
        ninv_sh = sh_analysis(2 * lmax, self.level.ninv)
        self.beam_noise_diag = compute_approximate_Yt_D_Y_diagonal(
            self.level.ninv.shape[0], 0, lmax, ninv_sh)
        self.beam_noise_diag *= scatter_l_to_lm(self.level.beam**2)

    def precompute_prior_term(self):
        buf = self.beam_noise_diag.copy()
        buf += scatter_l_to_lm(self.level.dl)
        wl = 1 if self.wl is None else scatter_l_to_lm(self.wl)
        self.w_J_diag = wl / buf

    def approx_solve_inplace(self, x, logger):
        with log_done_task(logger, 'smoother-sh-diagonal'):
            x *= self.w_J_diag

    def save_noise_term(self, h5group):
        g = h5group.require_group('sh-diagonal-smoother')
        if self.level.digest not in g:
            g.create_dataset(self.level.digest, data=self.beam_noise_diag)

    def load_noise_term(self, h5group):
        self.beam_noise_diag = h5group['sh-diagonal-smoother'][self.level.digest][:]
            
    def __repr__(self):
        return 'ShDiagonal'

    
class ShSolve(Smoother):
    exact = True

    def matvec(self, u):
        return np.dot(self.A, u)

    def precompute_noise_term(self):
        #self.Ni_arr = mblocks.compute_Yh_D_Y_healpix(lmax, lmax, self.level.ninv)

        lmax = self.level.lmax
        nside = nside_of(self.level.ninv)
        lmax_obs = 2 * lmax + 1
        ninv_winv_sh = sh_adjoint_synthesis(lmax_obs, self.level.ninv)
        ninv_gauss = sh_synthesis_gauss(lmax_obs, ninv_winv_sh)
        theta, nphi, weights = sphere.gauss_legendre_grid(lmax_obs)
        ninv_gauss = (ninv_gauss.reshape(theta.shape[0], nphi) * weights[:, None]
                      ).reshape(theta.shape[0] * nphi)
        self.Ni_arr = mblocks.compute_Yh_D_Y_gauss(lmax_obs, lmax, lmax, ninv_gauss)
        blm = scatter_l_to_lm(self.level.beam)
        self.Ni_arr *= blm[:, None]
        self.Ni_arr *= blm[None, :]


    def precompute_prior_term(self):
        arr = self.Ni_arr.copy()
        dlm = scatter_l_to_lm(self.level.dl)
        i = np.arange(arr.shape[0])
        arr[i,i] += dlm
        self.arr = arr
        self.factor = cho_factor(arr, lower=True, overwrite_a=False)

    def solve_inplace(self, u, f, logger):
        u[:] = cho_solve(self.factor, f, overwrite_b=False)

    def save_noise_term(self, h5group):
        pass
        #g = h5group.require_group('ShSolve').require_group('Ni_arr')
        #if self.level.digest not in g:
        #    g.create_dataset(self.level.digest, data=self.Ni_arr)

    def load_noise_term(self, h5group):
        raise KeyError()
        #self.Ni_arr = h5group['ShSolve']['Ni_arr'][self.level.digest][:]

    def __repr__(self):
        return 'ShSolve'
        
class ICC(Smoother):
    def __init__(self, tilesize, ridge=None, omega=1):
        self.tilesize = tilesize
        self.ridge = ridge
        self.omega = omega

    def set_level(self, level):
        Smoother.set_level(self, level)

    def precompute_noise_term(self):
        self.nside_level = self.level.nside // self.tilesize
        nside_obs = nside_of(self.level.ninv)
        self.indptr_lower, self.indices_lower = csc_neighbours_lower(self.nside_level)
        indptr_full, indices_full = mirror_csc_indices(self.indptr_lower, self.indices_lower)

        ninv_tiled = convert_ring2tiled(self.nside_level, self.level.ninv)
        with timed('Computing B'):
            B_blocks = compute_csc_beam_matrix(False, nside_obs, self.level.nside, self.nside_level,
                                               self.level.beam, indptr_full, indices_full)
        with timed('Assembling B Ni B'):
            Ni_blocks = block_At_D_A(indptr_full, indices_full, B_blocks, ninv_tiled,
                                     self.indptr_lower, self.indices_lower)
        self.Ni_blocks = Ni_blocks

    def precompute_prior_term(self):
        nside = self.level.nside
        with timed('Computing Si'):
            Si_blocks = compute_csc_beam_matrix(True, nside, nside, self.nside_level,
                                                self.level.dl, self.indptr_lower,
                                                self.indices_lower)
        blocks = Si_blocks + self.Ni_blocks
        with timed('ICC(0)'):
            bmax = blocks.max()
            ridge = self.ridge
            if ridge is None:
                alpha_factor = 1.5
                try:
                    alpha, ncalls = probe_cholesky_ridging(self.indptr_lower, self.indices_lower, blocks,
                                                           ridge=0, eps_log10=.01)
                except NotImplementedError:
                    print self
                    raise
                self.probed_ridge = ridge = alpha_factor * alpha / bmax
                print 'relative ridge', '%.2e' % self.probed_ridge
            block_incomplete_cholesky_factor(self.indptr_lower, self.indices_lower, blocks,
                                             alpha=ridge * bmax)
        #blocks = blocks.astype(np.float32).astype(np.float64)
        self.smoother_blocks = blocks

    def approx_solve_inplace(self, x, logger):
        with log_done_task(logger, 'smoother-tri-solve'):
            x_tiled = convert_ring2tiled(self.nside_level, x)
            block_triangular_solve('N', self.indptr_lower, self.indices_lower,
                                   self.smoother_blocks, x_tiled)
            block_triangular_solve('T', self.indptr_lower, self.indices_lower,
                                   self.smoother_blocks, x_tiled)
            x[:] = self.omega * convert_tiled2ring(x_tiled)

    def save_noise_term(self, h5group):
        g = h5group.require_group('ICC-%d' % self.tilesize).require_group('Ni_blocks')
        if self.level.digest not in g:
            g.create_dataset(self.level.digest, data=self.Ni_blocks)

    def load_noise_term(self, h5group):
        self.Ni_blocks = h5group['ICC-%d' % self.tilesize]['Ni_blocks'][self.level.digest][:].copy('F')
        self.nside_level = self.level.nside // self.tilesize
        self.indptr_lower, self.indices_lower = csc_neighbours_lower(self.nside_level)
        
    def __repr__(self):
        return 'ICC(tilesize=%d)' % self.tilesize

#
# Setup, persistence
#

class MgSolver:
    def __init__(self, levels, observation, Cl):
        """
        MultiGrid solver.
        
        Parameters
        ----------
        
        levels : iterable, MgLevel instances
            List of MgLevel instances defining the MG cycle to follow in each 
            iteration of the MG solver. The list should be ordered starting 
            with the level with the finest grid (i.e. the level where the 
            actual system is being solved, rather than a residual system).
                    
        observation : SkyObservation instance
            A SkyObservation instance which provides data, beam, and noise 
            characteristics.
            
        Cl : array_like, floats
            Angular power spectrum, C_l, used in constructing the signal prior 
            term, S. This is constant for a given MgSolver instance, and cannot 
            be modified. Use with_cl() to update C_l properly.
        """
        self.obs_hexdigest = observation.instrumental_digest()
        self.levels = levels
        self.ninv = observation[0].load_ninv_map()
        self.beam = observation[0].load_sh_beam_and_pixwin()
        self.dl = 1 / Cl
        self.dl[0] = self.dl[1] = 0. # Set mono/dipole to zero
        self.prepare_levels(self.ninv, self.beam, self.dl)

    def with_cl(self, cl):
        """
        Return a copy of this MgSolver instance with an updated power spectrum. 
        This re-runs prepare_levels() on each MgLevel and updates the priors 
        using precompute_prior_term().
        """
        raise NotImplementedError
        # FIXME: Is this the best way of doing things?
        return MgSolver(self.levels, self.observation, cl)
        #self.dl = 1 / cl
        #self.prepare_levels(self.ninv, self.beam, self.dl)
        #self.precompute_prior_term()
        

    def prepare_levels(self, ninv, beam, dl):
        h = hashlib.sha256()
        digest_array(h, ninv)
        ninv_digest = h.digest()
        for level in self.levels:
            level.prepare(ninv, ninv_digest, beam[:level.lmax + 1], dl[:level.lmax + 1])
            beam, dl = level.beam, level.dl

    def cached_compute_noise_term(self, cache_filename):
        import h5py
        with h5py.File(resolve_path(cache_filename), 'a') as f:
            g = self._open_h5_group(f)
            for level in self.levels:
                level.precompute_noise_term() # currently all cheap, do not cache
                
                level_group = g.require_group(level.h5_group_name())
                for smoother in level.smoothers:
                    try:
                        print "\tLoading noise term for", smoother.level
                        smoother.load_noise_term(level_group)
                    except KeyError:
                        print '\t(precomputing)'
                        smoother.precompute_noise_term()
                        print '\t(saving)'
                        smoother.save_noise_term(level_group)

    def precompute_noise_term(self):
        for level in self.levels:
            level.precompute_noise_term()
            for smoother in level.smoothers:
                smoother.precompute_noise_term()

    def precompute_prior_term(self):
        for level in self.levels:
            level.precompute_prior_term()
            for smoother in level.smoothers:
                smoother.precompute_prior_term()

    def _open_h5_group(self, f):
        g = f.require_group('commander-mg-precomputations')
        g = g.require_group(PRECOMPUTED_VERSION)
        g = g.require_group(self.obs_hexdigest[:8])
        return g
        
    def save_noise_term(self, filename):
        import h5py
        with h5py.File(resolve_path(filename), 'a') as f:
            g = self._open_h5_group(f)
            for level in self.levels:
                level.save_noise_term(g.require_group(level.h5_group_name()))

    def sample(self, rhs, tol=1e-6, maxiters=1000):
        """
        Return a sample by solving the defined system.
        
        Parameters
        ----------
        
        rhs : array_like
            Array of (lmax+1)^2 a_lm's in mmajor ordering, corresponding to the 
            right-hand side of the equation A x = b being solved.
            
        tol : float, optional
            Minimum required accuracy of the solution. Once the max. error per 
            ell mode, max(||A x - b||_l) < tol, the solution is returned. 
            Default: 1e-6.
        
        maxiters : int, optional
            If no solution is found after this number of MG V-cycles, the 
            algorithm gives up and returns an error. Default: 1000.
        """
        #rhs = levels[0].matvec(u0)
        
        # Initial guess for solution vector
        u = np.zeros((self.lmax + 1)**2)
        errscale = np.sqrt(self.dl) # Scaling for relative error (1 / Sqrt(Cl))
        
        # For system A u = b, convergence criterion is defined using the 
        # measure: maxerr = max( |u - b|_l / Sqrt(C_l) ), which is the maximum 
        # of the per-ell errors.
        err = errscale * norm_by_l(rhs - self.levels[0].matvec(u))
        maxerr0 = np.max(np.abs(err))
        maxerr = np.copy(maxerr0)
        
        # Run MG V-cycles until convergence
        iters = 0
        while maxerr > tol:
            
            # Do single V-cycle
            mg_cycle(0, levels, u, rhs, logger)
            
            # Calculate error
            err = errscale * norm_by_l(rhs - self.levels[0].matvec(u))
            maxerr = np.max(np.abs(err))
            print "\t%d -- max. error = %.4e" % (iters, maxerr)
            
            # Check iteration count
            iters += 1
            if (iters > maxiters) and (maxerr > tol):
                raise ValueError("Reached max. number of iterations without \
                finding a solution (final err=%.4e, tol=%.4e)." % (maxerr, tol))
        
        # Return solution and infodict
        info = { 'name': 'MgSolver.sample',
                 'final_err': maxerr,
                 'tol': tol,
                 'iters': iters }
        return u, info


    def plot_levels(self, fig=None):
        from matplotlib import pyplot as plt

        if fig is None:
            fig = plt.gcf()
        fig.clear()

        ninv_max = self.ninv.max()
        nside = nside_of(self.ninv)

        Yt_Ni_Y_max = 12 * nside**2 / 4 / np.pi * ninv_max
        
        lmax = self.dl.shape[0] - 1
        l = np.arange(lmax + 1)

        pix_levels = [level for level in self.levels if isinstance(level, PixelLevel)]

        L = len(pix_levels)
        axs = np.zeros((L, 3), dtype=object)
        for i in range(L):
            for j, name in zip(range(3), ['theta', 'theta', r'$\ell$']):
                axs[i, j] = fig.add_subplot(L, 3, 3 * i + j + 1)

        colors = ['black', 'red', 'blue']
        for j, lev in enumerate(pix_levels):
            axs[j, 0].set_ylabel(r'$N_\mathrm{side}=%d$' % lev.nside)        

            pw = healpix_pixel_size(lev.nside)
            thetas = np.linspace(0, 30 * pw, 300)

            f_dl = beam_by_theta(lev.dl, thetas)
            f_r = beam_by_theta(lev.r_beam, thetas)
            tau_h = f_dl[0]

            if Yt_Ni_Y_max == 0:
                f_ni = None
            else:
                f_ni = beam_by_theta(lev.beam**2, thetas)
                f_ni *= Yt_Ni_Y_max


            # Near-profile plot
            for i, (f, col, name) in enumerate(zip([f_dl, f_ni, f_r], colors, ['Si', 'Ni', 'R'])):
                if f is not None:
                    axs[j, 0].plot(thetas[:150] / pw, f[:150] / f[0], color=col, label=name)

            # Far-ringing plot

            if f_ni is not None:        
                m = np.abs(f_ni).max()
                axs[j, 1].semilogy(thetas / pw, np.abs(f_ni), color=colors[1])
                axs[j, 1].semilogy(thetas / pw, np.abs(f_dl), color=colors[0])
                axs[j, 1].semilogy(thetas / pw, np.abs(f_ni), color=colors[1])
                axs[j, 1].axhline(tau_h, color='k')
            else:
                axs[j, 1].semilogy(thetas / pw, np.abs(f_dl), color=colors[0])
            

            for k in range(3):
                #axs[j, 0].axvline(k, ls=':', color='k')
                #axs[j, 1].axvline(k, ls=':', color='k')
                for smoother in lev.smoothers:
                    if isinstance(smoother, ICC):
                        axs[j, 1].axvline(smoother.tilesize, ls='-', lw=1, color='r')

            for bl, col in zip([lev.dl, Yt_Ni_Y_max * lev.beam**2, lev.r_beam], colors):
                axs[j, 2].semilogy(bl, color=col)


            axs[j, 2].set_xlim((0, lmax))

        axs[-1, 0].set_xlabel(r'Pixels')
        axs[-1, 1].set_xlabel(r'Pixels')
        axs[-1, 2].set_xlabel(r'$\ell$')
        axs[0,0].legend()
        return fig, axs

                
#
# Multigrid algorithm
#



def mg_cycle(ilevel, levels, u_h, f, logger):
    logger.info('enter %r' % levels[ilevel])
    indlogger = indented_logger(logger)
    h = levels[ilevel]
    if ilevel == len(levels) - 1:
        # last level; presumably a solve; we ignore npost for this
        h.pre_smooth_inplace(u_h, f, indlogger)
    else:
        H = levels[ilevel + 1]
        # Pre-smoothing
        h.pre_smooth_inplace(u_h, f, indlogger)

        # Recurse
        #r_h = u_h.copy()
        #with log_done_task(indlogger, 'residual matvec'):
        #    h.matvec_inplace(r_h)
        #np.subtract(f, r_h, r_h) # r_h = f - matvec(u_H)
        with log_done_task(indlogger, 'matvec'):
            r_h = f - h.matvec(u_h)
        with log_done_task(indlogger, 'restrict'):
            r_H = restrict(H, h, r_h)
            
        delta_H = np.zeros_like(r_H)
        for i in range(H.nrec):
            mg_cycle(ilevel + 1, levels, delta_H, r_H, indlogger)

        with log_done_task(indlogger, 'prolong'):
            delta_h = prolong(H, h, delta_H)

        u_h += delta_h

        # Post-smoothing
        h.post_smooth_inplace(u_h, f, logger)
    logger.info('exit %r' % levels[ilevel])

#
# Diagnosticts
#

