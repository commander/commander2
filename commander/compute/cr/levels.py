
from .mg_smoother import *
from ... import sphere
from . import mblocks
from ...sphere import *
from .sh_integrals import compute_approximate_Yt_D_Y_diagonal
from scipy.linalg import cho_factor, cho_solve
from ...logger_utils import timed, log_done_task

from .multilevel import truncate_alm, pad_alm


def _matvec_inplace_D_plus_B_Ni_B( ninv_sht_plan, mixing_sht_plan, 
                                   d_lm, f_lm, b_lm, ninv, mixing_maps, xlm ):
    """
    In-place multiplication of a vector by the linear system matrix for a given 
    MG level.
    
    Parameters
    ----------
    
    ninv_sht_plan : libsharp SH transform plan
    
    d_lm, f_lm, b_lm : array_like
        Prior term of linear operator (D_h), restriction beam operator (F_h),
        and instrumental beam operator (B).
    
    ninv : list of array_like
        List of Ninv arrays, one per observation.
    
    xlm : array_like
        Solution vector/correction vector to multiply the linear system with.
        N.B. Assumes that x_lm all have the same length, for now.
    """
    
    # xlm has shape (Ncomp, (lmax+1)^2)
    # d_lm has shape (Ncomp, (lmax+1)^2)
    
    nobs = len(ninv)
    ncomp = len(mixing_maps)
    
    lmax = lmax_of(xlm[0])
    _xlm = xlm.copy()
    
    # Apply prior term directly (already includes f_lm), D_h . x_h
    xlm *= d_lm
    
    # Apply restriction operator to entire solution vector, F_h . x_h
    _xlm *= f_lm
    
    
    for each nobs:
        precompute (Y^T N^-1 Y)
    
    # Loop over observations for noise term
    for k in range(nobs):
      for j in range(ncomp):
        ninv_xlm = _xlm[j].copy()
            
        # Apply mixing matrix, (Y_m^T w M_j Y_m) (...)
        # (unless it's the CMB, in which case we do nothing)
        if mixing_map[j] is not None: # If None, special case: CMB
            ninv_x = mixing_sht_plan[k][j].synthesis(ninv_xlm)
            ninv_x *= mixing_map[j]
            ninv_xlm += mixing_sht_plan[k][j].analysis(ninv_x) # FIXME
        
      # Apply beam, B_lm (...)
      ninv_xlm *= b_lm[k]
      # FIXME
    #############################################################
        
        # Do (Y_obs^T N^-1 Y_obs) (...)
        ninv_x = sht_plan.synthesis(ninv_xlm)
        ninv_x *= ninv[k]
        sht_plan.adjoint_synthesis(ninv_x, out=ninv_xlm)
        
        # Apply beam, B^T_lm (...)
        ninv_xlm *= b_lm[k]
      
        # Loop through transpose of mixing matrix and add results to x_lm
        for i in range(ncomp):
            _ninv_xlm = ninv_xlm.copy()
            
            # Apply mixing matrix, (Y_m^T M_i w Y_m) (...)
            if mixing_map[i] is not None:
                ninv_x = mixing_sht_plan[k][i].synthesis(_ninv_xlm)
                ninv_x *= mixing_map[i]
                mixing_sht_plan[k][i].analysis(ninv_x, out=_ninv_xlm)
            
            # Apply restriction operator and add to result
            xlm[i] += _ninv_xlm * f_lm
    return xlm

class MgLevel(object):
    
    def __init__(self, f_l=None, lmax=None, npre=1, npost=1, nrec=1):
        """
        Base class for a MultiGrid level. 
        
        Parameters
        ----------
        
        f_l : array_like
            Restriction beam, defined in spherical harmonic (l-) space. If no 
            restriction beam is specified, a uniform beam (f_l = 1) will be 
            assumed.
        
        lmax : float
            Max. l value that this level 
        
        npre, npost : int
            Number of smoother iterations to perform before restriction (npre),
            and after restriction (npost).
        
        nrec : int
            This defines the type of multi-grid cycle that is being used (e.g. 
            V-cycle or W-cycle). nrec is the number of times the (recursive) 
            restrict -> smooth -> interpolate operation is run from this level, 
            down to lower levels and back again. 
            
            For example, if nrec=1, a V-cycle will be performed. If nrec=2, a 
            W-cycle will be performed.
        """
        
        if f_l is None: f_l = np.ones(lmax + 1)
        
        # Define restriction beam. This is the restriction beam *for this level 
        # only*. Use self.fl_tot for the MG restriction (includes the effects 
        # of all finer levels too).
        self.f_l = f_l
        self.lmax = lmax
        self.nside = None
        
        # Specify no. of pre/post-restriction smoother iterations and define 
        # MG cycle type
        self.npre = npre
        self.npost = npost
        self.nrec = nrec
        
        # Exact smoother flag (i.e. whether the "smoother" actually tries to 
        # completely solve the current system, rather than just trying to 
        # improve the solution)
        self.exact = False
        
            
    def prepare( self, ninv, ninv_winv_lm, ninv_digest, prev_fltot, prev_dl, 
                 beams, sh_mixing_maps ):
        """
        Prepare restriction operator for this level.
        
        Parameters
        ----------
        
        ninv : list of array_like
            List of inverse noise maps per freq.
            
        ninv_winv_lm : list of array_like
            Diagonal of ``W^{-1} N^{-1}``, in spherical harmonic
            domain, that is, inverse noise map multiplied with inverse
            HEALPix ring weights.
        
        ninv_digest : SHA digest
            Hash of the inverse noise map, used to uniquely identify it. This 
            is used to decide whether cached precomputations involving Ninv 
            need to be recalculated (e.g. because the map was changed).
            (At the moment, you can just set this to 'None')
        
        prev_fltot : array_like
            Cumulative restriction beam used on the previous (finer) MG level. 
            This is needed to define the restriction/interpolation operator 
            between levels. (Size: self.lmax)
        
        prev_dl : array_like
            Inverse prior term ("prior beam", ~ 1/Cl) used on the previous 
            (finer) level. (Size: self.lmax)
        
        beams : list of array_like
            List of instrumental beams per freq.
        
        sh_mixing_maps : list of array_like
            List of harmonic-space mixing maps per frequency, (Nobs x Ncomp).
        """
        
        lmax = self.lmax
        lmax_gauss = 2 * lmax
        npix_gauss = (lmax_gauss + 1) * 2 * (lmax_gauss + 1)
        
        # FIXME: Should choose lmax based on mixing maps too.
        
        self.nobs = len(ninv)
        self.ncomp = len(sh_mixing_maps[0])
        
        # Prepare instrumental beams
        self.b_lm = [scatter_l_to_lm(beams[i]) for i in range(nobs)]
        
        # Prepare to store SH transform info for Ninv and mixing maps
        self.ninv = []; self.ninv_for_matvec = []; self.ninv_winv_lm = []
        self.mixing_maps = []; self.mixing_sht_plan = []
        self.ninv_lmax = []; self.mixing_lmax = []
        
        # Loop over observations, defining degraded Ninvs for this level
        for i in range(nobs):
            nside_obs = nside_of(ninv[i])
            
            # (1) Set-up noise map (degrading if possible)
            if npix_gauss < 12 * nside_obs**2:
                # Define restricted Ninv operator on a Gauss-Legendre grid. 
                # Degrade as it says in the paper, but multiply together W N^-1 
                # (using adjoint_analysis) so that we can consistently use
                # adjoint_synthesis.
                if lmax_gauss < lmax_of(ninv_winv_lm[i]):
                    ninv_winv_lm[i] = truncate_alm( ninv_winv_lm[i], 
                                                    lmax_of(ninv_winv_lm[i]),
                                                    lmax_gauss )
                    lmax_sh = lmax_gauss
                else:
                    lmax_sh = lmax_of(ninv_winv_lm[i])
                
                ninv_matvec = sh_adjoint_analysis_gauss( lmax_gauss, 
                                                         ninv_winv_lm[i],
                                                         lmax_sh=lmax_sh )
                self.ninv_for_matvec.append(ninv_matvec)
                self.ninv_sht_plan = sphere.sharp.RealMmajorGaussPlan(lmax_gauss, lmax)
                self.lmax_gauss = lmax_gauss
            else:
                # We keep Ninv on the original observational HEALPix grid
                self.ninv_sht_plan = sphere.sharp.RealMmajorHealpixPlan(
                                       nside_obs, lmax, weights=None )
                self.ninv_for_matvec.append(ninv[i])
        
            # Set-up inverse noise maps needed for MG restriction operation
            self.ninv.append(ninv[i])
            self.ninv_winv_lm.append(ninv_winv_lm[i])
            self.ninv_lmax.append(lmax_of(ninv_winv_lm[i]))
            
            # Set-up mixing maps on Gauss-Legendre grid, (w \hat{M}_j)
            # Shape: mixing_maps[Nobs][Ncomponents]
            mixing_maps = []; mixing_lmax = []
            for m_lm in sh_mixing_maps[i]:
                lmax_m = None; m_map = None
                if m_lm is not None:
                    lmax_m = lmax_h + lmax_of(m_lm)
                    m_map = sh_adjoint_analysis_gauss(lmax_m, m_lm, lmax_sh=None)
                
                mixing_maps.append(m_map)
                mixing_lmax.append(lmax_m)
            
            # Store mixing map info for this band
            self.mixing_maps.append(mixing_maps)
            self.mixing_lmax.append(mixing_lmax)
        
        # Define prior term for each component
        self.dl = []
        for i in range(self.ncomp):
            if prev_dl[i] != 0:
                self.dl.append( prev_dl[i] * self.f_l**2 ) # D_h = (f_l,tot)^2 / C_l
            else:
                self.dl.append(0)
        
        # Total SH restriction beam and single-level restr. beam
        self.fl_tot = self.f_l * prev_fltot
        self.f_lm = scatter_l_to_lm(self.f_l) # Use f_l, *not* fl_tot here

    # Precomputation methods
    def precompute_noise_term(self):
        """
        Precompute the noise operator for this level. Subclasses should override 
        this method, doing whatever precomputations are necessary for their 
        smoothing operator.
        """
        pass

    def precompute_prior_term(self):
        """
        Precompute the prior operator for this level. Subclasses should override 
        this method, doing whatever precomputations are necessary for their 
        smoothing operator.
        """
        for dl in self.dl:
            if dl != 0:
                self.d_lm.append(scatter_l_to_lm(dl))
            else:
                self.d_lm.append(0)
            
    
    # Smoother methods
    def apply_smoother_inplace(self, x, logger):
        """
        Apply smoothing operator to a vector in-place. Subclasses should 
        override this method with their own smoothing operator.
        """
        raise NotImplementedError("Base MgLevel class doesn't have a smoother "+ \
                                  "defined for it. Use one of the subclasses!")

    #def smooth_inplace(self, u, f, logger):
    #    """
    #    Apply smoother to residual on this level.
    #    """
    #    # FIXME: DUPLICATE METHOD!
    #    r = f - self.matvec(u)
    #    self.apply_smoother_inplace(r, logger)
    #    u += r

    # Linear operator multiplication routines
    def matvec(self, x):
        """
        Multiply a given vector with the restricted linear operator for this 
        level, A_h = D_h + B_h Y^T_obs N^-1 Y_obs B_h.
        """
        y = x.copy()
        self.matvec_inplace(y)
        return y

    def matvec_inplace(self, xlm):
        """
        Multiply a given vector with the restricted linear operator for this 
        level, A_h = D_h + B_h Y^T_obs N^-1 Y_obs B_h.
        (Operation is performed in-place, to save memory.)
        """
        # Apply restricted operator A_h using a Gauss-Legendre grid (or Healpix 
        # grid if GL grid not defined)
        _matvec_inplace_D_plus_B_Ni_B( self.ninv_sht_plan, self.mixing_sht_plan, 
                                       self.d_lm, self.f_lm, self.b_lm,
                                       self.ninv_for_matvec, self.mixing_maps, 
                                       xlm )

    def smooth_inplace(self, x, b, logger):
        """
        Apply error smoother, that is:

            x = x + M (b - A x)

        where M is self.approx_solve_inplace.

        x can be None, meaning all-zero (but faster); in which case an
        array is allocated.

        Overwrites input x, returns new x.
        """
        
        if x is None:
            r = b.copy()
            self.approx_solve_inplace(r, logger)
            return r
        else:
            x_orig = x.copy()
            self.matvec_inplace(x)
            r = b - x
            self.approx_solve_inplace(r, logger)
            r += x_orig
            return r


class ShLevel(MgLevel):
    
    def __init__(self, lmax, wl, **kw):
        """
        Pure spherical harmonic MG level, with a diagonal smoother in SH-space.
        """
        self.wl = wl
        MgLevel.__init__(self, lmax=lmax, **kw)
    
    def precompute_noise_term(self):
        """
        Precompute the (block diagonal approx.) noise operator for this level, 
        diag( sum_nu[ (Y^T Mhat w Y) . B Y^T N^-1 Y B . (Y^T w Mhat Y) ] )
        """
        MgLevel.precompute_noise_term(self)
        lmax = self.lmax # Level lmax
        hugemem = True # TODO: Should allow the user to set this somewhere
        
        # Diagonal approx. to (B Y^T N^-1 Y B) term
        if hugemem:
            beam_noise_diag = []
            for obs in range(self.nobs):
                ninv_sh = sh_analysis(2 * lmax, self.ninv[obs])
                _beam_noise_diag = compute_approximate_Yt_D_Y_diagonal( 
                                           self.ninv[obs].shape[0], 0, lmax, ninv_sh)
                _beam_noise_diag *= scatter_l_to_lm(self.beam[obs]**2)
                beam_noise_diag.append(_beam_noise_diag)
        
        # Construct noise part of (block diagonal) linear operator
        self.diag_noise_term = []
        for comp in range(self.ncomp):
            print "\t Computing approx. noise term for %s (component %d)" % (self, comp)
            
            # Block diagonal operator for each component
            Adiag = 0
            for obs in range(self.nobs):
                
                # Use precomputed (B N^-1 B), or else compute it on the fly
                if hugemem:
                    beam_noise_part = beam_noise_diag[obs]
                else:
                    ninv_sh = sh_analysis(2 * lmax, self.ninv[obs])
                    _beam_noise_diag = compute_approximate_Yt_D_Y_diagonal( 
                                         self.ninv[obs].shape[0], 0, lmax, ninv_sh)
                    _beam_noise_diag *= scatter_l_to_lm(self.beam[obs]**2)
                    beam_noise_part = _beam_noise_diag
                
                # Precomputed wM -> (wM)_lm (GL -> SH)
                wM_sh = sh_analysis_gauss( self.mixing_lmax[obs][comp], 
                                           self.mixing_maps[obs][comp],
                                           lmax_sh=2*lmax )

                # Calculate diagonal of mixing map operator
                # (Keeps only modes up to level.lmax)
                Yt_wM_Y = compute_approximate_Yt_D_Y_diagonal( 
                                           self.mixing_maps[obs][comp].shape[0],
                                           0, lmax, wM_sh )
                
                # Add to linear operator
                # FIXME: Is restriction operator self.fl_tot correctly applied here?
                Adiag += beam_noise_part * (Yt_wM_Y)**2. * (self.fl_tot)**2.
            
            self.diag_noise_term.append(Adiag)
        
        ########################################################################
        # NOTES
        # compute_approximate_Yt_D_Y_diagonal computes diagonal
        # Compute diag of (Y^T_obs N^-1 Y_obs), and
        # diag of ()()
        # adjoint_analysis of m_lm gives diag. of (W M)
        # then use Yt_D_Y on the result.
        # Use 'outermost' l_max everywhere (where lmax is self.lmax == lmax^h)
        
        # Diagonal approx. for everything
        # diag(Y^T w M Y) = compute_approximate_Yt_D_Y_diagonal( adjoint_analysis(m_lm) )
        # (for lmax = lmax^h)
        ########################################################################

    def precompute_prior_term(self):
        """
        Precompute the prior operator, D_l, for this level.
        """
        MgLevel.precompute_prior_term(self)
        
        # Sum the (diagonal-approximated) prior and noise terms and invert to 
        # form approximate smoother
        w_J_diag = []
        for i in range(self.ncomp):
            # Add prior to noise term
            d_lm = 0.
            if self.dl[i] != 0:
                d_lm = scatter_l_to_lm(self.dl[i])
            buf = d_lm + self.diag_noise_term[i]
            
            # Apply window function
            wl = 1 if self.wl is None else scatter_l_to_lm(self.wl)
            w_J_diag.append( wl / buf )
            
        self.w_J_diag = np.concatenate(w_J_diag)

    def approx_solve_inplace(self, x, logger):
        """
        Apply diagonal SH smoother to system.
        """
        with log_done_task(logger, 'smoother-sh-diagonal'):
            x *= self.w_J_diag
    
    def __repr__(self):
        return 'ShLevel lmax=%d' % self.lmax

    def h5_group_name(self):
        return 'lmax=%d' % self.lmax


class ShSolveLevel(MgLevel):
    
    def __init__(self, lmax, **kw):
        """
        Pure spherical harmonic MG level with an exact Cholesky solver in place 
        of an approximate smoothing operator.
        """
        MgLevel.__init__(self, lmax=lmax, **kw)
        self.exact = True

    def precompute_noise_term(self):
        """
        Precompute the noise operator, B N^-1 B, for this level.
        """
        
        MgLevel.precompute_noise_term(self)
        lmax = self.lmax
        nside = nside_of(self.ninv[0])
        lmax_obs = 2 * lmax + 1
        f_lm = scatter_l_to_lm(self.fl_tot) # FIXME: Correct restr. operator?
        
        ########################################################################
        # FIXME: Just recompute the whole thing for multiple components
        ########################################################################
        
        # Empty block matrix operator
        self.block_A = [[0 for i in range(self.ncomp)] for j in range(self.ncomp)]
        
        for obs in range(self.nobs):
            for j in range(self.ncomp):
                lmax_mj = self.mixing_lmax[obs][j]
                b_lm = scatter_l_to_lm(self.beam[obs])
                
                # y = B_lm (Y^T w M_j Y) F_h
                y = sh_synthesis_gauss(lmax_mj, f_lm, lmax_sh=self.lmax)
                y *= self.mixing_maps[obs][j]
                y = sh_analysis_gauss(lmax_mj, y, lmax_sh=2*(lmax_mj+self.lmax))
                y *= b_lm
            
                # y' = Yobs^T N^-1 Yobs . y
                # FIXME: Does this sht_plan object have a high enough lmax?
                y = self.ninv_sht_plan[obs].synthesis(y)
                y *= self.ninv[obs]
                y = self.ninv_sht_plan[obs].adjoint_synthesis(y)
                
                for i in range(self.ncomp):
                    lmax_mi = self.mixing_lmax[obs][i]
                    yi = y.copy()
                    
                    # y'' = F_h (Y^T M_i w Y) B_lm . y'
                    yi *= b_lm
                    yi = sh_adjoint_analysis_gauss( lmax_mi, yi, 
                                                    lmax_sh=2*(lmax_mi+self.lmax) )
                    yi *= self.mixing_maps[obs][i]
                    yi = sh_adjoint_synthesis_gauss(lmax_mi, yi, lmax_sh=self.lmax)
                    yi *= f_lm
            
                    # Add contribution from this band
                    self.block_A[i][j] += yi

    def precompute_prior_term(self):
        """
        Precompute the prior operator, D_l, for this level.
        """
        MgLevel.precompute_prior_term(self)
        
        # Compute priors for each component and add to diagonal of block_A
        for i in range(self.ncomp):
            if self.dl[i] is not None:
                self.block_A[i][i] += scatter_l_to_lm(self.dl[i])
        
        # Pack operator matrix into one big array
        # (Assumes all blocks are the same shape)
        nx, ny = self.block_A[0][0].shape
        arr = np.zeros((self.ncomp*nx, self.ncomp*ny))
        for i in range(self.ncomp):
            for j in range(self.ncomp):
                arr[i*nx:(i+1)*nx, j*ny:(j+1)*ny] = self.block_A[i][j]
        
        # Factor resulting matrix
        self.factor = cho_factor(arr, lower=True, overwrite_a=False)
        
    
    def solve_inplace(self, u, f, logger):
        """
        Perform dense Cholesky solve.
        """
        u[:] = cho_solve(self.factor, f, overwrite_b=False)

    def approx_solve_inplace(self, x, logger):
        self.solve_inplace(x, x, logger)

    def matvec(self, u):
        return np.dot(self.A, u)

    def __repr__(self):
        return 'ShSolveLevel'
        

class PixelLevel(MgLevel):

    def __init__( self, nside, tilesize, lmax=None, ridge=None, omega=1, 
                  hugemem=False, accurate=False, **kw ):
        """
        SH level with sibling pixel level (where smoothing is performed).
        
        Parameters
        ----------
        
        nside : int
            Healpix NSIDE which defines the 'sibling' pixel level resolution.
        
        tilesize : int
            Divide the sphere into tiles of this size, to define a sparse block 
            smoothing operator.
            
            Only couplings between pixels in the same and neighbouring tiles 
            are used; couplings are therefore included in a radius of at least 
            'tilesize' pixels around every pixel.
        
        lmax : float
            Maximum l to use on this level.
        
        ridge : float
            Ridge adjustment to use on the pixel-space linear operator 
            \hat{A}_h when performing Incomplete Cholesky factorisation (ICC).
            
            Without a ridge adjustment, the factorisation usually fails, either 
            due to the sparse approximant of the full dense matrix ending up 
            non-positive-definite, or because of elements dropped during ICC. 
            See Sect. 3.5 of Seljebotn et al. (2013) for more information.
            
            If 'ridge' is not set, precompute_prior_term() will search for the 
            smallest ridge adjustment that makes the matrix positive definite 
            (this may take some time).
            
        omega : float
            Weight to apply to updated solution vector 'x' when converting back 
            from pixel -> SH space.
        
        hugemem : bool (False)
            If False, precompute_noise_term() will compute the block 
            representation of the beam, B, on the fly. This will save lots of 
            memory, but make the precomputation slower.
        
        accurate : bool (False)
            Sets the accuracy of 'nearest neighbour'-finding algorithm in 
            precompute_noise_term().
        """
        MgLevel.__init__(self, lmax=lmax, **kw)
        self.nside = nside
        
        # Set pixel-space restriction beam properties and smoother settings
        self.tilesize = tilesize
        self.ridge = ridge
        self.omega = omega
        self.hugemem = hugemem
        self.accurate = accurate
    
    def precompute_noise_term(self):
        """
        Precompute the noise operator, B N^-1 B, for this level. This must be 
        recomputed if the beam, mask, or noise map change. (The ICC must also 
        be recomputed; to do this, call precompute_prior_term() afterwards.)
        
        This method is very time-intensive (see "Time obs." column in Table 2 
        of Seljebotn et al. 2013), but should only need to be run once for a 
        given Commander 2 run.
        
        See the 'hugemem' and 'accurate' kwargs in the PixelLevel() constructor 
        for tuning options for this method.
        """
        
        """
        # HEALPIX
        sh_synthesis(nside, alm, out=None)
        sh_adjoint_synthesis(lmax, map, out=None)
        sh_analysis(lmax, map, out=None)
        sh_adjoint_analysis(nside, alm, out=None)
        
        # GAUSS-LEGENDRE
        sh_synthesis_gauss(lmax_grid, alm, out=None, lmax_sh=None)
        sh_analysis_gauss(lmax_grid, map, out=None, lmax_sh=None)
        sh_adjoint_analysis_gauss(lmax_grid, alm, out=None, lmax_sh=None)
        sh_adjoint_synthesis_gauss(lmax_grid, map, out=None, lmax_sh=None)
        """
        
        MgLevel.precompute_noise_term(self)
        self.nside_level = self.nside // self.tilesize # Nside of tile pattern
        
        # Define Nside of each part of operator
        nside_tile = self.nside // self.tilesize # Nside of tile pattern
        nside_obs = nside_of(self.ninv[0]) # Nside of observations
        nside_h = self.nside # Nside of restricted level
        nside_m = 2 * self.nside # Nside of mixing map
        # FIXME: Should it be 2*Nside_h, or 2*nside_level?
        
        # Get indices of neighbouring pixels for each block
        # (Only use lower triangle for symmetric matrices, but full pattern for 
        # non-symmetric matrices)
        self.indptr_lower, self.indices_lower = csc_neighbours_lower(self.nside_level)
        if self.accurate:
            indptr_full, indices_full = csc_24_neighbours(self.nside_level)
        else:
            indptr_full, indices_full = mirror_csc_indices(self.indptr_lower,
                                                           self.indices_lower)
        
        # Construct mixing map tiles on Healpix grid
        M_tiled = []
        for i in range(self.ncomp):
            # Handle components with no mixing map (e.g. CMB)
            if self.mixing_maps[obs][i] is None:
                M_tiled.append(None)
                continue
            
            # Construct tiled mixing map
            mixing_map = sh_synthesis(nside_m, self.mixing_maps[obs][i])
            _tiled = convert_ring_dp_to_tiled_sp(self.nside_level, mixing_map)
            M_tiled.append(_tiled)
        
        # Compute Bhat = Yobs B (Y_M)^T w (non-symmetric matrix)
        # FIXME: How to deal with the w?
        Bhat = compute_csc_beam_matrix(True, nside_obs, nside_m, nside_tile, 
                                       self.b_lm[obs], indptr_full, indices_full)
        Bhat_tiled = convert_ring_dp_to_tiled_sp(self.nside_level, Bhat)
        # FIXME: nside_level should be changed to whatever the value for that operator is
        
        # Compute Fhat = Y_M F_h (Y_h)^T (non-symmetric matrix)
        # FIXME: Is this f_lm or (fl_tot)_lm
        # FIXME: How to deal with the transpose?
        Fhat = compute_csc_beam_matrix(True, nside_m, nside_h, nside_tile,
                                       self.f_lm, indptr_full, self.indices_full)
        Fhat_tiled = convert_ring_dp_to_tiled_sp(self.nside_level, Fhat)
        
        
        # Calculate full RHS mixing term, (Bhat M Fhat)
        block_rhs = block_A_D_B( indptr_full, indices_full, 
                                 Bhat_tiled, Fhat_tiled, M_tiled[**INDEX**],
                                 indptr_full, indices_full )
        
        ############
        # OLD
        # Bhat = Y_obs . B . F_h . Y_h
        compute_csc_beam_matrix(
            symmetric,      False
            nside_left,     nside_obs
            nside_right,    self.nside
            nside_level,    self.nside_level
            dl,             self.beam[i].astype(np.float32)
            indptr,         indptr_full
            indices,        indices_full
            blocks          (opt)
        )
        
        # Dhat = Y_h F_h S^-1 F_h Y^T_h
        compute_csc_beam_matrix(
            symmetric,      True
            nside_left,     nside
            nside_right,    nside
            nside_level,    self.nside_level
            dl,             self.dl.astype(np.float32)
            indptr,         self.indptr_lower        
            indices,        self.indices_lower
            blocks          (opt)
        )
        
        ########################################################################
        # FIXME: Need to repack for error smoother. Vector has order:
        # (cmb, synch, dust, cmb, synch, dust, ...) "interleaved"
        # rather than:
        # (cmb, cmb, cmb, .... synch, synch, synch, ... dust, dust, dust...)
        # -> You keep the peanut sparsity pattern, but each block in the 
        # sparsity pattern is now 3x3x(Npx x Npx), rather than just (Npx x Npx)
        
        # For x_lm matrix of dimension [Ncomp, (lmax+1)^2]
        # To get into interleaved format, do:
        # (x_lm.T).flatten(), and then probably need to copy.
        
        # Interleaving code?
        xi = np.zeros(x.size)
        n = x.size / self.ncomp # Assumes all are the same size
        for i in range(self.ncomp):
            xi[i::self.ncomp] = x[i*n:(i+1)*n]
        
        # Need to worry about C/Fortran array ordering.
        # See copy("F")
        
        # Mixing map (w M) needs to be projected to Healpix grid, since this is 
        # the grid that the sparsity pattern is defined for.
        # N^-1 map should also be projected to a Healpix grid.
        
        # Need to implement At_D_B, which is a sparse matrix calculation for 
        # 
        # Old version:
        #                 not this -----v
        # At_D_A calculates: (Y B Y^T) N^-1 (Y B Y^T)
        #                      ^---- sparse ----^
        
        # How it currently works:
        # block_At_D_A(indptr_full, indices_full, B_blocks, 
        #              ninv_tiled, self.indptr_lower, self.indices_lower)
        #
        # ninv_tiled has already been blocked into tiles
        # indptr: Ntiles+1 elements
        # indices, indptr define the sparsity pattern. (lower half, or full 
        # pattern for non-sym. matrices, like the beam approx. matrix (Y_obs B Y^T)).
        # Blocks has shape (8, 8, N_nonzero)... Ni or B blocks?
        
        # Bhat = (Y_obs B Y^T)
        # Fhat = (Y F Y^T)
        # Need to write routine "block_At_D_B", which does:
        # Bhat . M . Fhat, where M is the mixing map (Y B Y and M should be 
        # pixelised on some Healpix grid, low res, but one step in Nside above 
        # the current level's effective grid resolution)
        # This needs to be an adjoint analysis
        
        # compute_At_D_A: Multiply diag. matrix D by sparse matrices A (A, A^T 
        # are the same) (A is nonsymmetric, output *is* symmetric)
        # compute_At_D_B: Multiply diag matrix by two different sparse matrices
        # (nonsymmetric A, B, and output)
        
        ########################################################################
        MgLevel.precompute_noise_term(self)
        self.nside_level = self.nside // self.tilesize
        nside_obs = nside_of(self.ninv[0])
        nobs = len(self.ninv)
        self.indptr_lower, self.indices_lower = csc_neighbours_lower(self.nside_level)

        # Noise term is null, so just return
        if np.all(np.array(self.ninv) == 0):
            self.Ni_blocks = 0
            return

        # Get neighbouring pixel indices (accuracy-dependent)
        if self.accurate:
            indptr_full, indices_full = csc_24_neighbours(self.nside_level)
        else:
            indptr_full, indices_full = mirror_csc_indices(self.indptr_lower,
                                                           self.indices_lower)
        
        # Precompute inv. noise blocks (and sum over observations)
        Ni_blocks = 0
        for i in range(len(self.ninv)):
            ninv_tiled = convert_ring_dp_to_tiled_sp(self.nside_level, self.ninv[i])

            # Compute B N^-1 B and blocks of B (strategy depends on memory 
            # availability; B will be computed on the fly if hugemem=False)
            if not self.hugemem:
                with timed('Computing B Ni B, blocks of B computed on the fly'):
                    Ni_blocks += compute_Bt_Ni_B(self.nside, nside_obs,
                                  self.nside_level, self.beam[i].astype(np.float32),
                                  ninv_tiled, self.indptr_lower, self.indices_lower)
            else:
                with timed('Computing B'):
                    B_blocks = compute_csc_beam_matrix(False, nside_obs, self.nside, 
                                  self.nside_level, self.beam[i].astype(np.float32), 
                                  indptr_full, indices_full)
                                  # FIXME: self.beam doesn't exist!?
                with timed('Assembling B Ni B'):
                    Ni_blocks += block_At_D_A(indptr_full, indices_full, B_blocks, 
                                  ninv_tiled, self.indptr_lower, self.indices_lower)
        self.Ni_blocks = Ni_blocks

    def precompute_prior_term(self):
        """
        Precompute the prior operator, D_l, for this level, using ICC 
        (Incomplete Cholesky factorisation).
        
        This must be recomputed every time the prior term (~1/C_l) changes. It 
        must also be recomputed if B N^-1 B changes, so as to update the ICC 
        (see precompute_noise_term() for more details).
        
        This method takes some time, but is nevertheless much faster than 
        precompute_noise_term(). See the 'ridge' kwarg in the PixelLevel() 
        constructor for tuning options for this method.
        """
        MgLevel.precompute_prior_term(self)
        nside = self.nside
        
        # Compute block S^-1 matrix
        with timed('Computing Si'):
            Si_blocks = compute_csc_beam_matrix(True, nside, nside, 
                          self.nside_level, self.dl.astype(np.float32),
                          self.indptr_lower, self.indices_lower)
        blocks = Si_blocks + self.Ni_blocks
        
        # Perform Incomplete Cholesky factorisation (ICC) of block matrix
        # (and search for minimal ridge adjustment to make system +ve definite)
        with timed('ICC(0)'):
            bmax = blocks.max()
            ridge = self.ridge
            if ridge is None:
                alpha_factor = 1.5
                try:
                    alpha, ncalls = probe_cholesky_ridging(self.indptr_lower,
                                      self.indices_lower, blocks, ridge=0, 
                                      eps_log10=.1)
                except NotImplementedError:
                    print self
                    raise
                self.probed_ridge = ridge = alpha_factor * alpha / bmax
                print '\t  Relative ridge', '%.2e' % self.probed_ridge
            block_incomplete_cholesky_factor(self.indptr_lower, self.indices_lower, 
                                             blocks, alpha=ridge * bmax)
        self.smoother_blocks = blocks

    def approx_solve_inplace(self, x_sh, logger):
        """
        Apply smoother on pixel level.
        """
        with log_done_task(logger, 'restrict to nside=%d' % self.nside):
            x = sh_synthesis(self.nside, x_sh)
        
        # Repacks to tiles
        x_tiled = convert_ring_dp_to_tiled_sp(self.nside_level, x)
        
        # FIXME: Repack components individually
        
        block_triangular_solve('N', self.indptr_lower, self.indices_lower,
                               self.smoother_blocks, x_tiled)
        block_triangular_solve('T', self.indptr_lower, self.indices_lower,
                               self.smoother_blocks, x_tiled)
        r_pix = self.omega * convert_tiled_sp_to_ring_dp(x_tiled)
        with log_done_task(logger, 'prolong from nside=%d' % self.nside):
            c = sh_adjoint_synthesis(self.lmax, r_pix)
        x_sh[:] = c
        return c

    def __repr__(self):
        return 'PixelLevel nside=%d, ICC(tilesize=%d)' % (self.nside, self.tilesize)

    def h5_group_name(self):
        return 'nside=%d,lmax=%d' % (self.nside, self.lmax)

