
import numpy as np
from ...sphere import sh_synthesis
from ...sphere.mmajor import scatter_l_to_lm

def sample_amplitude_dummy(sky):
    """
    Dummy amplitude sampler. Simply makes a realisation of the CMB using only
    the current Cl's (and no data) as constraint.
    
    Arguments
    ---------
    
    sky: SkyAnalysis object
        Samples are automatically added to the chain managed by this object.
    """
    
    # Get parameter names from CMB component (assumed the first component)
    name_amp = sky.components[0].parameters[0]
    name_cl = sky.components[0].parameters[1]
    
    cls = sky.chain[name_cl]
    c_lm = scatter_l_to_lm(cls)
    alm = np.sqrt(c_lm) * sky.rng.normal(size=c_lm.size)
    
    # Add sample to chain
    sky.chain.gibbs((name_amp,), [alm], info={'name':'sample_dummy_amplitude()'})
