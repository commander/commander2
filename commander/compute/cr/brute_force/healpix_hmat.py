from .pix_matrices import (domain_distance, aca_pixel_gauss_beam, bruteforce_pixel_gauss_beam)
from .hmatrix import DenseLeaf, UXV_leaf, ZeroLeaf
from .. import beams
from scipy import linalg
import numpy as np

class BeamHMatrixGenerator(object):
    """
    Domains are represented as tuples (nside, idx, size, virtual); where
    nside is the partition level, set to `None` for the initial
    12-pixel division. `virtual` is set to `True` during initial 12-splitting
    and for every half-step.

    The cluster tree always divides domains in 4 according to the
    HEALPix nested scheme, with left domain (d1) and right domain (d2)
    always have the same geometric size. `nside1` and `nside2` specifies
    the *sampling resolution* within that geometric size.
    """

    def __init__(self, nside1, nside2, minsize, fwhm, eps, k_max, eta_max):
        self.fwhm = fwhm
        self.nside1 = nside1
        self.nside2 = nside2
        self.eps = eps
        self.k_max = k_max
        self.eta_max = eta_max
        self.minsize = minsize
        self.beam_normalization = beams.gaussian_beam_normalization(fwhm)
        self.beam_cutoff = beams.gaussian_beam_angular_cutoff(fwhm, 1e-12)

    def left_root_domain(self):
        return (None, 1, 12*self.nside1**2, True)

    def right_root_domain(self):
        return (None, 1, 12*self.nside2**2, True)

    def is_admissible(self, d1, d2):
        nside1, ipix1, _, virtual1 = d1
        nside2, ipix2, _, virtual2 = d2
        if virtual1 or virtual2:
            return False, None
        else:
            dist, rho = domain_distance(nside1, ipix1, nside2, ipix2)
            if dist == 0:
                return False, None
            else:
                eta = rho / dist
                if eta < self.eta_max:
                    return True, (dist, eta)
                else:
                    return False, None

    def generate_compressed(self, d1, d2, params):
        dist, eta = params
        if dist > self.beam_cutoff:
            return ZeroLeaf(self.domain_size(d1), self.domain_size(d2))

        d1tup = (self.nside1,) + d1[:2]
        d2tup = (self.nside2,) + d2[:2]
        
        U, V = aca_pixel_gauss_beam(self.beam_normalization,
                                    self.fwhm, d1tup, d2tup, self.k_max, self.eps, eta)
        if U.shape[1] == self.k_max:
            print d1, d2, eta
        UQ, UR = linalg.qr(U, mode='economic')
        VQ, VR = linalg.qr(V, mode='economic')
        X = np.dot(UR, VR.T)
        # optional: svd...
        return UXV_leaf(UQ, X, VQ)

    def generate_exact(self, d1, d2):
        #return np.zeros((d1.size, d2.size))
        d1tup = (self.nside1,) + d1[:2]
        d2tup = (self.nside2,) + d2[:2]
        block = bruteforce_pixel_gauss_beam(self.beam_normalization,
                                            self.fwhm, d1tup, d2tup)
        return DenseLeaf(block)

    def split_domain(self, d):
        """Get children of domain `d`"""
        nside, idx, size, virtual = d

        if virtual and nside is None:
            # Is in initial splitting of 12 base-pixels; we use
            # the heap indexing scheme for the nodes involved, root is 1.
            #
            # Using the notation (idx lchild rchild), B means a HEALPix
            # basepixel, the initial tree is
            # 
            # (1 (2
            #      (4 (8 B B) (9 B B))
            #      (5 (10 B B) (11 B B))
            #    (3
            #      (6 (12 B B) (13 B B))
            #      (7 <is empty>))))
            if idx == 1:
                # root, split in 8/12 and 4/12
                return [(None, 2, 8 * size // 12, True),
                        (None, 3, 4 * size // 12, True)]
            elif idx == 3:
                # single-child node
                return [(None, 6, size, True), None]
            elif idx < 8:
                # inner nodes in heap ordering
                return [(None, 2 * idx, size // 2, True),
                        (None, 2 * idx + 1, size // 2, True)]
            else:
                # node's 2 children represents base pixels
                basepixduo = idx - 8
                return [(1, 2 * basepixduo, size // 2, False),
                        (1, 2 * basepixduo + 1, size // 2, False)]

        elif virtual and nside is not None:
            # In the half-step towards new nside, simply split again
            return [(nside, 2 * idx, size // 2, False),
                    (nside, 2 * idx + 1, size // 2, False)]
        elif size // 2 < self.minsize:
            return []
        else:
            # create half-step towards new nside
            return [(2 * nside, 2 * idx, size // 2, True),
                    (2 * nside, 2 * idx + 1, size // 2, True)]
        
    def domain_size(self, d):
        return d[2]
