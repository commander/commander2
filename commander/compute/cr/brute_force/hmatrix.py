from __future__ import division
import numpy as np
from scipy import linalg
from numpy import dot


#
# Utils
#

registry = {}

# Guido's 5-minute multi-method
class MultiMethod(object):
    def __init__(self, name):
        self.name = name
        self.typemap = {}
    def __call__(self, *args):
        types = tuple(arg.__class__ for arg in args)
        function = self.typemap.get(types)
        if function is None:
            raise TypeError("%s can not be called with types %r" % (self.name, types))
        return function(*args)
    def register(self, types, function):
        if types in self.typemap:
            raise TypeError("duplicate registration")
        self.typemap[types] = function

def multimethod(*types):
    def register(function):
        function = getattr(function, "__lastreg__", function)
        name = function.__name__
        mm = registry.get(name)
        if mm is None:
            mm = registry[name] = MultiMethod(name)
        mm.register(types, function)
        mm.__lastreg__ = function
        return mm
    return register

def first_less_than(a, eps):
    i = (a < eps).nonzero()[0]
    if i.shape[0] == 0:
        return a.shape[0]
    else:
        return i[0]

def s_rank(s, eps):
    return first_less_than(s, eps * s[0])

def dots(*mats):
    l = mats[0]
    for r in mats[1:]:
        l = dot(l, r)
    return l

def hsplit(mat, k):
    return mat[:, :k], mat[:, k:]

def vsplit(mat, k):
    return mat[:k, :], mat[k:, :]


#
# Types
#

class BaseNode(object):

    def __repr__(self):
        return '<%s %dx%d>' % (self.__class__.__name__, self.shape[0], self.shape[1])

    def treesize(self):
        return 1

    def as_dense(self, out=None):
        if out is None:
            out = np.empty(self.shape)
        self._compute_dense(out)
        return out

    def matvec(self, x, accumulate_to=None):
        if accumulate_to is None:
            accumulate_to = np.zeros((self.shape[0], x.shape[1]))
        self._matvec_add(x, accumulate_to)
        return accumulate_to

    def add_At_D_A_dense(self, d, out):
        """
        Compute the dense At * D * A matrix and adds it to out
        """
        dense = np.zeros(self.shape)
        self.compute_dense(dense)
        out[:, :] += np.dot(dense.T, d[:, None] * dense)

    def add_At_D_A_block_diagonal(self, blocksize, d, out):
        """
        Compute the diagonal blocks of of At * D * A for some diagonal
        matrix D and A=self, i.e.::

            R_i = \sum_k A_{k,i}^t D_k A_{k,i}

        The computation is done in a simplistic fashion where
        blocks smaller than blocksize are simply converted to
        dense blocks; i.e., there is no hierarchical matmul.
        This routine accumulates the contribution of the node to
        ``out[k,:,:]`` for various k.

        Cases:

         * If `self.shape[1] < blocksize`, then it is an error,
           because as_dense should have been called instead.

         * If `self.shape[1] == blocksize`, call `as_dense`

         * Otherwise, produce block-row-wise contributions to `diagonal`
        """
        if self.shape[1] < blocksize:
            raise AssertionError("blocksize didn't exactly match a level in partition")
        elif self.shape[1] == blocksize:
            assert out.shape[0] == 1
            self.add_At_D_A_dense(d, out[0, :, :])
        else:
            self._add_At_D_A_block_diagonal(blocksize, d, out)

    def _add_At_D_A_block_diagonal(self, blocksize, d, out):
        assert False, (type(self), blocksize, self.shape)

class ZeroLeaf(BaseNode):
    def __init__(self, ncols, nrows):
        self.shape = (ncols, nrows)

    @staticmethod
    def read_from_stream(stream):
        padding, height, width = struct.unpack('=iii', stream.read(12))
        return ZeroLeaf(height, width)

    def serialize(self, stream):
        stream.write(struct.pack('=iiii', ZERO_ENUM, 0, self.shape[0], self.shape[1]))

    def nnz(self):
        return 0

    def transpose(self):
        return ZeroLeaf(self.shape[1], self.shape[0])

    def _matvec_add(self, u, out):
        pass

    def _compute_dense(self, out):
        out[:,:] = 0

    def add_At_D_A_block_diagonal(self, blocksize, d, out):
        # note lack of _, does not check for matching the partition
        pass

    def as_UXV(self, eps):
        return UXV_leaf(
            np.zeros((self.shape[0], 0)),
            np.zeros((0, 0)),
            np.zeros((self.shape[1], 0)))

itemsize_to_dtype = {4: np.float32, 8: np.float64}

class DenseLeaf(BaseNode):
    def __init__(self, block):
        self.shape = block.shape
        self.block = block

    @staticmethod
    def read_from_stream(stream):
        itemsize, nrows, ncols = struct.unpack('=iii', stream.read(12))
        data = np.fromfile(stream, itemsize_to_dtype[itemsize], nrows * ncols)
        block = data.reshape((nrows, ncols), order='F')
        node = DenseLeaf((nrows, ncols))
        node.set_data(block)
        return node

    def serialize(self, stream):
        stream.write(struct.pack('=iiii', DENSE_ENUM, self.block.itemsize,
                     self.shape[0], self.shape[1]))
        assert self.block.flags.f_contiguous
        stream.write(buffer(self.block))

    def as_dense(self):
        return self

    def nnz(self):
        return np.prod(self.block.shape)

    def transpose(self):
        return DenseLeaf(self.block.T)

    def _matvec_add(self, u, out):
        out[:,:] += np.dot(self.block, u)

    def _compute_dense(self, out):
        out[:,:] = self.block

    def add_At_D_A_dense(self, d, out):
        # save the extra copy of going through compute_dense
        out[:,:] += np.dot(self.block.T, d[:, None] * self.block)

    def as_UXV(self, eps):
        U, s, Vt = np.linalg.svd(self.block, full_matrices=False)
        k = s_rank(s, eps)
        X = np.diag(s[:k])
        return UXV_leaf(U[:, :k], X, Vt.T[:, :k])


class UXV_leaf(BaseNode):

    def __init__(self, U, X, V):
        assert U.shape[1] == X.shape[0]
        assert X.shape[1] == V.shape[1]

        self.shape = (U.shape[0], V.shape[0])
        self.U = U
        self.X = X
        self.V = V
        self.rank = U.shape[1]

    @staticmethod
    def read_from_stream(stream):
        1/0 # TODO: X
        rank, nrows, ncols = struct.unpack('=iii', stream.read(12))
        data = np.fromfile(stream, np.float32, nrows * rank)
        U = data.reshape((nrows, rank), order='F')
        data = np.fromfile(stream, np.float32, ncols * rank)
        V = data.reshape((ncols, rank), order='F')
        node = CompressedLeaf((nrows, ncols))
        node.set_data(U, V)
        return node

    def serialize(self, stream):
        stream.write(struct.pack('=iiii', UXV_ENUM, self.rank,
                     self.shape[0], self.shape[1]))
        assert self.U.flags.f_contiguous
        assert self.V.flags.f_contiguous
        stream.write(buffer(self.U))
        stream.write(buffer(self.V))

    def nnz(self):
        return np.prod(self.U.shape) + np.prod(self.V.shape)

    def transpose(self):
        return UXV_leaf(self.V, self.X.T, self.U)

    def _matvec_add(self, u, out):
        out[:, :] += dot(self.U, dot(self.X, dot(self.V.T, u)))

    def _compute_dense(self, out):
        out[:, :] = dots(self.U, self.X, self.V.T)

    def as_UXV(self, eps):
        return self

    def _add_At_D_A_block_diagonal(self, blocksize, d, out):
        1/0 # TODO X
        assert self.shape[1] % blocksize == 0
        U, V = self.U, self.V
        Ut_D_U = np.dot(U.T, d[:, None] * U)
        for t in range(self.shape[1] // blocksize):
            i = t * blocksize
            V_chunk = self.V[i:i + blocksize, :]
            out[t,:,:] += dots(V_chunk, Ut_D_U, V_chunk.T)



INNER_ENUM, UXV_ENUM, DENSE_ENUM, ZERO_ENUM = range(4)

class Node(BaseNode):
    """
    Nodes can have `None` children which represents 'leaf node that is
    not computed'.
    """
    
    def __init__(self, m, n, vsplit, hsplit):
        self.shape = (m, n)
        self.vsplit = vsplit
        self.hsplit = hsplit
        self.a = self.b = self.c = self.d = None

    @staticmethod
    def create_like(a):
        return Node(a.shape[0], a.shape[1], a.vsplit, a.hsplit)

    @staticmethod
    def from_blocks(blocks):
        a, b, c, d = blocks
        node = Node(a.shape[0] + c.shape[0], a.shape[1] + b.shape[1], a.shape[0], a.shape[1])
        node.a = a
        node.b = b
        node.c = c
        node.d = d
        return node

    @staticmethod
    def read_from_stream(stream):
        stream.read(4) # padding
        children = [load_from_stream(stream) for _ in range(4)]
        return Node(children)

    def set(self, i, j, node):
        if (i, j) == (0, 0):
            self.a = node
        elif (i, j) == (0, 1):
            self.b = node
        elif (i, j) == (1, 0):
            self.c = node
        elif (i, j) == (1, 1):
            self.d = node

    def get(self, i, j):
        if (i, j) == (0, 0):
            return self.a
        elif (i, j) == (0, 1):
            return self.b
        elif (i, j) == (1, 0):
            return self.c
        elif (i, j) == (1, 1):
            return self.d

    def __getitem__(self, index):
        i, j = index
        return self.get(i, j)

    def __setitem__(self, index, value):
        i, j = index
        self.set(i, j, value)

    def serialize(self, stream):
        stream.write(struct.pack('=ii', INNER_ENUM, 0))
        for child in self.children:
            child.serialize(stream)

    def nnz(self):
        s = 0
        for block in [self.a, self.b, self.c, self.d]:
            s += block.nnz()
        return s

    def transpose(self):
        r = Node(self.shape[1], self.shape[0], self.hsplit, self.vsplit)
        r.a = self.a.transpose()
        r.b = self.c.transpose()
        r.c = self.b.transpose()
        r.d = self.d.transpose()
        return r

    def _matvec_add(self, u, out):
        vs, hs = self.vsplit, self.hsplit
        self.a._matvec_add(u[:hs], out[:vs, :])
        self.b._matvec_add(u[hs:], out[:vs, :])
        self.c._matvec_add(u[:hs], out[vs:, :])
        self.d._matvec_add(u[hs:], out[vs:, :])
        
    def treesize(self):
        return sum(child.treesize() for child in [self.a, self.b, self.c, self.d])
        
    def _compute_dense(self, out):
        vs, hs = self.vsplit, self.hsplit
        self.a._compute_dense(out[:vs, :hs])
        self.b._compute_dense(out[:vs, hs:])
        self.c._compute_dense(out[vs:, :hs])
        self.d._compute_dense(out[vs:, hs:])

    def _add_At_D_A_block_diagonal(self, blocksize, d, out):
        assert self.hsplit % blocksize == 0
        t = self.hsplit // blocksize
        vs = self.vsplit
        self.a.add_At_D_A_block_diagonal(blocksize, d[:vs], out[:t, :, :])
        self.b.add_At_D_A_block_diagonal(blocksize, d[:vs], out[t:, :, :])
        self.c.add_At_D_A_block_diagonal(blocksize, d[vs:], out[:t, :, :])
        self.d.add_At_D_A_block_diagonal(blocksize, d[vs:], out[t:, :, :])

    def agglomerate(self, eps):
        # Follow Bebendorf notation; [A1, A2; A3 A4]
        A1 = self.a.as_UXV(eps)
        A2 = self.b.as_UXV(eps)
        A3 = self.c.as_UXV(eps)
        A4 = self.d.as_UXV(eps)

        # Let self.shape == ((m1 + m3), (n1 + n2)), and k1,k2,k3,k4 be ranks
        # of sub-blocks.
        m1, n1 = A1.shape[0], A1.shape[1]
        m3 = A3.shape[0]
        n2 = A2.shape[1]

        k1, k2, k3, k4 = A1.X.shape[0], A2.X.shape[0], A3.X.shape[0], A4.X.shape[0]
        assert (k1, k2, k3, k4) == (A1.U.shape[1], A2.U.shape[1], A3.U.shape[1], A4.U.shape[1])
        assert (k1, k2, k3, k4) == (A1.X.shape[1], A2.X.shape[1], A3.X.shape[1], A4.X.shape[1])
        assert (k1, k2, k3, k4) == (A1.V.shape[1], A2.V.shape[1], A3.V.shape[1], A4.V.shape[1])

        def qr_hstack(u1, u2):
            if u1.size == 0:
                return u2, np.eye(u2.shape[1])
            elif u2.size == 0:
                return u1, np.eye(u1.shape[1])
            else:
                return linalg.qr(np.hstack([u1, u2]), mode='economic')
        
        Q1, R1 = qr_hstack(A1.U, A2.U)
        Q2, R2 = qr_hstack(A3.U, A4.U)
        Q3, R3 = qr_hstack(A1.V, A3.V)
        Q4, R4 = qr_hstack(A2.V, A4.V)

        R1p, R1pp = hsplit(R1, k1)
        R2p, R2pp = hsplit(R2, k3)
        R3p, R3pp = hsplit(R3, k1)
        R4p, R4pp = hsplit(R4, k2)

        Xh1 = dot(dot(R1p, A1.X), R3p.T)
        Xh2 = dot(dot(R1pp, A2.X), R4p.T)
        Xh3 = dot(dot(R2p, A3.X), R3pp.T)
        Xh4 = dot(dot(R2pp, A4.X), R4pp.T)
        Xh = np.vstack([np.hstack([Xh1, Xh2]), np.hstack([Xh3, Xh4])])

        XU, XX, XV = linalg.svd(Xh)
        i = s_rank(XX, eps)
        XU = XU[:, :i]
        XX = XX[:i]
        XV = XV.T[:, :i]

        Uleft = np.vstack([
            np.hstack([Q1, np.zeros((Q1.shape[0], Q2.shape[1]))]),
            np.hstack([np.zeros((Q2.shape[0], Q1.shape[1])), Q2])
            ])

        Vright = np.vstack([
            np.hstack([Q3, np.zeros((Q3.shape[0], Q4.shape[1]))]),
            np.hstack([np.zeros((Q4.shape[0], Q3.shape[1])), Q4])
            ])

        Q = np.dot(Uleft, XU)
        V = np.dot(Vright, XV)
        return UXV_leaf(Q, np.diag(XX), V)

    def as_UXV(self, eps):
        agg = self.agglomerate(eps)
        return UXV_leaf(agg.U, agg.X, agg.V)



enum_to_class = {
    INNER_ENUM: Node,
    UXV_ENUM: UXV_leaf,
    DENSE_ENUM: DenseLeaf,
    ZERO_ENUM: ZeroLeaf}






#
# Arithmetic
#

def dense_add(A, B):
    return DenseLeaf(A.block + B.block)


@multimethod(DenseLeaf, DenseLeaf, float)
def rounded_add(a, b, eps):
    return DenseLeaf(a.block + b.block)

@multimethod(Node, Node, float)
def rounded_add(a, b, eps):
    if a.vsplit == b.vsplit and a.hsplit == b.hsplit:
        # Two matrices with same partition
        result = Node.create_like(a)
        for i in range(2):
            for j in range(2):
                result.set(i, j, rounded_add(a.get(i, j), b.get(i, j), eps))
        return result
    else:
        raise NotImplementedError()
    
@multimethod(UXV_leaf, UXV_leaf, float)
def rounded_add(A, B, eps):
    def qr_merge(Ax, Bx):
        Zx = dot(Ax.T, Bx)
        Yx = Bx - dot(Ax, Zx)
        q, r = linalg.qr(Yx, mode='economic')
        return Zx, q, r

    Zu, Qu, Ru = qr_merge(A.U, B.U)
    Zv, Qv, Rv = qr_merge(A.V, B.V)

    Xh1 = A.X + dots(Zu, B.X, Zv.T)
    Xh2 = dots(Zu, B.X, Rv.T)
    Xh3 = dots(Ru, B.X, Zv.T)
    Xh4 = dots(Ru, B.X, Rv.T)
    Xh = np.vstack([np.hstack([Xh1, Xh2]), np.hstack([Xh3, Xh4])])
    XU, XX, XV = linalg.svd(Xh)
    i = s_rank(XX, eps)
    XU = XU[:, :i]
    XX = XX[:i]
    XV = XV.T[:, :i]
    UU = dot(np.hstack([A.U, Qu]), XU)
    VV = dot(np.hstack([A.V, Qv]), XV)
    return UXV_leaf(UU, np.diag(XX), VV)

@multimethod(DenseLeaf, UXV_leaf, float)
def rounded_add(A, B, eps):
    # Convert UXV to dense
    return DenseLeaf(A.block + B.as_dense())

@multimethod(Node, UXV_leaf, float)
def rounded_add(A, B, eps):
    # Need to (temporarily) refine B; note that the buffers are reused
    v, h = A.vsplit, A.hsplit
    U1, U2 = vsplit(B.U, v)
    V1, V2 = vsplit(B.V, h)
    X = B.X
    B1 = UXV_leaf(U1, X, V1)
    B2 = UXV_leaf(U1, X, V2)
    B3 = UXV_leaf(U2, X, V1)
    B4 = UXV_leaf(U2, X, V2)
    B_refined = Node.from_blocks([B1, B2, B3, B4])
    return rounded_add(A, B_refined, eps)

@multimethod(ZeroLeaf, ZeroLeaf, float)
@multimethod(ZeroLeaf, DenseLeaf, float)
@multimethod(ZeroLeaf, UXV_leaf, float)
@multimethod(ZeroLeaf, Node, float)
def rounded_add(A, B, eps):
    return B

@multimethod(DenseLeaf, ZeroLeaf, float)
@multimethod(UXV_leaf, ZeroLeaf, float)
@multimethod(Node, ZeroLeaf, float)
def rounded_add(A, B, eps):
    return A

# All transpose cases
@multimethod(UXV_leaf, DenseLeaf, float)
@multimethod(UXV_leaf, Node, float)
def rounded_add(A, B, eps):
    return rounded_add(B.transpose(), A.transpose(), eps).transpose()

@multimethod(DenseLeaf, DenseLeaf, float)
def matmul(A, B, eps):
    return DenseLeaf(dot(A.block, B.block))

@multimethod(UXV_leaf, UXV_leaf, float)
def matmul(A, B, eps):
    X = dots(A.X, dot(A.V.T, B.U), B.X)
    return UXV_leaf(A.U, X, B.V)

@multimethod(DenseLeaf, UXV_leaf, float)
@multimethod(Node, UXV_leaf, float)
def matmul(A, B, eps):
    A_Bu = A.matvec(B.U)
    Q, R = linalg.qr(A_Bu, mode='economic')
    return UXV_leaf(Q, dot(R, B.X), B.V)

@multimethod(Node, Node, float)
def matmul(A, B, eps):
    res = Node(A.shape[0], B.shape[1], A.vsplit, B.hsplit)
    for i in (0, 1):
        for j in (0, 1):
            C0 = matmul(A[i, 0], B[0, j], eps)
            C1 = matmul(A[i, 1], B[1, j], eps)
            res[i, j] = rounded_add(C0, C1, eps)
    return res

@multimethod(ZeroLeaf, ZeroLeaf, float)
@multimethod(ZeroLeaf, DenseLeaf, float)
@multimethod(ZeroLeaf, UXV_leaf, float)
@multimethod(ZeroLeaf, Node, float)
@multimethod(DenseLeaf, ZeroLeaf, float)
@multimethod(UXV_leaf, ZeroLeaf, float)
@multimethod(Node, ZeroLeaf, float)
def matmul(A, B, eps):
    return ZeroLeaf(A.shape[0], B.shape[1])

@multimethod(UXV_leaf, DenseLeaf, float)
@multimethod(UXV_leaf, Node, float)
def matmul(A, B, eps):
    return matmul(B.transpose(), A.transpose(), eps).transpose()



#
# Persistence
#
        
def load_from_stream(stream):
    typeenum, = struct.unpack('=i', stream.read(4))
    return enum_to_class[typeenum].read_from_stream(stream)



#
# Generation
#
def _generate_and_set_leaf(node, i, j, genfunc, *args):
    leaf = genfunc(*args)
    node.set(i, j, leaf)

def _generate_h_matrix(gen, parent_node, i, j, d1, d2, executor):
    admissible, params = gen.is_admissible(d1, d2)
    if admissible:
        executor.submit(_generate_and_set_leaf, parent_node, i, j,
                        gen.generate_compressed, d1, d2, params)
    else:
        children1 = gen.split_domain(d1)
        children2 = gen.split_domain(d2)
        if len(children1) == len(children2) == 0:
            executor.submit(_generate_and_set_leaf, parent_node, i, j,
                            gen.generate_exact, d1, d2)
        else:
            m, n = gen.domain_size(d1), gen.domain_size(d2)

            def fix_children(d, lst):
                if len(lst) == 0:
                    return [d, None]
                elif len(lst) == 1:
                    return [lst[0], None]
                else:
                    return lst

            children1 = fix_children(d1, children1)
            children2 = fix_children(d2, children2)

            vsplit = gen.domain_size(children1[0])
            hsplit = gen.domain_size(children2[0])
            node = Node(m, n, vsplit, hsplit)
            parent_node.set(i, j, node)
            for ip, child1 in enumerate(children1):
                for jp, child2 in enumerate(children2):
                    if child1 is not None and child2 is not None:
                        _generate_h_matrix(gen, node, ip, jp, child1, child2, executor)
                    else:
                        leaf = ZeroLeaf(0 if child1 is None else gen.domain_size(child1),
                                        0 if child2 is None else gen.domain_size(child2))
                        node.set(ip, jp, leaf)
            return node

class RootNodeFuture(object):
    def __init__(self):
        self.root_node = None

    def set(self, i, j, value):
        assert i == j == 0
        self.root_node = value

def generate_h_matrix(gen, d1, d2, executor):
    future = RootNodeFuture()
    _generate_h_matrix(gen, future, 0, 0, d1, d2, executor)
    return future


#
# Plotting
#

def h_matrix_to_svg(filename, hmatrix, minsize):
    def draw_block(i, j, node, nlevels):
        if nlevels == 0:
            return
        n, m = node.shape
        sw = 1
        x = j * sx
        y = i * sy
        w = m * sx
        h = n * sy
        if isinstance(node, (DenseLeaf, UXV_leaf)):
            if isinstance(node, DenseLeaf):
                fill = "red"
                k = None #min(*node.shape)
            else:
                k = node.rank
                fill = "white"
                
            doc.write('<rect fill="{fill}" stroke="black" stroke-width="1" '
                      'height="{h}" width="{w}" x="{x}" y="{y}" />\n'.format(**locals()))
            gsx = w / width
            gsy = h / height
            if k is not None:
                tx = x
                ty = y + .9 * h
                fs = w * .5
                doc.write('<text x="{tx}" y="{ty}" font-weight="normal" font-family="sans-serif" letter-spacing="0" font-size="{fs}">{k}</text>\n'.format(**locals()))
        elif isinstance(node, ZeroLeaf):
            pass
        else:
            assert isinstance(node, Node)
            vs, hs = node.vsplit, node.hsplit
            draw_block(i, j, node.a, nlevels - 1)
            draw_block(i, j + hs, node.b, nlevels - 1)
            draw_block(i + vs, j, node.c, nlevels - 1)
            draw_block(i + vs, j + hs, node.d, nlevels - 1)

    n, m = hmatrix.shape
    width = min(40 * n / minsize, 3000)
    height = width * n / m
    sx = sy = width / m
    
    doc = open(filename, 'w')
    doc.write('<?xml version="1.0" encoding="utf-8" ?>\n'
              '<svg baseProfile="full" height="{height}" width="{width}" '
              'version="1.1" xmlns="http://www.w3.org/2000/svg">\n'.format(**locals()))
    draw_block(0, 0, hmatrix, 10000)
    doc.write("</svg>\n")
    doc.close()
