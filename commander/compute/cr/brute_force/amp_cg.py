from __future__ import division

import numpy as np
import oomatrix as om

from . import matrices, signal
from .memoize import memoize
from .logger_utils import log_after_task
from .sphere import sh_integrals, mmajor

from collections import namedtuple

from matplotlib import pyplot as plt

def getattrs(obj, names):
    return [getattr(obj, name) for name in names.split()]

#
# memoized results (used more than one place)
#

@memoize()
def compute_masked_inverse_noise_map(ctx, obs):
    rms = ctx.get_TT_rms_map(obs)
    mask = ctx.get_mask_map(obs)
    return mask / rms**2

def compute_Ni(ctx, obs):
    return om.diagonal_matrix(compute_masked_inverse_noise_map(ctx, obs), 'Ni_%s' % obs.name)

def next_power_of_2(k):
    return 2**int(np.log2(k))

@memoize('Yh_Ni_Y', tags=['disk'])
def compute_Yh_Ni_Y(ctx, obs, lmax):
    from .preconditioner.mblocks import compute_Yh_D_Y_healpix
    inverse_noise_map = compute_masked_inverse_noise_map(ctx, obs)
    return compute_Yh_D_Y_healpix(lmax, lmax, inverse_noise_map)

def compute_masked_inverse_noise_map_sh(ctx, obs, lmax):
    YhW = ctx.sh_analysis_matrix(obs.nside, 0, lmax)
    map = compute_masked_inverse_noise_map(ctx, obs)
    return om.compute_array(YhW * map[:, None])[:, 0]

@memoize('diagonal_Bh_Ni_B', tags=['disk'])
def compute_diagonal_Bh_Ni_B(ctx, obs, lmin, lmax):
    inv_noise_map = compute_masked_inverse_noise_map_sh(ctx, obs, 2 * lmax)
    buf = sh_integrals.compute_approximate_Yt_D_Y_diagonal(obs.npix, lmin, lmax, inv_noise_map)
    B = ctx.get_symmetric_beam_and_pixel_window_matrix(obs, lmin, lmax)
    beam_lm = B.diagonal()
    return buf * beam_lm**2
    

#
# formulas
#
def get_F_D(ctx, component, observations, lmin=0, lmax=None):
    S, mu = component.get_amplitude_prior(ctx, observations, lmin=lmin, lmax=lmax)
    if mu is not None:
        raise NotImplementedError()
    if S is None:
        # Infinite-variance prior -> 0s on D and 1s on F
        assert not isinstance(component, signal.SphericalHarmonicSignal)
        n = component.get_amplitude_count(ctx, observations)
        F = om.identity_matrix(n)
        D = om.zero_matrix(n)
    else:
        F = om.compute(S.f, name='F_%s' % component.name)
        D = om.identity_matrix(F.ncols)
    return F, D

def compute_B_YhW_M_Y(ctx, comp, obs, lmax_inner, lmax):
    # need frequency content up to 2 * lmax_inner, and need nside=lmax
    nside_mixing = next_power_of_2(2 * lmax_inner)

    B = ctx.get_symmetric_beam_and_pixel_window_matrix(obs, 0, lmax_inner, 'B_%s' % obs.name)

    lmax_pix = comp.lmax
    gauss_mixing_map = comp.get_gauss_mixing_map(ctx, obs, True) # get it with weights
    if np.isscalar(gauss_mixing_map):
        S = matrices.lselection_matrix((0, lmax_inner + 1), (0, lmax + 1))
        C = om.diagonal_matrix(np.ones(S.ncols) * gauss_mixing_map)
        P_sh = B * S * C
    else:
        from .preconditioner.mblocks import compute_Yh_D_Y_gauss

        block = compute_Yh_D_Y_gauss(lmax_pix, lmax_inner, lmax, gauss_mixing_map)
        P_sh = om.compute(B * om.as_matrix(block))

    return P_sh

@memoize('dense_lhs')
def compute_dense_lhs(ctx, signal_components, observations, lmax):
    logger = ctx.get_logger('precond.low-l')

    # First form all dense M_sh; only use with a (2*lprecond, lprecond) block of each
    # which in experiments was good to 1-2%.
    lmax_inner = min([2 * lmax] + [obs.lmax_beam for obs in observations])

    Yh_Ni_Y_list = []
    for obs in observations:
        with log_after_task(logger, 'Computed [Y.h * Ni_%s * Y] in {dt}' % obs.name):
            Yh_Ni_Y_list.append(om.as_matrix(compute_Yh_Ni_Y(ctx, obs, lmax_inner)))
    Ni_list = [compute_Ni(ctx, obs) for obs in observations]            

    # Get projection matrices for each (comp, obs) combination
    # We define P = Y * P_sh; each component may provide either one depending
    # on what is the best match. Then when we assemble below we combine that
    # with Yh_Ni_Y or Ni in order to be able to reuse the Yh_Ni_Y blocks
    # (since we often don't want to store P, only P_sh)
    P_matrices = np.empty((len(signal_components), len(observations)), object)
    Psh_matrices = np.empty((len(signal_components), len(observations)), object)
    for i, comp in enumerate(signal_components):
        for j, obs in enumerate(observations):
            with log_after_task(logger, 'Computed P_%s_%s in {dt}' % (obs.name, comp.name)):
                if isinstance(comp, signal.SphericalHarmonicSignal):
                    P_sh = compute_B_YhW_M_Y(ctx, comp, obs, lmax_inner, lmax)
                    Psh_matrices[i, j] = P_sh
                else:
                    P = comp.get_single_projection_matrix(ctx, observations, j)
                    P_matrices[i, j] = P

    sizes = [mmajor.lm_count(0, lmax) if isinstance(comp, signal.SphericalHarmonicSignal)
             else comp.get_amplitude_count(ctx, observations)
             for comp in signal_components]
    offsets = np.concatenate([[0], np.cumsum(sizes)[:-1]])
    n = sum(sizes)
    out = np.zeros((n, n))

    with log_after_task(logger, 'Assembled low-l LHS in {dt}'):
        for i, (row, height) in enumerate(zip(offsets, sizes)):
            for ip, (col, width) in enumerate(zip(offsets, sizes)):
                if ip <= i:
                    # Compute for lower-left & diagonal
                    F, D = get_F_D(ctx, signal_components[i], observations, 0, lmax)
                    Fp, Dp = get_F_D(ctx, signal_components[ip], observations, 0, lmax)

                    block = 0
                    for j, obs in enumerate(observations):
                        P, P_sh = P_matrices[i, j], Psh_matrices[i, j]
                        Pp, Pp_sh = P_matrices[ip, j], Psh_matrices[ip, j]
                        if P is Pp is None:
                            A = P_sh.h * Yh_Ni_Y_list[j] * Pp_sh
                            hammer = False
                        else:
                            # At least one coefficient in pixel-domain. If any component
                            # does not have a pixel-domain projection, we multiply with Y --
                            # which will have the effect of pulling the pixel-domain P through
                            # into spherical-harmonic before the multiplication
                            Y = ctx.sh_synthesis_matrix(obs.nside, 0, lmax_inner)
                            if P is None:
                                P = Y * P_sh
                            if Pp is None:
                                Pp = Y * Pp_sh
                            A = P.h * Ni_list[j] * Pp
                            hammer = True
                        from oomatrix.computation import ImpossibleOperationError
                        logger.info('Assembling (%s,%s) for Ni_%s' % (signal_components[i].name,
                                                                      signal_components[ip].name,
                                                                      obs.name))
                        if not hammer:
                            try:
                                block += om.compute_array(A)
                            except ImpossibleOperationError:
                                hammer = True

                        if hammer:
                            # TODO have oomatrix get its act together here, this is just
                            # ridiculous
                            if A.nrows < A.ncols:
                                try:
                                    block += om.compute_array(np.eye(A.nrows) * A)
                                except:
                                    block += om.compute_array(A * np.eye(A.ncols))
                            else:
                                try:
                                    block += om.compute_array(A * np.eye(A.ncols))
                                except:
                                    block += om.compute_array(np.eye(A.ncols) * A)

                    block = om.compute_array(F.h * om.as_matrix(block) * Fp)
                    out[row:row + height, col:col + width] = block

                    if ip == i:
                        # add prior term
                        iarr = np.arange(col, col + width)
                        out[iarr, iarr] += D.diagonal()

                if ip != i:
                    # mirror to upper-right
                    out[col:col + width, row:row + height] = out[row:row + height, col:col + width].T

    return out

def compute_approximate_Pt_Bt_Ni_B_P_diagonal(ctx, comp, observations, lmin, lmax):
    assert isinstance(comp, signal.SphericalHarmonicSignal)
    logger = ctx.get_logger('precond.high-l')
    out = 0
    for obs in observations:
        # compute (Y.h * M * N.i * M * Y).diagonal()
        with log_after_task(logger, 'Computed high-l LHS diagonal for %s in {dt}' % obs.name):
            inv_noise_map = compute_masked_inverse_noise_map(ctx, obs)
            nside_mixing, mixing_map = comp.get_mixing_map(ctx, obs)
            if nside_mixing is None:
                mixing_map = np.ones(obs.npix) * mixing_map
            else:
                mixing_map = ctx.regrade_map(nside_mixing, obs.nside, mixing_map)
            map = inv_noise_map * mixing_map**2
            # Expand map to 2*lmax
            YhW = ctx.sh_analysis_matrix(obs.nside, 0, 2 * comp.lmax)
            map_sh = om.compute_array(YhW * map[:, None])[:, 0]
            d = sh_integrals.compute_approximate_Yt_D_Y_diagonal(obs.npix, lmin, lmax, map_sh)

            # convolve with beam
            B = ctx.get_symmetric_beam_and_pixel_window_matrix(obs, lmin, lmax)
            beam_lm = B.diagonal()
            d *= beam_lm**2

            out += d
    return out

def compute_approximate_lhs_diagonal(ctx, comp, observations, lmin, lmax):
    out = compute_approximate_Pt_Bt_Ni_B_P_diagonal(ctx, comp, observations, lmin, lmax)
    # Add prior term
    F, D = get_F_D(ctx, comp, observations, lmin, lmax)
    out *= F.diagonal()**2
    out += D.diagonal()
    return out

def compute_approximate_noise_power(ctx, signal_component, observations):
    if not isinstance(signal_component, signal.SphericalHarmonicSignal):
        raise TypeError('needs SphericalHarmonicSignal')
    noise_term_sh = compute_approximate_Pt_Bt_Ni_B_P_diagonal(ctx, signal_component,
                                                              observations, 0, signal_component.lmax)
    noise_term_sh = 1 / noise_term_sh
    noise_power = mmajor.compute_power_spectrum(0, signal_component.lmax, noise_term_sh)
    return noise_power
        

class CrSampler(object):

    LoadedObservation = namedtuple('LoadedObservation',
                                   'observation name mask Ni nside npix')
    
    def __init__(self, ctx, observations, signal_components,
                 eliminate_masked_pixels=True, lprecond=30, eps=1e-7,
                 compare_with_truth=False):
        
        self.nonfit_signal_components = [comp for comp in signal_components
                                         if not comp.has_amplitudes]
        signal_components = [comp for comp in signal_components if comp.has_amplitudes]

        self.ctx = ctx
        self.signal_components = signal_components
        self.observations = observations
        self.logger = ctx.logger
        self.eliminate_masked_pixels = eliminate_masked_pixels
        self.lprecond = lprecond
        self.eps = eps
        self.compare_with_truth = compare_with_truth

        # Load the data here (at least for now)
        self.obs_data_list = obs_data_list = []
        for obs in observations:
            rms = ctx.get_TT_rms_map(obs)

            mask = 1 if not eliminate_masked_pixels else ctx.get_mask_map(obs)
            Ni = om.diagonal_matrix(mask / rms**2, 'Ni_%s' % obs.name)
            obs_data_list.append(CrSampler.LoadedObservation(
                obs, obs.name, mask, Ni, obs.nside, obs.npix))

        # Priors
        D_matrices = []
        F_matrices = []
        for component in signal_components:
            n = component.get_amplitude_count(ctx, observations)
            S, mu = component.get_amplitude_prior(ctx, observations)
            if mu is not None:
                raise NotImplementedError()
            F, D = get_F_D(ctx, component, observations)
            F_matrices.append(F)
            D_matrices.append(D)
        self.D_matrices = D_matrices
        self.F_matrices = F_matrices            
 
        # Mixing matrices
        mixing_matrix_columns = [comp.get_projection_matrix(ctx, observations)
                                 for comp in signal_components]
        self.M = om.stack_matrices_horizontally(mixing_matrix_columns)

        # Create the big block matrices
        #self.M = om.block_matrix(mixing_matrices)
        self.Ni = om.block_diagonal_matrix([obs.Ni for obs in self.obs_data_list])
        self.Ni_f = om.block_diagonal_matrix([om.compute(obs.Ni.f, name='Ni.f_%s' % obs.name)
                                              for obs in self.obs_data_list])
        self.D = om.block_diagonal_matrix(D_matrices)
        self.F = om.block_diagonal_matrix(F_matrices)
        assert self.F.nrows == self.F.ncols == self.M.ncols
        
        self.E = self.make_preconditioner()
        self.LHS = self.make_lhs()
        om.compute(self.LHS * np.zeros((self.LHS.ncols, 1))) # put oomatrix compilation outside of profiling


    def make_preconditioner(self):
        low_l_signals = self.signal_components
        high_l_signals = [comp for comp in self.signal_components
                          if isinstance(comp, signal.SphericalHarmonicSignal)]
        # Dense
        with log_after_task(self.ctx.compute_logger, 'Computed low-l LHS in {dt}'):
            lhs = compute_dense_lhs(self.ctx, low_l_signals, self.observations, self.lprecond)
        with log_after_task(self.ctx.compute_logger, 'Inverted low-l LHS in {dt}'):
            E_dense = om.as_matrix(np.linalg.inv(lhs), 'E_dense')
        # Diagonal
        diagonals = [compute_approximate_lhs_diagonal(self.ctx, comp, self.observations,
                                                      self.lprecond + 1, comp.lmax)
                     for comp in high_l_signals]
        E_diagonal = om.block_diagonal_matrix([om.diagonal_matrix(1 / x) for x in diagonals])
        
        E = om.block_diagonal_matrix([E_dense, E_diagonal])

        # Need to permute [comp0, comp1, ...] -> [comp0_lo, comp1_lo, ..., comp0_hi, comp1_hi, ...]
        P_lo_list = []
        P_hi_list = []
        for comp in self.signal_components:
            if isinstance(comp, signal.SphericalHarmonicSignal):
                P_lo = matrices.lselection_matrix((0, self.lprecond + 1), (0, comp.lmax + 1))
                P_hi = matrices.lselection_matrix((self.lprecond + 1, comp.lmax + 1), (0, comp.lmax + 1))
            else:
                # pixel-domain (templates), include entire in dense preconditioner
                n = comp.get_amplitude_count(self.ctx, self.observations)
                P_lo = om.identity_matrix(n)
                P_hi = om.as_matrix(np.zeros((0, n)))
            P_lo_list.append(P_lo)
            P_hi_list.append(P_hi)
        P = om.stack_matrices_vertically([
            om.block_diagonal_matrix(P_lo_list),
            om.block_diagonal_matrix(P_hi_list)])
        return P.h * E * P

    def make_rhs(self, add_fluctuations):
        F, M, Ni, Ni_f = getattrs(self, 'F M Ni Ni_f')

        data_list = []
        for obs in self.observations:
            data = self.ctx.get_T_map(obs).copy()
            for comp in self.nonfit_signal_components:
                comp.subtract_from_data_inplace(self.ctx, obs, data)
            data_list.append(data)
        d = om.as_matrix(np.vstack([x[:, None] for x in data_list]), 'd_all')

        w_sh = 0
        pix_term = Ni * d
        if add_fluctuations:
            w_pix = self.ctx.nc_rng.normal(size=(d.nrows, d.ncols)).astype(np.double)
            pix_term += Ni_f * w_pix

            w_sh = np.zeros((F.nrows, d.ncols))
            i = 0
            for comp in self.signal_components:
                n = comp.get_amplitude_count(self.ctx, self.observations)
                S, mu = comp.get_amplitude_prior(self.ctx, self.observations)
                if S is not None:
                    w_sh[i:i + n, :] = self.ctx.nc_rng.normal(size=(n, d.ncols))
                else:
                    # infinite prior variance means no contribution to w_sh
                    pass
                assert mu is None # todo
                i += n
        R = F.h * M.h * pix_term + w_sh
        return om.compute_array(R)

    def make_lhs(self):
        D, F, M, Ni = getattrs(self, 'D F M Ni')
        U = M * F
        return D + U.h * Ni * U

    def _solve(self, add_fluctuations):
        from .solvers.conjugate_gradients import stopping_cg_generator

        logger = self.ctx.get_logger('cg')
        rhs = self.make_rhs(add_fluctuations=add_fluctuations)

        gen = stopping_cg_generator(self.LHS, rhs, self.E, eps=self.eps, logger=logger,
                                    stop_rule='residual', maxit=20000)

        comp = self.signal_components[0]
        n = comp.get_amplitude_count(self.ctx, self.observations)

        #plt.figure()
        for pre_x, r, delta, info in gen:
            pass
            #x = om.compute_array(self.F.h * pre_x)
            #Cl = mmajor.compute_power_spectrum(0, comp.lmax, x[:n, 0])
            #plt.plot(Cl)
            #plt.draw()
        #plt.show()
            
            
        
        x = om.compute_array(self.F.h * pre_x)
        assert x.shape[1] == 1
        i = 0
        result_list = []
        for comp in self.signal_components:
            n = comp.get_amplitude_count(self.ctx, self.observations)
            result_list.append(x[i:i + n, 0])
            i += n
        return result_list

    def constrained_realization(self):
        return self._solve(add_fluctuations=True)

    def wiener_filter(self):
        return self._solve(add_fluctuations=False)
