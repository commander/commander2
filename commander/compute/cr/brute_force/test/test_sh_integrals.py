from ....._testsupport import *

from .....sphere.mmajor import lm_count
from ... import sh_integrals

from matplotlib.pyplot import *

def test_compute_approximate_Yt_D_Y_diagonal():
    def constant_test(lmin, lmax):
        npix = 12 * 16**2
        n = lm_count(lmin, lmax)

        D_sh = np.zeros(lm_count(0, 2 * lmax), dtype=np.float64)
        out = np.zeros(n, dtype=np.float64)

        # Test d00==1, rest 0
        D_sh[0] = 1
        out = sh_integrals.compute_approximate_Yt_D_Y_diagonal(npix, lmin, lmax, D_sh)

        print D_sh
        print out
        #plot(out)
        #show()

        assert np.max(np.abs(out[0] - out)) < 1e-13
        assert_almost_equal(out, npix / 4 / np.pi / np.sqrt(4 * np.pi))

    yield constant_test, 0, 10
    yield constant_test, 2, 7
