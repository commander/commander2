from __future__ import division
import numpy as np
import os
from numpy.linalg import norm

from matplotlib import pyplot as plt
from commander.plot import plotmatrix

from ..hmatrix import *

DEBUG = bool(int(os.environ.get('D', '0')))

class SingleThreadExecutor(object):
    def submit(self, func, *args, **kw):
        func(*args, **kw)
executor = SingleThreadExecutor()

def view(hmat):
    h_matrix_to_svg('/tmp/test.svg', hmat, 1)
    os.system('eog /tmp/test.svg')

def maxabs(x):
    return np.max(np.abs(x))

def relerr(a0, a):
    return norm(a0 - a) / norm(a0)

class MockMatrixGenerator(object):
    # domains represented by gridpoints

    def __init__(self):
        # represent 1 / max((x - y), .1)**2 with x and y in R^1, though we
        # don't care about how well it is represented. We quadtree
        # divide space; blocks are admissible when rho < 1.
        # NOTE: On the first two levels we divide into 3x3 rather than
        # 4x4, in order to test the ability to cope with ZeroLeaf.
        self.shape = (50, 50)
        self.minwidth = 2
        self.width = 30
        self.eta_max = 1
        self.k = 3
        self.xgrid = np.linspace(0, self.width, self.shape[0])
        self.ygrid = np.linspace(0, self.width, self.shape[1])

    def left_root_domain(self):
        return 0, self.xgrid

    def right_root_domain(self):
        return 0, self.ygrid

    def split_domain(self, d):
        level, grid = d
        x0, x1 = grid[0], grid[-1]
        n = grid.shape[0]
        if (x1 - x0) < 2 * self.minwidth or n == 1:
            return []
        elif level == 0:
            split = 2 * n // 3
            return [(level + 1, grid[:split]), (level + 1, grid[split:])]
        elif level == 1:
            if x0 == 0:
                split = n // 2
                return [(level + 1, grid[:split]), (level + 1, grid[split:])]
            else:
                # single-child node
                return [(level + 1, grid)]
        else:
            split = n // 2
            return [(level + 1, grid[:split]), (level + 1, grid[split:])]

    def is_admissible(self, d1, d2):
        xlevel, xgrid = d1
        ylevel, ygrid = d2
        if xlevel <= 1 or ylevel <= 1:
            return False, None
        x0, x1 = xgrid[0], xgrid[-1]
        y0, y1 = ygrid[0], ygrid[-1]
        rho = min(x1 - x0, y1 - y0)
        if x0 > y0:
            # just reverse roles so x interval is left of y interval
            x0, x1, y0, y1 = y0, y1, x0, x1
        dist = max(0, y0 - x1)
        if dist == 0 or rho / dist > self.eta_max:
            return False, None
        else:
            return True, rho

    def generate_compressed(self, d1, d2, rho):
        block = self.generate_exact(d1, d2).block
        U, s, V = np.linalg.svd(block)
        k = min(self.k, len(s))
        U = U[:, :k]
        V = V.T[:, :k]
        X = np.diag(s[:k])
        return UXV_leaf(U, X, V)

    def generate_exact(self, d1, d2):
        xlevel, xgrid = d1
        ylevel, ygrid = d2
        den = xgrid[:, None] - ygrid[None, :]
        den[np.abs(den) < .1] = .1
        block = 1 / den**2
        return DenseLeaf(block)
    
    def domain_size(self, d):
        return len(d[1])

def setup():
    global hmat, gen, hmat_dense
    gen = MockMatrixGenerator()
    d1 = gen.left_root_domain()
    d2 = gen.right_root_domain()
    hmat = generate_h_matrix(gen, d1, d2, executor).root_node
    hmat_dense = hmat.as_dense()
    

def test_cluster_tree():
    # basically visually inspect resulting cluster tree and do a crude check
    # automatically
    if DEBUG:
        view(hmat)
        print hmat.treesize()
    assert hmat.treesize() == 85

def test_approximation():
    exact = gen.generate_exact(gen.left_root_domain(), gen.right_root_domain()).block
    if DEBUG or False:
        plotmatrix(np.abs(hmat_dense - exact)/np.linalg.norm(exact), logabs=False)
        plt.show()
    assert gen.k == 3 # otherwise bound below must be adjusted
    assert relerr(exact, hmat_dense) < 1e-7

def test_matvec():
    x = np.arange(2 * hmat.shape[1], dtype=np.double).reshape((hmat.shape[1], 2))
    y = hmat.matvec(x)
    assert relerr(np.dot(hmat_dense, x), y) < 1e-15

def test_transpose():
    b0 = hmat.b.as_dense().T
    b = hmat.b.transpose().as_dense()
    assert relerr(b0, b) < 1e-15

def test_agglomerate_leaf():
    node = hmat.c.b
    dense = node.as_dense()
    assert maxabs(node.agglomerate(1e-13).as_dense() - dense) < 1e-13
    delta = maxabs(node.agglomerate(1e-3).as_dense() - dense)
    assert 1e-4 < delta < 1e-2

def test_agglomerate_tree():
    assert relerr(hmat_dense, hmat.agglomerate(1e-2).as_dense()) < 1e-2
    
def test_uxv_rounded_add():
    for eps in [1e-2, 1e-4]:
        a = hmat.b.agglomerate(eps)
        b = hmat.c.transpose().agglomerate(eps)
        assert isinstance(a, UXV_leaf) and isinstance(b, UXV_leaf)
        rounded = rounded_add(a, b, eps).as_dense()
        dense = hmat.b.as_dense() + hmat.c.as_dense().T
        assert relerr(dense, rounded) < eps

def test_hmatrix_rounded_add():
    eps = 1e-2
    twice_hmat = rounded_add(hmat, hmat, eps)
    assert relerr(2 * hmat_dense, twice_hmat.as_dense()) < eps

def test_matmul_uxv():
    a = hmat.a.agglomerate(1e-8)
    b = hmat.b.agglomerate(1e-8)
    assert isinstance(a, UXV_leaf) and isinstance(b, UXV_leaf)
    hmul = matmul(a, b, 1.).as_dense()
    dmul = np.dot(a.as_dense(), b.as_dense())
    q = dots(a.U, a.V.T, a.X, b.U, b.X, b.V.T)
    assert relerr(dmul, hmul) < 1e-8

def test_matmul_node_uxv():
    hmat_sq = matmul(hmat, hmat.agglomerate(1e-2), 1e-2)
    assert relerr(np.dot(hmat_dense, hmat_dense), hmat_sq.as_dense()) < 1e-2
    hmat_sq = matmul(hmat.agglomerate(1e-2), hmat, 1e-2)
    assert relerr(np.dot(hmat_dense, hmat_dense), hmat_sq.as_dense()) < 1e-2

def test_matmul_full():
    hmat_sq = matmul(hmat, hmat, 1e-2)
    assert relerr(np.dot(hmat_dense, hmat_dense), hmat_sq.as_dense()) < 1e-2
    
