from __future__ import division

from scipy.linalg import cho_solve, cho_factor

from ....plot import plotmatrix
from matplotlib.pyplot import *

from ..mblocks import compute_Yh_D_Y_healpix, compute_Yh_D_Y_gauss
from ....sphere.mmajor import scatter_l_to_lm

#def build_B_Ni_B_matrices(ctx, observations):

def counts_to_offsets(counts):
    return np.concatenate([[0], np.cumsum(counts)])

class CrSampler(object):

    def __init__(self, ctx, observations, components):
        self.ctx = ctx
        self.observations = observations
        self.components = components

        ncoefs_comp = [comp.get_amplitude_count(ctx, observations) for comp in components]
        self.offsets_comp = counts_to_offsets(ncoefs_comp)
        ncoefs_obs = [(obs.lmax_beam + 1)**2 for obs in observations]
        self.offsets_obs = counts_to_offsets(ncoefs_obs)

        self.ninv_maps = ninv_maps = []
        for obs in observations:
            rms_map = obs.get_TT_rms_map(ctx)
            mask_map = obs.get_mask_map(ctx)
            ninv_map = mask_map / rms_map**2
            ninv_maps.append(ninv_map)

        A, P = self._build_matrix()
        self.P = P
        self.A_factor = cho_factor(A, overwrite_a=True)
        

    def _build_matrix(self):
        ctx = self.ctx
        observations = self.observations
        components = self.components
        offsets_comp = self.offsets_comp
        offsets_obs = self.offsets_obs
        
        # Build N^-1 matrix in spherical harmonic domain
        B_Ni_B_matrices = []
        for ninv_map, obs in zip(self.ninv_maps, observations):
            arr = compute_Yh_D_Y_healpix(obs.lmax_beam, obs.lmax_beam, ninv_map)
            beam = obs.get_symmetric_beam_and_pixel_window_transfer(ctx, 0, obs.lmax_beam)
            beam_lm = scatter_l_to_lm(0, obs.lmax_beam, beam)
            arr *= beam_lm[:, None]
            arr *= beam_lm[None, :]
            B_Ni_B_matrices.append(arr)

        # Build big projection matrix P
        P = np.zeros((offsets_obs[-1], offsets_comp[-1]))
        for i, obs in enumerate(observations):
            for j, comp in enumerate(components):
                w = comp.get_gauss_mixing_map(ctx, obs, with_weights=True)
                if isinstance(w, (int, float)):
                    assert comp.lmax == obs.lmax_beam, 'TODO'
                    block = np.eye((comp.lmax + 1)**2) * w
                else:
                    block = compute_Yh_D_Y_gauss(comp.lmax, obs.lmax_beam, comp.lmax, w)
                P[offsets_obs[i]:offsets_obs[i + 1], offsets_comp[j]:offsets_comp[j + 1]] = block

        # Multiply in Ni_P
        Ni_P = np.empty_like(P)
        for i, (obs, Ni) in enumerate(zip(observations, B_Ni_B_matrices)):
            start, stop = offsets_obs[i], offsets_obs[i + 1]
            Ni_P[start:stop, :] = np.dot(Ni, P[start:stop, :])

        A = np.dot(P.T, Ni_P)

        for j, comp in enumerate(components):
            start, stop = offsets_comp[j], offsets_comp[j + 1]
            k = np.arange(start, stop)
            Cl = comp.load_power_spectrum(comp.lmax)
            dlm = scatter_l_to_lm(0, comp.lmax, 1 / Cl)
            A[k, k] += dlm

        return A, P
    
    def _make_rhs(self, add_fluctuations):
        ctx = self.ctx
        offsets_obs = self.offsets_obs
        offsets_comp = self.offsets_comp

        # Need to compute
        #
        #     b = P^T * (Ni * d + sqrt(Ni) * omega1) + sqrt(Si) * omega2
        #
        # where omega1 and omega2 are Gaussian white-noise maps

        b = np.zeros(offsets_comp[-1])

        for j, comp in enumerate(self.components):
            comp_start, comp_stop = offsets_comp[j], offsets_comp[j + 1]
            b_comp = b[comp_start:comp_stop]

            # Noise part
            
            for i, (obs, ninv_map) in enumerate(zip(self.observations, self.ninv_maps)):
                obs_start, obs_stop = offsets_obs[i], offsets_obs[i + 1]

                # Ni * d
                d = obs.get_T_map(ctx)
                y = ninv_map * d


                # sqrt(Ni) * omega1
                if add_fluctuations:
                    omega = ctx.nc_rng.normal(size=d.shape)
                    y += np.sqrt(ninv_map) * omega
                    

                # b_comp += P^T B^T Y^T y
                y_sh = ctx.sh_adjoint_synthesis(obs.nside, 0, obs.lmax_beam, y)
                
                beam = obs.get_symmetric_beam_and_pixel_window_transfer(ctx, 0, obs.lmax_beam)
                beam_lm = scatter_l_to_lm(0, obs.lmax_beam, beam)
                y_sh *= beam_lm

                y_sh = np.dot(self.P[obs_start:obs_stop, comp_start:comp_stop].T, y_sh)

                b_comp[:] += y_sh

            # Signal part
            if add_fluctuations:
                Cl = comp.load_power_spectrum(comp.lmax)
                sqrt_invCl_lm = scatter_l_to_lm(0, comp.lmax, 1 / np.sqrt(Cl))
                omega = ctx.nc_rng.normal(size=sqrt_invCl_lm.shape)
                b_comp[:] += sqrt_invCl_lm * omega

        from commander.plot import plot_ring_map
        from matplotlib.pyplot import clf, show

        return b_comp

    def draw(self, add_fluctuations=True):
        b = self._make_rhs(add_fluctuations)
        cho_solve(self.A_factor, b, overwrite_b=True)
        # split up b by components
        x_list = []
        for j, comp in enumerate(self.components):
            start, stop = self.offsets_comp[j], self.offsets_comp[j + 1]
            x_list.append(b[start:stop])
        return x_list

        

