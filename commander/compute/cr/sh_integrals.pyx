#cython: embedsignature=True

"""
:mod:`commander.sphere.sh_integrals`
------------------------------------

Routines (well, only one currently) for direct evaluation of matrix elements
in spherical harmonic basis when the input (noise map etc.) is in pixel
domain. This typically leads to Gaunt integrals which are computed
using Wigner 3j coefficients.

"""

from __future__ import division

import numpy as np
cimport numpy as np
cimport cython
from cython cimport parallel

cdef double pi
from numpy import pi

from commander.sphere.mmajor cimport index_t, lm_to_idx
from commander.sphere.mmajor import scatter_l_to_lm, find_lmax, lm_count

from commander.sphere.wigner cimport drc3jj_fast

from concurrent.futures import ThreadPoolExecutor

cimport openmp

cdef inline index_t iabs(index_t i) nogil:
    return -i if i < 0 else i

cdef extern from "math.h":
    double sqrt(double) nogil
    double acos(double) nogil
    double cos(double) nogil
    double sin(double) nogil
    double round(double) nogil

cdef extern from "complex.h":
    double complex conj(double complex) nogil


cdef int do_abort(int arg) with gil:
    import sys
    print 'drc3jj error: %d' % arg
    sys.exit(10)

@cython.profile(False)
cdef int drc3jj(index_t l2, index_t l3, index_t m2, index_t m3,
                index_t* l1min, index_t* l1max, double* thrcof, size_t thrcof_len) nogil:
    # Convenience function for calling drc3jj_fast
    cdef int ier
    cdef double l1min_d, l1max_d
    ier = drc3jj_fast(l2, l3, m2, m3, &l1min_d, &l1max_d, thrcof, thrcof_len)
    if ier != 0:
        do_abort(ier)
    l1min[0] = <index_t>l1min_d
    l1max[0] = <index_t>l1max_d
    return 0

def compute_approximate_Yt_D_Y_diagonal(index_t npix,
                                        index_t lmin,
                                        index_t lmax,
                                        np.ndarray[double] D_sh,
                                        np.ndarray[double] out=None):
    cdef double[::1] W_zero, W_diag, W_anti # per-thread buffers
    if out is None:
        out = np.empty(lm_count(lmin, lmax), np.double)
    elif out.shape[0] != lm_count(lmin, lmax):
        raise ValueError('out has wrong shape')


    cdef index_t chunk_size_m = 20, mstart, mstop, bufsize = 2 * (lmax + 1)
    cdef int nthreads = openmp.omp_get_max_threads()

    executor = ThreadPoolExecutor(max_workers=nthreads)
    for mstart in range(0, lmax + 1, chunk_size_m):
        mstop = mstart + chunk_size_m
        if mstop > lmax + 1:
            mstop = lmax + 1
        executor.submit(compute_approximate_Yt_D_Y_diagonal_mblock,
                        npix, lmin, lmax, mstart, mstop - 1,
                        D_sh, out)
    executor.shutdown(wait=True)
    return out

def compute_approximate_Yt_D_Y_diagonal_mblock(index_t npix,
                                               index_t lmin,
                                               index_t lmax,
                                               index_t mmin,
                                               index_t mmax,
                                               np.ndarray[double] D_sh,
                                               np.ndarray[double] out, # NOTE: out contains *all* ms!
                                               ):
    """
    Computes elements of the diagonal of ``Y.h * D * Y``.

    Above, ``Y`` is spherical harmonic synthesis and ``D`` is a diagonal
    matrix in pixel basis. The spherical harmonic expansion of ``D``
    is provided as input.

    Note that this is an *approximation* which becomes more valid as
    npix -> infty. It is assumed that the pixel size is 4*pi/npix.

    TODO: Take `ms` in order to facilitate MPI distribution.
    
    Real mmajor spherical harmonics are assumed.


    """
    cdef:
        index_t D_lmax, idx, i, l, abs_m, l3
        index_t minus_one_to_m, bufsize
        double dlm, l_scale, C_val_diag, C_val_anti
        
        double w, w1, w2 # storage of Wigner symbol scalars

        index_t l3min_zero, l3max_zero
        index_t l3min_diag, l3max_diag
        index_t l3min_anti, l3max_anti

        double npix_by_four_pi_to_three_halfs
        double sqrt_half = np.sqrt(.5)
        double[::1] W_zero # buffers of size `bufsize`
        double[::1] W_diag
        double[::1] W_anti


    bufsize = 2 * (lmax + 1)

    W_zero = np.zeros(bufsize, dtype=np.double)
    W_diag = np.zeros(bufsize, dtype=np.double)
    W_anti = np.zeros(bufsize, dtype=np.double)

    npix_by_four_pi_to_three_halfs = npix / (4. * pi * sqrt(4. * pi))

    D_lmax = find_lmax(0, D_sh.shape[0])
    if D_lmax < 2 * lmax:
        raise ValueError("D_sh must be expanded to at least 2 * lmax")

    with nogil:
        if mmin == 0:
            # m=0 is a special case
            i = 0 # we stream out to out[i] and increment i as we go
            abs_m = 0
            if lmin == 0:
                # special case for a_{0,0}; drc3jj returns 0 but value (for our purposes at least) should be 1
                l = 0
                C_val_diag = D_sh[lm_to_idx(0, D_lmax, 0, l, abs_m)]
                out[i] = C_val_diag * npix_by_four_pi_to_three_halfs
                i += 1

            for l in range(max(1, lmin), lmax + 1):
                # cases a_{l,0}
                l_scale = (2 * l + 1) * npix_by_four_pi_to_three_halfs
                drc3jj(l, l, abs_m, abs_m, &l3min_zero, &l3max_zero, &W_zero[0], bufsize)
                C_val_diag = 0
                for l3 in range(l3min_zero, l3max_zero + 1):
                    w = W_zero[l3 - l3min_zero]
                    dlm = D_sh[lm_to_idx(0, D_lmax, 0, l3, abs_m)]
                    C_val_diag += sqrt(2 * l3 + 1) * w * w * dlm
                C_val_diag *= l_scale
                out[i] = C_val_diag
                i += 1

        for abs_m in range(max(mmin, 1), mmax + 1):
            minus_one_to_m = -1 if abs_m % 2 == 1 else 1
            for l in range(max(lmin, abs_m), lmax + 1):
                l_scale = (2 * l + 1) * npix_by_four_pi_to_three_halfs
                drc3jj(l, l, 0, 0, &l3min_zero, &l3max_zero, &W_zero[0], bufsize)

                # C_val_anti is understood to be Re[C_{lm,l-m}]

                # Let R be the real result (of which we compute the diagonal), and
                # C the complex covariance matrix computed through the 3j symbols.
                # Then the diagonal of R is given by the diagonal AND the anti-diagonal
                # of C for each m.  We compute C_val_diag = C_{lm,lm} (leads to m3 = 0)
                # and C_val_anti = C_{lm,l-m} which results
                # in m3 = m-(-m) = 2*m > 0; both are direct lookup in the alms.

                drc3jj(l, l, -abs_m, abs_m, &l3min_diag, &l3max_diag, &W_diag[0], bufsize)
                drc3jj(l, l, -abs_m, -abs_m, &l3min_anti, &l3max_anti, &W_anti[0], bufsize)
                C_val_diag = 0
                C_val_anti = 0
                for l3 in range(l3min_diag, l3max_diag + 1):
                    # Diagonal contribution for this l3
                    w1 = W_zero[l3 - l3min_zero]
                    w2 = W_diag[l3 - l3min_diag]
                    dlm = D_sh[lm_to_idx(0, D_lmax, 0, l3, 0)]
                    C_val_diag += sqrt(2 * l3 + 1) * w1 * w2 * dlm

                    if l3 >= l3min_anti and l3 <= l3max_anti:
                        if 2 * abs_m > l3:
                            with gil:
                                raise AssertionError("This should not happen")
                        w1 = W_zero[l3 - l3min_zero]
                        w2 = W_anti[l3 - l3min_anti]
                        dlm = sqrt_half * D_sh[lm_to_idx(0, D_lmax, 0, l3, 2 * abs_m)]
                        C_val_anti += sqrt(2 * l3 + 1) * w1 * w2 * dlm
                C_val_diag *= l_scale * minus_one_to_m
                C_val_anti *= l_scale * minus_one_to_m

                i = lm_to_idx(lmin, lmax, 0, l, abs_m) # NOTE that mmin=0 when indexing out (for now)
                # Store (l, abs_m)                
                out[i] = C_val_diag + minus_one_to_m * C_val_anti
                # Store (l, -abs_m)
                out[i + 1] = C_val_diag - minus_one_to_m * C_val_anti

    return out
