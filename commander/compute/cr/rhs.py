
import numpy as np
from ...sphere import sh_adjoint_synthesis

def make_rhs(d, bl, cl, ninv_map, rng, add_fluctuations=True):
    """
    Make right-hand side of linear system to solve for constrained realisation 
    of CMB (and other component amplitudes).
    
        b = B^T Y^T (N^-1 d + N^(-1/2) w_1) + S^1/2 w_2
    
    Arguments
    ---------
    
    d: array_like (map)
        Data map
    
    bl: array_like
        Legendre beam coefficients. Should have already been multiplied by 
        pixel window. Size lmax+1.
    
    cl: array_like
        Power spectrum, c_l, size lmax+1.
    
    ninv_map: array_like (map)
        Inverse noise map.
    
    rng: numpy.random.RandomState
        Random number generator state
    
    add_fluctuations: bool, optional
        Whether or not to add fluctuations (Gaussian random noise) to the 
        constrained realisation. Default: True.
        
    ###  b = P^T * (Ni * d + sqrt(Ni) * omega1) + sqrt(Si) * omega2
    ### where omega1 and omega2 are Gaussian white-noise maps
    """
    
    # Infer lmax from beam, bl
    lmax = bl.size - 1
    lmax_beam = lmax
    nside = np.sqrt(d.size/12) # npix = 12 nside^2
    
    # RHS array size
    n_coeffs = (lmax+1)**2 # No. a_lm coefficients
    b_comp = np.zeros(n_coeffs)

    # Noise part
    y = ninv_map * d
    if add_fluctuations:
        omega = rng.normal(size=d.shape)
        y += np.sqrt(ninv_map) * omega
    
    # Apply beam and projection operator, b_comp += P^T B^T Y^T y
    y_sh = sh_adjoint_synthesis(y)
    beam_lm = scatter_l_to_lm(bl)
    y_sh *= beam_lm
    b_comp[:] += y_sh

    # Signal part
    if add_fluctuations:
        Sinv = np.zeros(cl.size)
        Sinv[2:] = 1. / np.sqrt(cl[2:]) # Deal with zero monopole/dipole
        sqrt_invCl_lm = scatter_l_to_lm()
        omega = rng.normal(size=sqrt_invCl_lm.shape)
        b_comp[:] += sqrt_invCl_lm * omega

    return b_comp
    
