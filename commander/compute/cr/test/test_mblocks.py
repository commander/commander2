from __future__ import division
from matplotlib import pyplot as plt
import numpy as np
from nose import SkipTest

from .. import mblocks
from ....sphere import mmajor
from ....context import CommanderContext

def test_mblock_generation():
    raise SkipTest('out of date')
    # Test data
    nside = 8
    lmax = 20
    npix = 12 * nside**2

    def get_truth(map):
        ctx = CommanderContext()
        Y = ctx.sh_synthesis_matrix(nside, 0, lmax)
        M = om.compute_array(Y.h * om.diagonal_matrix(map) * Y * np.eye(Y.ncols))
        return M        

    def doit(map):
        matrix = mblocks.compute_Yh_D_Y(lmax, map)
        matrix0 = get_truth(map)
        assert np.max(np.abs(matrix - matrix0)) / np.linalg.norm(matrix0) < 1e-13
        #from ...plot import plotmatrix
        #plotmatrix((matrix - matrix0) / np.linalg.norm(matrix0), logabs=True)
        #plt.figure()
        #plotmatrix(matrix0)
        #plt.show()
    
    # Test with one completely random map; during development this was definitely
    # the worst-case behaviour as it also has phases at high m which is
    # typically
    random_map = np.random.RandomState(32).normal(size=npix)
    yield doit, random_map
    
