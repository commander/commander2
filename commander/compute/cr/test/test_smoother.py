from __future__ import division
import numpy as np
from ..mg_smoother import *
from ....sphere import gaussian_beam_by_l

from nose import SkipTest

from numpy.linalg import norm

from matplotlib.pyplot import *
from commander.plot import plotmatrix, plot_ring_map


nside = 4
nside_level = 2
nside_tile = nside // nside_level
lmax = 6 * nside
bl = gaussian_beam_by_l(lmax, 2 * np.pi / (4 * nside))


def test_convert():
    nside = 2
    npix = 12 * nside**2
    nside_level = 1
    
    x = np.arange(npix, dtype=np.double)
    x_tiled = convert_ring_dp_to_tiled_sp(nside_level, x)
    assert np.all(convert_tiled_sp_to_ring_dp(x_tiled) == x) # values in x fits in float32
    assert list(x_tiled[:, 0]) == [13., 5., 4., 0.]
    #plot_ring_map(x_tiled.mean(axis=0))
    #show()

def test_sort():
    x = [4, 3, 2, 5, 1]
    assert insertion_sort(x) == sorted(x)

def pattern_to_dense(indptr, indices):
    from scipy.sparse import csc_matrix
    values = np.ones(indices.shape[0])
    return np.asarray(csc_matrix((values, indices, indptr)).todense())
    

def test_mirror_csc():
    nside = 4
    indptr, indices = csc_neighbours_lower(nside)
    M_lower = pattern_to_dense(indptr, indices)
    M_full = pattern_to_dense(*mirror_csc_indices(indptr, indices))
    assert np.all(np.tril(M_full) == M_lower)
    assert np.all(np.tril(M_full.T) == M_lower)
    
def test_neighbours():
    nside = 4

    indptr, indices = csc_neighbours_full(nside)
    M_full = pattern_to_dense(indptr, indices)
    assert np.sum(M_full) == 9 * 12 * nside**2 - 24

    indptr, indices = csc_neighbours_lower(nside)
    M_lower = pattern_to_dense(indptr, indices)

    # For now just test that triangular matches dense...
    assert np.all(np.tril(M_full) == M_lower)
    assert np.all(np.tril(M_full.T) == M_lower)

    # 24-neighbours
    indptr, indices = csc_24_neighbours(nside)
    M_full_24 = pattern_to_dense(indptr, indices)
    assert np.all(M_full_24 * M_full == M_full)

    # Adaptive
    rmap = np.zeros(12 * nside**2, np.int8)
    indptr, indices = csc_neighbours_adaptive(nside, rmap)
    assert np.all(pattern_to_dense(indptr, indices) == np.eye(12 * nside**2))

    rmap[:] = 1
    indptr, indices = csc_neighbours_adaptive(nside, rmap)
    assert np.all(pattern_to_dense(indptr, indices) == M_lower)

    q = rmap.shape[0] // 4
    rmap[:q] = 5
    rmap[3*q:] = 5
    indptr, indices = csc_neighbours_adaptive(nside, rmap)
    M_adapt = pattern_to_dense(indptr, indices)
    M_adapt[3*q:,2*q:3*q] = 1
    #plotmatrix(M_adapt)
    #show()
    assert np.all(M_adapt * M_lower == M_lower) # is superset
    assert np.any(M_adapt[3*q:,:q] == 1)
    assert np.all(M_adapt[3*q:,q:2*q] == 0)

    
def test_beam_matrix():
    raise SkipTest()
    indptr, indices = csc_neighbours_lower(nside_level)
    blocks = compute_csc_beam_matrix(nside, nside_level, bl, indptr, indices)
    a = blocks_to_dense(indptr, indices, blocks)
    # No test for now, just plotting
    #plotmatrix(a)
    #show()

def test_reg_matmul():
    from scipy.sparse import csc_matrix, tril

    nside_left = 2 * nside
    
    indptr, indices = csc_neighbours_full(nside_level) 
    # Figure out out-pattern by doing matmul with scipy.sparse
    M = csc_matrix((np.ones(indices.shape[0]), indices, indptr))
    Mt_M = tril(M.T * M).tocsc()
    Mt_M.sort_indices()
    out_indices = Mt_M.indices
    out_indptr = Mt_M.indptr
    
    D = 1 + np.random.normal(size=((nside_left/nside_level)**2, 12*nside_level**2))
    D = D.astype(np.float32).copy('F')
    D_flat = D.reshape(12*nside_left**2, order='F')

    # Compute input matrix
    blocks = compute_csc_beam_matrix(False, nside_left, nside, nside_level, bl.astype(np.float32),
                                     indptr, indices)
    
    A = blocks_to_dense(indptr, indices, blocks, False)
    At_D_A_0 = np.dot(A.T, D_flat[:, None] * A)

    # Full product
    At_D_A_blocks = block_At_D_A(indptr, indices, blocks, D, out_indptr, out_indices)
    At_D_A = blocks_to_dense(out_indptr, out_indices, At_D_A_blocks, True)
    assert np.all(np.abs(At_D_A - At_D_A_0) < 1e-4)

    # Incomplete product
    out_indptr, out_indices = csc_neighbours_lower(nside_level)
    At_D_A_blocks = block_At_D_A(indptr, indices, blocks, D, out_indptr, out_indices)
    At_D_A = blocks_to_dense(out_indptr, out_indices, At_D_A_blocks, True)
    mask = blocks_to_dense(out_indptr, out_indices, np.ones(At_D_A_blocks.shape), True)
    assert np.all(np.abs(At_D_A - At_D_A_0) * mask < 1e-4)

def test_onthefly():
    nside_left = 2 * nside
    D = 1 + np.random.normal(size=((nside_left/nside_level)**2, 12*nside_level**2))
    D = D.astype(np.float32, order='F')
    
    out_indptr, out_indices = csc_neighbours_lower(nside_level)
    
    # Incomplete product using on-the-fly block computation
    At_D_A_blocks2 = compute_Bt_Ni_B(nside, nside_left, nside_level,
                                     bl.astype(np.float32), D,
                                     out_indptr, out_indices)

    # Precomputing B and doing multiply
    indptr, indices = csc_neighbours_full(nside_level) 
    blocks = compute_csc_beam_matrix(False, nside_left, nside, nside_level, bl.astype(np.float32),
                                     indptr, indices)
    At_D_A_blocks = block_At_D_A(indptr, indices, blocks, D, out_indptr, out_indices)
    
    assert np.all(At_D_A_blocks == At_D_A_blocks)
    
    

    
#
## # To convert to testcases:
## #

## if 0:
##     # Test triangular solves
##     L = blocks_to_dense(indptr, indices, blocks)
##     Atilde = np.dot(L, L.T)
##     x0 = np.random.normal(size=12*nside**2)
##     y = np.dot(Atilde, convert_ring2tiled(nside_level, x0).reshape(x0.shape, order='F'))
##     y = convert_tiled2ring(y.reshape((nside_tile**2, 12*nside_level**2), order='F'))

##     #x0 = np.linalg.solve(L, y)

##     y_tiled = convert_ring2tiled(nside_level, y)
##     x_tiled = y_tiled.copy('F')
##     block_triangular_solve('N', indptr, indices, blocks, x_tiled)
##     block_triangular_solve('T', indptr, indices, blocks, x_tiled)
##     x = convert_tiled2ring(x_tiled)

##     clf()
##     plot(x0)
##     plot(x)
##     show()
##     draw()



## if 0:
##     values = np.ones(indices.shape[0])
##     values[:] = indices % 2 + 1
##     import scipy.sparse
##     C = scipy.sparse.csc_matrix((values, indices, indptr), shape=(npi,npi)).todense()
##     plotmatrix(C)
##     show()
