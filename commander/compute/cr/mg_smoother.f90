module pix_matrices
  use iso_c_binding
  use types_as_c
  use pix_tools ! HEALPix
  use constants

  implicit none

  type, bind(c) :: domain_t
     integer(i4b) sample_nside, level_nside, ipix
  end type domain_t

  interface
     SUBROUTINE DPOTRS( UPLO, N, NRHS, A, LDA, B, LDB, INFO )
       CHARACTER          UPLO
       INTEGER            INFO, LDA, LDB, N, NRHS
       DOUBLE PRECISION   A( LDA, * ), B( LDB, * )
     END SUBROUTINE DPOTRS

     SUBROUTINE SPOTRS( UPLO, N, NRHS, A, LDA, B, LDB, INFO )
       CHARACTER          UPLO
       INTEGER            INFO, LDA, LDB, N, NRHS
       REAL               A( LDA, * ), B( LDB, * )
     END SUBROUTINE SPOTRS

     SUBROUTINE DPOTRF( UPLO, N, A, LDA, INFO )
       CHARACTER          UPLO
       INTEGER            INFO, LDA, N
       DOUBLE PRECISION   A( LDA, * )
     END SUBROUTINE DPOTRF

     SUBROUTINE SPOTRF( UPLO, N, A, LDA, INFO )
       CHARACTER          UPLO
       INTEGER            INFO, LDA, N
       REAL               A( LDA, * )
     END SUBROUTINE SPOTRF

     SUBROUTINE DTRSM(SIDE,UPLO,TRANSA,DIAG,M,N,ALPHA,A,LDA,B,LDB)
       DOUBLE PRECISION ALPHA
       INTEGER LDA,LDB,M,N
       CHARACTER DIAG,SIDE,TRANSA,UPLO
       DOUBLE PRECISION A(LDA,*),B(LDB,*)
     END SUBROUTINE DTRSM

     SUBROUTINE STRSM(SIDE,UPLO,TRANSA,DIAG,M,N,ALPHA,A,LDA,B,LDB)
       REAL ALPHA
       INTEGER LDA,LDB,M,N
       CHARACTER DIAG,SIDE,TRANSA,UPLO
       REAL A(LDA,*),B(LDB,*)
     END SUBROUTINE STRSM

     SUBROUTINE DTRSV(UPLO,TRANS,DIAG,N,A,LDA,X,INCX)
       INTEGER INCX,LDA,N
       CHARACTER DIAG,TRANS,UPLO
       DOUBLE PRECISION A(LDA,*),X(*)
     END SUBROUTINE DTRSV

     SUBROUTINE STRSV(UPLO,TRANS,DIAG,N,A,LDA,X,INCX)
       INTEGER INCX,LDA,N
       CHARACTER DIAG,TRANS,UPLO
       REAL A(LDA,*),X(*)
     END SUBROUTINE STRSV

     SUBROUTINE DGEMM(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
       DOUBLE PRECISION ALPHA,BETA
       INTEGER K,LDA,LDB,LDC,M,N
       CHARACTER TRANSA,TRANSB
       DOUBLE PRECISION A(LDA,*),B(LDB,*),C(LDC,*)
     END SUBROUTINE DGEMM

     SUBROUTINE SGEMM(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
       REAL ALPHA,BETA
       INTEGER K,LDA,LDB,LDC,M,N
       CHARACTER TRANSA,TRANSB
       REAL A(LDA,*),B(LDB,*),C(LDC,*)
     END SUBROUTINE SGEMM

     SUBROUTINE DSYRK(UPLO,TRANS,N,K,ALPHA,A,LDA,BETA,C,LDC)
       DOUBLE PRECISION ALPHA,BETA
       INTEGER K,LDA,LDC,N
       CHARACTER TRANS,UPLO
       DOUBLE PRECISION A(LDA,*),C(LDC,*)
     END SUBROUTINE DSYRK

     SUBROUTINE SSYRK(UPLO,TRANS,N,K,ALPHA,A,LDA,BETA,C,LDC)
       REAL ALPHA,BETA
       INTEGER K,LDA,LDC,N
       CHARACTER TRANS,UPLO
       REAL A(LDA,*),C(LDC,*)
     END SUBROUTINE SSYRK

     SUBROUTINE DGEMV(TRANS,M,N,ALPHA,A,LDA,X,INCX,BETA,Y,INCY)
       DOUBLE PRECISION ALPHA,BETA
       INTEGER INCX,INCY,LDA,M,N
       CHARACTER TRANS
       DOUBLE PRECISION A(LDA,*),X(*),Y(*)
     END SUBROUTINE DGEMV

     SUBROUTINE SGEMV(TRANS,M,N,ALPHA,A,LDA,X,INCX,BETA,Y,INCY)
       REAL ALPHA,BETA
       INTEGER INCX,INCY,LDA,M,N
       CHARACTER TRANS
       REAL A(LDA,*),X(*),Y(*)
     END SUBROUTINE SGEMV

     subroutine legendre_transform_recfac_d(recfac, lmax) bind(c)
       use iso_c_binding, only: c_int, c_double
       integer(c_int), value :: lmax
       real(c_double), dimension(0:lmax) :: recfac
     end subroutine

     subroutine legendre_transform_d(bl, recfac, lmax, x, out, n) bind(c)
       use iso_c_binding, only: c_int, c_double
       integer(c_int), value :: lmax, n
       real(c_double), dimension(0:lmax) :: bl, recfac
       real(c_double), dimension(1:n) :: x, out
     end subroutine

     subroutine legendre_transform_recfac_s(recfac, lmax) bind(c)
       use iso_c_binding, only: c_int, c_float
       integer(c_int), value :: lmax
       real(c_float), dimension(0:lmax) :: recfac
     end subroutine

     subroutine legendre_transform_s(bl, recfac, lmax, x, out, n) bind(c)
       use iso_c_binding, only: c_int, c_float
       integer(c_int), value :: lmax, n
       real(c_float), dimension(0:lmax) :: bl, recfac
       real(c_float), dimension(1:n) :: x, out
     end subroutine
  end interface

contains

  ! Find k when one has *exactly* 2^k
  function int_log2(i) result(r)
    integer(i4b), value :: i
    integer(i4b) :: r
    r = -1
    do while (.true.)
       if (i == 0) return
       i = ishft(i, -1)
       r = r + 1
    end do
  end function int_log2

  integer(i4b) function fring2nest(nside, ipix)
    integer(i4b) :: nside, ipix
    call ring2nest(nside, ipix, fring2nest)
  end function fring2nest

  integer(i4b) function fnest2ring(nside, ipix)
    implicit none

    integer(i4b) :: nside, ipix
    call nest2ring(nside, ipix, fnest2ring)
  end function fnest2ring

  subroutine convert_nest2ring_wrapper(nside, map) bind(c,name='convert_nest2ring')
    integer(i4b), value :: nside
    real(dp), dimension(12*nside**2) :: map
    !--
    call convert_nest2ring(nside, map)
  end subroutine convert_nest2ring_wrapper

  subroutine convert_ring2nest_wrapper(nside, map) bind(c,name='convert_ring2nest')
    integer(i4b), value :: nside
    real(dp), dimension(12*nside**2) :: map
    !--
    call convert_ring2nest(nside, map)
  end subroutine convert_ring2nest_wrapper

  subroutine ring2nest_wrapper(nside, ringpix, nestpix) bind(c,name='ring2nest')
    integer(i4b), value :: nside, ringpix
    integer(i4b), intent(out) :: nestpix
    !--
    call ring2nest(nside, ringpix, nestpix)
  end subroutine ring2nest_wrapper

  subroutine udgrade_ring_wrapper(map_in, nside_in, map_out, nside_out) bind(c)
    use udgrade_nr
    integer(i4b), value :: nside_in, nside_out
    real(dp), dimension(0:12*nside_in**2-1) :: map_in
    real(dp), dimension(0:12*nside_out**2-1) :: map_out
    !--
    call udgrade_ring(map_in, nside_in, map_out, nside_out)
  end subroutine udgrade_ring_wrapper

  subroutine nest2ring_indices(nside, indices) bind(c)
    integer(i4b), value :: nside
    integer(i4b), dimension(0:12 * nside**2 - 1) :: indices
    !--
    integer(i4b) :: iring, inest
    do inest = 0, 12 * nside**2 - 1
       call nest2ring(nside, inest, iring)
       indices(inest) = iring
    end do
  end subroutine nest2ring_indices

  subroutine pixel_beam_profile(recfac, rescaled_bl, lmax, theta, val) bind(c)
    integer(i4b), value :: lmax
    real(dp), dimension(0:lmax) :: rescaled_bl, recfac
    real(dp), value :: theta
    real(dp), intent(out) :: val
    !---
    real(dp), dimension(1) :: x_arr, val_arr
    x_arr(1) = cos(theta)
    call legendre_transform_d(rescaled_bl, recfac, lmax, x_arr, val_arr, 1)
    val = val_arr(1)
  end subroutine pixel_beam_profile

  logical function is_parent(nside_parent, pix_parent, nside_child, pix_child)
    integer(i4b), value :: nside_parent, pix_parent, nside_child, pix_child
    !--
    is_parent = (pix_parent == pix_child / (nside_child / nside_parent)**2)
  end function is_parent

  subroutine assert_(cnd, msg)
    logical :: cnd
    character(len=*), optional :: msg
    if (.not. cnd) then
       if (present(msg)) then
          print *, 'pix_matrices.f90: ASSERTION FAILED:', msg
       else 
          print *, 'pix_matrices.f90: ASSERTION FAILED'
    end if
    end if
  end subroutine assert_

  real(dp) function fastangdist(u, v)
    real(dp), dimension(3) :: u, v
    fastangdist = acos(max(min(sum(u * v), 1.d0), -1.d0))
  end function fastangdist

  subroutine query_tile_ring(nside, nside_level, ipix_D, indices) bind(c)
    integer(i4b), value :: nside, nside_level, ipix_D
    integer(i4b), dimension((nside / nside_level)**2) :: indices
    !--
    integer(i4b) :: i, n, offset

    n = (nside / nside_level)**2
    offset = fring2nest(nside_level, ipix_D) * n - 1
    do i = 1, n
       indices(i) = fnest2ring(nside, offset + i)
    end do
  end subroutine query_tile_ring

  subroutine rescale_bl_d(bl, rescaled_bl, lmax)
    integer(i4b) :: lmax
    real(dp), dimension(0:lmax) :: bl, rescaled_bl
    !--
    integer(i4b) :: l
    do l = 0, ubound(bl, 1)
       rescaled_bl(l) = bl(l) * real(2 * l + 1, dp) / (4.0_dp * pi)
    end do
  end subroutine rescale_bl_d

  subroutine rescale_bl_s(bl, rescaled_bl, lmax)
    integer(i4b) :: lmax
    real(sp), dimension(0:lmax) :: bl, rescaled_bl
    !--
    integer(i4b) :: l
    do l = 0, ubound(bl, 1)
       rescaled_bl(l) = bl(l) * real(real(2 * l + 1, dp) / (4.0_dp * pi), sp)
    end do
  end subroutine rescale_bl_s

  ! Compute a dense block between arbitrary pixels, going delta pixels out
  subroutine compute_arbitrary_smoother_block(nside, pixels, len, &
       dl, bl, lmax, ninv, nside_obs, pixrad, out) bind(c)
    integer(i4b), value :: nside, nside_obs, lmax, pixrad, len
    integer(i4b), dimension(1:len) :: pixels
    real(dp), dimension(0:lmax) :: dl, bl
    real(dp), dimension(0:12 * nside_obs**2 - 1) :: ninv
    real(sp), dimension(len, len) :: out
    !--
    integer(i4b) :: i, j, k, t, nk, nk_i, nk_j
    real(dp), dimension(3) :: vec_j, vec_i, vec_k
    real(dp), dimension(:), allocatable :: recfac, rescaled_bl, rescaled_dl
    real(dp) :: theta, radius, s
    integer(i4b), dimension(:), allocatable :: k_set_i, k_set_j
    logical :: handled

    radius = pixrad * sqrt(4 * pi / (12 * nside_obs**2))
    nk = 4 * pixrad**2

    allocate(recfac(0:lmax), rescaled_bl(0:lmax), rescaled_dl(0:lmax))
    call legendre_transform_recfac_d(recfac, lmax)
    call rescale_bl_d(bl, rescaled_bl, lmax)
    call rescale_bl_d(dl, rescaled_dl, lmax)

    !$OMP parallel default(none) private(i,j,k,t,nk_i,nk_j,vec_i,vec_j,vec_k,k_set_i,k_set_j,&
    !$OMP                                theta,s,handled) &
    !$OMP                        shared(ninv,lmax,nside_obs,radius,out,pixels,nside,len,&
    !$OMP                               nk,recfac,rescaled_bl,rescaled_dl)

    allocate(k_set_i(nk), k_set_j(nk))

    !$OMP do schedule(dynamic,1)
    do j = 1, len
       print *, 'arbitrary_smoother_block', j, len
       call pix2vec_ring(nside, pixels(j), vec_j)
       call query_disc(nside_obs, vec_j, radius, k_set_j, nk_j, nest=0)
       
       do i = 1, j - 1
          ! zero upper triangle
          out(i, j) = 0
       end do
       do i = j, len
          ! Store contribution from D in s
          call pix2vec_ring(nside, pixels(i), vec_i)
          theta = fastangdist(vec_j, vec_i)
          call pixel_beam_profile(recfac, rescaled_dl, lmax, theta, s)

          ! Add contribution from B Ninv B; add contributions around both vec_i and vec_j
          ! k_set_j
          do k = 1, nk_j
             call pix2vec_ring(nside_obs, k_set_j(k), vec_k)
             s = s + ninv(k_set_j(k)) * beamval(vec_i, vec_k) * beamval(vec_j, vec_k)
          end do

          ! k_set_i
          call query_disc(nside_obs, vec_i, radius, k_set_i, nk_i, nest=0)
          do k = 1, nk_i
             ! only handle each pixel once
             handled = .false.
             do t = 1, nk_j
                if (k_set_i(k) == k_set_j(t)) then
                   handled = .true.
                   exit
                end if
             end do
             if (.not. handled) then
                call pix2vec_ring(nside_obs, k_set_i(k), vec_k)
                s = s + ninv(k_set_i(k)) * beamval(vec_i, vec_k) * beamval(vec_j, vec_k)
             end if
          end do

          out(i, j) = real(s, sp)
       end do
    end do
    !$OMP end do
    deallocate(k_set_i, k_set_j)
    !$OMP end parallel
  contains
    real(dp) function beamval(avec, bvec)
      real(dp), dimension(3) :: avec, bvec
      !--
      call pixel_beam_profile(recfac, rescaled_bl, lmax, fastangdist(avec, bvec), beamval)
    end function beamval

  end subroutine compute_arbitrary_smoother_block

  subroutine compute_YDYt_block(nside_D1, nside_D2, triang_diag, nside_level, ipix_D1, ipix_D2, &
       recfac, rescaled_dl, lmax, out)
    integer(i4b), value :: nside_D1, nside_D2, nside_level, ipix_D1, ipix_D2, lmax, triang_diag
    real(sp), dimension(0:lmax) :: rescaled_dl, recfac
    real(sp), dimension(1:(nside_D1/nside_level)**2,1:(nside_D2/nside_level)**2) :: out
    !--
    integer(i4b) :: ipix_D1_nest, ipix_D2_nest, i, j, tilesize_D1, tilesize_D2, ipix, idx
    real(dp), dimension(3) :: vec_j
    real(dp), dimension(:,:), allocatable :: vecs_i
    real(sp), dimension(:), allocatable :: xs, out1d

    tilesize_D1 = (nside_D1 / nside_level)**2
    tilesize_D2 = (nside_D2 / nside_level)**2
    ipix_D1_nest = fring2nest(nside_level, ipix_D1)
    ipix_D2_nest = fring2nest(nside_level, ipix_D2)

    allocate(vecs_i(3, tilesize_D1))
    allocate(xs(tilesize_D1 * tilesize_D2), out1d(tilesize_D1 * tilesize_D2))

    do i = 1, tilesize_D1
       ipix = tilesize_D1 * ipix_D1_nest - 1 + i
       call pix2vec_nest(nside_D1, ipix, vecs_i(:, i))
    end do

    do j = 1, tilesize_D2
       do i = 1, tilesize_D1
       end do
    end do

    if (triang_diag == 1 .and. ipix_D1 == ipix_D2 .and. nside_D1 == nside_D2) then
       ! triangular case, fill xs triangularly

       idx = 0
       do j = 1, tilesize_D2
          ipix = tilesize_D2 * ipix_D2_nest - 1 + j
          call pix2vec_nest(nside_D2, ipix, vec_j)
          do i = j, tilesize_D1
             idx = idx + 1
             xs(idx) = real(sum(vec_j * vecs_i(:, i)), sp)
          end do
       end do

       call legendre_transform_s(rescaled_dl, recfac, lmax, xs, out1d, idx)
       idx = 0
       do j = 1, tilesize_D2
          do i = 1, j - 1
             out(i, j) = 0.0_sp
          end do
          do i = j, tilesize_D1
             idx = idx + 1
             out(i, j) = out1d(idx)
          end do
       end do

    else
       ! rectangular case
       idx = 0
       do j = 1, tilesize_D2
          ipix = tilesize_D2 * ipix_D2_nest - 1 + j
          call pix2vec_nest(nside_D2, ipix, vec_j)
          do i = 1, tilesize_D1
             idx = idx + 1
             xs(idx) = real(sum(vec_j * vecs_i(:, i)), sp)
          end do
       end do

       call legendre_transform_s(rescaled_dl, recfac, lmax, xs, out1d, idx)

       idx = 0
       do j = 1, tilesize_D2
          do i = 1, tilesize_D1
             idx = idx + 1
             out(i, j) = out1d(idx)
          end do
       end do
    end if

  end subroutine compute_YDYt_block

  subroutine sort_and_filter_gt(lst, treshold, n)
    integer(i4b), dimension(:) :: lst
    integer(i4b), value :: treshold
    integer(i4b), intent(out) :: n
    !--
    integer(i4b) :: i, j, to_insert, next

    n = 0
    do i = 1, size(lst)
       to_insert = lst(i)
       if (to_insert > treshold) then
          ! Find location to insert in lst(1:n)
          do j = 1, n
             if (lst(j) > to_insert) exit
          end do
          ! Move remaining elements in lst(1:n) up, overwrites lst(n+1)
          do j = j, n + 1
             next = lst(j)
             lst(j) = to_insert
             to_insert = next
          end do
          n = n + 1
       end if
    end do
  end subroutine sort_and_filter_gt

  subroutine insertion_sort(lst, len) bind(c)
    integer(i4b), dimension(1:len) :: lst
    integer(i4b), value :: len
    !--
    integer(i4b) :: i, j, to_insert, next, n

    ! Insertion sort
    n = 0
    do i = 1, size(lst)
       to_insert = lst(i)
       ! Find location to insert in lst(1:n)
       do j = 1, n
          if (lst(j) > to_insert) exit
       end do
       ! Move remaining elements in lst(1:n) up, overwrites lst(n+1)
       do j = j, n + 1
          next = lst(j)
          lst(j) = to_insert
          to_insert = next
       end do
       n = n + 1
    end do
  end subroutine insertion_sort

  subroutine neighbours_ring(nside, ipix, lst, n)
    integer(i4b), value :: nside, ipix
    integer(i4b), intent(out) :: n
    integer(i4b), dimension(8) :: lst
    !--
    integer(i4b) :: i
    call neighbours_nest(nside, fring2nest(nside, ipix), lst, n)
    do i = 1, n
       lst(i) = fnest2ring(nside, lst(i))
    end do
  end subroutine neighbours_ring

  ! Get the neighbours coming after (in ring ordering), in sorted order
  subroutine later_neighbours(nside, ipix, lst, n)
    integer(i4b), value :: nside, ipix
    integer(i4b), intent(out) :: n
    integer(i4b), dimension(8) :: lst
    !--
    call neighbours_ring(nside, ipix, lst, n)
    call sort_and_filter_gt(lst(:n), ipix, n)
  end subroutine later_neighbours

  ! Get the neighbours and the pixel itself in sorted order (in ring ordering)
  subroutine neighbours_and_self(nside, ipix, lst, n)
    integer(i4b), value :: nside, ipix
    integer(i4b), intent(out) :: n
    integer(i4b), dimension(9) :: lst
    !--
    call neighbours_ring(nside, ipix, lst, n)
    n = n + 1
    lst(n) = ipix
    call insertion_sort(lst(:n), n)
  end subroutine neighbours_and_self

  subroutine csc_neighbours_lower(nside, indptr, indices) bind(c)
    integer(i4b), value :: nside
    integer(i4b), dimension(0:12 * nside**2 + 1 - 1) :: indptr
    integer(i4b), dimension(0:5 * 12 * nside**2 - 12 - 1) :: indices
    !--
    integer(i4b), dimension(8) :: neighbours
    integer(i4b) :: i, j, n, nind
    nind = 0
    do j = 0, 12 * nside**2 - 1
       indptr(j) = nind
       ! Diagonal elements
       indices(nind) = j
       nind = nind + 1
       ! Sub-diagonal elements in column
       call later_neighbours(nside, j, neighbours, n)
       do i = 1, n
          indices(nind) = neighbours(i)
          nind = nind + 1
       end do
    end do
    indptr(j) = nind
    if (nind /= 5 * 12 * nside**2 - 12) then
       print *, 'pix_matrices.f90: assertion failed'
       stop
    end if
  end subroutine csc_neighbours_lower

  subroutine csc_neighbours_full(nside, indptr, indices) bind(c)
    integer(i4b), value :: nside
    integer(i4b), dimension(0:12 * nside**2 + 1 - 1) :: indptr
    integer(i4b), dimension(0:9 * 12 * nside**2 - 24 - 1) :: indices
    !--
    integer(i4b) :: j, n, nind
    nind = 0
    do j = 0, 12 * nside**2 - 1
       indptr(j) = nind
       call neighbours_and_self(nside, j, indices(nind:nind + 9 - 1), n)
       nind = nind + n
    end do
    indptr(j) = nind
    if (nind /= 9 * 12 * nside**2 - 24) then
       print *, 'pix_matrices.f90: assertion failed'
       stop
    end if
  end subroutine csc_neighbours_full

  subroutine csc_24_neighbours(nside, indptr, indices) bind(c)
    ! Gets the sparsity pattern for (approximately) 24 neighbours, that is,
    ! neighbours of neighbours. DOES NOT SORT, and gives full pattern.

    integer(i4b), value :: nside
    integer(i4b), dimension(0:12 * nside**2) :: indptr
    integer(i4b), dimension(0:25 * 12 * nside**2 - 1) :: indices
    !--
    integer(i4b), dimension(8) :: neighbours1, neighbours2
    integer(i4b) :: j, k, t, npix, nneigh1, nneigh2, nind
    logical(lgt), dimension(:), allocatable :: hit

    npix = 12 * nside**2
    allocate(hit(0:npix-1))

    hit = .false.
    nind = 0
    do j = 0, npix - 1
       indptr(j) = nind
       ! Diagonal
       hit(j) = .true.
       indices(nind) = j
       nind = nind + 1

       ! Neighbours 2 pixels away. Keep hit-map to avoid duplicates
       call neighbours_ring(nside, j, neighbours1, nneigh1)
       do k = 1, nneigh1
          hit(neighbours1(k)) = .true.
          indices(nind) = neighbours1(k)
          nind = nind + 1
       end do
       do k = 1, nneigh1
          call neighbours_ring(nside, neighbours1(k), neighbours2, nneigh2)
          do t = 1, nneigh2
             if (.not. hit(neighbours2(t))) then
                hit(neighbours2(t)) = .true.
                indices(nind) = neighbours2(t)
                nind = nind + 1
             end if
          end do
       end do

       ! Clear hit
       do k = indices(indptr(j)), nind - 1
          hit(indices(k)) = .false.
       end do

       call insertion_sort(indices(indptr(j):nind - 1), nind - indptr(j))
    end do
    indptr(npix) = nind
  end subroutine csc_24_neighbours

  ! get lower triangular pattern
  subroutine csc_neighbours_adaptive(nside, indptr, indices, radiusmap, nnz, nind) bind(c)
    integer(i4b), value :: nside, nnz
    integer(i4b), dimension(0:nnz - 1) :: indptr
    integer(i4b), dimension(0:nnz - 1) :: indices
    integer(i4b), intent(out) :: nind ! number of nnz used, -1 if more needed
    integer(i1b), dimension(0:12 * nside**2 - 1) :: radiusmap
    !--
    real(dp) :: pixwidth, radius
    integer(i4b) :: j, n, k, npicked, npix, icorner
    real(dp), dimension(3) :: vec, vec_other
    real(dp), dimension(3, 4) :: corners
    real(dp) :: theta

    npix = 12 * nside**2
    pixwidth = sqrt(4 * pi / npix)

    nind = 0
    do j = 0, npix - 1
       indptr(j) = nind
       if (radiusmap(j) == 0) then
          indices(nind) = j
          n = 1
       else
          if (radiusmap(j) == 1) then
             if (nnz - nind < 9) then
                nind = -1
                return
             end if
             call neighbours_ring(nside, j, indices(nind:nind + 8 - 1), n)
             n = n + 1
             indices(nind + n - 1) = j
          else
             call pix2vec_ring(nside, j, vec, corners)
             n = nnz - nind
             radius = radiusmap(j) * pixwidth
             ! use crude upper bound to abort in time since HEALPix has no error handling;
             ! 2 * (area of disc one pixel larger / area of pixels)
             if (2 * 2 * pi * (radius + pixwidth)**2 / pixwidth**2 .ge. n) then
                nind = -1
                return
             end if
             ! Call query_disc...
             call query_disc(nside, vec, min(radiusmap(j) * (pixwidth + 1), pi), indices(nind:), &
                  n, nest=1, inclusive=0)
             ! fake inclusive=1, see http://sourceforge.net/p/healpix/bugs/34/
             do k = 0, n - 1
                indices(nind + k) = fnest2ring(nside, indices(nind + k))
             end do
             ! For each pixel, check if it would have reached past this pixels' nearest corner
             npicked = 0
             do k = 0, n - 1
                call pix2vec_ring(nside, indices(nind + k), vec_other)
                theta = 4 ! > pi
                do icorner = 1, 4
                   theta = min(theta, fastangdist(corners(:, icorner), vec_other))
                end do
                if (radiusmap(indices(nind + k)) * pixwidth > theta) then
                   ! Include it!
                   npicked = npicked + 1
                   indices(nind + npicked - 1) = indices(nind + k) ! we always have k >= npicked
                end if
             end do
             n = npicked
          end if
          call sort_and_filter_gt(indices(nind:nind + n - 1), j - 1, n)
       end if
       nind = nind + n
    end do
    indptr(j) = nind
  contains
    function dist_to_nearest_corner(nside, vec, ipix) result(theta)
      real(dp), dimension(3) :: vec
      integer(i4b) :: ipix, nside
      real(dp) :: theta
      !--
      real(dp), dimension(3) :: ivec
      real(dp), dimension(3, 4) :: corners
      integer(i4b) :: c
      call pix2vec_ring(nside, ipix, ivec, corners)
      theta = 100 ! > pi
      do c = 1, 4
         theta = min(theta, fastangdist(vec, corners(:, c)))
      end do
    end function dist_to_nearest_corner
  end subroutine csc_neighbours_adaptive
  
  ! Takes sorted lower-triangular CSC indices and produces *unsorted* full matrix CSC indices
  subroutine mirror_csc_indices(n, indptr_in, indices_in, indptr_out, indices_out) bind(c)
    integer(i4b), value :: n
    integer(i4b), dimension(0:n) :: indptr_in, indptr_out
    integer(i4b), dimension(0:indptr_in(n) - 1) :: indices_in
    integer(i4b), dimension(0:2 * indptr_in(n) - n - 1) :: indices_out
    !--
    integer(i4b), dimension(:), allocatable :: entries_per_row, col_lens
    integer(i4b) :: j, iptr, k, kptr, i

    allocate(entries_per_row(0:n - 1))

    ! Count entries per row outside of diagonal
    entries_per_row = 0
    do j = 0, n - 1
       do iptr = indptr_in(j) + 1, indptr_in(j + 1) - 1
          i = indices_in(iptr)
          entries_per_row(i) = entries_per_row(i) + 1
       end do
    end do

    ! Set up indptr_out. indices_out should contain the indices in
    ! the column j in indptr/indices_in, + the indices in row j
    ! (across the columns in indptr/indices_in).
    indptr_out(0) = 0
    do j = 0, n - 1
       k = entries_per_row(j) + (indptr_in(j + 1) - indptr_in(j)) ! row + (col & diagonal)
       indptr_out(j + 1) = indptr_out(j) + k
    end do

    deallocate(entries_per_row)
    allocate(col_lens(0:n - 1))

    ! copy data
    col_lens = 0
    do j = 0, n - 1
       ! diagonal entry
       kptr = indptr_out(j) + col_lens(j)
       indices_out(kptr) = j
       col_lens(j) = col_lens(j) + 1
       ! loop over sub-diagonal entries
       do iptr = indptr_in(j) + 1, indptr_in(j + 1) - 1
          i = indices_in(iptr)
          ! Copy entry from row to corresponding column and increase column len
          kptr = indptr_out(i) + col_lens(i)
          indices_out(kptr) = j
          col_lens(i) = col_lens(i) + 1
          ! Copy column entry
          kptr = indptr_out(j) + col_lens(j)
          indices_out(kptr) = i
          col_lens(j) = col_lens(j) + 1
       end do
    end do
  end subroutine mirror_csc_indices

  ! Computes the lower half of the symmetric beam matrix as a block sparse CSC
  subroutine compute_csc_beam_matrix(triang_diag, nside_left, nside_right, nside_level, dl, lmax, &
        indptr, n, indices, blocks) bind(c)
    integer(i4b), value :: nside_left, nside_right, nside_level, lmax, triang_diag, n
    real(sp), dimension(0:lmax) :: dl
    integer(i4b), dimension(0:n) :: indptr
    integer(i4b), dimension(0:indptr(n) - 1) :: indices
    real(sp), dimension(1_i8b:int((nside_left/nside_level)**2, i8b), &
                        1_i8b:int((nside_right/nside_level)**2, i8b), &
                        0_i8b:int(indptr(n) - 1, i8b)) :: blocks
    !--
    integer(i4b) :: i, iptr, j
    real(sp), dimension(:), allocatable :: recfac, rescaled_dl
    
    allocate(recfac(0:lmax))
    allocate(rescaled_dl(0:lmax))
    call rescale_bl_s(dl, rescaled_dl, lmax)
    call legendre_transform_recfac_s(recfac, lmax)

    !$OMP parallel default(shared) private(i,j,iptr)
    !$OMP do schedule(dynamic,1)
    do j = 0, n - 1
       if (mod(j, 10000) == 0) print *, j, 'of', n
       do iptr = indptr(j), indptr(j + 1) - 1
          i = indices(iptr)
          call compute_YDYt_block(nside_left, nside_right, triang_diag, nside_level, &
               i, j, recfac, rescaled_dl, lmax, blocks(:, :, iptr))
       end do
    end do
    !$OMP end do
    !$OMP end parallel
  end subroutine compute_csc_beam_matrix

  subroutine convert_ring_dp_to_tiled_sp(nside, nside_level, map, tiled_map) bind(c)
    integer(i4b), value :: nside, nside_level
    real(dp), dimension(0:12*nside**2 - 1), intent(in) :: map
    real(sp), dimension((nside/nside_level)**2, 0:12*nside_level**2 - 1), intent(out) :: tiled_map
    !--
    integer(i4b) :: nside_tile, tilesize, j, itile, itile_nest, ipix_ring

    nside_tile = (nside / nside_level)
    tilesize = nside_tile**2

    do itile = 0, 12 * nside_level**2-1
       itile_nest = fring2nest(nside_level, itile)
       do j = 1, tilesize
          ipix_ring = fnest2ring(nside, itile_nest * tilesize + j - 1)
          tiled_map(j, itile) = real(map(ipix_ring), sp)
       end do
    end do
  end subroutine convert_ring_dp_to_tiled_sp
    
  subroutine convert_tiled_sp_to_ring_dp(nside, nside_level, tiled_map, map) bind(c)
    integer(i4b), value :: nside, nside_level
    real(sp), dimension((nside/nside_level)**2, 0:12*nside_level**2 - 1) :: tiled_map
    real(dp), dimension(0:12*nside**2 - 1), intent(out) :: map
    !--
    integer(i4b) :: nside_tile, tilesize, j, itile, itile_nest, ipix_ring

    nside_tile = (nside / nside_level)
    tilesize = nside_tile**2

    do itile = 0, 12 * nside_level**2-1
       itile_nest = fring2nest(nside_level, itile)
       do j = 1, tilesize
          ipix_ring = fnest2ring(nside, itile_nest * tilesize + j - 1)
          map(ipix_ring) = real(tiled_map(j, itile), dp)
       end do
    end do
  end subroutine convert_tiled_sp_to_ring_dp

  subroutine scale_rows(diag, buf)
    real(sp), dimension(:) :: diag
    real(sp), dimension(:,:) :: buf
    !--
    integer(i8b) :: i, j
    do j = 1, ubound(buf, 2)
       do i = 1, ubound(buf, 1)
          buf(i, j) = buf(i, j) * diag(i)
       end do
    end do
  end subroutine scale_rows

  subroutine zero_upper(buf)
    real(sp), dimension(:,:) :: buf
    !--
    integer(i8b) :: i, j
    do j = 1, ubound(buf, 2)
       do i = 1, j - 1
          buf(i, j) = 0.0_dp
       end do
    end do
  end subroutine zero_upper




! ------------------------------------------------------------------------------
! IN PROGRESS
! ------------------------------------------------------------------------------

  subroutine block_A_D_B( bs_left, bs_mid, bs_right, n, indptr, indices, &
                          A_blocks, B_blocks, C_blocks, D ) bind(c,name='block_A_D_B')
    
    ! Matrix multiplication of tiled operators, A * D * B, where
    !   A : sparse LHS
    !   D : diagonal
    !   B : sparse RHS
    ! A, B are non-symmetric matrices with the same sparsity pattern. 
    ! The output matrix C is also non-symmetric.
    
    integer(i4b), value                          :: bs_left, bs_mid, bs_right, n
    real(sp), dimension(bs_left, 0:n - 1)        :: D
    integer(i4b), dimension(0:n)                 :: indptr
    integer(i4b), dimension(0:indptr(n) - 1)     :: indices
    
    !integer(i8b)                   :: npx_tile_L, npx_tile_M, npx_tile_R, Ntiles
    integer(i4b)                   :: i, j, k, l, iptr, kptr, lptr
    
    ! Dimensions of matrix in each tile
!    npx_tile_L = int(bs_left, i8b)
!    npx_tile_M = int(bs_mid, i8b)
!    npx_tile_R = int(bs_right, i8b)
!    Ntiles = int(indptr(n), i8b) - 1_i8b
    
    ! Matrices for each block must have correct no. of pixels
    real(sp), dimension(1_i8b:int(bs_left, i8b), 1_i8b:int(bs_mid, i8b), 0_i8b:int(indptr(n), i8b) - 1_i8b) :: A_blocks
    real(sp), dimension(1_i8b:int(bs_mid, i8b), 1_i8b:int(bs_right, i8b), 0_i8b:int(indptr(n), i8b) - 1_i8b) :: B_blocks
    real(sp), dimension(1_i8b:int(bs_left, i8b), 1_i8b:int(bs_right, i8b), 0_i8b:int(indptr(n), i8b) - 1_i8b) :: C_blocks
    real(sp), dimension(:,:), allocatable :: B_buffer
    
    ! Sparse CSC: indptr(n) gives the index of the last row, hence the size 
    ! of 'indices'. 'n' is the number of pixels 
    
    ! A_blocks : Couplings for a given operator, dimension 
    ! All blocks have shape: (px_in_tile_LHS, px_in_tile_RHS, Ntiles)
    ! n = Ntiles (or Ntiles + 1?)
    
    ! Assume in comments that indptr/indices are given in CSC ordering.
    ! The *full* matrix sparsity pattern is given for A, B, and C
    
    ! -------------- GO ------------------
    !$OMP parallel default(none) &
    !$OMP     shared(indptr, indices, A_blocks, B_blocks, C_blocks, D, &
    !$OMP            n, bs_left, bs_mid, bs_right) &
    !$OMP     private(i, j, k, l, iptr, kptr, lptr, B_buffer)

    allocate(B_buffer(bs_mid, bs_right))
    
    ! Multiply all blocks in sparsity pattern
    ! (i, j) are tile indices in C
    ! (k, j) are tile indices in A
    ! (l, j) are tile indices in B
    
    !$OMP do schedule(dynamic,1)
    do j = 0, n - 1
       do iptr = indptr(j), indptr(j + 1) - 1
          i = indices(iptr)
          C_blocks(:, :, iptr) = 0.0_dp
          do kptr = indptr(i), indptr(i + 1) - 1
             k = indices(kptr)
             do lptr = indptr(j), indptr(j + 1) - 1
                l = indices(lptr)
                if (l == k) then
                
                   ! If resulting block lies within sparsity pattern,
                   ! multiply blocks to get C[i,j] = A[k,j] * D * B[l,j]
                   B_buffer = B_blocks(:, :, lptr)
                   call scale_rows(D(:, k), B_buffer) ! (D * B)
                   ! FIXME: Really easy to get dimensions of bs_xxx wrong!
                   write(*,*) i, j, k, l
                   call SGEMM('N', 'N', bs_left, bs_right, bs_mid, 1.0_sp, &
                              A_blocks(:, :, kptr), bs_left, &
                              B_buffer, bs_mid, 1.0_sp, &
                              C_blocks(:, :, iptr), bs_left)
                end if
             end do
          end do
       end do
    end do
    !$OMP end do

    deallocate(B_buffer)
    !$OMP end parallel

  end subroutine block_A_D_B





  subroutine block_At_D_A( bs_left, bs_right, n, &
                           A_indptr, A_indices, A_blocks, D, &
                           C_indptr, C_indices, C_blocks) bind(c,name='block_At_D_A')
    
    ! Do sparse multiplication of diagonal matrix D with matrix A (RHS) and its 
    ! transpose A^T (LHS). The result is symmetric, so only the lower triangular 
    ! part of the sparsity pattern is returned.
    !
    ! The sparsity pattern is defined for some "tiled map". Each input operator 
    ! must have been tiled according to the same tiling pattern. The block 
    ! sparse matrix multiplication then proceeds according to the specified 
    ! tilings.
    !
    ! Parameters
    ! ----------
    !
    ! bs_left, bs_right : int
    !
    !   No. pixels on each side of the operator matrix within each block.
    !   A has shape (bs_left, bs_right) (so A^T has shape (bs_right, bs_left))
    !   C has shape (bs_right, bs_right)
    ! 
    ! n : int
    !
    !   Number of pixels in tiled map.
    !
    ! A_indptr, A_indices, C_indptr, C_indices : CSC index arrays
    !
    !   Row pointers and row indices of non-zero blocks in the sparsity pattern.
    !   (See csc_24_neighbours(), csc_neighbours_lower())
    !   
    !   A can be full or lower triangular sparsity pattern (depending on 
    !   whether A is symmetric).
    !
    !   C must be the lower triangular sparsity pattern (since C is symmetric)
    !
    ! A_blocks, C_blocks, D : tiled operator matrices/maps
    !
    !   Input/output operators, that have been tiled according to the sparsity 
    !   pattern. (See compute_csc_beam_matrix() for matrix operators, and 
    !   convert_ring_dp_to_tiled_sp() for vector operators)
    !   
    !   Blocks (tiled operators) have dimensions:
    !
    !     A_blocks:  (bs_left, bs_right, Ntiles)
    !     C_blocks:  (bs_right, bs_right, Ntiles)
    !     D:         (bs_left, Ntiles)
    
    integer(i4b), value                        :: bs_left, bs_right, n
    real(sp), dimension(bs_left, 0:n - 1)      :: D
    integer(i4b), dimension(0:n)               :: A_indptr, C_indptr
    integer(i4b), dimension(0:A_indptr(n) - 1) :: A_indices
    integer(i4b), dimension(0:C_indptr(n) - 1) :: C_indices
    integer(i4b)                               :: iptr, i, j, kptr, k, lptr
    real(sp), dimension(:,:), allocatable      :: buf
    real(sp), dimension(1_i8b:int(bs_left, i8b), 1_i8b:int(bs_right, i8b), &
                        0_i8b:int(A_indptr(n), i8b) - 1_i8b) :: A_blocks
    real(sp), dimension(1_i8b:int(bs_right, i8b), 1_i8b:int(bs_right, i8b), &
                        0_i8b:int(C_indptr(n), i8b) - 1_i8b) :: C_blocks

    ! Assume in comments that indptr/indices are given in CSC ordering.
    ! The full matrix is given for A, the lower matrix for C.

    !$OMP parallel default(none) &
    !$OMP     shared(C_indptr,C_indices,C_blocks,A_indptr,A_indices,A_blocks, &
    !$OMP            n,bs_left,bs_right,D) &
    !$OMP     private(i, j, k, iptr, kptr, lptr, buf)

    allocate(buf(bs_left, bs_right))
    
    ! (i, j) are tile indices in C
    ! (k, j) are tile indices in A
    ! (l, j) are tile indices in B
    
    !$OMP do schedule(dynamic,1)
    do j = 0, n - 1
       iptr = C_indptr(j)
       call assert_(C_indices(iptr) == j, 'Diagonal not in expected place in C_indices')

       ! Diagonal C block; i=j. We could use SYRK, though only if D
       ! is non-negative, and it would require taking the sqrt of D,
       ! so it's not obvious it will be faster for small blocksizes
       ! (?).  So we just zero the upper half manually for now.
       C_blocks(:, :, iptr) = 0.0_sp
       do kptr = A_indptr(j), A_indptr(j + 1) - 1
          k = A_indices(kptr)
          buf = A_blocks(:, :, kptr)
          call scale_rows(D(:, k), buf)
          call SGEMM('T', 'N', bs_right, bs_right, bs_left, 1.0_sp, &
               A_blocks(:, :, kptr), bs_left, &
               buf, bs_left, &
               1.0_sp, C_blocks(:, :, iptr), bs_right)
       end do
       call zero_upper(C_blocks(:, :, iptr))

       ! Sub-diagonal blocks, i>j.
       do iptr = C_indptr(j) + 1, C_indptr(j + 1) - 1
          i = C_indices(iptr)
          C_blocks(:, :, iptr) = 0.0_dp
          do kptr = A_indptr(i), A_indptr(i + 1) - 1
             k = A_indices(kptr)
             do lptr = A_indptr(j), A_indptr(j + 1) - 1
                if (A_indices(lptr) == k) then
                   buf = A_blocks(:, :, lptr)
                   call scale_rows(D(:, k), buf)
                   call SGEMM('T', 'N', bs_right, bs_right, bs_left, 1.0_sp, &
                       A_blocks(:, :, kptr), bs_left, &
                       buf, bs_left, &
                       1.0_sp, C_blocks(:, :, iptr), bs_right)
                end if
             end do
          end do
       end do
    end do
    !$OMP end do

    deallocate(buf)

    !$OMP end parallel

  end subroutine block_At_D_A

  subroutine block_At_D_A_callback(bs_left, bs_right, n, A_indptr, A_indices, A_callback, &
       D, C_indptr, C_indices, C_blocks)
    integer(i4b), parameter :: cachesize = 16

    integer(i4b), value :: bs_left, bs_right, n
    real(sp), dimension(bs_left, 0:n - 1) :: D
    integer(i4b), dimension(0:n) :: A_indptr, C_indptr
    integer(i4b), dimension(0:A_indptr(n) - 1) :: A_indices
    integer(i4b), dimension(0:C_indptr(n) - 1) :: C_indices
    real(sp), dimension(1_i8b:int(bs_right, i8b), 1_i8b:int(bs_right, i8b), &
                        0_i8b:int(C_indptr(n), i8b) - 1_i8b) :: C_blocks
    interface
       subroutine A_callback(i, j, blk)
         use types_as_c
         integer(i4b), value :: i, j
         real(sp), dimension(:, :) :: blk
       end subroutine A_callback
    end interface
    !--
    integer(i4b) :: iptr, i, j, kptr, k, lptr
    real(sp), dimension(:,:), allocatable :: buf

    integer(i8b) :: age, misses
    integer(i4b), dimension(cachesize) :: cache_indices
    integer(i8b), dimension(cachesize) :: cache_ages
    real(sp), dimension(:,:,:), allocatable, target :: cache_blocks
    real(sp), dimension(:,:), pointer :: A_kptr, A_lptr

    integer :: omp_get_num_threads, omp_get_thread_num

    ! We call a callback function to compute blocks from A on the fly.
    ! To reuse blocks we keep a cache of blocks around and evict the
    ! least recently created block.

    ! Assume in comments that indptr/indices are given in CSC ordering.
    ! The full matrix is given for A, the lower matrix for C.

    misses = 0

    !$OMP parallel default(none) &
    !$OMP     shared(C_indptr,C_indices,C_blocks,A_indptr,A_indices, &
    !$OMP            n,bs_left,bs_right,D) &
    !$OMP     private(i, j, k, iptr, kptr, lptr, buf,cache_ages,cache_indices,cache_blocks,age, &
    !$OMP             A_kptr, A_lptr) &
    !$OMP     reduction(+:misses)

    age = 0
    cache_ages = 0
    cache_indices = -1
    allocate(cache_blocks(bs_left, bs_right, cachesize))
    allocate(buf(bs_left, bs_right))

    ! Use static scheduling with no chunk size to minimize overlap between threads

    !$OMP do schedule(static)
    do j = 0, n - 1
       if (omp_get_thread_num() == omp_get_num_threads() - 1) then
          print *, 'block_At_D_A_callback:', j, 'of', n
       end if
       iptr = C_indptr(j)
       call assert_(C_indices(iptr) == j, 'Diagonal not in expected place in C_indices')

       ! Diagonal C block; i=j. We could use SYRK, though only if D
       ! is non-negative, and it would require taking the sqrt of D,
       ! so it's not obvious it will be faster for small blocksizes
       ! (?).  So we just zero the upper half manually for now.
       C_blocks(:, :, iptr) = 0.0_sp
       do kptr = A_indptr(j), A_indptr(j + 1) - 1
          k = A_indices(kptr)
          call get_A_block(k, j, kptr, cache_indices, cache_ages, cache_blocks, A_kptr, age, misses)

          buf = A_kptr
          call scale_rows(D(:, k), buf)
          call SGEMM('T', 'N', bs_right, bs_right, bs_left, 1.0_sp, &
               A_kptr, bs_left, &
               buf, bs_left, &
               1.0_sp, C_blocks(:, :, iptr), bs_right)
       end do
       call zero_upper(C_blocks(:, :, iptr))

       ! Sub-diagonal blocks, i>j.
       do iptr = C_indptr(j) + 1, C_indptr(j + 1) - 1
          i = C_indices(iptr)
          C_blocks(:, :, iptr) = 0.0_dp
          do kptr = A_indptr(i), A_indptr(i + 1) - 1
             k = A_indices(kptr)
             do lptr = A_indptr(j), A_indptr(j + 1) - 1
                if (A_indices(lptr) == k) then

                   call get_A_block(k, i, kptr, cache_indices, cache_ages, cache_blocks, &
                                    A_kptr, age, misses)
                   call get_A_block(k, j, lptr, cache_indices, cache_ages, cache_blocks, &
                                    A_lptr, age, misses)
                   
                   buf = A_lptr

                   call scale_rows(D(:, k), buf)
                   call SGEMM('T', 'N', bs_right, bs_right, bs_left, 1.0_sp, &
                       A_kptr, bs_left, &
                       buf, bs_left, &
                       1.0_sp, C_blocks(:, :, iptr), bs_right)
                end if
             end do
          end do
       end do
    end do
    !$OMP end do

    deallocate(buf)
    deallocate(cache_blocks)

    !$OMP end parallel

    print *, 'block_At_D_A_callback: misses/nnz', misses, A_indptr(n), misses / real(A_indptr(n), dp)
  contains
    subroutine get_A_block(i, j, index, cache_indices, cache_ages, cache_blocks, ptr, age, misses)
      integer(i4b), value :: i, j, index
      integer(i8b), intent(inout) :: age, misses
      real(sp), dimension(:,:), pointer :: ptr
      integer(i4b), dimension(:) :: cache_indices
      integer(i8b), dimension(:) :: cache_ages
      real(sp), dimension(:,:,:), target :: cache_blocks
      !--
      integer(i4b) :: t, oldest
      integer(i8b) :: oldest_age
      oldest_age = age
      oldest = 1
      do t = 1, ubound(cache_indices,1)
         if (cache_indices(t) == index) then
            ptr => cache_blocks(:, :, t)
            return
         else if (cache_ages(t) < oldest_age) then
            oldest_age = cache_ages(t)
            oldest = t
         end if
      end do
      ! Not found
      misses = misses + 1
      call A_callback(i, j, cache_blocks(:, :, oldest))
      ptr => cache_blocks(:, :, oldest)
      cache_indices(oldest) = index
      age = age + 1
      cache_ages(oldest) = age
    end subroutine get_A_block

  end subroutine block_At_D_A_callback


  subroutine compute_Bt_Ni_B(nside, nside_obs, nside_level, dl, lmax, ninv_tiled, &
                             B_indptr, B_indices, C_indptr, C_indices, C_blocks) &
                            bind(c,name='compute_Bt_Ni_B')
    integer(i4b), value :: nside, nside_obs, nside_level, lmax
    real(sp), dimension(0:lmax) :: dl
    integer(i4b), dimension(0:12 * nside_level**2) :: B_indptr, C_indptr
    integer(i4b), dimension(0:B_indptr(12 * nside_level**2) - 1) :: B_indices
    integer(i4b), dimension(0:C_indptr(12 * nside_level**2) - 1) :: C_indices
    real(sp), dimension(1_i8b:int((nside/nside_level)**2, i8b), &
                        1_i8b:int((nside/nside_level)**2, i8b), &
                        0_i8b:int(C_indptr(12 * nside_level**2) - 1, i8b)) :: C_blocks
    real(sp), dimension((nside_obs / nside_level)**2, 0:12 * nside_level**2 - 1) :: ninv_tiled
    !--
    real(sp), dimension(:), allocatable :: recfac, rescaled_dl
    
    allocate(recfac(0:lmax))
    allocate(rescaled_dl(0:lmax))
    call rescale_bl_s(dl, rescaled_dl, lmax)
    call legendre_transform_recfac_s(recfac, lmax)

    call block_At_D_A_callback((nside_obs / nside_level)**2, (nside / nside_level)**2, &
         12 * nside_level**2, B_indptr, B_indices, get_B_block, ninv_tiled, &
         C_indptr, C_indices, C_blocks)
  contains
    subroutine get_B_block(i, j, blk)
      integer(i4b), value :: i, j
      real(sp), dimension(:, :) :: blk
      !--
      call compute_YDYt_block(nside_obs, nside, 0, nside_level, &
               i, j, recfac, rescaled_dl, lmax, blk)
    end subroutine get_B_block
  end subroutine compute_Bt_Ni_B

  subroutine block_incomplete_cholesky_factor(bs, n, indptr, indices, blocks, ridge, info) bind(c)
    integer(i4b), value :: bs, n
    integer(i4b), dimension(0:n) :: indptr
    integer(i4b), dimension(0:indptr(n) - 1) :: indices
    real(sp), dimension(1_i8b:int(bs, i8b), 1_i8b:int(bs, i8b), 0_i8b:int(indptr(n), i8b)) :: blocks
    real(sp), value :: ridge
    integer(i4b), intent(out) :: info
    !--
    integer(i4b) :: iptr, diagptr, i, j, kptr, k, lptr, t
    logical :: found
    real(sp), dimension(bs, bs) :: buf
    ! Follow notation of FLAME paper; for every step, we have
    ! the remaining bottom-right matrix partitioned as
    ! [ A11 A12 ]
    ! [ A21 A22 ]
    ! NOTE: Note sure about IKJ vs KIJ and so on, the indices here are probably invented
    ! rather than taking on their traditional meaning.
    ! TODO: k refers to source, j to modified dest, so k and j should switch meanings.
    info = 0

    ! Add ridge to diagonal
    ! TODO: Move to inner loop to avoid memory transfer
    if (ridge /= 0.0_dp) then
       do j = 0, n - 1
          iptr = indptr(j)
          do t = 1, bs
             blocks(t, t, iptr) = blocks(t, t, iptr) + ridge
          end do
       end do
    end if

    do j = 0, n - 1
       ! Factor A11
       diagptr = indptr(j)
       call SPOTRF('L', bs, blocks(:, :, diagptr), bs, info)
       if (info /= 0) then
          return
       end if
       do iptr = indptr(j) + 1, indptr(j + 1) - 1
          ! A21 <- A21 * L^-T
          call STRSM('Right', 'Lower', 'Transpose', 'Not unit triangular', &
                     bs, bs, 1.0_sp, blocks(:, :, diagptr), bs, blocks(:, :, iptr), bs)
          ! Handle diagonal part of A22 <- A22 - A21 * A21^T update right away
          i = indices(iptr)
          kptr = indptr(i)
          call SSYRK('L', 'N', bs, bs, &
                     -1.0_sp, blocks(:, :, iptr), bs, &
                     1.0_sp, blocks(:, :, kptr), bs)
       end do
       ! "Barrier", need to complete above loop for A21 <- A21 * L^-T.

       ! Now handle non-diagonal outer-product blocks to complete A22 <- A22 - A21 * A21^T
       do iptr = indptr(j) + 1, indptr(j + 1) - 1
          i = indices(iptr)
          ! We're treating block stored on row i; iterate over blocks above row i in 
          ! same column for blocks we should outer-product with
          do kptr = indptr(j) + 1, iptr - 1
             k = indices(kptr)
             ! Should update A22 -= A21(iptr) * A21(kptr)^T; search for target block
             ! below diagonal in row i, column k
             found = .false.
             do lptr = indptr(k) + 1, indptr(k + 1) - 1
                if (indices(lptr) == i) then
                   ! Update
                   call SGEMM('N', 'T', bs, bs, bs, &
                        -1.0_sp, blocks(:, :, iptr), bs, blocks(:, :, kptr), bs, &
                        1.0_sp, blocks(:, :, lptr), bs)
                   found = .true.
                   exit
                end if
             end do
             if ((.not. found) .and. .false.) then
                ! MILU step: Add discarded elements to diagonal of corresponding row instead
                lptr = indptr(i) ! diagonal on row/col i
                ! gemm -> buf
                call SGEMM('N', 'T', bs, bs, bs, &
                     -1.0_sp, blocks(:, :, kptr), bs, blocks(:, :, iptr), bs, &
                     0.0_sp, buf, bs)
                ! sum each row of buf and add to diagonal of blocks(:,:,lptr)
                do t = 1, bs!
                   blocks(t, t, lptr) = blocks(t, t, lptr) + sum(buf(t,:))
                end do
             end if
          end do
       end do
    end do
  end subroutine block_incomplete_cholesky_factor

  subroutine block_triangular_solve(trans, bs, n, indptr, indices, blocks, x) bind(c)
    integer(i4b), value :: trans ! 1 if transpose, 0 otherwise
    integer(i4b), value :: bs, n
    integer(i4b), dimension(0_i8b:int(n, i8b)) :: indptr
    integer(i4b), dimension(0_i8b:int(indptr(n), i8b)) :: indices
    real(sp), dimension(1_i8b:int(bs, i8b), 1_i8b:int(bs, i8b), 0_i8b:int(indptr(n), i8b)) :: blocks
    real(sp), dimension(1_i8b:int(bs, i8b), 0_i8b:int(n, i8b)-1_i8b) :: x
    !--
    integer(i4b) :: jptr, j, iptr, i

    if (trans == 0) then
       ! CSC ordering

       do j = 0, n - 1
          ! Solve for diagonal block, x_j <- A_jj^-1 x_j
          call STRSV('Lower', 'Not transposed', 'Not unit triangular', &
               bs, blocks(:, :, indptr(j)), bs, x(:, j), 1)
          ! Push updates for blocks in this column, x_i <- x_i - A_ij x_j
          do iptr = indptr(j) + 1, indptr(j + 1) - 1
             i = indices(iptr)
             call SGEMV('Not transposed', bs, bs, -1.0_sp, blocks(:, :, iptr), bs, &
                  x(:, j), 1, 1.0_sp, x(:, i), 1)
          end do
       end do

    else
       ! Comments refer to A_ij as if it is transposed, i.e., just assume CSR
       ! ordering instead of CSC
       do i = n - 1, 0, -1
          ! Pull updates from blocks in this row, x_i <- x_i - A_ij x_j
          do jptr = indptr(i) + 1, indptr(i + 1) - 1
             j = indices(jptr)
             call SGEMV('Transposed', bs, bs, -1.0_sp, blocks(:, :, jptr), bs, &
                  x(:, j), 1, 1.0_sp, x(:, i), 1)
          end do

          ! Solve for diagonal block, x_i <- A_ii^-1 x_i
          call STRSV('Lower', 'Transposed', 'Not unit triangular', &
               bs, blocks(:, :, indptr(i)), bs, x(:, i), 1)
       end do

    end if
  end subroutine block_triangular_solve


end module pix_matrices
