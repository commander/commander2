from __future__ import division

import numpy as np

import h5py
from ...sphere import *
from ...logger_utils import *

class IndentLogger(object):
    def __init__(self, wrapped, indent):
        self.wrapped = wrapped
        self._indent = indent

    def debug(self, msg):
        self.wrapped.debug(self._indent + msg)

    def info(self, msg):
        self.wrapped.info(self._indent + msg)

    def warning(self, msg):
        self.wrapped.warning(self._indent + msg)

    def error(self, msg):
        self.wrapped.error(self._indent + msg)

def indented_logger(logger):
    if isinstance(logger, IndentLogger):
        return IndentLogger(logger, logger._indent + '  ')
    else:
        return IndentLogger(logger, '  ')

def truncate_alm(alm, lmax_from, lmax_to):
    s = np.zeros(lmax_from + 1)
    s[:lmax_to + 1] = 1
    return alm[scatter_l_to_lm(s) == 1]

def pad_alm(alm, lmax_from, lmax_to):
    out = np.zeros((lmax_to + 1)**2)
    s = np.zeros(lmax_to + 1)
    s[:lmax_from + 1] = 1
    out[scatter_l_to_lm(s) == 1] = alm
    return out

class MgSolver:
    def __init__(self, levels, observations, mixing_maps, Sl):
        """
        Manages MG level specification and precomputed data for a given 
        observational setup.
        
        Mixing maps is a 2D NumPy array of a_lm arrays for the mixing maps. 
        'None' is taken to mean CMB (identity), which is special-cased.
        
        Sl is an array of size Nobs. Components must all have the same length.
        """
        
        #self.obs_hexdigest = observation.instrumental_digest()
        self.levels = levels
        Sl = np.array(Sl)
        lmax = Sl.shape[1] - 1
        self.lmax = lmax # Max. l is defined by CMB component
        self.Nobs = len(observations)
        
        self.ninv = []
        self.beam = []
        self.dl = 1. / np.array(Sl)
        print "Shape of prior:", self.dl.shape
        
        self.mixing_maps = mixing_maps
        
        # Loop through observations, load noise and beams
        for obs in observations:
            self.ninv.append( obs.load_ninv_map() )
            beam = obs.load_sh_beam_and_pixwin()
            if beam.shape[0] < lmax + 1:
                buf = np.zeros_like(Sl[0])
                buf[:beam.shape[0]] = beam
                beam = buf
            self.beam.append(beam)
            
        self.prepare_levels(self.ninv, self.beam, self.dl, self.mixing_maps)

    def prepare_levels(self, ninv, beam, dl, mixing_maps):
        """
        Perform necessary precomputations for each of the defined MgLevels.
        """
        lmax_ninv = 2 * self.lmax + 2 - 1
        
        # Get SH-space Ninv for each band
        ninv_winv_lm = [ sh_adjoint_synthesis(lmax_ninv, ninv[i])
                         for i in range(self.Nobs) ]
        
        # Compute restriction beams for each level
        fl_tot = np.ones(self.lmax + 1) # Restric. beam on top level is unity
        for level in self.levels:
            print "\tPreparing", level
            #prev_beam = [bm[:level.lmax + 1].copy() for bm in beam]
            #level.prepare(ninv, ninv_winv_lm, None,
            #              prev_beam, dl[:level.lmax + 1].copy())
            #beam, dl = level.beam, level.dl
            prev_fltot = fl_tot[:level.lmax + 1].copy()
            level.prepare( ninv, ninv_winv_lm, None, prev_fltot, 
                           dl[:,:level.lmax + 1].copy(), self.beam,
                           self.mixing_maps )
            fl_tot, dl = level.fl_tot, level.dl
            

    def precompute_noise_term(self):
        """
        Precompute noise (N^-1) terms for all MG levels.
        """
        for level in self.levels:
            level.precompute_noise_term()

    def precompute_prior_term(self):
        """
        Precompute prior (S^-1) terms for all MG levels.
        """
        for level in self.levels:
            level.precompute_prior_term()

    # REMOVED: def _h5_groupname(self, p)
    # REMOVED: def save_noise_term(self, filename)
    # REMOVED: def plot_levels(self, fig=None)
    # REMOVED: def cached_compute_noise_term(self, cache_filename, levels=None)


def mg_cycle(ilevel, levels, x, b, logger):
    """
    Perform a single MG cycle over the specified level configuration. This 
    function acts recursively, following the specified cycle pattern (e.g. 
    V-cycle or W-cycle).
    
    See Fig. 5 of Seljebotn et al. (2013) for the algorithm (CR-Cycle function).
    
    Parameters
    ----------
    
    ilevel : int
        The ID of the current level. This is used for tracking where the 
        algorithm is in the MG cycle.
        
    levels : list
        List of MgLevel instances. This defines a single MG cycle.
        
    x, b : array_like
        The current state of the solution vector, x, and the right-hand side 
        of the linear system, b. x can be None, which means all-zero, but is
        faster.
    
    logger : Logger
        Logger object, for keeping track of performance information etc.
    
    Returns
    -------
    
    x : array_like
        Updated state of the solution vector, x.
    
    """
    indlogger = indented_logger(logger)
    level = levels[ilevel]
    if ilevel == len(levels) - 1:
        # Apply approximate inverse and return if we're at the bottom level (end recursion)
        x = b.copy()
        level.approx_solve_inplace(x, indlogger)
        return x
    else:
        # Pre-smoothing: Smooth on current level before going to lower levels
        for i in range(level.npre):
            x = level.smooth_inplace(x, b, indlogger)
        assert x is not None, 'require level.npre >= 1'

        # Restrict residual: apply restriction operator to residual
        with log_done_task(indlogger, 'matvec @ %s' % level):
            r = b - level.matvec(x)

        next_level = levels[ilevel + 1]
        r_H = truncate_alm(r, level.lmax, next_level.lmax)
        r_H *= next_level.f_lm

        # Recurse through lower levels, following pre-defined cycle pattern
        delta_H = np.zeros(b.shape[0])
        for i in range(level.nrec):
            delta_H = mg_cycle(ilevel + 1, levels, delta_H, r_H, indlogger)

        # Apply correction: Add correction to solution vector on this level
        delta_H *= next_level.f_lm
        delta_h = pad_alm(delta_H, next_level.lmax, level.lmax)
        x += delta_h

        # Post-smoothing: Smooth again on current level before returning to upper levels
        for i in range(level.npost):
            x = level.smooth_inplace(x, b, indlogger)

        # Return solution, and statistics
        return x
    
    
