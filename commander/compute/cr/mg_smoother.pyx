from libc.stdint cimport int32_t, int8_t, int64_t
from libc.math cimport sqrt, log10, pow
import numpy as np
cimport numpy as cnp
cimport cython

from commander.sphere.healpix import npix_to_nside



cdef struct domain_t:
    int32_t sample_nside, level_nside, ipix

class NotPosDefError(Exception):
    pass
    
cdef extern:

    void nest2ring_indices_ "nest2ring_indices"(int32_t nside, int32_t *indices)

    void convert_nest2ring_ "convert_nest2ring"(int32_t nside, double *map)
    void convert_ring2nest_ "convert_ring2nest"(int32_t nside, double *map)
    void ring2nest_ "ring2nest"(int32_t nside, int32_t ringpix, int32_t *nestpix)

    void udgrade_ring_wrapper(double *map_in, int32_t nside_in, double *map_out, int32_t nside_out)

    void query_tile_ring_ "query_tile_ring"(int32_t nside, int32_t nside_level,
                                            int32_t ipix_D, int32_t *indices)

    void compute_smoother_blocks_ "compute_smoother_blocks"(
        int32_t nside, int32_t nside_level, double A, double fwhm,
        double *ninv_map, int32_t nside_obs, double *dl, int32_t lmax,
        int32_t *indptr, int32_t *indices, double *blocks) nogil

    void insertion_sort_ "insertion_sort"(int32_t *lst, int32_t n) nogil
    
    void csc_neighbours_lower_ "csc_neighbours_lower"(
        int32_t nside, int32_t *indptr, int32_t *indices) nogil

    void csc_neighbours_full_ "csc_neighbours_full"(
        int32_t nside, int32_t *indptr, int32_t *indices) nogil

    void csc_neighbours_adaptive_ "csc_neighbours_adaptive"(
        int32_t nside, int32_t *indptr, int32_t *indices, int8_t *radiusmap,
        int32_t nnz, int32_t *nind) nogil

    void csc_24_neighbours_ "csc_24_neighbours"(int32_t nside, int32_t *indptr, int32_t *indices) nogil

    void mirror_csc_indices_ "mirror_csc_indices"(
        int32_t n, int32_t *indptr_in, int32_t *indices_in,
        int32_t *indptr_out, int32_t *indices_out) nogil
    
    void block_incomplete_cholesky_factor_ "block_incomplete_cholesky_factor"(
        int32_t bs, int32_t n, int32_t *indptr, int32_t *indices,
        float *blocks, float alpha, int32_t *info) nogil

    void convert_tiled_sp_to_ring_dp_ "convert_tiled_sp_to_ring_dp"(
        int32_t nside, int32_t nside_level, float *tiled_map, double *map) nogil
    void convert_ring_dp_to_tiled_sp_ "convert_ring_dp_to_tiled_sp"(
        int32_t nside, int32_t nside_level, double *map, float *tiled_map) nogil

    void block_triangular_solve_ "block_triangular_solve"(
        int32_t trans, int32_t bs, int32_t n,
        int32_t *indptr, int32_t *indices,
        float *blocks, float *x) nogil

    void block_symm_At_D_A_ "block_symm_At_D_A"(
        int32_t bs, int32_t n,
        int32_t *A_indptr, int32_t *A_indices, double *A_blocks,
        double *D, int32_t *C_indptr, int32_t *C_indices, double *C_blocks) nogil
    
    void block_A_D_B_ "block_A_D_B"(
        int32_t bs_left, int32_t bs_mid, int32_t bs_right, int32_t n,
        int32_t *indptr, int32_t *indices, float *A_blocks, float *B_blocks,
        float *C_blocks, float *D) nogil
    
    void block_At_D_A_ "block_At_D_A"(
        int32_t bs_left, int32_t bs_right, int32_t n,
        int32_t *A_indptr, int32_t *A_indices, float *A_blocks,
        float *D, int32_t *C_indptr, int32_t *C_indices, float *C_blocks) nogil

    void compute_csc_beam_matrix_ "compute_csc_beam_matrix"(
        int32_t triang_diag, int32_t nside_left, int32_t nside_right,
        int32_t nside_level, float *dl, int32_t lmax,
        int32_t *indptr, int32_t n, int32_t *indices, float *blocks) nogil

    void compute_arbitrary_smoother_block_ "compute_arbitrary_smoother_block"(
        int32_t nside, int32_t *pixels, int32_t len_,
        double *dl, double *bl, int32_t lmax, double *ninv, int32_t nside_obs,
        int32_t pixrad, float *out) nogil

    void compute_Bt_Ni_B_ "compute_Bt_Ni_B"(
                         int32_t nside, int32_t nside_obs, int32_t nside_level,
                         float *dl, int32_t lmax, float *ninv_tiled,
                         int32_t *B_indptr, int32_t *B_indices,
                         int32_t *C_indptr, int32_t *C_indices, float *C_blocks) nogil

    
cdef domain_t as_domain(tup) except *:
    cdef domain_t r
    r.sample_nside, r.level_nside, r.ipix = tup
    return r

cdef int32_t domainsize(domain_t d) except *:
    return (d.sample_nside / d.level_nside)**2

def convert_nest2ring(cnp.ndarray[double, mode='fortran'] map):
    convert_nest2ring_(npix_to_nside(map.shape[0]), &map[0])
    return map

def convert_ring2nest(cnp.ndarray[double, mode='fortran'] map):
    convert_ring2nest_(npix_to_nside(map.shape[0]), &map[0])
    return map

def ring2nest(nside, ipix):
    cdef int32_t result
    ring2nest_(nside, ipix, &result)
    return result

def udgrade_ring(cnp.ndarray[double, mode='fortran'] map_in, int32_t nside_out):
    cdef cnp.ndarray[double, mode='fortran'] map_out = np.zeros(12*nside_out**2)
    udgrade_ring_wrapper(&map_in[0], npix_to_nside(map_in.shape[0]), &map_out[0], nside_out)
    return map_out

def nest2ring_indices(nside):
    cdef cnp.ndarray[int32_t] indices = np.empty(12*nside**2, np.int32)
    nest2ring_indices_(nside, &indices[0])
    return indices

def query_tile_ring(int32_t nside, int32_t nside_level, int32_t ipix_D):
    cdef cnp.ndarray[int32_t, mode='fortran'] indices
    indices = np.empty((nside / nside_level)**2, np.int32, order='F')
    query_tile_ring_(nside, nside_level, ipix_D, &indices[0])
    return indices

def insertion_sort(lst):
    cdef cnp.ndarray[int32_t, mode='fortran'] lst_cpy = np.asarray(lst, np.int32)
    insertion_sort_(&lst_cpy[0], lst_cpy.shape[0])
    return list(lst_cpy)

def mirror_csc_indices(cnp.ndarray[int32_t, mode='fortran'] indptr_in,
                       cnp.ndarray[int32_t, mode='fortran'] indices_in):
    cdef cnp.ndarray[int32_t, mode='fortran'] indptr_out, indices_out
    cdef int32_t n = indptr_in.shape[0] - 1, nnz = indptr_in[n]

    indptr_out = np.empty(n + 1, np.int32)
    indices_out = np.empty(2 * nnz - n, np.int32)
    with nogil:
        mirror_csc_indices_(n, &indptr_in[0], &indices_in[0], &indptr_out[0], &indices_out[0])
    return indptr_out, indices_out

def csc_neighbours_lower(int32_t nside):
    cdef cnp.ndarray[int32_t, mode='fortran'] indptr, indices
    indptr = np.empty(12 * nside**2 + 1, dtype=np.int32, order='F')
    indices = np.empty(5 * 12 * nside**2 - 12, dtype=np.int32, order='F')
    with nogil:
        csc_neighbours_lower_(nside, &indptr[0], &indices[0])
    return indptr, indices

def csc_neighbours_full(int32_t nside):
    cdef cnp.ndarray[int32_t, mode='fortran'] indptr, indices
    indptr = np.empty(12 * nside**2 + 1, dtype=np.int32, order='F')
    indices = np.empty(9 * 12 * nside**2 - 24, dtype=np.int32, order='F')
    with nogil:
        csc_neighbours_full_(nside, &indptr[0], &indices[0])
    return indptr, indices

def csc_neighbours_adaptive(int32_t nside, cnp.ndarray[int8_t, mode='fortran'] radiusmap):
    cdef cnp.ndarray[int32_t, mode='fortran'] indptr, indices
    cdef int32_t nnz, nnz_out

    # Crude estimate: 12 * npix; then increase with 20% iteratively until we hit it
    nnz = 12 * 12 * nside**2
    nnz_out = -1
    while nnz_out == -1:
        indptr = np.empty(12 * nside**2 + 1, dtype=np.int32, order='F')
        indices = np.empty(nnz, dtype=np.int32, order='F')
        with nogil:
            csc_neighbours_adaptive_(nside, &indptr[0], &indices[0], &radiusmap[0],
                                     nnz, &nnz_out)
        if nnz_out == -1:
            nnz = int(1.2 * nnz)
    return indptr, indices[:nnz_out]

def csc_24_neighbours(int32_t nside):
    cdef cnp.ndarray[int32_t, mode='fortran'] indptr, indices
    cdef int32_t nnz
    indptr = np.empty(12 * nside**2 + 1, dtype=np.int32, order='F')
    indices = np.empty(25 * 12 * nside**2, dtype=np.int32, order='F')
    with nogil:
        csc_24_neighbours_(nside, &indptr[0], &indices[0])
    return indptr, indices[:indptr[-1]]

def block_incomplete_cholesky_factor(cnp.ndarray[int32_t, mode='fortran'] indptr,
                                     cnp.ndarray[int32_t, mode='fortran'] indices,
                                     cnp.ndarray[float, ndim=3, mode='fortran'] blocks,
                                     double alpha=0):
    cdef int32_t bs = blocks.shape[0], n = indptr.shape[0] - 1, info
    if blocks.shape[1] != blocks.shape[0]:
        raise ValueError('invalid shape on blocks arg')
    with nogil:
        block_incomplete_cholesky_factor_(bs, n, &indptr[0], &indices[0], &blocks[0,0,0],
                                          alpha, &info)
    if info != 0:
        raise NotPosDefError("DPOTRF or DPOSV failed with code %d" % info)

def probe_cholesky_ridging(cnp.ndarray[int32_t, mode='fortran'] indptr,
                           cnp.ndarray[int32_t, mode='fortran'] indices,
                           cnp.ndarray[float, ndim=3, mode='fortran'] blocks,
                           double ridge,
                           double eps_log10=0.1):
    """Retries block_incomplete_cholesky_factor until the lowest term to add to the diagonal
    for success is found (using binary search); continues until `highest-lowest/lowest < eps).

    Returns (alpha, ncalls). Does NOT alter input blocks, you should call
    block_incomplete_cholesky_factor again with the given alpha, usually after adding a bit extra.
    """
    cdef cnp.ndarray[float, ndim=3, mode='fortran'] out_blocks
    cdef double lower, upper, mid
    upper = np.max(blocks) + ridge
    ncalls = 0
    
    # May be that we succeed right away...
    try:
        out_blocks = blocks.copy('F')
        ncalls += 1
        block_incomplete_cholesky_factor(indptr, indices, out_blocks, alpha=ridge)
    except NotPosDefError:
        pass
    else:
        return ridge, ncalls
    
    # OK, validate the highest end of the bracket indeed works
    try:
        out_blocks = blocks.copy('F')
        ncalls += 1
        block_incomplete_cholesky_factor(indptr, indices, out_blocks, alpha=upper)
    except NotPosDefError:
        raise NotImplementedError('Did not work even by adding the maximum matrix val to diagonal')

    # Binary search, using geometric mean. Use log10 to match eps given.
    lower = upper * 1e-6
    lower = log10(lower)
    upper = log10(upper)
    while upper - lower > eps_log10:
        mid = .5 * (lower + upper)
        out_blocks = blocks.copy('F')
        try:
            success = False
            ncalls += 1
            block_incomplete_cholesky_factor(indptr, indices, out_blocks, alpha=pow(10, mid))
        except NotPosDefError:
            print '%.2e' % pow(10, mid), 'Failed'
            lower = mid
        else:
            print '%.2e' % pow(10, mid), 'Success'
            success = True
            upper = mid
    return pow(10, upper), ncalls
    
def convert_tiled_sp_to_ring_dp(cnp.ndarray[float, ndim=2, mode='fortran'] tiled_map,
                                cnp.ndarray[double, ndim=1, mode='fortran'] out=None):
    cdef int32_t npix, nside, nside_level
    npix = tiled_map.shape[0] * tiled_map.shape[1]
    if out is None:
        out = np.empty(npix, np.double, order='F')
    elif out.shape[0] != npix:
        raise ValueError('invalid shape on out')
    nside_level = npix_to_nside(tiled_map.shape[1])
    nside = npix_to_nside(npix)
    with nogil:
        convert_tiled_sp_to_ring_dp_(nside, nside_level, &tiled_map[0,0], &out[0])
    return out

def convert_ring_dp_to_tiled_sp(int32_t nside_level,
                               cnp.ndarray[double, ndim=1, mode='fortran'] map,
                               cnp.ndarray[float, ndim=2, mode='fortran'] out=None):
    cdef int32_t npix, nside, nside_tile
    npix = map.shape[0]
    nside = npix_to_nside(npix)
    nside_tile = nside / nside_level
    if out is None:
        out = np.empty((nside_tile**2, npix / nside_tile**2), np.float32, order='F')
    elif out.shape[0] != npix:
        raise ValueError('invalid shape on out')
    with nogil:
        convert_ring_dp_to_tiled_sp_(nside, nside_level, &map[0], &out[0,0])
    return out


def block_triangular_solve(transpose,
                           cnp.ndarray[int32_t, mode='fortran'] indptr,
                           cnp.ndarray[int32_t, mode='fortran'] indices,
                           cnp.ndarray[float, ndim=3, mode='fortran'] blocks,
                           cnp.ndarray[float, ndim=2, mode='fortran'] x):
    cdef int32_t bs = blocks.shape[0], n = indptr.shape[0] - 1, info
    cdef int32_t trans_int
    if blocks.shape[1] != blocks.shape[0]:
        raise ValueError('invalid shape on blocks arg')
    if transpose == 'N':
        trans_int = 0
    elif transpose == 'T':
        trans_int = 1
    else:
        raise ValueError('transpose not in ("N", "T")')
    with nogil:
        block_triangular_solve_(trans_int, bs, n, &indptr[0], &indices[0], &blocks[0,0,0], &x[0,0])


def block_A_D_B( cnp.ndarray[int32_t, mode='fortran'] indptr,
                 cnp.ndarray[int32_t, mode='fortran'] indices,
                 cnp.ndarray[float, ndim=3, mode='fortran'] A_blocks,
                 cnp.ndarray[float, ndim=3, mode='fortran'] B_blocks,
                 cnp.ndarray[float, ndim=2, mode='fortran'] D,
                 cnp.ndarray[float, ndim=3, mode='fortran'] C_blocks=None):
    """
    Multiply diagonal matrix D by sparse matrices A and B.
    """
    
    # Dimensions of matrices contained within each tile
    cdef int32_t bs_left = A_blocks.shape[0]
    cdef int32_t bs_mid  = B_blocks.shape[0]
    cdef int32_t bs_right = B_blocks.shape[1]
    cdef int32_t n = indptr.shape[0] - 1 # No. tiles
    
    # A: (bs_left, bs_mid)
    # B: (bs_mid, bs_right)
    # C: (bs_left, bs_right)
    # D: (bs_mid,)
    
    if C_blocks is None:
        C_blocks = np.empty((bs_left, bs_right, indices.shape[0]), np.float32, order='F')
    with nogil:
        block_A_D_B_( bs_left, bs_mid, bs_right, n, &indptr[0], &indices[0], 
                      &A_blocks[0,0,0], &B_blocks[0,0,0], &C_blocks[0,0,0],
                      &D[0, 0] )
    return C_blocks


def block_At_D_A(cnp.ndarray[int32_t, mode='fortran'] A_indptr,
                 cnp.ndarray[int32_t, mode='fortran'] A_indices,
                 cnp.ndarray[float, ndim=3, mode='fortran'] A_blocks,
                 cnp.ndarray[float, ndim=2, mode='fortran'] D,
                 cnp.ndarray[int32_t, mode='fortran'] C_indptr,
                 cnp.ndarray[int32_t, mode='fortran'] C_indices,
                 cnp.ndarray[float, ndim=3, mode='fortran'] C_blocks=None):
    cdef int32_t bs_left = A_blocks.shape[0], bs_right = A_blocks.shape[1], n = A_indptr.shape[0] - 1

    if C_blocks is None:
        C_blocks = np.empty((bs_right, bs_right, C_indices.shape[0]), np.float32, order='F')
    with nogil:
        block_At_D_A_(bs_left, bs_right, n, &A_indptr[0], &A_indices[0], &A_blocks[0,0,0],
                      &D[0, 0], &C_indptr[0], &C_indices[0], &C_blocks[0,0,0])
    return C_blocks

def compute_csc_beam_matrix(symmetric, int32_t nside_left, int32_t nside_right, int32_t nside_level,
                            cnp.ndarray[float, mode='fortran'] dl,
                            cnp.ndarray[int32_t, mode='fortran'] indptr,
                            cnp.ndarray[int32_t, mode='fortran'] indices,
                            cnp.ndarray[float, ndim=3, mode='fortran'] blocks=None):
    cdef int lmax = dl.shape[0] - 1, triang_diag
    cdef int32_t npix_left, npix_right
    npix_left = 12 * nside_left**2
    npix_right = 12 * nside_right**2
    triang_diag = 0
    if symmetric:
        if nside_left != nside_right:
            raise ValueError('symmetric and (nside_left != nside_right)')
        triang_diag = 1
    if blocks is None:
        blocks = np.empty(((nside_left // nside_level)**2,
                           (nside_right // nside_level)**2,
                           indices.shape[0]), np.float32, order='F')
    with nogil:
        compute_csc_beam_matrix_(triang_diag, nside_left, nside_right, nside_level, &dl[0], lmax,
                                 &indptr[0], indptr.shape[0] - 1,
                                 &indices[0], &blocks[0,0,0])
    return blocks

def compute_Bt_Ni_B(int32_t nside, int32_t nside_obs, int32_t nside_level,
                    cnp.ndarray[float, ndim=1, mode='fortran'] bl,
                    cnp.ndarray[float, ndim=2, mode='fortran'] ninv_tiled,
                    cnp.ndarray[int32_t, mode='fortran'] out_indptr,
                    cnp.ndarray[int32_t, mode='fortran'] out_indices,
                    cnp.ndarray[int32_t, mode='fortran'] B_indptr=None,
                    cnp.ndarray[int32_t, mode='fortran'] B_indices=None,
                    cnp.ndarray[float, ndim=3, mode='fortran'] out_blocks=None):
    if B_indptr is None:
        B_indptr, B_indices = mirror_csc_indices(out_indptr, out_indices)
    cdef int32_t lmax = bl.shape[0] - 1
    if out_blocks is None:
        out_blocks = np.empty(((nside // nside_level)**2,
                               (nside // nside_level)**2,
                               out_indices.shape[0]), np.float32, order='F')
    with nogil:
        compute_Bt_Ni_B_(nside, nside_obs, nside_level, &bl[0], lmax,
                         &ninv_tiled[0,0],
                         &B_indptr[0], &B_indices[0], &out_indptr[0], &out_indices[0],
                         &out_blocks[0,0,0])
    return out_blocks


def compute_arbitrary_smoother_block(int32_t nside, cnp.ndarray[int32_t, mode='fortran'] pixels,
                                     cnp.ndarray[double, mode='fortran'] dl,
                                     cnp.ndarray[double, mode='fortran'] bl,
                                     cnp.ndarray[double, mode='fortran'] ninv,
                                     int32_t pixrad=10):
    cdef cnp.ndarray[float, mode='fortran', ndim=2] out
    cdef int32_t lmax, nside_obs
    out  = np.empty((pixels.shape[0], pixels.shape[0]), dtype=np.float32, order='F')
    lmax = dl.shape[0] - 1
    nside_obs = npix_to_nside(ninv.shape[0])
    with nogil:
        compute_arbitrary_smoother_block_(nside, &pixels[0], pixels.shape[0],
                                            &dl[0], &bl[0], lmax, &ninv[0], nside_obs,
                                            pixrad, &out[0, 0])
    return out


def blocks_to_dense(indptr, indices, blocks, mirror=False):
    n = indptr.shape[0] - 1
    bs_left = blocks.shape[0]
    bs_right = blocks.shape[1]
    if mirror and bs_left != bs_right:
        raise ValueError('cannot mirror non-symmetric matrix')
    out = np.zeros((n*bs_left,n*bs_right))
    for j in range(n):
        for iptr in range(indptr[j], indptr[j + 1]):
            i = indices[iptr]
            
            if not mirror or i != j:
                b = blocks[:, :, iptr]
            else:
                b = np.tril(blocks[:, :, iptr]) + np.tril(blocks[:, :, iptr], -1).T
            out[i * bs_left:(i+1)*bs_left,j*bs_right:(j+1)*bs_right] = b
            if mirror and i != j:
                assert bs_left == bs_right
                out[j * bs_left:(j+1)*bs_left,i*bs_left:(i+1)*bs_left] = b.T
    return out
