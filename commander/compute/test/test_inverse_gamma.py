
import numpy as np

from numpy.testing import assert_almost_equal
from nose.tools import eq_, ok_

import commander as cm
import commander.compute.inverse_gamma as inverse_gamma
from commander.sphere import mmajor, sh_synthesis

def test_sample_cls():
    def test(nside, lmax):
        
        # Generate fake Cl's and create realisation
        ctx = cm.context.CommanderContext()
        cl = np.arange(lmax + 1).astype(np.double)
        clm = mmajor.scatter_l_to_lm(cl)
        s_lm = np.sqrt(clm) * ctx.nc_rng.normal(size=(lmax + 1)**2)
        s = sh_synthesis(nside, s_lm) # Realised amplitude map
        
        # Calculate power spectrum of realised Cl's
        #alm = ctx.sh_analysis(nside, 0, lmax, s)
        #cl_realisation = mmajor.compute_power_spectrum(0, lmax, alm)
        
        # Sample Cl's using the amplitude map we just generated
        cl_sample = inverse_gamma.sample_cls(s, lmax)
        
        # Plot results
        # plot.plot_power_spectrum(cl)
        # plot.plot_power_spectrum(cl_realisation)
        # plot.plot_power_spectrum(cl_sample)
        
        # Check that correct-sized array is returned
        if not (cl_sample.size == lmax + 1):
            print cl_sample
            print cl_sample.size, lmax + 1
        return ok_, (cl_sample.size == lmax + 1)

    yield test(64, 3*64)
    yield test(256, 3*256)
    

def test_sample_as_gaussian():
    
    def test(npix, b, size):
        # Generate fake data, with noise give by diagonal noise matrix 
        # N = b . N_0, where N_0 is known and b is the parameter we wish to 
        # sample. The conditional pdf for sampling this has inverse gamma form, 
        # p(b|d,m) ~ exp(-1/2 (d-m)^T (b N_0)^-1 (d-m)) / sqrt(det(b N_0)).
        
        n = np.random.normal(size=npix) # Noise is unit Gaussian
        m = np.ones(npix) # 'Model' is just a constant ones
        d = m + np.sqrt(b)*n # data = model + noise
        
        # Construct residuals and shape parameters for inverse gamma dist.
        r = d - m
        alpha = 0.5 * npix - 1.
        beta = 0.5 * np.sum((r/1.)**2.) # This is the factor in the exponential
        
        # Return samples from approximate Gaussian dist. and exact inv. gamma 
        # dist. If the Gaussian is a good approximation to inv. gamma (which 
        # it should be for large alpha (i.e. large npix), histograms of the two 
        # samples should look very similar.
        samp_gaussian = inverse_gamma.sample_invgamma_as_gaussian(alpha, beta, size=size)
        samp_invgamma = inverse_gamma.sample_invgamma_exact(alpha, beta, size=size)
        
        # Plot histograms
        # ...
        
        # Compare means and standard deviations
        u_g = np.mean(samp_gaussian); s_g = np.std(samp_gaussian)
        u_i = np.mean(samp_invgamma); s_i = np.std(samp_invgamma)
        
        # If the mean is > 5 std. dev. away from the correct answer, something 
        # has probably gone wrong
        if np.abs(u_g - b) > 5. * s_g:
            print u_g, b
            return ok_, False
        
        # If the two standard deviations differ by a sizeable factor, there is 
        # probably something wrong too
        if np.abs((s_g - s_i) / s_i) > 1.:
            print s_g, s_i
            return ok_, False
        
        # Otherwise, it looks fine
        return ok_, True
        
    yield test(200, 3.6, 1000)
    yield test(200, 300.6, 1000)
    yield test(1000, 3.6, 1000)
        

def test_sample_exact():
    def logpdf(alpha, beta, x):
        return alpha * np.log(beta) - (alpha + 1.) * np.log(x) - (beta / x)
        
    def test(alpha, beta, size):
        # Compare the analytic form of the pdf for an inverse gamma distribution 
        # with a histogram of the samples
        
        # Get analytic form for inv. gamma pdf
        # x = ...
        # logp = logpdf(alpha, beta, x)
        # logp -= np.max(logp) # Subtract max. to avoid any overruns in exp()
        # pdf = np.exp(logp)
        
        # Sample using sample_exact()
        sample = inverse_gamma.sample_exact(alpha, beta, size=size)
        
        # Plot results
        # n, bins, patches = plot.hist(sample, bins=30, normed=True)
        # plot.plot(x, np.max(n)*pdf/np.max(pdf), 'r-', lw=1.5)
        # plot.show()
        
        if not (sample.size == size):
            print sample.size, size
            return ok_, False
        return ok_, True


