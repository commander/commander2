"""

.. automodule:: commander.samplers.adaptive_rejection
  :members:

.. automodule:: commander.samplers.inverse_gamma
  :members:

"""

from .adaptive_rejection import (
    sample_piecewise_exponential,
    sample_adaptive_rejection)

from .inverse_gamma import (
    sample_cls,
    sample_invgamma_as_gaussian,
    sample_invgamma_exact)

import cr
