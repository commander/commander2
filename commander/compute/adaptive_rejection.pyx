"""
Adaptive Rejection Sampling algorithm, using chords to refine the sample 
envelope. See Section 3.2 ("Adaptive Rejection Sampling") of Gilks, Best and 
Tan (1995).
"""

# TODO
# * Cythonize
# * Use drand48 and pass seeds for threadsafe random no. generation

import numpy as np
#cimport numpy as np
import pylab as P
import cython
#from libc.rand48 import drand48, seed48

_MAX_ARS_TRIES = 100 # Max. number of ARS iterations to try before giving up
_EXP_OVERFLOW_LIMIT = 20. # Max. argument of np.exp() before switching to approximation in sample_piecewise_exponential()

cdef _get_vertices(x, y, n, xbounds):
    """
    Get vertices of the piecewise-continuous envelope function, for a given 
    set of samples of the log-likelihood.
     x: the current set of sample points
     y: logL at each of the sample points
     xbounds: a tuple containing the prior bounds, (xmin, xmax). The likelihood 
     does not need to be evaluated at these bounds.
    N.B. There must be a minimum of 3 sample points.
    """
    # Do validity check (must have >3 samples)
    if n < 3: raise ValueError("Not enough samples to construct hull.")
    N = 2*n + 1
    
    # Allocate arrays for vertex values
    xv = np.zeros(N)
    yv = np.zeros(N)
    xmin, xmax = xbounds
    
    # Add initial points (for where the usual algorithm is undefined)
    xv[0] = xmin; yv[0] = y[0] + (y[1]-y[0])*(xmin-x[0])/(x[1]-x[0])
    xv[1] = x[0]; yv[1] = y[0]
    xv[2] = x[0]; yv[2] = y[1] + (y[2]-y[1])*(x[0]-x[1])/(x[2]-x[1])
    xv[3] = x[1]; yv[3] = y[1]
    
    # Add points where both line segments are defined
    for i in range(1, n-2):
        b0 = y[i] - y[i-1]; b1 = y[i+2] - y[i+1]
        a0 = x[i] - x[i-1]; a1 = x[i+2] - x[i+1]
        xv[2*i+2] = (b0*a1*x[i-1] - b1*a0*x[i+1] + a0*a1*(y[i+1] - y[i-1])) \
                    / (b0*a1 - b1*a0)
        yv[2*i+2] = y[i-1] + b0*(xv[2*i+2] - x[i-1])/a0
        xv[2*i+3] = x[i+1]
        yv[2*i+3] = y[i+1]
    
    # Add end points (for where the usual algorithm is not defined)
    xv[N-3] = x[n-1]; yv[N-3] = y[n-3] + (y[n-2]-y[n-3])*(x[n-1]-x[n-3])/(x[n-2]-x[n-3])
    xv[N-2] = x[n-1]; yv[N-2] = y[n-1]
    xv[N-1] = xmax;  yv[N-1] = y[n-2] + (y[n-1]-y[n-2])*(xmax-x[n-2])/(x[n-1]-x[n-2])
    return xv, yv


def _update_sample_set(x_new, y_new, x, y, n):
    """
    Add new values to the (ordered) set of samples.
    """
    # Find out where to insert the new sample
    j = -1
    dx = 1e60
    this_dx = 0.
    for i in range(n):
        this_dx = x_new - x[i]
        if (this_dx >= 0. and this_dx < dx):
            j = i
            dx = this_dx
    
    # Check to see if we need to reallocate arrays
    if x.size == n:
        # Reallocate and then rebuild arrays in correct order
        _x = np.zeros(n+1)
        _y = np.zeros(n+1)
        for i in range(n-1, j-1, -1):
            _x[i+1] = x[i]
            _y[i+1] = y[i]
        for i in range(0, j+1):
            _x[i] = x[i]
            _y[i] = y[i]
        _x[j+1] = x_new
        _y[j+1] = y_new
        return _x, _y, n+1
    else:
        # No need to reallocate, just shuffle the arrays around
        for i in range(n-1, j, -1):
            x[i+1] = x[i]
            y[i+1] = y[i]
        x[j+1] = x_new
        y[j+1] = y_new
        return x, y, n+1

def _envelope_function(x, xv, yv, n):
    """
    Return the value of the envelope function at x, for a given set of vertices.
    """
    # Check that x is within the prior bounds
    if (x < xv[0] or x > xv[-1]):
        print xv[0], x, xv[-1]
        raise ValueError("x is outside the prior bounds.")
    
    # Find which piece of the envelope function x corresponds to
    n = 2*n + 1 # No. vertices
    j = n-1
    dx = 2.*(xv[n-1] - xv[0])
    this_dx = 0.
    for i in range(n):
        this_dx = xv[i] - x
        if (this_dx > 0. and this_dx < dx):
            j = i
            dx = this_dx
    # Calculate m and c
    m = (yv[j] - yv[j-1]) / (xv[j] - xv[j-1])
    c = yv[j] - m*xv[j]
    return m*x + c
    

def sample_piecewise_exponential(xv, yv, n):
    """
    Draw a sample from a piecewise exponential distribution defined by the 
    vertices (xv, yv), where yv = logL(xv).
    """
    # Rescale yv, to help avoid overruns
    y0 = np.max(yv)
    
    # Calculate cdf, and gradient between vertices
    N = 2*n + 1
    m = np.zeros(N-1); cdf = np.zeros(N)
    for i in range(N-1):
        # Test for sharp steps (i.e. gradient = infinity)
        if (xv[i+1] == xv[i]):
            cdf[i+1] = cdf[i]
        else:
            m[i] = (yv[i+1] - yv[i]) / (xv[i+1] - xv[i])
            cdf[i+1] = cdf[i] + ( np.exp(yv[i+1]-y0) - np.exp(yv[i]-y0) ) / m[i]
    
    # Test CDF for validity (infinite, or too big), and positive semi-definiteness
    if np.isinf(cdf[-1]) or (cdf[-1] > 1e100):
        raise ValueError("CDF is invalid; integrates to infinity.")
    if np.where(cdf < 0.)[0].size > 0:
        raise ValueError("CDF is invalid; negative values detected.")
    
    # Draw a uniform random number
    # FIXME: Should use something stateful/threadsafe
    u = np.random.uniform()
    
    # Find which piece of the envelope it corresponds to
    j = n-1
    du = 2.*cdf[-1]
    this_du = 0.
    for i in range(N):
        this_du = u*cdf[-1] - cdf[i]
        if (this_du > 0. and this_du <= du):
            j = i
            du = this_du
    
    # Get sample of x using analytic equation for this piece
    if (-(yv[j]-y0)) > _EXP_OVERFLOW_LIMIT:
        # Approximation; for f >> 1, take log(1+f) ~ log(f)
        x = xv[j] + ( np.log((u*cdf[-1] - cdf[j]) * m[j]) - (yv[j]-y0) ) / m[j]
    else:
        f = (u*cdf[-1] - cdf[j]) * m[j] * np.exp(-(yv[j] - y0))
        assert(f > -1.)
        x = xv[j] + np.log(1. + f) / m[j] # FIXME: Numerical stability?
    return x
    

def sample_adaptive_rejection(logl, prior, xbounds):
    """
    Return a sample using adaptive rejection sampling of the likelihood.
     logl: log-likelihood function (callable, logl = logl(x))
     xbounds: a tuple containing the prior bounds, (xmin, xmax).
    """
    # Set prior bounds, and initialise arrays (with a bit of zero-padding, to 
    # avoid having to resize them too often if more samples must be added)
    x = np.zeros(10)
    y = np.zeros(10)
    
    # Get initial sample points (min. 3 required; draw from prior and then sort)
    n = 3
    for i in range(n): x[i] = prior()
    #x[:n] = np.linspace(xbounds[0], xbounds[1], n+2)[1:-1]
    for i in range(n-1): # Bubble sort
      for i in range(n-1):
        if (x[i] > x[i+1]):
            tmp = x[i]
            x[i] = x[i+1]
            x[i+1] = tmp
    for i in range(n):
        y[i] = logl(x[i])
    xv, yv = _get_vertices(x, y, n, xbounds)
    
    # Loop until a sample has been drawn successfully
    k = 0
    while k < _MAX_ARS_TRIES:
        k += 1
        
        # Draw from piecewise exponential envelope
        try:
            x_new = sample_piecewise_exponential(xv, yv, n)
        except(ValueError):
            # Problem constructing a well-behaved envelope. Try adding more 
            # samples to see if that helps.
            # (Alternative: Modify envelope s.t. boundaries are better behaved)
            x_new = prior()
            y_new = logl(x_new)
            x, y, n = _update_sample_set(x_new, y_new, x, y, n)
            xv, yv = _get_vertices(x, y, n, xbounds)
            continue
        
        # Draw uniform random number
        u = np.random.uniform() # FIXME: Use thread-safe RNG
        
        # Find likelihood and envelope value at x_new
        y_new = logl(x_new)
        h = _envelope_function(x_new, xv, yv, n)
        
        # Accept or reject
        if(u > np.exp(y_new - h)):
            # Reject; adapt the envelope
            x, y, n = _update_sample_set(x_new, y_new, x, y, n)
            xv, yv = _get_vertices(x, y, n, xbounds)
        else:
            # Accept; return sample
            return x_new, k

