
import numpy as np
from scipy.stats import gamma, invgamma

from ..sphere import sh_analysis, compute_power_spectrum

def sample_cls(amp_map, lmax):
    return sample_cl_general(amp_map, lmax)

def sample_cl_simple(amp_map, lmax):
    """
    Draw sample of the power spectrum, Cl, given a current amplitude map.
    
    Samples are drawn using the simple algorithm defined in Eriksen et al., 
    astro-ph/0407028v2 (2004).
    
    Parameters
    ----------
    amp_map : Commander mmajor map
        Map of amplitudes
    lmax : int
        Max. l for spherical harmonic analysis
    
    Returns
    -------
    cl : array_like
        Sampled power spectrum C_l's, up to l = lmax
    
    """
    # Get alms for the amplitude map
    nside = int(np.sqrt(amp_map.size/12.))
    alm = sh_analysis(lmax, amp_map)
    sigma_l = compute_power_spectrum(0, lmax, alm)
    
    # Draw (lmax^2 - 1) unit Gaussian random values (2l - 1 values per l)
    # for l >= 2 (l=0 must be handled separately)
    ells = np.arange(1, lmax+1)
    g = np.random.normal(size=lmax**2 - 1)**2.
    cls = np.zeros(lmax+1)
    cls[1:] = (2*ells+1) * sigma_l[1:]
    cls[1:] /= np.array( [np.sum(g[l*l - 2*l + 1:l*l]) for l in ells] )
    cls[0] = sample_cl_monopole(sigma_l[0])
    return cls # FIXME: Check convention for factor of 2l+1

def sample_cl_general(amps, lmax, lmax_exact=100, ampmap=False):
    """
    Draw samples of the power spectrum, C_l, given a current set of signal 
    alm's, or an amplitude map.
    
    Samples are drawn using either the exact inverse gamma sampler or a 
    Gaussian approximation (see below), depending on the size of the inverse 
    gamma shape parameter, alpha = (2l - 1)/2.
    
    Parameters
    ----------
    amps : Commander map
        Input amplitudes. Can be either spherical harmonic a_lm's in mmajor 
        ordering (ampmap=False), or a map of amplitudes (ampmap=True).
        
    lmax : int
        Max. l for spherical harmonic analysis
        
    lmax_exact : int, optional
        Maximum ell for which the exact inverse gamma sampler can be used. An 
        approximate, Gaussian sampler is used for l >= lmax_exact.
        
        This is useful because the exact inverse gamma sampler becomes 
        inaccurate or can fail for large alpha = (2 l - 1) / 2.
        
    ampmap: bool, default: False
        Whether 'amps' is an array of a_lm's (default), or a map of amplitudes.
        
    Returns
    -------
    cl : array_like
        Sampled power spectrum C_l's, up to l = lmax
    
    """
    # Get alms (using SH analysis if an amplitude map was given)
    if ampmap:
        alm = sh_analysis(lmax, amps)
    else:
        alm = amps
    sigma_l = compute_power_spectrum(0, lmax, alm)
    
    # Calculate shape/scale parameters for inverse gamma dist.
    cl = np.zeros(lmax+1)
    ells = np.arange(lmax+1)
    alpha = 0.5*(2*ells - 1.)
    beta = 0.5*(2*ells + 1) * sigma_l
    
    # Sample from inv. gamma dist., using the exact sampler for l < lmax_exact, 
    # and the approximate, Gaussian sampler for higher l.
    # (The monopole is a special case.)
    if lmax_exact > lmax: lmax_exact = lmax
    cl[2:lmax_exact] = [ sample_invgamma_exact(alpha[i], beta[i]) 
                         for i in range(2, lmax_exact) ]
    cl[lmax_exact:] = [ sample_invgamma_as_gaussian(alpha[i], beta[i])
                        for i in range(lmax_exact, lmax+1) ]
    # Disregard the monopole and dipole
    cl[0] = 0. #sample_cl_monopole(sigma_l[0])
    cl[1] = 0.
    return cl

def sample_cl_monopole(sigma_0):
    """
    Sample the monopole, C_0, as a special case.
    
    The monopole does not follow follow a valid inverse gamma distribution, 
    since the shape parameter alpha = (2l - 1)/2 is negative for l=0. Instead, 
    the sample is drawn from a gamma distribution in 1/C_0.
    
    Parameters
    ----------
    sigma_0 : float
        Monopole (l=0) term of amplitude map power spectrum, sigma_l.
        
    Returns
    -------
    C_0 : float
        Sampled power spectrum C_l for monopole.
        
    """
    # FIXME: Problem with this?
    # return 1. / sample_gamma(alpha=1.5, beta=0.5*sigma_0)
    print "[Warning] sample_cl_monopole() is buggy."
    return 1.


def sample_invgamma_as_gaussian(alpha, beta, size=1):
    r"""
    Sample from a Gaussian approximation to the inverse Gamma distribution.
    
    Inverse gamma is well-approximated by a Gaussian when the shape parameter 
    alpha >> 1. For an inverse gamma pdf of the form 
    :math:`\beta^\alpha x^{-\alpha - 1} \exp(-\beta/x)`,
    the Gaussian approximation has a mean and standard deviation of:
      \mu      = \beta / (\alpha - 1)
      
      \sigma^2 = \mu^2 / (\alpha - 1)
    
    Parameters
    ----------
    alpha : float
        Shape parameter, alpha > 0.
    beta : float
        Scale parameter, beta > 0.
    size : int, optional
        Number of samples to return.
        
    Returns
    -------
    samples : float or array_like
        Samples from the approximated inverse gamma distribution.
        
    """
    assert(alpha > 0.)
    assert(beta > 0.)
    mu = beta / (alpha - 1.)
    sigma = mu / np.sqrt(alpha - 1.)
    return np.random.normal(loc=mu, scale=sigma, size=size)

def sample_invgamma_exact(alpha, beta, size=1):
    r"""
    Sample from an exact inverse gamma distribution.
    
    Use an exact algorithm to sample from the inverse gamma pdf with the form
    :math:`pdf(x) \propto \beta^\alpha x^{-\alpha - 1} \exp(-\beta/x)`.
    If alpha >> 1, it is more reliable to use sample_invgamma_as_gaussian() 
    instead.
    
    Parameters
    ----------
    alpha : float
        Shape parameter, alpha > 0.
    beta : float
        Scale parameter, beta > 0.
    size : int, optional
        Number of samples to return.
        
    Returns
    -------
    samples : float or array_like
        Samples from the specified inverse gamma distribution.
    
    """
    assert(alpha > 0.)
    assert(beta > 0.)
    return beta * invgamma.rvs(alpha, size=size)

def sample_gamma(alpha, beta, size=1):
    r"""
    Sample from an exact gamma distribution.
    
    Use an exact algorithm to sample from the gamma pdf with the form
    :math:`pdf(x) \propto \beta^\alpha x^{\alpha - 1} \exp(-\beta x)`.
    
    Parameters
    ----------
    alpha : float
        Shape parameter, alpha > 0.
    beta : float
        Scale parameter, beta > 0.
    size : int, optional
        Number of samples to return.
        
    Returns
    -------
    samples : float or array_like
        Samples from the specified gamma distribution.
    
    """
    assert(alpha > 0.)
    assert(beta > 0.)
    return gamma.rvs(alpha, scale=1./beta, size=size)
