import numpy as np

if sizeof(index_t) == 4:
    index_dtype = np.int32
elif sizeof(index_t) == 8:
    index_dtype = np.int64
else:
    assert False

def lm_to_idx_half(index_t l, index_t m, index_t lmin):
    return (l * (l+1) - (lmin * (lmin + 1))) // 2 + m

def lm_to_idx_full(index_t l, index_t m, index_t lmin):
    return l*l + l + m - lmin*lmin

def lm_count_full(index_t lmin, index_t lmax):
    return (lmax + 1)**2 - lmin**2

def lm_count_half(index_t lmin, index_t lmax):
    return lm_to_idx_half(lmax + 1, 0, lmin)
