/* Provide NaN facilities to Fortran */

#include <math.h>

float fortran_get_nan_single() {
  return nanf("");
}

double fortran_get_nan_double() {
  return nan("");
}
