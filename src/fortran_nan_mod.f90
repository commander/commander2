module fortran_nan_mod
  implicit none

  interface
     function nanf() bind(c, name="fortran_get_nan_single")
       use iso_c_binding, only: c_float
       real(c_float) :: nanf
     end function nanf

     function nan() bind(c, name="fortran_get_nan_double")
       use iso_c_binding, only: c_double
       real(c_double) :: nan
     end function nan
  end interface

contains

end module fortran_nan_mod
