These sources are taken from libpsht by Martin Reinecke. Development
of libpsht is discontinued in favour of libsharp (aka libpsht 2), and
libsharp does not isolate the P_lm generation in the same way but
computes them on the fly when needed. Martin agreed (private comm.)
that ripping out these sources was the cleanest thing to do.

See source code headers for copyright and license information (GPL).
