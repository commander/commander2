module pixel_ordering
  use types_as_c
  implicit none

  INTEGER(KIND=i4b), private, parameter :: ns_max4=8192
  integer(KIND=i4b), private, save, dimension(0:1023) :: pix2x=0, pix2y=0
  ! coordinate of the lowest corner of each face
  INTEGER(KIND=I4B), private, save, dimension(0:11) :: jrll1 = (/ 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 /) ! in unit of nside
  INTEGER(KIND=I4B), private, save, dimension(0:11) :: jpll1 = (/ 1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7 /) ! in unit of nside/2

contains

  subroutine mk_pix2xy()
    !=======================================================================
    !     constructs the array giving x and y in the face from pixel number
    !     for the nested (quad-cube like) ordering of pixels
    !
    !     the bits corresponding to x and y are interleaved in the pixel number
    !     one breaks up the pixel number by even and odd bits
    !=======================================================================
    INTEGER(KIND=I4B) ::  kpix, jpix, ix, iy, ip, id

    !cc cf block data      data      pix2x(1023) /0/
    !-----------------------------------------------------------------------
    !      print *, 'initiate pix2xy'
    do kpix=0,1023          ! pixel number
       jpix = kpix
       IX = 0
       IY = 0
       IP = 1               ! bit position (in x and y)
!        do while (jpix/=0) ! go through all the bits
       do
          if (jpix == 0) exit ! go through all the bits
          ID = MODULO(jpix,2)  ! bit value (in kpix), goes in ix
          jpix = jpix/2
          IX = ID*IP+IX

          ID = MODULO(jpix,2)  ! bit value (in kpix), goes in iy
          jpix = jpix/2
          IY = ID*IP+IY

          IP = 2*IP         ! next bit (in x and y)
       enddo
       pix2x(kpix) = IX     ! in 0,31
       pix2y(kpix) = IY     ! in 0,31
    enddo

    return
  end subroutine mk_pix2xy


!=======================================================================
!     nest2ring
!
!     performs conversion from NESTED to RING pixel number
!=======================================================================
  subroutine nest2ring  (nside, ipnest, ipring)
    integer(i4b), parameter :: MKD = I4B
    INTEGER(KIND=I4B), INTENT(IN)  :: nside
    INTEGER(KIND=MKD), INTENT(IN)  :: ipnest
    INTEGER(KIND=MKD), INTENT(OUT) :: ipring

    INTEGER(KIND=MKD) :: npix, npface, n_before, ipf
    INTEGER(KIND=I4B) :: ip_low, ip_trunc, ip_med, ip_hi
    INTEGER(KIND=I4B) :: face_num, ix, iy, kshift, scale, i, ismax
    INTEGER(KIND=I4B) :: jrt, jr, nr, jpt, jp, nl4
    integer(kind=i4b) :: ipring4, ipnest4

    !-----------------------------------------------------------------------
    npix = 12 * nside * nside

    !     initiates the array for the pixel number -> (x,y) mapping
    if (pix2x(1023) <= 0) call mk_pix2xy()

    npface = nside*int(nside, MKD)
    nl4    = 4*nside

    !     finds the face, and the number in the face
    face_num = ipnest/npface  ! face number in {0,11}
    ipf = iand(ipnest, npface-1)  ! pixel number in the face {0,npface-1}

    !     finds the x,y on the face (starting from the lowest corner)
    !     from the pixel number
    if (nside <= ns_max4) then
       ip_low = iand(ipf,1023)       ! content of the last 10 bits
       ip_trunc =    ipf/1024        ! truncation of the last 10 bits
       ip_med = iand(ip_trunc,1023)  ! content of the next 10 bits
       ip_hi  =      ip_trunc/1024   ! content of the high weight 10 bits

       ix = 1024*pix2x(ip_hi) + 32*pix2x(ip_med) + pix2x(ip_low)
       iy = 1024*pix2y(ip_hi) + 32*pix2y(ip_med) + pix2y(ip_low)
    else
       ix = 0
       iy = 0
       scale = 1
       ismax = 4
       do i=0, ismax
          ip_low = iand(ipf,1023)
          ix = ix + scale * pix2x(ip_low)
          iy = iy + scale * pix2y(ip_low)
          scale = scale * 32
          ipf   = ipf/1024
       enddo
       ix = ix + scale * pix2x(ipf)
       iy = iy + scale * pix2y(ipf)
    endif

    !     transforms this in (horizontal, vertical) coordinates
    jrt = ix + iy  ! 'vertical' in {0,2*(nside-1)}
    jpt = ix - iy  ! 'horizontal' in {-nside+1,nside-1}

    !     computes the z coordinate on the sphere
    jr =  jrll1(face_num)*nside - jrt - 1   ! ring number in {1,4*nside-1}

    if (jr < nside) then     ! north pole region
       nr = jr
       n_before = 2 * nr * (nr - 1_MKD)
       kshift = 0

    else if (jr <= 3*nside) then  ! equatorial region (the most frequent)
       nr = nside                
       n_before = 2 * nr * ( 2 * jr - nr - 1_MKD)
       kshift = iand(jr - nside, 1)

    else ! south pole region
       nr = nl4 - jr
       n_before = npix - 2 * nr * (nr + 1_MKD)
       kshift = 0

    endif

    !     computes the phi coordinate on the sphere, in [0,2Pi]
    jp = (jpll1(face_num)*nr + jpt + 1_MKD + kshift)/2  ! 'phi' number in the ring in {1,4*nr}
    if (jp > nl4) jp = jp - nl4
    if (jp < 1)   jp = jp + nl4

    ipring = n_before + jp - 1_MKD ! in {0, npix-1}

    return

  end subroutine nest2ring

  subroutine convert_nest2ring_double_1d(nside, map) bind(c)
    !=======================================================================
    integer(kind=I4B),   parameter :: KMAP = DP
    real   (kind=KMAP),  dimension(0:12*nside*nside-1), intent(inout) ::  map
    real   (kind=KMAP),  dimension(:), allocatable :: map_tmp

    integer(kind=I4B), intent(in), value :: nside
    integer(kind=I4B) :: ipn4, ipr4, npix

    npix = 12 * nside * nside
    allocate(map_tmp(0:npix-1)) ! added 2009-07-09

!$OMP parallel default(none) &
!$OMP   shared(map, map_tmp, npix, nside) private(ipr4, ipn4)
!$OMP do schedule(dynamic,64)
       do ipn4 = 0_i4b, npix-1
          call nest2ring(nside, ipn4, ipr4)
          map_tmp(ipr4) = map(ipn4)
       enddo
!$OMP end do
!$OMP end parallel

    map = map_tmp

    deallocate(map_tmp)
    return
  end subroutine convert_nest2ring_double_1d

end module pixel_ordering
