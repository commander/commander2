module pix_tools_gutted
  use types_as_c
  implicit none

  INTEGER(KIND=i4b), private, parameter :: ns_max=8192

contains

  subroutine vec2pix_ring(nside, vector, ipix)
    use constants

    integer(i4b), parameter :: MKD = I4B
    integer(i4b), parameter :: mykind = I4B
    INTEGER(KIND=I4B), INTENT(IN)                :: nside
    REAL(KIND=DP),     INTENT(IN), dimension(1:) :: vector
    INTEGER(KIND=MKD), INTENT(OUT)               :: ipix

    INTEGER(mykind)   :: nl4, jp, jm, ir, ip
    REAL(KIND=DP)     :: z, za, tt, tp, tmp, dnorm, phi,temp1,temp2
    INTEGER(KIND=I4B) :: kshift, ipix4

    !-----------------------------------------------------------------------
    dnorm = SQRT(vector(1)**2+vector(2)**2+vector(3)**2)
    z = vector(3) / dnorm
    phi = 0.0_dp
    if (vector(1) /= 0.0_dp .or. vector(2) /= 0.0_dp) &
         &     phi = ATAN2(vector(2),vector(1)) ! phi in ]-pi,pi]

    za = ABS(z)
    if (phi < 0.0_dp)     phi = phi + twopi ! phi in [0,2pi[
    tt = phi / halfpi   ! in [0,4)

    nl4 = 4_mykind*nside
    if ( za <= twothird ) then ! Equatorial region ------------------
       temp1 = nside*(0.50000_dp + tt)
       temp2 = nside* 0.75000_dp * z
       jp = int(temp1-temp2, kind=mykind) ! index of  ascending edge line
       jm = int(temp1+temp2, kind=mykind) ! index of descending edge line

       ir = nside + jp - jm ! in {0,2n} (ring number counted from z=2/3)
       kshift = iand(ir, 1) ! kshift=1 if ir is odd, 0 otherwise

       ip = INT( ( jp+jm - nside + kshift + 1 ) / 2, kind=mykind) ! in {0,4n-1}
       if (ip >= nl4) ip = ip - nl4

       ipix = 2*nside*(nside-1_MKD) + nl4*int(ir,MKD) + ip

    else ! North & South polar caps -----------------------------

       tp = tt - INT(tt)      !MODULO(tt,1.0_dp)
       tmp = nside * SQRT( 3.0_dp*(1.0_dp - za) )

       jp = INT(  tp          * tmp, kind=mykind) ! increasing edge line index
       jm = INT((1.0_dp - tp) * tmp, kind=mykind) ! decreasing edge line index

       ir = jp + jm + 1        ! ring number counted from the closest pole
       ip = INT( tt * ir, kind=mykind)     ! in {0,4*ir-1}
       if (ip >= 4*ir) ip = ip - 4*ir

       if (z>0._dp) then
          ipix = 2*ir*(ir-1_MKD) + ip
       else
          ipix = 3*nside*int(nl4,MKD) - 2*ir*(ir+1_MKD) + ip
       endif

    endif
  end subroutine vec2pix_ring

  subroutine pix2ang_ring(nside, ipix, theta, phi) bind(c)
    use constants
    use iso_c_binding

    integer(i4b), parameter :: MKD = I4B
    INTEGER(KIND=I4B), INTENT(IN), value  :: nside
    INTEGER(KIND=MKD), INTENT(IN), value  :: ipix
    REAL(KIND=DP),     INTENT(OUT) :: theta, phi

    INTEGER(KIND=I4B) ::  nl2, nl4, iring, iphi
    INTEGER(KIND=MKD) ::  npix, ncap, ip
    REAL(KIND=DP) ::  fodd, dnside
    real(kind=dp), parameter :: half = 0.500000000000000_dp
    real(kind=dp), parameter :: one  = 1.000000000000000_dp
    real(kind=dp), parameter :: three = 3.00000000000000_dp
    real(kind=dp), parameter :: threehalf = 1.50000000000000_dp
    !-----------------------------------------------------------------------
    npix = nside2npix(nside)       ! total number of points

    nl2  = 2*nside
    ncap = nl2*(nside-1_MKD) ! points in each polar cap, =0 for nside =1
    dnside = real(nside, kind=dp)

    if (ipix < ncap) then ! North Polar cap -------------

       iring = nint( sqrt( (ipix+1) * half ), kind=MKD) ! counted from North pole
       iphi  = ipix - 2*iring*(iring - 1_MKD)

       theta = ACOS( one - (iring/dnside)**2 / three )
       phi   = (real(iphi,kind=dp) + half) * HALFPI/iring

    elseif (ipix < npix-ncap) then ! Equatorial region ------

       ip    = ipix - ncap
       nl4   = 4*nside
       iring = INT( ip / nl4 ) + nside ! counted from North pole
       iphi  = iand(ip, nl4-1)

       fodd  = half * ( iand(iring+nside+1,1) )  ! 0 if iring+nside is odd, 1/2 otherwise
       theta = ACOS( (nl2 - iring) / (threehalf*dnside) )
       phi   = (real(iphi,kind=dp) + fodd) * HALFPI / dnside

    else ! South Polar cap -----------------------------------

       ip    = npix - ipix
       iring = nint( sqrt( ip * half ), kind=MKD)     ! counted from South pole
       iphi  = 2*iring*(iring + 1_MKD) - ip

       theta = ACOS( (iring/dnside)**2 / three  - one)
       phi   = (real(iphi,kind=dp) + half) * HALFPI/iring

    endif

    return
  end subroutine pix2ang_ring

  subroutine pix2vec_ring  (nside, ipix, vector) bind(c)
    use constants
    use iso_c_binding

    integer(i4b), parameter :: MKD = I4B
    INTEGER(KIND=I4B), INTENT(IN), value                       :: nside, ipix
    REAL(KIND=DP),     INTENT(OUT),dimension(1:3)              :: vector

    INTEGER(KIND=I4B) :: nl2, nl4, iring, iphi
    INTEGER(KIND=MKD) ::  npix, ncap, ip
    REAL(KIND=DP) ::  fact1, fact2, fodd, z, sth, phi
    real(kind=dp), parameter :: half = 0.500000000000000_dp

    real(kind=DP) :: phi_nv, phi_wv, phi_sv, phi_ev, sin_phi, cos_phi
    real(kind=DP) :: z_nv, z_sv, sth_nv, sth_sv
    real(kind=DP) :: hdelta_phi
    integer(kind=I4B) :: iphi_mod, iphi_rat
    integer(kind=i4b) :: diff_phi
    !-----------------------------------------------------------------------
    npix = nside2npix(nside)       ! total number of points

    nl2   = 2*nside
    ncap  = nl2*(nside-1_MKD) ! points in each polar cap, =0 for nside =1

    if (ipix < ncap) then ! North Polar cap -------------

       iring = nint( sqrt( (ipix+1) * half ), kind=MKD) ! counted from North pole
       iphi  = ipix - 2*iring*(iring - 1_MKD)

       fact2 = (3.00000_dp*nside)*nside
       z =  1.0_dp - (iring / fact2) * iring
       phi   = (real(iphi,kind=dp) + half) * HALFPI/iring

    elseif (ipix < npix - ncap) then ! Equatorial region ------

       nl4 = 4*nside
       ip    = ipix - ncap
       iring = INT( ip / nl4 ) + nside ! counted from North pole
       iphi  = iand(ip, nl4-1)

       fact1 =  1.50000_dp*nside
       fodd  = half * ( iand(iring+nside+1,1) )  ! 0 if iring+nside is odd, 1/2 otherwise
       z = (nl2 - iring) / fact1
       phi   = (real(iphi,kind=dp) + fodd) * HALFPI / nside

    else ! South Polar cap -----------------------------------

       ip    = npix - ipix
       iring = nint( sqrt( ip * half ), kind=MKD)     ! counted from South pole
       iphi  = 2*iring*(iring + 1_MKD) - ip

       fact2 = (3.00000_dp*nside)*nside
       z = -1.0_dp + (iring / fact2) * iring
       phi   = (real(iphi,kind=dp) + half) * HALFPI/iring

    endif

    ! pixel center
    sth = SQRT((1.0_dp-z)*(1.0_dp+z))
    cos_phi = cos(phi)
    sin_phi = sin(phi)
    vector(1) = sth * cos_phi
    vector(2) = sth * sin_phi
    vector(3) = z
    return
  end subroutine pix2vec_ring


  function nside2npix(nside) result(npix_result)
    !=======================================================================
    ! given nside, returns npix such that npix = 12*nside^2
    !  nside should be a power of 2 smaller than ns_max
    !  if not, -1 is returned
    ! EH, Feb-2000
    ! 2009-03-04: returns i8b result, faster
    !=======================================================================
    INTEGER(KIND=I8B)             :: npix_result
    INTEGER(KIND=I4B), INTENT(IN) :: nside

    INTEGER(KIND=I8B) :: npix
    CHARACTER(LEN=*), PARAMETER :: code = "nside2npix"
    !=======================================================================

    npix = (12_i8b*nside)*nside
    if (nside < 1 .or. nside > ns_max .or. iand(nside-1,nside) /= 0) then
       write (*,*), nside, " is not a power of 2."
       npix = -1
    endif
    npix_result = npix

    return
  end function nside2npix


end module
