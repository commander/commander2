! The Mollweide projection routine gutted out of HEALPix 2.20a.
! Changes:
!   - change lon0, lat0 to radians rather than degrees
!   - use types_as_c instead of healpix_types and provide C wrapper
!   - mask by using NaN, rather than a separate mask array
!
! See below for original authors.
!
! October 2012, Dag Sverre Seljebotn
!
!

module map2image
  use types_as_c

  implicit none

contains

subroutine proj_mollw_cwrapper(pmap, nside, ordering, xsize, lon0, lat0, pimage) bind(c)
    type(c_ptr), intent(in), value :: pmap, pimage
    integer(i4b), value :: nside, ordering, xsize
    real(dp), value :: lon0, lat0

    real(sp), pointer, dimension(:) :: map
    real(sp), pointer, dimension(:,:) :: image

    call c_f_pointer(pmap, map, [12 * nside * nside])
    call c_f_pointer(pimage, image, [xsize, xsize / 2])
    call proj_mollw(map, nside, ordering, xsize, lon0, lat0, image)

end subroutine

subroutine proj_mollw(&
     map,      &
     nside,    &
     ordering, &
     xsize,    &
     lon0rad,     &
     lat0rad,     &
     image     )
   ! --------------------------------------------------------------
   ! This subroutine takes an input HEALPIX sky map array and
   ! generates the (u,v) position on a mollweide projection.
   !
   ! INPUTS:
   !       map     = array containing pixel temperatures
   !       nside   = defines number of pixels on the sky
   !       ordering= whether input map is in RING or NESTED format
   !       xsize   = x-dimension of output image array
   !       lon0    = longitude (in degrees) of the center of the plot
   !       image   = output mollweide projection image
   !
   ! PROCEDURES USED:
   !            ang_pix, ang_nest_pix
   !
   !  RELATED LITERATURE:
   !  see the web site http://www.tac.dk/~healpix
   !
   ! MODIFICATION HISTORY:
   !    October 1997, Eric Hivon, TAC (original IDL code)
   !       December 1998, A.J. Banday MPA (conversion to F90)
   !    Nov 2010, E.H., IAP (implement lat0 rotation)
   !
   ! --------------------------------------------------------------

   use constants
   use pix_tools_gutted
   use fortran_nan_mod, only: nanf

   IMPLICIT NONE

   REAL(SP), DIMENSION(0:), intent(in) :: map
   INTEGER(I4B),            intent(in) :: ordering, nside, xsize
   REAL(SP),     DIMENSION(0:xsize-1,0:xsize/2-1), intent(out) :: image
   REAL(DP), intent(in) :: lon0rad, lat0rad

   INTEGER(I4B) :: ysize,I,J
   INTEGER(I4B) :: xc, dx, yc, dy
   INTEGER(I4B) :: id_pix

   REAL(DP), DIMENSION(:,:), ALLOCATABLE :: u, v
   real(DP), dimension(1:3) :: vector
   real(DP), dimension(3,3) :: matrix
   real(DP) :: s_tmp, sz_tmp, z_tmp, phi_tmp, uu, vv
   real(sp) :: nan_single


   INTEGER(I4B) :: status

   nan_single = nanf()

   ! --------------------------------------------------------------

   ! --- define rotation matrix ---
   ! euler matrix: ZYX convention (itype=1)
   ! the minus sign on the second angle is because we work on latitude
   ! while the Euler matrix works on co-latitude
   call euler_matrix(-lon0rad, lat0rad, 0.0_dp, matrix, 1_i4b)

   !--- generates the (u,v) position on the mollweide map ---
   image(:,:) = nan_single                         ! nan outside of image

   ysize = xsize/2
   xc = (xsize-1)/2
   dx = xc
   yc = (ysize-1)/2
   dy = yc

   !--- allocate memory for arrays ---
   ALLOCATE(u(0:xsize-1,0:ysize-1),stat = status)
   do i = 0,xsize-1
      u(i,:) = real(i,dp)
   end do
   ALLOCATE(v(0:xsize-1,0:ysize-1),stat = status)
   do i = 0,ysize-1
      v(:,i) = real(i,dp)
   end do

   u =  2._dp*(u - real(xc,dp))/(real(dx,dp)/1.02_dp)   ! 1.02 : fudge factor in [-2,2]*1.02
   v =        (v - real(yc,dp))/(real(dy,dp)/1.02_dp)   ! in [-1,1] * 1.02

!$OMP PARALLEL DEFAULT(NONE) &
!$OMP SHARED(u,v,xsize,ysize,matrix,nside,ordering,image,map) &
!$OMP PRIVATE(i,j,uu,vv,s_tmp,z_tmp,sz_tmp,phi_tmp,vector,id_pix)
!$OMP DO schedule(dynamic, 1000)
   DO J = 0,ysize-1
      DO I = 0,xsize-1
         uu = u(i,j)
         vv = v(i,j)
         if ((uu*uu*0.25_dp + vv*vv) <= 1._dp) then
            !--- for each point on the mollweide map looks for the corresponding
            !    vector position on the sphere ---
            s_tmp  = sqrt( (1._dp-vv)*(1._dp+vv) )
            z_tmp  = (asin(vv) + vv*s_tmp) / HALFPI
            sz_tmp = SQRT( (1._dp - z_tmp)*(1._dp + z_tmp) )

            phi_tmp   = - HALFPI * uu / s_tmp
            vector(1) = sz_tmp * cos(phi_tmp)
            vector(2) = sz_tmp * sin(phi_tmp)
            vector(3) = z_tmp
            ! rotate position
            vector = matmul(matrix, vector)
            ! read out input map

            if (ordering == 2) then
               write (*,*) 'nested not implemented!'
               !call vec2pix_nest( nside, vector, id_pix )
            else ! default
               call vec2pix_ring( nside, vector, id_pix )
            end if

            if (isnan(map(id_pix))) then
               image(i, j) = nan_single
            else
               image(i, j) = map(id_pix)
            end if
         endif
      enddo
   enddo
!$OMP END DO
!$OMP END PARALLEL

   !--- deallocate memory for arrays ---
   deallocate( u )
   deallocate( v )

end subroutine proj_mollw

subroutine euler_matrix(       &
                            a1,    &
                            a2,    &
                            a3,    &
                            matrix,&
                            itype   &
                           )

  ! --------------------------------------------------------------
  !
  ! this subroutine computes the Euler matrix for different prescription
  !          X: +0, Y: +1, Z: +2
  !
  !itype=1 :    rotation a1 around original Z
  !             rotation a2 around interm   Y   (+1)
  !             rotation a3 around final    X   (+0)
  !     aeronautics convention                   =1
  !
  !itype=2 :    rotation a1 around original Z
  !             rotation a2 around interm   X   (+0)
  !             rotation a3 around final    Z   (+2)
  !     classical mechanics convention           =2
  !
  !
  !itype=3 :    rotation a1 around original Z
  !             rotation a2 around interm   Y   (+1)
  !             rotation a3 around final    Z   (+2)
  !     quantum mechanics convention             =3
  !
  ! see H. Goldestein, Classical Mechanics (2nd Ed.) p. 147 for
  ! discussion
  ! INPUTS:
  !        a1, a2, a3 = Euler angles, in radians
  !         all the angles are measured counterclockwise
  !
  ! MODIFICATION HISTORY:
  !    October 1997, Eric Hivon, TAC (original IDL code)
  !    January 1999, A.J. Banday MPA (conversion to F90)
  !    March 22 1999, E.H. Caltech   (modification to match IDL version,
  !    correction of a bug on the matrix product)
  !    May 2002, E. H. Caltech (transformation of euler_matrix into euler_matrix_new)
  !       M_new(a,b,c)  =  M_old(-a, b,-c)
  ! --------------------------------------------------------------

   IMPLICIT NONE

   REAL(DP)                , INTENT(IN)  :: a1,a2,a3
   REAL(DP), DIMENSION(3,3), INTENT(OUT) :: matrix
   INTEGER(I4B)            , INTENT(IN)  :: itype

   REAL(DP)                 :: c1,c2,c3, s1,s2,s3
   REAL(DP)                 :: ze,un
   REAL(DP), DIMENSION(3,3) :: m1,m2,m3
   character(len=*), parameter :: code ="Euler_Matrix_New"

   ! --------------------------------------------------------------

   !-- convert to sin and cosine values --
   c1 = COS(a1)
   s1 = SIN(a1)
   c2 = COS(a2)
   s2 = SIN(a2)
   c3 = COS(a3)
   s3 = SIN(a3)

   ze = 0.0_dp
   un = 1.0_dp

   !-- form full rotation matrix --
   !-- FOR REFERENCE:

   ! itype : 1
   ! m1 = [[ c1,-s1,  0],[ s1, c1,  0],[  0,  0,  1]] ; around   z
   ! m2 = [[ c2,  0, s2],[  0,  1,  0],[-s2,  0, c2]] ; around   y
   ! m3 = [[  1,  0,  0],[  0, c3,-s3],[  0, s3, c3]] ; around   x

   ! itype : 2
   ! m1 = [[ c1,-s1,  0],[ s1, c1,  0],[  0,  0,  1]] ; around   z
   ! m2 = [[  1,  0,  0],[  0, c2,-s2],[  0, s2, c2]] ; around   x
   ! m3 = [[ c3,-s3,  0],[ s3, c3,  0],[  0,  0,  1]] ; around   z

   ! itype : 3
   ! m1 = [[ c1,-s1,  0],[ s1, c1,  0],[  0,  0,  1]] ; around   z
   ! m2 = [[ c2,  0, s2],[  0,  1,  0],[-s2,  0, c2]] ; around   y
   ! m3 = [[ c3,-s3,  0],[ s3, c3,  0],[  0,  0,  1]] ; around   z


   ! matrix = m1 ( m2 m3 )

   m1(:,1) = (/ c1,-s1, ze /)
   m1(:,2) = (/ s1, c1, ze /)
   m1(:,3) = (/ ze, ze, un /)

   select case (itype)

   case(1) ! ZYX

      m2(:,1) = (/ c2, ze, s2 /)
      m2(:,2) = (/ ze, un, ze /)
      m2(:,3) = (/-s2, ze, c2 /)

      m3(:,1) = (/ un, ze, ze /)
      m3(:,2) = (/ ze, c3,-s3 /)
      m3(:,3) = (/ ze, s3, c3 /)

   case(2) ! ZXZ

      m2(:,1) = (/ un, ze, ze /)
      m2(:,2) = (/ ze, c2,-s2 /)
      m2(:,3) = (/ ze, s2, c2 /)

      m3(:,1) = (/ c3,-s3, ze /)
      m3(:,2) = (/ s3, c3, ze /)
      m3(:,3) = (/ ze, ze, un /)

   case(3) ! ZYZ

      m2(:,1) = (/ c2, ze, s2 /)
      m2(:,2) = (/ ze, un, ze /)
      m2(:,3) = (/-s2, ze, c2 /)

      m3(:,1) = (/ c3,-s3, ze /)
      m3(:,2) = (/ s3, c3, ze /)
      m3(:,3) = (/ ze, ze, un /)

   case default

      print*,'Unknow rotation prescription in '//code
      print*,'Must be in [1,2,3]'
      print*,'is ',itype
      stop

   end select

   matrix = matmul(m1,matmul(m2,m3))

 end subroutine euler_matrix


end module
