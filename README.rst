
Installation
------------

Commander is still under heavy development, so installation is a little tricky 
at the moment. This will be cleared up in the future, once the dust has 
settled.

For now, try the following instructions, which have been tested with Python 
2.7.4 on Ubuntu 13.04.

NOTE: Before following these instructions, read the "Known Issues" section 
      below.

 1. Ensure that recent versions of the following Python packages are installed:
     - numpy
     - scipy
     - h5py
     - pyfits
     - pytables
     - python-concurrent.futures
   
   You will also require standard build tools like autoconf, an MPI-enabled 
   compiler, and a working version of Cython (tested using 0.17.4 here).

 2. Install and compile libsharp as follows:
     a) Clone the libsharp git repository:
          $ git clone git://git.code.sf.net/p/libsharp/code libsharp
         
     b) Change to the newly-created libsharp/ directory, run 'autoconf' to 
        create a configure script, and then configure with the following flags:
          $ cd libsharp/
          $ autoconf
          $ CC=mpicc ./configure --enable-mpi --enable-pic
       
        It's important to explicitly define an MPI-enabled compiler. Otherwise, 
        you'll get error messages like "commander/sphere/sharp.so: undefined 
        symbol: sharp_execute_mpi" when you try to run Commander.
       
     c) Run `make test' to ensure everything is working.

  3. Install a recent version of Healpix (at least version 3.11) from 
     http://healpix.sourceforge.net/. Build it with gfortran (or ifort).
     Make sure to answer yes to enable PIC. Only the Fortran portion is
     needed, i.e., you can save some time by building using ``make f90-all``.
  
  4. Download and build a recent version of OpenBLAS from 
     http://xianyi.github.io/OpenBLAS/. This should be built in **single-threaded**
     mode, e.g., ``make USE_THREAD=0``, because Commander will do its own
     OpenMP parallelization where each thread does matrix operations on very
     small matrices.
  
  5. In the root directory of the Commander 2 git checkout, make a copy of the 
     file called 'config.example' and name it (e.g.) 'config.mymachine'. Then, 
     add the following options to the "./waf configure" line, making sure to 
     replace the paths given here with the appropriate ones for your machine:
     
     ./waf configure --inplace \
      --with-libsharp=$SOMEWHERE/libsharp/auto \
      --with-healpix-include=$SOMEWHERE/Healpix_3.11/include_gfortran \
      --with-healpix-lib=$SOMEWHERE/Healpix_3.11/lib_gfortran \
      --with-lapack=$SOMEWHERE/OpenBLAS \
      --with-lapack-lib=openblas
    
    Ensure that 'config.mymachine' is executable, and then run:
      $ ./config.mymachine
  
  6. Assuming that the configure script found everything it needs, you can now 
     build Commander:
       $ ./waf install wscript
  
  7. Once Commander has been built successfully, add the git root directory to 
     your PYTHONPATH. For example, if the absolute path to the commander2/ git 
     directory is /home/joe/commander2, you would use:
       $ export PYTHONPATH=$PYTHONPATH:/home/joe/commander2
  
  8. Check that everything is working properly by seeing if you can import the 
     'commander' Python package, e.g.
       $ python -c "import commander"
     
     If you don't get any error messages, then everything is probably fine.


Known issues
------------

MKL and NumPy/SciPy: Some scientific Python distributions (e.g., Enthought
Python Distribution) ship with NumPy/SciPy linked with MKL. The problem
with this is that MKL is loaded in such a way that the symbols from the
library overwrite the symbols of the single-threaded OpenBLAS (as described
above). The result is that rather than using N threads, N^2 threads are
used, usually with resulting program crash due to running out of memory.

For now the workaround is to build NumPy/SciPy oneself and add it to
PYTHONPATH. For instance, compile *another* version of OpenBLAS, this
time with ``USE_THREAD=1``, and then build NumPy using it, and then
build SciPy with the rebuilt NumPy in ``PYTHONPATH``.
