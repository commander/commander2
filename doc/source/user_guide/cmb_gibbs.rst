Joint Bayesian component separation and model estimation
========================================================

The main goal of Commander is to perform joint Bayesian component
separation and model estimation of the CMB data (see
http://arxiv.org/abs/0709.1058). Here we describe everything that can
go into a Commander analysis of CMB data; of course, one is free
to sample only a subset of the parameters in a given run.

Input
-----

Data
''''

The input is a number of maps, indexed by :math:`i`,
each containing the following information:

Temperature and polarization maps :math:`\mathbf{d}_{i}`:
   In the HEALPix pixelization.

Noise properties :math:`\mathbf{N}_i`:
   Currently supported: RMS maps (independent noise between pixels)

Mask :math:`\mathbf{m}_i`:
   Internally the mask is treated as part of :math:`\mathbf{N}^{-1}_i`

Beams :math:`\mathbf{B}_i`:
   Currently supported: Symmetric beams; input the spherical harmonic
   transfer function.
   Todo: FEBeCOP beam

Frequency band information :math:`f(\nu)`:
   Either :math:`[\nu_\text{min}, \nu_{max}]` or :math:`F_i(\nu)`



Priors
''''''


Output
------

:math:`\xi^2` map:
    ...


Instrument parameters
'''''''''''''''''''''

Monopole, dipole:
     One per detector map :math:`i`.
     Choose to arbitrarily fix monopole at some frequencies.


Bandpass shift:

Gain :math:`g_i`:

Noise calibration factor :math:`\tau_i`:
      Multiplicative factor with :math:`\tau_i`


Foreground parameters
'''''''''''''''''''''

Dust :math:`A^d_p f(\nu; T, e) g_\nu`:
     Priors on all parameters.

Synchrotron, :math:`A^s_p (\frac{\nu}{\nu_\text{ref}}^{\beta + C \log(\frac{\nu}{\nu_\text{ref})}`:

Free-free, :math:`A^f_p (\frac{\nu}{\nu_\text{ref}}^{\beta_f}`
     Prior: :math:`N(-2.15, 0.02^2)`

CO :math:`A^c_p \alpha_\nu`
     Specifically, :math:`\alpha^{217}, \alpha^{353}`


CMB model parameters
''''''''''''''''''''

Currently only power spectrum estimation is supported:

CMB power spectrum :math:`C_\ell`, :math:`\sigma_\ell` :
    Standard inverse-Wishart
