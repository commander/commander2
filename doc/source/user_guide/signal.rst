Modelling signal components
===========================

Every signal component on the sky (CMB, synchrotron, free-free, etc.) comes
with two classes to implement them. The first one, with the suffix `Signal`,
is a *model description*::

    synchrotron_info = cm.SynchrotronSignal(
        nu_ref=nu_ref, lmax=lmax,
        beta_prior=(-2.0, 1.0), # mean, std. dev
        )

The model description is immutable (carries no state), serializable,
hashable, and in general does no work on construction.

Given a model description of a signal, one can create the `Sampler`
for it, typically attached to a `Chain` instance::

    # Get descriptions of parameters to sample. This can depend on resolution
    # of the observations
    synchrotron_params = synchrotron_info.get_parameters([obs1, obs2])

    # Now, we can construct a chain involving these parameters
    chain = cm.Chain([obs1, obs2], synchrotron_params)

    # Finally, create an instance of cm.signal.synchrotron.SynchrotronSampler
    # attached to the given chain
    synchrotron_sampler = cm.create_sampler(chain, synchrotron_info)

The sampler is stateful (may store precomputations necesarry for
the sampling process etc.). On construction the sampler may read out
parameters that are relevant for it from the `chain` argument passed
in.

The dependencies between the constructed objects are as follows:

.. graphviz::
   
   digraph {
      "synchrotron_sampler" -> "synchrotron_info" [label="Get parameters from",fontsize=8];
      "synchrotron_sampler" -> "chain" [label="Read state from, store samples to",fontsize=8];
   }




.. automodule:: commander.signal
   :members:
   :undoc-members:
   :show-inheritance:
