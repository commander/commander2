
Welcome to Commander's documentation!
=====================================

The main purpose of Commander is to be the best CMB Gibbs sampler
around. However, doing that depends on a lot of building blocks,
and it is also an aim to package those building blocks nicely
enough so that Commander (as a library) is applicable to other
settings as well.

Contents:

.. toctree::
   :maxdepth: 2

   user_guide/user_guide
   library_guide/library_guide
   dev_guide/dev_guide



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

