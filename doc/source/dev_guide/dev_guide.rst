The Commander Developer's Guide
===============================

Conventions
-----------

 * Every module should start with ``from __future__ import division``
 * Commander imported as ``cm``, NumPy as ``np``, SciPy as ``sp``, oomatrix as ``om``,
   matplotlib.pyplot as ``plt``


Random number generation
------------------------

Each program typically need two RNGs: One that is
guaranteed to be different between ranks (given the variable name
``nc_rng``, for non-collective RNG), and one that has the same seed
for every rank and is always called collectively (called ``c_rng``).


