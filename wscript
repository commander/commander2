#./waf-light --tools=compat15,swig,fc,compiler_fc,fc_config,fc_scan,gfortran,g95,ifort,gccdeps;

import os
from textwrap import dedent

from waflib import TaskGen

top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_c')
    opt.load('compiler_fc')
    opt.load('python')
    opt.load('inplace', tooldir='tools')
    opt.add_option('--with-libsharp', help='path to libsharp to use for benchmark comparison '
                   '(NOTE: must be built with -fPIC)')
    opt.add_option('--with-lapack')
    opt.add_option('--with-lapack-lib')
    opt.add_option('--with-healpix-include')
    opt.add_option('--with-healpix-lib')

def configure(conf):
    conf.add_os_flags('PATH')
    conf.add_os_flags('PYTHON')
    conf.add_os_flags('PYTHONPATH')
    conf.add_os_flags('INCLUDES')
    conf.add_os_flags('LIB')
    conf.add_os_flags('LIBPATH')
    conf.add_os_flags('STLIB')
    conf.add_os_flags('STLIBPATH')
    conf.add_os_flags('CFLAGS')
    conf.add_os_flags('CXXFLAGS')
    conf.add_os_flags('LINKFLAGS')
    conf.add_os_flags('CYTHONFLAGS')
    conf.add_os_flags('FCFLAGS')

    conf.load('compiler_c')
    conf.load('compiler_fc')

    conf.load('python')
    conf.check_python_version((2,5))
    conf.check_python_headers()

    conf.check_tool('numpy', tooldir='tools')
    conf.check_numpy_version(minver=(1,3))
    conf.check_tool('cython', tooldir='tools')
    conf.check_cython_version(minver=(0,16,0))
    conf.check_tool('inplace', tooldir='tools')

    conf.check_libsharp()
    conf.check_mpi()
    conf.check_mpi4py()

    conf.env.CFLAGS_OPENMP = ['-fopenmp']
    conf.env.FCFLAGS_OPENMP = ['-fopenmp']
    conf.env.LINKFLAGS_OPENMP = ['-fopenmp']

    conf.env.INCLUDES_HEALPIX = [conf.options.with_healpix_include]
    conf.env.LIBPATH_HEALPIX = conf.options.with_healpix_lib.split(',')
    conf.env.LIB_HEALPIX = ['healpix', 'cfitsio']

    conf.env.RPATH_LAPACKSINGLE = [conf.options.with_lapack]
    conf.env.LIBPATH_LAPACKSINGLE = [conf.options.with_lapack]
    conf.env.LIB_LAPACKSINGLE = conf.options.with_lapack_lib.split(',')
    


def build(bld):
    bld(source=bld.srcnode.ant_glob('src/slatec/*.f'),
        target='slatec',
        use='fcshlib',
        features='fc fcshlib')

    # Static library
    bld.shlib(
        source=['src/types_as_c.f90', 'src/constants.f90',
                'src/fortran_nan.c', 'src/fortran_nan_mod.f90'],
        target='fortranutils',
        features='fcshlib fc cshlib')

    bld(source=['src/spline_2D_mod.f90', 'src/spline_1D_mod.f90', 'src/locate_mod.f90',
                'src/math_tools.f90'],
        use='fortranutils LAPACKSINGLE',
        target='spline2d_f90',
        features='fcshlib fc cshlib')

    bld(source=['src/healpix/map2image.f90', 'src/healpix/pix_tools_gutted.f90',
                'src/healpix/pixel_ordering.f90'],
        use='fortranutils',
        target='gutted_healpix',
        features='fcshlib fc cshlib')

    bld(source=['commander/utils/spline2d.pyx'],
        target='spline2d',
        use='spline2d_f90',
        features='pyext cshlib c')

    bld(source=['commander/common.pyx'],
        target='common',
        features='pyext cshlib c')


    #
    # commander.sphere
    #
    bld(source=(['commander/sphere/_mpi4py_bridge.pyx']),
        target='_mpi4py_bridge',
        use='MPI4PY MPI',
        features='c pyext cshlib')

    bld(source=(['commander/sphere/sharp.pyx']),
        target='sharp',
        use='NUMPY SHARP MPI',
        features='c pyext cshlib')

    #bld(source=['commander/sphere/shtools.pyx'],
    #    target='shtools',
    #    use='NUMPY HEALPIX OPENMP',
    #    features='c pyext cshlib')

    bld(source=['commander/sphere/mmajor.pyx'],
        target='mmajor',
        features='pyext cshlib c')

    bld(source=['commander/sphere/healpix.pyx'],
        target='healpix',
        use='gutted_healpix',
        features='pyext cshlib c')

    bld(source=['commander/sphere/wigner.pyx'],
        target='wigner',
        use='slatec',
        features='pyext cshlib c')

    bld(source=['commander/sphere/legendre.pyx', 'src/legendre/ylmgen_c.c',
                'src/legendre/c_utils.c', 'src/legendre/roots.c',
                'src/legendre/legendre_transform.c.in'],
        target='legendre',
        includes=['src/legendre'],
        use='NUMPY OPENMP',
        features='pyext c cshlib')

    #
    # commander.compute.cr.brute_force
    #
    bld(source=['commander/compute/cr/mblocks.pyx'],
        target='mblocks',
        features='pyext cshlib c')

    bld(source=['commander/compute/cr/sh_integrals.pyx'],
        target='sh_integrals',
        use='slatec OPENMP',
        features='pyext cshlib c')

    bld(source=['commander/compute/cr/mg_smoother.pyx',
                'commander/compute/cr/mg_smoother.f90',
                'src/legendre/legendre_transform.c.in'],
        use='fortranutils HEALPIX OPENMP LAPACKSINGLE',
        target='mg_smoother',
        features='pyext fc fcshlib cshlib c')

    #
    # commander.compute
    #
    bld(source=(['commander/compute/adaptive_rejection.pyx']),
        target='adaptive_rejection',
        use='NUMPY',
        features='c pyext cshlib')

from waflib.Configure import conf
from os.path import join as pjoin

@conf
def check_mpi(conf):
    # See also FindMPI.cmake         
    #if conf.options.enable_mpi:

    mpicc = conf.env.MPICC or 'mpicc'

    conf.check_cfg(path=mpicc, args=['--showme:compile'],
                   package='', uselib_store='MPI', mandatory=False,
                   msg='MPI (OpenMPI-like)', okmsg=mpicc)
    if conf.env.HAVE_MPI:
        conf.check_cfg(path=mpicc, args=['--showme:link'],
                       package='', uselib_store='MPILINK', mandatory=False,
                       msg='MPI (OpenMPI-like linker flags)', okmsg=mpicc)
        conf.env.LIBPATH_MPI = conf.env.LIBPATH_MPILINK
        conf.env.LIB_MPI = conf.env.LIB_MPILINK
        conf.env.LINKFLAGS_MPI = conf.env.LINKFLAGS_MPILINK
        return

    # MPICH2
    conf.check_cfg(path=mpicc, args='-compile-info -link-info',
                   package='', uselib_store='MPI', mandatory=True,
                   msg='MPI (mpich2-like)', okmsg=mpicc)

@conf
def check_libsharp(conf):
    """
    Settings for libsharp
    """
    conf.start_msg("Checking for libsharp")
    prefix = conf.options.with_libsharp
    if not prefix:
        conf.fatal("--with-libsharp not used (FIXME)")
    conf.env.LIB_SHARP = ['sharp', 'fftpack', 'c_utils']
    conf.env.LINKFLAGS_SHARP = ['-fopenmp']
    conf.env.LIBPATH_SHARP = [pjoin(prefix, 'lib')]
    conf.env.INCLUDES_SHARP = [pjoin(prefix, 'include')]
    # Check presence of libsharp in general
    cfrag = dedent('''\
    #include <sharp.h>
    #include <sharp_geomhelpers.h>
    sharp_alm_info *x;
    sharp_geom_info *y;
    int main() {
    /* Only intended for compilation */
      sharp_make_general_alm_info(10, 10, 1, NULL, 0, 0, &x);
      sharp_make_healpix_geom_info(4, 1, &y);
      return 0;
    }
    ''')
    conf.check_cc(
        fragment=cfrag,
        features = 'c',
        compile_filename='test.c',
        use='SHARP')
    conf.end_msg(prefix if prefix else True)

@conf
def check_mpi4py(conf):
    conf.start_msg("Checking mpi4py includes")
    conf.check_python_module('mpi4py')
    (mpi4py_include,) = conf.get_python_variables(
            ['mpi4py.get_include()'], ['import mpi4py'])
    conf.env.INCLUDES_MPI4PY = [mpi4py_include]

    # Check Cython compilation including mpi4py
    conf.check_cc(
        fragment='from mpi4py.MPI cimport Comm',
        features = 'pyext cshlib c',
        compile_filename='test.pyx',
        use='MPI MPI4PY')

    conf.end_msg('ok (%s)' % mpi4py_include)

def run_jinja(task):
    from jinja2 import Template, Environment
    import sys
    assert len(task.inputs) == len(task.outputs) == 1

    env = Environment(block_start_string='/*{',
                      block_end_string='}*/',
                      variable_start_string='{{',
                      variable_end_string='}}')
    template = env.from_string(task.inputs[0].read())
    result = template.render(len=len)
    #template = Template(task.inputs[0].read())
    #result = template.render(len=len)
    #result = tempita.sub(tmpl)
    #result, n = re.subn(r'/\*.*?\*/', '', result, flags=re.DOTALL)
    #result = '\n'.join('/*!*/  %s' % x for x in result.splitlines())
    #result = '/* DO NOT EDIT THIS FILE, IT IS GENERATED */\n%s' % result
    task.outputs[0].write(result)

TaskGen.declare_chain(
        name = "jinja",
        rule = run_jinja,
        ext_in = ['.c.in'],
        ext_out = ['.c'],
        reentrant = True,
        )
