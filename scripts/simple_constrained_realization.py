from __future__ import division

import commander as cm

import logging
from matplotlib import pyplot as plt
from oomatrix import Matrix, compute, explain, compute_array
from oomatrix.selection import RangeSelection
from oomatrix.solvers.conjugate_gradients import conjugate_gradients
import oomatrix as om

from commander.matrices import *
from commander.resourceloaders import *
from commander.plot import *
from commander.sphere.mmajor import lm_count

#
# Setup, parameters
#

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
rng = np.random.RandomState(34)
nside = 32
lmax = 60
lprecond = 20
npix = 12 * nside**2
nlm = lm_count(0, lmax)

#
# Make parameters and a simulation
#

ctx = cm.CommanderContext(nc_rng=rng)

Y = ctx.sh_synthesis_matrix(nside, 0, lmax, 'Y')
YhW = ctx.sh_analysis_matrix(nside, 0, lmax, 'YhW')

# Cosmological model
ls = np.arange(lmax + 1, dtype=np.double)
Cl = 1 / (ls + 1) / (ls + 2)
Cl[0:2] = 0

S_cmb = cm.matrices.diagonal_matrix_by_l(Cl, 0, lmax, name='S_cmb')
F_cmb = compute(S_cmb.f, name='F_cmb')

# Beam
beam = cm.gaussian_beam_by_l(lmax, '10 deg')
B = cm.matrices.diagonal_matrix_by_l(beam, 0, lmax, 'B')

# Noise + mask (the 1/3 north pole)
sigma_n = 0.01
rms = np.ones(npix) * sigma_n
mask = np.zeros(npix)
mask[:mask.shape[0] // 3] = 1
mask[2 * mask.shape[0] // 3:] = 1

# Both N and Ninv has mask=0, so strictly speaking, N.i != Ninv,
# although the point is simply to remove these pixels from the linear system
N = om.diagonal_matrix(mask * rms**2, name='N')
inverse_noise_map = mask / rms**2
Ni = om.diagonal_matrix(inverse_noise_map, name='Ni')

# Simulate signal and data
signal = compute_array(F_cmb * rng.normal(size=nlm)[:, None])
data = compute_array(Y * B * signal) + rms[:, None] * rng.normal(size=npix)[:, None]

# Set up LHS
n_per_component = lm_count(0, lmax)
d = np.zeros(n_per_component)
d[1:2 * (lmax + 1):2] = 1 # compensate for zero-padded rows/columns
D_zero = om.diagonal_matrix(d, name='[0]')
D_one = om.identity_matrix(n_per_component)

LHS = D_one + F_cmb.h * B.h * Y.h * Ni * Y * B * F_cmb

# Set up preconditioner matrix M
from commander.sphere.sh_integrals import compute_approximate_Yt_D_Y_diagonal

buf = compute_approximate_Yt_D_Y_diagonal(npix, 0, lmax,
                                          ctx.sh_analysis(nside, 0, 2 * lmax, inverse_noise_map))
Ni_sh_arr = buf.copy()
buf = buf * B.diagonal()**2 * S_cmb.diagonal() + 1
M = om.diagonal_matrix(1 / buf, 'M')

# Set up right-hand-side b
n = LHS.ncols
w_pix = rng.normal(size=npix)[:, None]
w_sh = rng.normal(size=nlm)[:, None]

b = compute_array(F_cmb.h * B.h * Y.h * Ni * data)
b += compute_array(F_cmb.h * B.h * Y.h * Ni.f * w_pix)
b += w_sh

# Solve system
unscaled_x, info = conjugate_gradients(LHS, b, M, eps=1e-4, logger=logger)
x = compute_array(F_cmb * unscaled_x)

# Plot maps

fig, axs = plt.subplots(2, 2)

cm.plot.plot_sh_map(ctx, x[:, 0], nside, 0, lmax, mask=mask, title='sample', ax=axs[0,0])
cm.plot.plot_sh_map(ctx, signal[:, 0], nside, 0, lmax, mask=mask, title='signal', ax=axs[0,1])
cm.plot.plot_ring_map(data[:, 0], mask=mask, title='data', ax=axs[1,0])


# Plot power spectra
from commander.sphere.mmajor import compute_power_spectrum

sigma_sample = compute_power_spectrum(0, lmax, x[:, 0])
sigma_signal = compute_power_spectrum(0, lmax, signal[:, 0])

ax = axs[1, 1]
scaling = ls * (ls + 1)
ax.plot(sigma_sample * scaling, label='Sample s')
ax.plot(sigma_signal * scaling, label='Original s')
ax.plot(sigma_n**2 * scaling, label='Noise power')
ax.plot(Cl * scaling, label='Cl')
ax.plot(Cl * beam**2 * scaling, label='Cl*beam^2')
ax.legend(loc='upper left')


plt.show()
