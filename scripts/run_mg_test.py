#!/usr/bin/python
"""
    COMMANDER 2 EXAMPLE SCRIPT

A pedagogical example of how to draw constrained realisations of masked, 
single-channel Planck data with Commander 2's multi-level solver.

In this example, we simulate a CMB sky map rather than performing the analysis 
on real data. This lets us check the actual error/convergence, since we know the 
correct solution.

To run this as-is, you need some Planck datafiles (see below). Also, you must 
set the environment variables $HEALPIX and $PLANCK.

  -- Phil Bull and Dag Sverre Seljebotn (August 2013)
"""

from __future__ import division
import numpy as np
from commander import *
from commander.logger_utils import timed, log_done_task
import commander.compute.cr.multilevel as mg
from commander.compute.cr.levels import *
import os, logging

#from commander.plot import *
#from commander.compute.cr import mg_cr as mg
#from commander.compute.cr import mg_smoother
#from commander.sphere.beams import mhwavelet_beam_by_l


# Set-up logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('mg')

rng = np.random.RandomState(10)


# (1) Load observations and define experimental setup
#
# The 'SkyObservation' object pulls together all of the necessary experimental 
# setup information for a single band. The data are specified as tuples, with 
# the filename and FITS extension specified.
#
# The SkyObservation object is intended to be immutable (i.e. it's just there 
# to keep track of filenames and other settings for a given analysis run, so 
# isn't expected to change). You can imagine keeping a big list of 
# SkyObservation objects for all the Planck observations in a library somewhere.
#
# Because of this, SkyObservation doesn't load the data files when first 
# constructed. You can use the clone_with() method, and the load_xxx() methods, 
# to create a copy of the SkyObservation with the data loaded into memory.
#
#   - You can use environment variables in the filename; e.g. we use $PLANCK to 
#     define the data directory here.
#
#   - The noise is specified as a variance map here, but you can specify an RMS 
#     map instead (see the SkyObservation class documentation)
#
#   - A list of masks can be specified. These are automatically added together 
#     by Commander to form the total mask used for the analysis.

print " * Loading sky observation..."
nside = 2048
obs = SkyObservation( nside=nside, lmax=3000,
    map =      ('$PLANCK/HFI_SkyMap_143_2048_R1.10_nominal.fits', 0),
    variance = ('$PLANCK/HFI_SkyMap_143_2048_R1.10_nominal.fits', 'II_COV'),
    masks =   [('$PLANCK/HFI_Mask_GalPlane_2048_R1.10.fits', 'GAL080'),
               ('$PLANCK/HFI_Mask_PointSrc_2048_R1.10.fits', 'F143_10') ],
    beam =      '$PLANCK/planck13_beam_143.fits',
    unique_basenames=True )

# Load datafiles to memory, using clone_with()
obs = obs.clone_with(rms=obs.load_rms_map(),
                     masks=[obs.load_mask_map()],
                     beam=obs.load_sh_beam())
print "\tDone loading."


# (2a) Load CMB power spectrum Cl's
#
# Note that these must be the actual Cl's, and not D_l = l(l+1)C_l/(2 pi), 
# which used in the Planck datafiles.
# 
# The multi-level algorithm is currently unable to condition on a monopole and 
# dipole, so C_0 and C_1 cannot be given zero variance (see p11 of 
# arXiv:1308.5299). As such, we fix them to have the same variance as the 
# quadrupole here.

# Define relevant angular scales for the analysis
lmax = 3000
lstar = 2100
lleft = 0
lright = 3000

# Load power spectrum, transform to C_l's, and fix mono/dipole
Cl = load_power_spectrum_from_file('$PLANCK/base_planck_lowl_lowLike_highL_post_lensing.bestfit_cl.fits', lmax)
ls = np.arange(2, lmax + 1)
Cl[2:] /= ls * (ls + 1) / 2 / np.pi
Cl[0] = Cl[1] = Cl[2]


# (2b) Define window function for the uppermost level
# 
# Here, we only perform the analysis for l<2100, so we construct a tophat 
# window function for the uppermost ShLevel to enforce this.

wl = np.ones(lmax + 1)
wl[:lstar] = 0
wl[lstar:lstar+100] = np.arange(100) / 100.


# (3) Define smoother levels
# 
# This list defines the multi-level smoother strategy. The levels are defined 
# in order, from finest to coarsest. The cycle strategy can be defined 
# 
#   - On the top level, we have a spherical harmonic level (ShLevel) with a 
#     simple diagonal smoother. This solves for the highest-freq. spatial modes.
#     To enforce a W-cycle, we set nrec=2 (i.e. 2 recursions through lower 
#     levels will be performed).
#
#   - The intermediate levels are all SH levels with sibling pixel levels 
#     (PixelLevel), where a localised pixel-space smoother is applied.
# 
#     The various settings for these levels tune the restriction beam, tile 
#     size and ridge adjustment (for the block ICC) and so on. See Sect. 3 of 
#     arXiv:1308.5299 and the class documentation for PixelLevel for more info.
#
#   - The bottom level is a brute-force Cholesky solver in SH space. This 
#     exactly solves for the lowest-frequency spatial modes (which is now 
#     possible, because on this level the system is relatively few elements).


# POSSIBLE BEAMS:
#r_beam = np.ones(lmax + 1)
#r_beam = modified_gaussian_beam(fwhm, b, lmax)
#r_beam = mhwavelet(lmax, fwhm, mexhat)
#r_beam = gaussian_beam_by_l(lmax, fwhm)
#r_beam = gaussian_pixel_beam_by_l(lmax, nside, w)


# FIXME: Pixel levels need r_beam
levels = [
  ShLevel(lmax, r_beam=np.ones(lmax+1), wl=wl, split='Si'),
  PixelLevel(1024, lmax=lmax, r_beam=fourth_order_beam(lmax, 2800), 
             split='w', tilesize=8, ridge=9.74e-04, omega=1, hugemem=True, 
             accurate=False, nrec=2),
  PixelLevel(512, lmax=4*512, r_beam=fourth_order_beam(4*512, 3*512, 0.1), 
             split='Si', tilesize=8, ridge=7.30e-05, omega=1, hugemem=True,
             accurate=False),
  PixelLevel(256, lmax=5*256, r_beam=gaussian_pixel_beam_by_l(5*256, 256, w=2), 
             split='Si', tilesize=8, ridge=1.61e-05, omega=1,
             hugemem=True, accurate=False),
  PixelLevel(128, lmax=6*128, r_beam=gaussian_pixel_beam_by_l(6*128, 128, w=1.7), 
             split='Si', tilesize=8, ridge=3.56e-06, omega=1, 
             hugemem=True, accurate=False),
  PixelLevel(64, lmax=6*64, r_beam=gaussian_pixel_beam_by_l(6*64, 64, w=1.7), 
             split='Si', tilesize=8, ridge=2.00e-02, omega=1, 
             hugemem=False, accurate=False),
  PixelLevel(32, lmax=7*32, r_beam=gaussian_pixel_beam_by_l(7*32, 32, w=1.7), 
             split='Si', tilesize=8, ridge=3.08e-02, omega=1, 
             hugemem=False, accurate=False),
  ShSolveLevel(lmax=40, r_beam=np.ones(40+1), split='Si'),
  ]


# (4) Create new 'MgSolver' instance and initialise levels
#
# The MgSolver class is used to manage precomputations of the smoothers for 
# each level. The precomputations are computationally-intensive, and so we 
# would normally want to cache them.

print " * Preparing solver..."
solver = mg.MgSolver(levels, obs, Cl)

# Precompute noise and prior operators for each level (comp. intensive)
print " * Precomputing noise terms..."
solver.precompute_noise_term()
print " * Precomputing prior terms..."
solver.precompute_prior_term()


# (5) Construct RHS of linear system
#
# Instead of using an actual sky map as our data, we just simulate one using 
# the input Cl's and Planck noise specification.
#
# FIXME: Note that we aren't constructing a proper RHS here.

q = np.zeros(lmax + 1)
q[:3001] = Cl[:3001]
q[:lleft] = 0
q[lright:] = 0

# Build RHS vector (levels[0].matvec is just the unrestricted 'A' operator, 
# so A u = b)
u0sh = scatter_l_to_lm(np.sqrt(q)) * rng.normal(size=(lmax + 1)**2)
u0 = u0sh
rhs = solver.levels[0].matvec(u0)

# Initial guess for solution (all zeros)
u = np.zeros((lmax + 1)**2)

# Lists to keep track of solution/residual after each iteration
us = []; rs = []
us.append(u.copy())
rs.append(rhs.copy())


# (6) Do a few iterations of the MG solver
#
# Each iteration returns a list of statistics, which we can analyse elsewhere.

for i in range(3):

    # Do single MG cycle
    mg_cycle(0, levels, u, rhs, logger)
    
    # Store results
    us.append(u.copy())
    rs.append(rhs - solver.levels[0].matvec(u))
    print '%.2e' % np.mean(norm_by_l(us[-1] - u0) / norm_by_l(u0))


