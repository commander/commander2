#!/usr/bin/env python
from __future__ import division

import numpy as np
import commander as cm
from commander.compute.cr.mg_smoother import (
    csc_neighbours_full,
    block_At_D_A, convert_ring2tiled, convert_tiled2ring,
    compute_csc_beam_matrix, udgrade_ring)


__doc__ = """
Tool to degrade an RMS map

deg_beam is the beam wanted on the output; i.e. the degradation process
uses (obs_beam / deg_beam).

One can use a string '40 min' instead of a FITS file to get a Gaussian beam.
"""

import argparse

def load_beam(lmax, beam_spec):
    if beam_spec.endswith('.fits'):
        return cm.load_beam_transfer_from_fits(args.obs_beam)
    else:
        return cm.gaussian_beam_by_l(lmax, beam_spec)

def main(args):
    if args.lmax is None:
        lmax = 4 * args.nside
    else:
        lmax = args.lmax
    nside = args.nside
    nside_tile = args.r
    nside_level = nside // nside_tile
    ntiles = 12 * (nside_level)**2


    if 0:
        from matplotlib.pyplot import show, plot, semilogy
        theta = np.linspace(0, 2 * nside_tile * pixwidth, 100)
        semilogy(np.abs(cm.beams.beam_by_theta(degrade_beam, theta)))
        show()
    

    #
    # Set up RMS map
    #
    rms_sq = cm.load_map((args.input, 1, 'II_cov'), 'raw')
    rms_sq *= 1e12
    rms_sq[rms_sq < 0] = 0

    if args.d:
        rms_sq = udgrade_ring(rms_sq.copy(), args.d)
    
    nside_obs = cm.nside_of(rms_sq)    

    weights = cm.sphere.get_healpix_temperature_ring_weights(nside_obs)
    weights_sq_map = cm.sphere.healpix_scatter_ring_weights_to_map(nside_obs, weights**2)
    rms_sq *= weights_sq_map


    #
    # Set up beam
    #
    orig_beam = load_beam(lmax, args.obs_beam)[:lmax + 1]
    wanted_beam = load_beam(lmax, args.wanted_beam)[:lmax + 1]

    pixwin_hi = cm.sphere.get_healpix_pixel_window(nside_obs)[0][:lmax + 1]
    pixwin_lo = cm.sphere.get_healpix_pixel_window(nside)[0][:lmax + 1]

    orig_beam *= pixwin_hi
    wanted_beam *= pixwin_lo
    degrade_beam = wanted_beam / orig_beam


    #
    # Print some stats
    #
    pixwidth = np.sqrt(4 * np.pi / (12 * nside**2))
    amp_pixwidth = np.max(np.abs(cm.beams.beam_by_theta(degrade_beam,
        np.linspace(nside_tile * pixwidth, (nside_tile + 1) * pixwidth, 100))))
    amp0 = cm.beams.beam_by_theta(degrade_beam, np.array([0.0]))[0]
    error = amp_pixwidth / amp0
    print 'Degrading to nside=%d, truncating beam at lmax=%d,' % (nside, lmax)
    print 'going %d pixels out results in error on level %2.e' % (nside_tile, error)

    #
    # Computation
    #    
    rms_sq_tiled = convert_ring2tiled(nside_level, rms_sq)
    B_indptr, B_indices = csc_neighbours_full(nside_level)
    B_blocks = np.empty(((nside_obs // nside_level)**2,
                         (nside // nside_level)**2,
                         B_indices.shape[0]), np.double, order='F')
    print 'Memory use for B matrix: %.2f GiB' % (B_blocks.itemsize * B_blocks.size / 1024.**3)
    B_blocks = compute_csc_beam_matrix(False, nside_obs, nside, nside_level,
                                       degrade_beam, B_indptr, B_indices, blocks=B_blocks)

    
    indptr_diagonal = np.arange(ntiles + 1, dtype=np.int32)
    indices_diagonal = np.arange(ntiles, dtype=np.int32)
    
    blocks = block_At_D_A(B_indptr, B_indices, B_blocks, rms_sq_tiled,
                          indptr_diagonal, indices_diagonal)
    out_tiled = np.zeros((nside_tile**2, ntiles), order='F')
    for i in range(ntiles):
        out_tiled[:, i] = blocks[:, :, i].diagonal()
    out_map = convert_tiled2ring(out_tiled)

    out_map = np.sqrt(out_map)

    cm.fits.ring_map_to_fits(args.output_rms, out_map) 

parser = argparse.ArgumentParser(description=__doc__)

parser.add_argument('-d', help='udgrade rms first to given nside for debug purposes',
                    type=int, default=None)
parser.add_argument('-r', help='How many pixels to include (minimum), on the coarse grid',
                    type=int, default=8)
parser.add_argument('--lmax', help='What lmax to use, default is 4*nside',
                    type=int, default=None)
parser.add_argument('obs_beam', help='FITS file with observational beam transfer by l')
parser.add_argument('wanted_beam', help='The wanted resulting beam of the degraded data')
parser.add_argument('nside', type=int, help='wanted nside')
parser.add_argument('input', help='Input Planck observation FITS file')
parser.add_argument('output_rms', help='Output rms map in uK')

args = parser.parse_args()
main(args)
