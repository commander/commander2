import commander as cm

#
# Data
#

# To save typing, we iterate over the WMAP bands using a loop
obs_list = []
for obs_name, nu, sigma0 in [('K1', 22.8, '1.437 mK')
                             ('Ka1', 33.0, '1.470 mK'),
                             ('Q1', 40.7, '2.254 mK'),
                             ('Q2', 40.7, '2.140 mK'),
                             ('V1', 60.8, '3.319 mK'),
                             ('V2', 60.8, '2.955 mK'),
                             ('W1', 93.5, '5.906 mK'),
                             ('W2', 93.5, '6.572 mK'),
                             ('W3', 93.5, '6.941 mK'),
                             ('W4', 93.5, '6.778 mK')]:
    obs = cm.SkyObservation(
        # The name is displayed in various places during debugging.
        # The only roles of source and description is to serve as metadata in the
        # resulting chain files (all the information in the SkyObservation is dumped
        # to the resulting chain files).
        name=obs_name,
        source='WMAP-r9'
        description='WMAP r9 7yr DA maps from http://lambda.gsfc.nasa.gov, '
                    'using temperature analysis mask'

        # Location of temperature map; (filename, fits_record, column_name)
        T_map=('$WMAPPATH/wmap_da_imap_r9_7yr_%s_v4.fits' % obs_name, 1, 'TEMPERATURE'),

        # Specify rms in terms of n_obs map and sigma0
        nobs_map=('$WMAPPATH/wmap_da_imap_r9_7yr_%s_v4.fits' % obs_name, 1, 'N_OBS'),
        sigma0=cm.as_temperature(sigma0),

        # Alternatively, one could pass rms_map:
        # rms_map = (filename, fits_record, fits_column)

        beam_transfer='$WMAPPATH/wmap_%s_ampl_bl_7yr_v4.txt',
        mask_map='$WMAPPATH/wmap_temperature_analysis_mask_r9_7yr_v4.fits',
        
        # Bandpass can be either:
        # - (low, high)
        # - 2D array f[i, :] = (nu_i, f(nu_i))
        bandpass=(nu - 5, nu + 5)
        )
    obs_list.append(obs)

K1, Ka1, Q1, Q2, V1, V2, W1, W2, W3, W4 = obs_list

#
# Model
#

# Note: These variables are simply reused below and have no meaning in themselves
nu_ref = 60
lmax = 1500

cmb = cm.IsotropicGaussianCmbSignal(
    # Specify degrees for a flat initial power spectrum, or a filename
    # to a text file such as 'wmap_lcdm_sz_lens_wmap7_cl_v4.dat' to start
    # a particular place
    power_spectrum=cm.as_temperature('3000 uK'),
    lmax=lmax
    )

synchrotron = cm.SynchrotronSignal(
    nu_ref=nu_ref, lmax=lmax,
    # priors...
    )
dust = cm.DustSignal(
    nu_ref=nu_ref, lmax=lmax,
    # priors....
    )
free_free = cm.FreeFreeSignal(
    nu_ref=nu_ref, lmax=lmax,
    # priors....
    )

monodipole = cm.MonoDipoleSignal(fix_at_nu=[22.8, 93.5])


#
# Run. It's called "...command_line" because the function will also
# parse sys.argv for additional options. The default behaviour is to
# do nothing but print an overview of the analysis to be performed; the
# command "run" will either start or resume the chain (depending on
# whether the target file exists); the --restart flag will always overwrite the
# target file.
#




cm.apps.cmb_gibbs_sampler_command_line(
    # Decide to drop K1 and Ka1 here; that data is then not loaded
    [Q1, Q2, V1, V2, W1, W2, W3, W4],
    [cmb, synchrotron, free_free, monodipole],

    # Any modelling options that are not naturally expressed in terms of
    # observations or signal components
    sample_gain=True,
    sample_noise_calibration=True,
    sample_bandpass_shift=True,

    # Run options.
    chain_count=10,
    sample_count=5000,
    output_filename='wmap-9r-analysis.h5'
    )
