#!/usr/bin/python

from commander import *

#set_healpix_data_path("path")

def fix_nan_pixels(mask, rms):
    print "WARNING: fix_nan_pixels() not implemented."
    return mask, rms

def smooth_map(data_map, smooth_beam):
    print "WARNING: smooth_map() not implemented."
    return data_map

def degrade_observation(obs, fwhm, reg_rms, rng):
    """
    Degrade an observation.
      obs: SkyObservation object
      fwhm: Gaussian beam FWHM [arcmin]
      reg_rms: regularisation noise [uK]
      rng: Numpy RNG handle
    Returns: new SkyObservation object
    """
    data_map = obs.load_map()
    mask, rms = fix_nan_pixels(obs.load_mask_map(), obs.load_rms_map())
    beam = obs.get_sh_beam()
    
    # Add noise
    rms += reg_rms
    data_map += reg_rms * rng.normal(size=data_map.shape)
    
    # Get smoothed beam
    smooth_beam = gaussian_beam_by_l(lmax, '15 arcmin')
    beam *= smooth_beam
    
    # Smooth the data and return new SkyObservation object with degraded map
    data_map = smooth_map(data_map, smooth_beam)
    return SkyObservation(map=data_map, noise=rms, mask=mask, beam=beam, lmax=obs.lmax)


# Initialise RNG
rng = np.random.RandomState(1)

# Define resolution, map, noise, beam, etc.
lmax = 3000

# Test data
DATADIR = "/home/phil/postgrad/sz_clusters/simulations/planck2013/"
P100 = SkyObservation(
    mask=(DATADIR+'empty_mask_n512.fits', 1, 0),
    map=(DATADIR+'planck13_rms_100_512.fits', 1, 0, 'force uK'),
    noise=(DATADIR+'planck13_rms_100_512.fits', 1, 0, 'force uK'),
    beam=DATADIR+'planck13_beam_100.fits',
    lmax=lmax)

"""
P100 = SkyObservation(
    mask=('$PLANCK/mask_gal60_ptsrcunion.fits', 1, 0),
    map=('$PLANCK/HFI_SkyMap_100_2048_R1.10_nominal.fits', 1, 'TEMPERATURE'),
    noise=('$PLANCK/HFI_SkyMap_100_2048_R1.10_nominal.fits', 1, 'NOISE'),
    beam='$PLANCK/beam_100_R1.10.fits',
    lmax=lmax)
"""

# Load initial power spectrum
#cl_initial = load_power_spectrum_from_file('$PLANCK/cls.fits', lmax)
cl_initial = load_power_spectrum_from_file(DATADIR+'base_planck_lowl_lowLike_highL_post_lensing.bestfit_cl.fits', lmax)

# Smooth map before running chain
P100dg = degrade_observation(P100, fwhm=30., reg_rms=30, rng=rng)

# Define CMB component
cmb = CmbSignalComponent(lmax, cl_initial, amp_param="cmb_amp", cl_param="cl")

# Run the analysis
sky_analysis_cli( components=[cmb],
                  observations=[P100],
                  sample_gain=False,
                  sample_noise=False,
                  seed=1)


#                  num_iter=5,
#                  chain_name="test.h5",

"""
#
# Alt B. Simple full-res analysis
#

lmax = 3000

P100 = planck_observation('100',
                          '$PLANCK/mask_gal60_ptsrcunion.fits')
cmb = CmbSignalComponent(lmax=lmax)

sky_analysis_cli([cmb], [P100],
                 sample_gain=False,
                 sample_noise=False
)


#
# With necesarry pre-processing -- add noise and beam convolution
#
from commander import *
import numpy as np

lmax = 1500
rng = np.random.RandomState(1)

data_map = load_map(('$PLANCK/HFI_SkyMap_100_2048_R1.10_nominal.fits', 1, 'TEMPERATURE'), 'temperature')
rms = load_map(('$PLANCK/HFI_SkyMap_100_2048_R1.10_nominal.fits', 1, 'NOISE'), 'rms')
mask = load_map(('$PLANCK/mask_gal60_ptsrcunion.fits', 1, 0), 'raw')
beam = load_beam('$PLANCK/beam_100_R1.10.fits')
mask, rms = fix_nan_pixels(mask, rms)
rms += 80
data_map += 80 * rng.normal(size=data.shape)
smooth_beam = gaussian_beam_by_l(lmax, '15 min')
data_map = smooth_map(data_map, smooth_beam)
beam *= smooth_beam


P100dg = CmbObservation(mask=mask, data=data_map, rms=rms, beam=beam)
cmb = CmbSignalComponent(lmax=lmax)

sky_analysis_cli([cmb], [P100],
                 sample_gain=False,
                 sample_noise=False
)
"""
