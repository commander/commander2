#!/usr/bin/env python
from __future__ import division

import numpy as np
import commander as cm
from commander.logger_utils import timed
from commander.compute.cr.mg_smoother import (
    csc_neighbours_full,
    block_At_D_A, convert_ring2tiled, convert_tiled2ring,
    compute_csc_beam_matrix, udgrade_ring)

#       Fortran  |  SSE2    | AVX | FMA4 | chunklen=3*veclen
# 512:  12600 ms |  531 ms  | 328 | 242  | 201

nside_obs = 512
nside = 256
nside_tile = 8
nside_level = nside // nside_tile
ntiles = 12 * (nside_level)**2
lmax = 4 * nside
bl = np.ones(lmax + 1)

indptr, indices = csc_neighbours_full(nside_level)
indptr = indptr[:65]
indices = indices[:indptr[-1]]
blocks = np.empty(((nside_obs // nside_level)**2,
                    (nside // nside_level)**2,
                    indices.shape[0]), np.double, order='F')

with timed('generating B'):
    compute_csc_beam_matrix(False, nside_obs, nside, nside_level,
                            bl, indptr, indices, blocks=blocks)
