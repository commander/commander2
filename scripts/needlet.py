from __future__ import division

from matplotlib.pyplot import *
import numpy as np
from scipy.integrate import quad

def f(t):
    if -1 < t < 1:
        return np.exp(-1 / (1 - t*t))
    else:
        return 0

phi_norm = 1 / quad(f, -1, 1)[0]
def phi(u):
    return quad(f, -1, u)[0] * phi_norm

def phi2(t, B):
    if t <= 1 / B:
        return 1
    elif t < 1:
        return phi(1 - (2 * B) / (B - 1) * (t - 1 / B))
    else:
        return 0

def b(eta, B):
    if eta < 1 / B:
        return 0
    elif eta < B:
        b_sq = phi2(eta / B, B) - phi2(eta, B)
        if b_sq <= 0:
            return 0
        else:
            return np.sqrt(b_sq)
    else:
        return 0

def bl(lmax, B, j):
    C = float(B)**j
    return np.asarray([b(l / C, B) for l in range(lmax + 1)])

def make_bl(lmax, B):
    j = (np.log(lmax) - np.log(B)) / np.log(B)
    return bl(lmax, B, j)    

def make_r_beam(B, l):
    j = (np.log(l) - np.log(B)) / np.log(B)
    # We take the square root of the needlet "beam" as our
    # restriction operator, because this makes R * Sinv * R.h
    # nearly a needlet -- a needlet is Y * D * Y.h with bl on D's
    # diagonal
    r_beam = np.sqrt(bl(l, B, j))
    r_beam[:np.argmax(r_beam)] = 1
    return r_beam

def zero_pad(x, n):
    y = np.zeros(n)
    y[:x.shape[0]] = x
    return y

def zero_set(x, n):
    y = x.copy()
    y[n:] = 0
    return y

def mexican_needlet(lmax, B, p, j):
    l = np.arange(lmax + 1)
    return (l / B**j)**(2 * p) * np.exp(-l**2 / (B**(2*j)))

def modified_gaussian(lmax, fwhm, b):
    from commander.sphere import fwhm_to_sigma
    sigma = fwhm_to_sigma(fwhm)
    l = np.arange(lmax + 1)
    return np.exp(-(.5 * l * (l + 1) * sigma**2 / b)**b)

def mexican_beam(lmax, B, p, j):
    bl = mexican_needlet(lmax, B, p, j)
    i = np.argmax(bl)
    bl /= bl[i]
    bl[:i] = 1
    return np.sqrt(bl)

if __name__ == '__main__':

    from commander.sphere import beam_by_theta, gaussian_beam_by_l, healpix_pixel_size

    
    import matplotlib as mpl
    mpl.rcParams['font.family'] = 'serif'
    
    def load_Cl(lmax):
        import os
        path = os.path.dirname(os.path.abspath(__file__))
        dat = np.loadtxt(os.path.join(path, 'cls.dat'))
        ls = np.arange(2, lmax + 1)
        Cl = dat[:, 1][:lmax + 1 - 2]
        Cl /= ls * (ls + 1) / 2 / np.pi
        Cl = np.hstack([Cl[0], Cl[0], Cl])
        return Cl

    avg_rms = 10.9 # for Planck 143 GHz with PowSpec mask from R1
    min_rms = 1.86 # planck 143 GHz
    scale = 12 * 2048**2 / 4 / np.pi / min_rms**2 # Y^T N^-1 Y w. const. avg. rms

    
    
    nside = 32
    lmax = 5 * nside
    pw = healpix_pixel_size(nside)
    Cl = load_Cl(lmax)
    
    beams = [
        #(zero_pad(make_r_beam(2, 3 * nside), lmax), 'Bernstein', 'k:'),
        #(mexican_beam(lmax, B=2, p=1, j=6.5), 'Mexican'),
        (gaussian_beam_by_l(lmax, pw), 'Gaussian 1pix', 'k', 'solid'),
        (gaussian_beam_by_l(lmax, 1.5 * pw), 'Gaussian 1.5pix', '#800000', 'solid'),
        (gaussian_beam_by_l(lmax, 2 * pw), 'Gaussian 2pix', '#ff0000', 'solid'),
        #(zero_set(gaussian_beam_by_l(lmax, 2 * pw), 3 * nside), 'Gaussian trunc.', 'r:'),
        (modified_gaussian(lmax, 2.6 * pw, 2), 'Mod. Gaussian', 'blue', 'dashed')
    ]

    clf()
    fig = gcf()

    if 0:
        # three panes
    
        ax0 = fig.add_subplot(1,3,1)
        ax1 = fig.add_subplot(1,3,2)
        ax2 = fig.add_subplot(1,3,3)

        pixels0 = np.linspace(0, 5, 200)
        pixels1 = np.linspace(0, 12, 200)

        for bl, label, color, ls in beams:
            f = beam_by_theta(bl**2, pixels0 * pw)
            f /= f[0]
            ax0.plot(pixels0, f, color=color, ls=ls, label=label)
            ax1.semilogy(pixels1, np.abs(beam_by_theta(bl**2, pixels1 * pw)), color=color, ls=ls)
            ax2.semilogy(bl, color=color, ls=ls)
            ax2.set_ylim((1e-10, 1))


        ax2.axvline(3 * nside, color='k')
        ax0.legend()


    elif 1:
        # one pane
        fig.set_size_inches(4, 2.5)
        ax = fig.add_subplot(1,1,1)
        fig.subplots_adjust(0.16, 0.18, 1-0.03, 1-0.01)
        pixels = np.linspace(0, 16, 200)

        rlsq_Cl_lst = []
        for bl, label, color, lty in beams:
            f = np.abs(beam_by_theta(bl**2, pixels * pw))
            f *= scale
            ax.semilogy(pixels, f, color=color,
                        ls=lty, label=label)

            rlsq_Cl_lst.append(beam_by_theta(Cl * bl**2, np.asarray([0]))[0])

        ax.axhspan(np.min(rlsq_Cl_lst), np.max(rlsq_Cl_lst), color='#c0c0c0')

        ax.set_ylabel('Density')
        ax.set_xlabel(r'$\theta$ in pixels')
        #ax.legend(loc='lower left')
        
        fig.savefig('r-beams.eps')

    draw()
        
