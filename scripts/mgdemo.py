#!/usr/bin/env python

from __future__ import division

import os
import numpy as np
from matplotlib.pyplot import *
from commander import *
from commander.plot import *
from commander.compute.cr import mg_cr as mg
from commander.compute.cr import mg_smoother
from commander.logger_utils import timed, log_done_task
import logging

def load_Cl(filename, lmax):
    dat = np.loadtxt(filename)
    ls = np.arange(2, lmax + 1)
    Cl = dat[:, 1][:lmax + 1 - 2]
    Cl /= ls * (ls + 1) / 2 / np.pi
    Cl = np.hstack([Cl[0], Cl[0], Cl])
    return Cl


CL_FILE = "cls.dat"
PRECOMPUTE_FILE = "mg-precomputed.h5"
LEVELS_PLOT = "levels.png"
CONVERGENCE_PLOT = "convergence.png"
ERROR_PLOT = "error.png"

obs = SkyObservation(
    nside=2048,
    lmax=3000,
    
    map=('$LOCAL/planck/HFI_SkyMap_143_2048_R1.10_nominal.fits', 0),
    variance=('$LOCAL/planck/HFI_SkyMap_143_2048_R1.10_nominal.fits', 'II_COV'),

    masks=[('$LOCAL/planck/HFI_Mask_GalPlane_2048_R1.10.fits', 'GAL080'),
           ('$LOCAL/planck/HFI_Mask_PointSrc_2048_R1.10.fits', 'F143_10'),
           ],

    #masks=[('$LOCAL/planck/dust-mask-n2048.fits', 0)],
    
    beam='$LOCAL/planck/beams/beam_143_R1.10.fits',
    unique_basenames=True
    )


nside = 2048
lmax = 3000
Cl = load_Cl(CL_FILE, lmax)

wl = np.ones(lmax + 1)
wl[:1900] = 0
wl[1900:2200] = np.arange(300) / 300.

levels = [
        
    mg.ShLevel(lmax, smoothers=[
        mg.ShDiagonal(wl)
        ]),
    mg.PixelLevel(1024, lmax=lmax, w=2.2, b=1.7, split='even', smoothers=[
        mg.ICC(tilesize=4)
        ]),
    mg.PixelLevel(512, lmax=lmax, w=1.7, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=4)
        ]),
    mg.PixelLevel(256, lfactor=6, w=1.9, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=8)
        ]),
    mg.PixelLevel(128, lfactor=6, w=1.9, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=8)
        ]),
    mg.PixelLevel(64, lfactor=6, w=1.9, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=8)
        ])
        
]

#
# Everything below here should probably not be customized for now
#


solver = mg.MgSolver(levels, obs, Cl)

clf()
fig, axs = mg.plot_levels(levels)
fig.savefig(LEVELS_PLOT)


solver.cached_compute_noise_term(PRECOMPUTE_FILE)
solver.precompute_prior_term()

rng = np.random.RandomState(10)
u0 = scatter_l_to_lm(np.sqrt(Cl)) * rng.normal(size=(lmax + 1)**2)
rhs = levels[0].matvec(u0)
u = np.zeros((lmax + 1)**2)

us = []
rs = []

us.append(u.copy())
rs.append(rhs - levels[0].matvec(u))

def plotit():
    clf()
    for u in us:
        semilogy(norm_by_l(u - u0) / norm_by_l(u0))
    for lev in levels:
        if hasattr(lev, 'nside'):
            axvline(lev.nside)
        else:
            axvline(lev.lmax, ls=':')
    #gca().set_ylim((1e-10, 1))
    draw()

def errmap(i):
    clf()
    plot_ring_map(sh_synthesis(nside, u0-us[i]))
    draw()

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('mg')

clf()
for i in range(6):
    with timed('V-cycle'):
        mg.mg_cycle(0, levels, u, rhs, logger)
        us.append(u.copy())
        rs.append(rhs - levels[0].matvec(u))
        print 'MEAN RELATIVE ERROR: %.2e' % np.mean(norm_by_l(us[-1] - u0) / norm_by_l(u0))
                  
print '%.2e' % np.mean(norm_by_l(us[-1] - u0) / norm_by_l(u0))

plotit()
savefig(CONVERGENCE_PLOT)


def errmap(i):
    clf()
    plot_ring_map(sh_synthesis(nside, u0-us[i]))

errmap(-1)
savefig(ERROR_PLOT)
