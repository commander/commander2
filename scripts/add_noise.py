#!/usr/bin/python
"""
Add noise to data and RMS maps.
"""

import numpy as np
import healpy as hp

np.random.seed(10)

DIR = "/home/phil/Desktop/"
fdata = "HFI_SkyMap_143_2048_R1.10_nominal.fits"

# Specify noise to add (in uK)
addnoise = 20.e-6
print "Adding", addnoise/1e-6, "uK of noise to:"
print "\t", fdata

# Load maps
d = hp.read_map(DIR+fdata, field=0)
rms = hp.read_map(DIR+fdata, field=2)
rms = np.sqrt(rms)

# Figure-out variance required to get the correct amount of noise to be added 
# in quadrature
sigma_add = np.sqrt(addnoise**2. + 2.*addnoise*rms)

# Add noise to data map
n = sigma_add*np.random.normal(size=d.shape)
d += n

outname = fdata[:-5] + "_addnoise.fits"
hp.write_map(outname, d)
print "\nNew data map:", outname

# Add noise to RMS map
rms += addnoise

outname = fdata[:-5] + "_rms_addnoise.fits"
hp.write_map(outname, rms)
print "New noise map:", outname

