#!/usr/bin/python
"""Test envelope sampling method."""

import numpy as np
import pylab as P
import scipy.integrate

np.random.seed(10)

def likelihood(x):
    """
       Some likelihood function that would in general be expensive 
       to evaluate.
    """
    var = 0.4**2.
    return 2. * np.exp(-(x-0.3)**2./var) * 4.*np.abs(x) / (np.sqrt(2.)*np.pi)

def proposal(x):
    """
       Proposal pdf, that should be easier to evaluate, and should 
       approximate the likelihood.
    """
    # Standard Gaussian RNG
    return np.exp(-x**2.) / (np.sqrt(2.)*np.pi)

def envelope(x):
    """
       Envelope function (gives upper bound of Uniform draw)
    """
    return 40.1*np.ones(x.size) 


# Return samples from proposal
#rnorm = np.random.multivariate_normal(size=10000)
rnorm = np.random.normal(size=10000)

# Attempt to sample from envelope
samp = []
for x in rnorm:
    u = np.random.uniform()
    if u < likelihood(x) / ( envelope(x) * proposal(x) ):
        samp.append(x)


# Plots
xmin = -5.
xmax = 5.
x = np.linspace(xmin, xmax, 200)
I = scipy.integrate.simps(likelihood(x), x)

P.subplot(211)
P.plot(x, likelihood(x), 'r-')
P.plot(x, proposal(x)*envelope(x), 'k-')
P.ylim((0., 1.1))
P.figtext(0.7, 0.85, "Accepted: "+str(round(100.*len(samp)/len(rnorm),1))+"%")

P.subplot(212)
P.hist(samp, bins=20, normed=True, alpha=0.4)
P.hist(rnorm, bins=20, normed=True, alpha=0.4)
P.plot(x, likelihood(x)/I, 'r-')
P.show()
