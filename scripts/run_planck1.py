#!/usr/bin/python
from __future__ import division

import numpy as np
from commander import *
from commander.compute.cr import mg_cr as mg
from commander.compute.cr import mg_smoother
import os, sys

os.environ['LOCAL'] = '../../../data'

################################################################################
# Basic parameters
################################################################################

nside = 2048
lmax = 3000
npix = 12 * nside**2


################################################################################
# Define observations (maps, noise, masks, and beam)
################################################################################

obs = SkyObservation(
    nside=2048,
    lmax=3000,
    map=     ('$LOCAL/planck/HFI_SkyMap_143_2048_R1.10_nominal.fits', 0),
    variance=('$LOCAL/planck/HFI_SkyMap_143_2048_R1.10_nominal.fits', 'II_COV'),
    masks=[  ('$LOCAL/planck/HFI_Mask_GalPlane_2048_R1.10.fits', 'GAL080'),
             ('$LOCAL/planck/HFI_Mask_PointSrc_2048_R1.10.fits', 'F143_10'),
           ],
    beam=     '$LOCAL/planck/planck13_beam_143.fits',
    unique_basenames=True
    )
# beam='$LOCAL/planck/beams/beam_143_R1.10.fits'


# Re-pack FITS files etc. for observations into HDF file (for speed)
obscache = expand_path('$LOCAL/obscache.h5', check_exists=False)
print  "Caching obs. data in", obscache
obs = obs.repack_to_hdf5(obscache)
print "\tDone."


################################################################################
# Define sky components
################################################################################

# Load initial power spectrum
cl_initial = load_power_spectrum_from_file(
    '$LOCAL/planck/base_planck_lowl_lowLike_highL_post_lensing.bestfit_cl.fits', lmax)

# Define CMB component
cmb = CmbSignalComponent(lmax, cl_initial, amp_param="cmb_amp", cl_param="cl")


################################################################################
# Multigrid solver configuration
################################################################################

# Set weights for top-level (ignore l<1900 and concentrate on small-scale modes)
wl = np.ones(lmax + 1)
wl[:1900] = 0
wl[1900:2200] = np.arange(300) / 300.

# Setup MG levels and smoothing scheme
levels = [
    mg.ShLevel(lmax, smoothers=[
        mg.ShDiagonal(wl)
        ]),
    mg.PixelLevel(1024, lmax=lmax, w=2.2, b=1.7, split='even', smoothers=[
        mg.ICC(tilesize=4)
        ]),
    mg.PixelLevel(512, lmax=lmax, w=1.7, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=4)
        ]),
    mg.PixelLevel(256, lfactor=6, w=1.9, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=8)
        ]),
    mg.PixelLevel(128, lfactor=6, w=1.9, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=8)
        ]),
    mg.PixelLevel(64, lfactor=6, w=1.9, b=1, split='Ni', smoothers=[
        mg.ICC(tilesize=8)
        ]),
    ]


################################################################################
# Run the analysis
################################################################################

sky_analysis_cli( sys.argv,
                  components=[cmb],
                  observations=[obs],
                  mglevels=levels )
                  
