from __future__ import division
import numpy as np

from matplotlib.pyplot import *
from commander.plot import *

import commander as cm
import oomatrix as om

from commander.matrices import diagonal_matrix_by_l
from commander.sphere import mmajor
from commander import resourceloaders



ctx = cm.CommanderContext(seed=37, cache_path='cache')

data_path = 'data/sim_cmb_synch/'
nside = 16
npix = 12 * nside**2

lmax = 3 * nside - 1

Cl = resourceloaders.load_power_spectrum_from_file(data_path + 'auxiliary/wmap_lcdm_sz_lens_wmap5_cl_v3.fits', lmax)
C_lm = mmajor.scatter_l_to_lm(0, lmax, Cl)

beam = cm.gaussian_beam_by_l(lmax, '5 deg')

pixwin = ctx.load_pixel_window_temperature(nside, lmax)
beam_lm = mmajor.scatter_l_to_lm(0, lmax, beam)
pixwin_lm = mmajor.scatter_l_to_lm(0, lmax, pixwin)

#S = diagonal_matrix_by_l(Cl, 0, lmax, name='S_cmb')
#B = diagonal_matrix_by_l(, 0, lmax)

# Noise
rms = cm.load_ring_map(('data/rms_n16.fits', 1, 0, 'force uK'), 'temperature')
rms *= 1000

# Mask
from cmb.maps import pixel_sphere_map
#mask = cm.load_ring_map(('/home/dagss/data/wmap/wmap_temperature_analysis_mask_r9_7yr_v4.fits', 1, 0), 'raw')
#mask = pixel_sphere_map(mask).change_resolution(nside).view(np.ndarray)
#mask[mask != 0] = 1 # round towards not masked! This gives 13% mask.

if 1:
    mask = np.ones(npix)
    mask[4 * npix // 8:5 * npix // 8] = 0


# Simulate signal and data
signal = np.sqrt(C_lm) * ctx.nc_rng.normal(size=(lmax + 1)**2)
data = ctx.sh_synthesis(nside, 0, lmax, pixwin_lm * beam_lm * signal) + rms * ctx.nc_rng.normal(size=npix)
data *= mask

# Analyse

obs = cm.SkyObservation(
    name='n16_sum',
    T_map=data,
    TT_rms_map=rms,
    beam_transfer=beam,
    lmax_beam=lmax,
    mask_map=mask)


Cl[0] = Cl[1] = Cl[2]

cmb = cm.IsotropicGaussianCmbSignal(
    name='cmb',
    power_spectrum=Cl,
    lmax=lmax
    )


figure()
plot_ring_map(data)

from commander.amplitude.amp_brute_force import CrSampler
sampler = CrSampler(ctx, [obs], [cmb])
cmb_sample, = sampler.draw(True)

figure()
plot_ring_map(ctx.sh_synthesis(nside, 0, lmax, cmb_sample))

figure()
spherical_signal_spectrum_plot(ctx, [obs], cmb, cmb_sample)

show()
