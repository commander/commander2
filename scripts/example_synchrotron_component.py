#!/usr/bin/python
"""Implement an example Synchrotron foreground component."""

import numpy as np
import scipy.integrate
import scipy.interpolate
import pylab as P


class BandSpec(object):

    def __init__(self, data):
        """Describes a frequency band. Units in GHz."""
        
        self.nu_samp = np.array(data[0])
        self.response_samp = np.array(data[1])
        self.nu_min = np.min(self.nu_samp)
        self.nu_max = np.max(self.nu_samp)
        self.nu_c = 0.5*(self.nu_max + self.nu_min) # Median (centre) frequency
        
        self.precomp_response() # Construct spline / integrate response fn.
    
    
    def precomp_response(self):
        """
           Construct spline from spectral response data and integrate 
           the response function.
        """
        # Convert a table of bandpass data into a spline.
        self._response = scipy.interpolate.interp1d(
                              self.nu_samp, self.response_samp, 
                              bounds_error=False, fill_value=0.)
        
        # Integrate response function (needed for normalisation)
        self.integrated_response = scipy.integrate.simps(self.response_samp, self.nu_samp)
    
    
    def spectral_response(self, nu):
        """
           Return the spectral response as an interpolated function 
           of frequency.
        """
        return self._response(nu)


def synch_spectrum(nu, beta, C, nu0=0.31):
     """A synchrotron spectrum. Freq. in GHz."""
     # See Kogut (2012), arXiv:1205.4041 (Eq. 6)
     return (nu/nu0)**(beta + C*np.log(nu/nu0))


def integrate_over_band(band, spec_fn, spec_args):
    """
       Integrate spectrum over a band, given a set of spectral 
       function parameters (e.g. spectral indices).
       Spectral fn. must have form fn(nu, param1, param2, ...),
       where spec_args = (param1, param2, ...)
    """
    x = band1.nu_samp
    y = band.response_samp * spec_fn(band.nu_samp, *spec_args)
    
    # Integrate spectrum over frequency
    return scipy.integrate.simps(y, x) / band.integrated_response


# Define some made-up bandpass data (fmt: 0:freq., 1:bandpass)
bandpass_data1 = np.array(  [ [53., 54., 57., 60., 63., 66., 68.],
                              [.71, .74, .86, .98, 1.,  .94, .87 ]  ] )
bandpass_data2 = np.array(  [ [84., 88., 92., 96., 100., 104., 105.],
                              [1.,  .95, .94, .91, .87,  .82,  .80 ]  ] )

# Define bandpasses
band1 = BandSpec(bandpass_data1)
band2 = BandSpec(bandpass_data2)

# Realistic ranges of spectral indices (for nu0=350MHz -- see Kogut 2012)
C = np.linspace(-0.1, 0., 10)
beta = np.linspace(-3.5, -1., 20)

# 2D arrays for spectral indices
_C = np.array([C,]*beta.size)
_beta = np.array([beta,]*C.size).T

# Get integrated spectra
I1 = [[integrate_over_band(band1, synch_spectrum, (bb, cc)) for cc in C] for bb in beta]
I2 = [[integrate_over_band(band2, synch_spectrum, (bb, cc)) for cc in C] for bb in beta]
I3 = [integrate_over_band(band2, synch_spectrum, (bb, -0.1)) for bb in beta] # Fix C


# Plot results
P.subplot(221)
P.title("Bandpass")
nu = np.linspace(10., 150., 200)
P.plot(nu, band1.spectral_response(nu))
P.plot(nu, band2.spectral_response(nu))

P.subplot(222)
P.title("Synch. spec. (nu=500 MHz)")
P.contourf(C, beta, synch_spectrum(60., _beta, _C))
P.colorbar()

P.subplot(223)
P.title("Synch. spec. (integ., band1)")
P.contourf(C, beta, np.log(I1))
P.colorbar()

P.subplot(224)
P.title("Synch. spec. (integrated, C=-0.1)")
P.plot(beta, I3)
P.xlabel("beta")
P.colorbar()

P.show()
