from __future__ import division
import numpy as np
from matplotlib import pyplot as plt

import commander as cm
from commander.sphere import healpix

#
# Parameters
#
data_path = 'data/sim_cmb_synch/'
lmax = 3 * 32 - 1
lprecond = 20

# To save typing, we iterate over the WMAP bands using a loop
observations = []
for obs_name, nu, sigma0 in [('K', 23, '1.437 mK'),
                             ('Ka', 33, '1.470 mK'),
                             ('Q', 41, '2.254 mK'),
                             ('V', 61, '3.319 mK'),
                             ('W', 94, '2.955 mK')]:
    obs = cm.SkyObservation(
        name=obs_name,
        source='WMAP-r9',
        description='WMAP r9 7yr maps degraded to common beam and resolution by HKE',

        # Location of temperature map; (filename, fits_record, column_name)
        T_map=(data_path + 'n32/map_%s_n32.fits' % obs_name, 1, 'TEMPERATURE', 'force uK'),

        # Specify rms in terms of n_obs map and sigma0
        TT_rms_map=(data_path + 'n32/rms_%sband_50muK_n32.fits' % obs_name, 1, 0, 'force uK'),

        beam_transfer=data_path + 'n32/beam_%sband.fits' % obs_name,
        lmax_beam=3 * 32 - 1,
        mask_map=(data_path + 'n32/mask_%sband_n32.fits' % obs_name, 1, 0),
        
        bandpass=(nu, nu)
        )
    observations.append(obs)


K, Ka, Q, V, W = observations

#
# Model
#

cmb = cm.IsotropicGaussianCmbSignal(
    name='cmb',
    # Specify degrees for a flat initial power spectrum, or a filename
    # to a text file such as 'wmap_lcdm_sz_lens_wmap7_cl_v4.dat' to start
    # a particular place
    power_spectrum=data_path + 'auxiliary/wmap_lcdm_sz_lens_wmap5_cl_v3.fits',
    lmax=lmax
    )

monodipole = cm.MonoAndDipoleSignal(name='monodipole',
                                    fix_at=[K])


ls = np.arange(lmax + 1)
ls[0] = ls[1]
prior = 6e6 * ls**-2.5

synchrotron = cm.FixedSpectrumSignal(
    name='synch',
    lmax=lmax,
    power_spectrum=prior,
    frequency_response={23.0: 1.0,
                        33.0: 0.33856,
                        41.0: 0.17654,
                        61.0: 0.05360,
                        94.0: 0.01465})

dust = cm.FixedSpectrumSignal(
    name='dust',
    lmax=lmax,
    power_spectrum=prior,
    frequency_response={23.0: 0.01,
                        33.0: 0.05,
                        41.0: 0.17654,
                        61.0: 0.3,
                        94.0: 0.8})

signal_descriptions = [cmb, synchrotron, monodipole]

ctx = cm.CommanderContext(cache_path='cache', seed=23)
realizations = cm.app.constrained_realization(ctx, 'out.h5',
                                              observations,
                                              signal_descriptions,
                                              eliminate_masked_pixels=True,
                                              wiener_only=False,
                                              lprecond=lprecond,
                                              eps=1e-5,
                                              filemode='w')

if 1:
    plt.figure()
    mask = None # or ctx.get_mask_map(V)
    cm.plot.plot_sh_map(ctx, realizations[0], 32, 0, lmax, title='cmb', mask=mask)
    plt.figure()
    cm.plot.plot_sh_map(ctx, realizations[1], 32, 0, lmax, title='synch', mask=mask)
    print 'Mono- and dipoles:'
    print realizations[2].reshape((4, 4))

#cm.plot.spherical_signal_spectrum_plot(ctx, observations, synchrotron, realizations[1])
#plt.figure()
cm.plot.spherical_signal_spectrum_plot(ctx, observations, cmb, realizations[0])
plt.show()



